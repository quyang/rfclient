package mobile.freshtime.com.freshtime;


import android.test.AndroidTestCase;

/**
 * Created by Administrator on 2016/11/1.
 */
public class QiangweiDAOTest extends AndroidTestCase {

    SQLiteHelper mHelper = new SQLiteHelper(getContext());


    public void add() throws Exception {
        QiangweiDAO dao = new QiangweiDAO(mHelper);
        dao.add("1", "123");
    }

    public void find() throws Exception {
        QiangweiDAO dao = new QiangweiDAO(mHelper);
        boolean b = dao.find("1");
        System.out.println("QiangweiDAOTest.find" + b);
    }

    public void delete() throws Exception {
        QiangweiDAO dao = new QiangweiDAO(mHelper);
        dao.delete("1");
    }


    public void getAll() throws Exception {
        QiangweiDAO dao = new QiangweiDAO(mHelper);
        int all = dao.getAll("1");
        System.out.println("QiangweiDAOTest.getAll");
        System.out.println("QiangweiDAOTest.getAll=" + all);
    }

}