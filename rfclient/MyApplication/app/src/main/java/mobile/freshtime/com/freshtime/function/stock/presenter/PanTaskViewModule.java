package mobile.freshtime.com.freshtime.function.stock.presenter;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.viewmodel.ModelList;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.BaseViewModel;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.guanli.module.KuCunInfo;
import mobile.freshtime.com.freshtime.function.search.PositionBarcodeInfo;
import mobile.freshtime.com.freshtime.function.stock.PanTaskActivity;
import mobile.freshtime.com.freshtime.function.stock.module.InSkuRequest;
import mobile.freshtime.com.freshtime.function.stock.module.ResponseSkuIn;
import mobile.freshtime.com.freshtime.function.stock.module.StockPage;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by Administrator on 2016/11/15.
 */
public class PanTaskViewModule extends BaseViewModel<StockPage> implements ModelList<StockPage>, Response.ErrorListener {

    private ArrayList<StockPage> mStockPages;

    private int mCurrentPageIndex;

    private ViewModelWatcher mModelWatcher;

    private PanTaskActivity mActivity;

    private PageListener mPageListener;

    private boolean mIsRequesting;

    private Request<ResponseSkuIn> mSkuRequest;

    private String itemInput;

    /**
     * sku请求回调
     */
    private Response.Listener<ResponseSkuIn> mSkuModelListener = new Response.Listener<ResponseSkuIn>() {
        @Override
        public void onResponse(ResponseSkuIn response) {
            PanTaskViewModule.this.onResponse(response);
        }
    };

    //请求商品sku信息的回调
    private void onResponse(final ResponseSkuIn response) {

        clearRequestState();

        if (response != null && response.isSuccess()) {

            long skuId = response.getModule().getSkuId();
            String token = Login.getInstance().getToken();

            //根据skuid获取库位
            String s = RequestUtils.INTO_STORAGE + "request=" +
                    "{token:\"" + token + "\",data:\"" + skuId + "\"}";

            //获取库位的请求
            GetProtocol protocol = new GetProtocol(s);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    linkedData(response, json);
                }

                @Override
                public void onFail() {
                }
            });
        }
    }

    /**
     * @param response 商品的基本信息
     * @param json     包含商品positionID的json
     */
    private void linkedData(ResponseSkuIn response, String json) {

        StockPage page = getCurrentPage();
        page.setSkuModel(response.getModule());
        page.getSkuModel().item_input = itemInput;

        if (json == null || "".equals(json) || "null".equals(json)) {
            getAllKuCun(response.getModule().getSkuId() + "");
            return;
        }

        Gson gson = new Gson();
        CuWeiBean bean = gson.fromJson(json, CuWeiBean.class);

        if (bean.success && bean.target != null && bean.target.size() > 0) {
            for (int i = 0; i < bean.target.size(); i++) {

                if (bean.target.get(0).pisitionVO != null) {

                    page.getSkuModel().kuwei = bean.target.get(0).pisitionVO.code;
                    page.getSkuModel().setPositionId(bean.target.get(0).positionId);
                    page.getSkuModel().setBarcode(bean.target.get(0).pisitionVO.barcode);

                    System.out.println("1 PanTaskViewModule.linkedData=skuId=" + response.getModule().getSkuId() + "=pid=" + bean.target.get(0).positionId);
                }
            }

        } else {
            ToastUtils.showToast("该商品没有库位,请扫描库位");
        }

        getAllKuCun(response.getModule().getSkuId() + "");


//        page.advance();//sku ,
//        mPageListener.onPageChanged(page);

    }

    private void getAllKuCun(String skuId) {

        final StockPage page = getCurrentPage();

        //GET_KUCUN
        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", skuId);

            String url = RequestUtils.GET_KUCUN + "?request=" + object;
            GetProtocol protocol = new GetProtocol(url);

            System.out.println("BangDingActivity.linkData=" + url);

            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("BangDingActivity.onSuccessdddd=" + json);
                    if (StringUtils.isEmpty(json)) {
                    } else {
                        try {
                            Gson gson = new Gson();
                            KuCunInfo info = gson.fromJson(json, KuCunInfo.class);
                            if (info.isSuccess()) {
                                page.getSkuModel().kucun = info.getTarget().getNumberShowTotal();
                                page.getSkuModel().safeKucun = info.getTarget().getGuardBit();

                            } else {
                                ToastUtils.showToast("" + info.getMessage());
                            }
                        } catch (Exception e) {
                            ToastUtils.showToast(e.getMessage());
                            mActivity.getKucunCount().setText(mActivity.getResources().getString(R.string.kucun));
                        }
                    }

                    mActivity.mItemCodeTwo.requestFocus();
                    page.advance();//sku ,
                    mPageListener.onPageChanged(page);
                }

                @Override
                public void onFail() {
                    mActivity.mItemCodeTwo.requestFocus();
                    page.advance();//sku ,
                    mPageListener.onPageChanged(page);

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void clearRequestState() {
        mIsRequesting = false;
    }


    public PanTaskViewModule(ViewModelWatcher watcher) {
        this.mStockPages = new ArrayList<>();
        this.mCurrentPageIndex = 0;
        this.mModelWatcher = watcher;
        mActivity = (PanTaskActivity) mModelWatcher;

        //初始化时为start
        StockPage page = new StockPage(null);
        mStockPages.add(mCurrentPageIndex, page);
    }

    public void onScanResult(String input) {

        StockPage page = getCurrentPage();
        StockPage.Phase phase = page.getPhase();
        if (phase == StockPage.Phase.START) {
            requestSku(input);
        } else if (phase == StockPage.Phase.SKU) {
            fillCuKei(input);
        } else if (phase == StockPage.Phase.COUNT) {

        }
    }

    //获取库位信息
    private void fillCuKei(String input) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", input);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    System.out.println("PanTaskViewModule.onSuccess" + "=" + json);

                    if (!StringUtils.isEmpty(json)) {
                        Gson gson = new Gson();
                        PositionBarcodeInfo positionBarcodeInfo = gson.fromJson(json, PositionBarcodeInfo.class);
                        if (positionBarcodeInfo.isSuccess()) {

                            StockPage currentPage = getCurrentPage();
                            currentPage.getSkuModel().setInfo(positionBarcodeInfo);

                            currentPage.getSkuModel().setPositionId(positionBarcodeInfo.getTarget().getId());
                            currentPage.getSkuModel().barcodeTwo = positionBarcodeInfo.getTarget().getBarcode();
                            currentPage.getSkuModel().kuwei = positionBarcodeInfo.getTarget().getCode();

                            StockPage page = getCurrentPage();
                            mPageListener.onPageChanged(page);

                            System.out.println("2 PanTaskViewModule.onSuccess=pid=" + positionBarcodeInfo.getTarget().getId() );

                            mActivity.mItemInput.requestFocus();

                        }
                    }
                }

                @Override
                public void onFail() {

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取sku信息
     *
     * @param code
     */
    public void requestSku(String code) {
        if (!mIsRequesting) {
            itemInput = code;
            mIsRequesting = true;
            mSkuRequest = new InSkuRequest(code, mSkuModelListener, this);
            FreshRequest.getInstance().addToRequestQueue(mSkuRequest, this.toString());
        }
    }

    @Override
    public void onInterrupt() {
        mIsRequesting = false;
        if (mSkuRequest != null)
            FreshRequest.getInstance().cancel(mSkuRequest.toString());
    }

    @Override
    public boolean isRequesting() {
        return mIsRequesting;
    }

    public StockPage getCurrentPage() {
        return mStockPages.get(mCurrentPageIndex);
    }

    @Override
    public void registerContext(BaseActivity activity) {
    }

    @Override
    public void unregisterContext() {
        this.mPageListener = null;
        this.mModelWatcher = null;
        this.mSkuRequest = null;
    }

    @Override
    public boolean hasNext() {
        return mCurrentPageIndex != mStockPages.size() - 1;
    }

    @Override
    public boolean hasBefore() {
        return mCurrentPageIndex != 0;
    }

    @Override
    public boolean isReady() {
        for (int i = 0; i < mStockPages.size(); i++) {
            StockPage page = mStockPages.get(i);
            if (page.getPhase() != StockPage.Phase.DONE || page.getPhase() != StockPage.Phase.COUNT)
                return false;
        }

        return true;
    }

    @Override
    public void navNext() {

        int len = mStockPages.size();

        StockPage currentPage = getCurrentPage();
        currentPage.getSkuModel().setCount(count);

        if (mCurrentPageIndex++ == len - 1) {
            // 新增一页
            StockPage page = new StockPage(null);
            mStockPages.add(mCurrentPageIndex, page);
        }

        StockPage page = getCurrentPage();
        mPageListener.onPageChanged(page); //更新界面
    }

    @Override
    public void navBefore() {
        if (mCurrentPageIndex != 0) {
            mCurrentPageIndex--;
            StockPage page = getCurrentPage();
//            mActivity.onCangwei(page.getSkuModel().getInfo());
            mPageListener.onPageChanged(page);
        }
    }


    public void addPageListener(PageListener pageListener) {
        this.mPageListener = pageListener;
        StockPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        clearRequestState();
        ToastUtils.showToast("获取商品信息失败");
    }

    //获取集合
    public ArrayList<StockPage> getStockPages() {
        return mStockPages;
    }

    public String count;

    public void setCount(String s) {
        this.count = s;
    }
}
