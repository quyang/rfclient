package mobile.freshtime.com.freshtime.function.requisition;

import android.os.Bundle;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONObject;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.function.requisition.model.ResponseRequisition;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * Created by orchid on 16/10/11.
 * 领用出库
 */

public class RequisitionListActivity extends BaseActivity implements Response.ErrorListener, Response.Listener<ResponseRequisition> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requisition);
    }

    @Override
    public void onScanResult(String result) {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            BaseRequest<ResponseRequisition> request =
                    new BaseRequest<>(RequestUtils.REQUISITION,
                            ResponseRequisition.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(ResponseRequisition response) {
        if (response.isSuccess()) {
            Log.e("test", response.toString());
        }
    }
}
