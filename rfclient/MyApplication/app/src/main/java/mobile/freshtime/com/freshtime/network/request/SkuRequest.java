package mobile.freshtime.com.freshtime.network.request;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;

import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.network.RequestUtils;

/**
 * Created by orchid on 16/9/27.
 */

public class SkuRequest extends Request<ResponseSku> {

    private String mParam;
    private WeakReference<Response.Listener<ResponseSku>> mListener;
    private String mBarcode;

    public SkuRequest(String barcode,
                      Response.Listener<ResponseSku> listener,
                      Response.ErrorListener errorListener) {
        //获取sku
        super(Method.POST, RequestUtils.SKU, errorListener);
        this.mBarcode = barcode;
        this.mParam = "skuCode=" + /*Utils.get69Code(barcode)*/ barcode;
        this.mListener = new WeakReference<>(listener);
    }

    @Override
    protected Response<ResponseSku> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =
                    new String(response.data, "UTF-8");
            Log.e("freshtime11111", "volley response : " + jsonString);
            ResponseSku result = new ResponseSku(new JSONObject(jsonString));
            result.getModule().setBarcode(this.mBarcode);
            return Response.success(
                    result,
                    HttpHeaderParser.parseCacheHeaders(response));

        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        } catch (JSONException e) {
            Log.e("freshtime11111","aaaaaaa",e);
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(ResponseSku response) {
        if (mListener != null && mListener.get() != null)
            mListener.get().onResponse(response);
        else
            Log.e("orkid", "request released!");
    }

    @Override
    public byte[] getBody() {
        try {
            return mParam == null ? null : mParam.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mParam, "utf-8");
            return null;
        }
    }
}
