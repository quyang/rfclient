package mobile.freshtime.com.freshtime.common.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;

/**
 * Created by orchid on 16/9/15.
 */
public class ExpandableAdapter extends BaseAdapter {

    private static final int KEY = R.id.expandableLayout;

    private Context mContext;
    private ArrayList<SkuModel> mLossUnits;
    private String mCount;

    public ExpandableAdapter(Context context, ArrayList<SkuModel> lossUnits, String count) {
        this.mContext = context;
        this.mLossUnits = lossUnits;
        mCount = count;


        System.out.println("ExpandableAdapter.ExpandableAdapter=" + lossUnits.size());
    }

    @Override
    public int getCount() {
        return mLossUnits.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_expand_common, null);
            viewHolder.mSkuInfo = (TextView) convertView.findViewById(R.id.sku_info);
            viewHolder.mSkuCode = (TextView) convertView.findViewById(R.id.sku_code);
            viewHolder.mQuantity = (TextView) convertView.findViewById(R.id.quantity);
            viewHolder.mSkuDetail = (TextView) convertView.findViewById(R.id.item_detail);
            viewHolder.mItemName = (TextView) convertView.findViewById(R.id.item_name);
            viewHolder.mItemCode = (TextView) convertView.findViewById(R.id.item_code);
            viewHolder.mItemSpec = (TextView) convertView.findViewById(R.id.item_spec);
            viewHolder.mItemUnit = (TextView) convertView.findViewById(R.id.item_unit);
            viewHolder.mItemCount = (TextView) convertView.findViewById(R.id.item_count);
            viewHolder.chagnjia = (TextView) convertView.findViewById(R.id.chagnjia);
            viewHolder.cuwei = (TextView) convertView.findViewById(R.id.kuwei);

            convertView.setTag(KEY, viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag(KEY);
        }
//        viewHolder.mQuantity.setText(mCount);
//        viewHolder.mSkuDetail.setText(model.getSubTitle());

        SkuModel model = mLossUnits.get(position);

        viewHolder.mSkuInfo.setText(model.getTitle());
        viewHolder.mSkuCode.setText(model.getSkuId() + "");
        viewHolder.mQuantity.setText(model.getCount() + "");
        viewHolder.mItemName.setText("品名:" + model.getTitle());
        viewHolder.chagnjia.setText("品名:" + model.getSupplierName());
        viewHolder.mItemCode.setText("编码:" + model.getSkuId());
        viewHolder.mItemSpec.setText("规格:" + (model.getCount() + "*" + model.getUnStandardSpecification()));
        viewHolder.mItemUnit.setText("库位:" + model.getKuwei());
        viewHolder.mItemCount.setText("数量:" + model.getCount());

        int type = model.getStorageType();
        String leixin = "";
        switch (type) {
            case 78:
                leixin = "活物";
                break;
            case 79:
                leixin = "常温";
                break;
            case 80:
                leixin = "恒温";
                break;
            case 81:
                leixin = "冷藏";
                break;
            case 82:
                leixin = "冷冻";
                break;
        }
        viewHolder.cuwei.setText("存储类型:" + leixin);

        if (StringUtils.isEmpty(model.getSupplierName())) {
            viewHolder.chagnjia.setText("厂家:暂无");
        }

        if (StringUtils.isEmpty(model.getCount() + "")) {
            viewHolder.mItemCount.setText("数量:暂无");
        }

        if (StringUtils.isEmpty(model.getSkuId() + "")) {
            viewHolder.mItemCode.setText("编码:暂无");
        }

        if (StringUtils.isEmpty(model.getTitle())) {
            viewHolder.mItemName.setText("品名:暂无");
        }


        return convertView;
    }

    private static class ViewHolder {
        public TextView mSkuInfo;
        public TextView mSkuCode;
        public TextView mQuantity;
        public TextView mSkuDetail;
        public TextView mItemCount;
        public TextView chagnjia;
        public TextView cuwei;

        // expandable part
        public TextView mItemName;
        public TextView mItemCode;
        public TextView mItemSpec;
        public TextView mItemUnit;
    }
}
