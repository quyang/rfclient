package mobile.freshtime.com.freshtime.function.pickwall;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.QiangweiDAO;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.SQLiteHelper;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.activity.MenuActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.model.wallposition.ResponseWallPosition;
import mobile.freshtime.com.freshtime.common.model.wallposition.WallPosition;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.function.pickwall.model.PickVO;
import mobile.freshtime.com.freshtime.function.pickwall.model.PickWall;
import mobile.freshtime.com.freshtime.function.pickwall.model.PickingWallPosition;
import mobile.freshtime.com.freshtime.function.pickwall.model.ResponsePickWall;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.PickBasketRequest;

import static mobile.freshtime.com.freshtime.R.id.done;
import static mobile.freshtime.com.freshtime.R.id.item_input;

/**
 * Created by orchid on 16/9/20.
 */
public class PickBasketActivity extends BaseActivity implements Response.ErrorListener, View.OnClickListener {

    private String mBasketBarcode;
    private WallPosition mWallPosition;
    private ResponsePickWall mModel;

    private EditText mPickInput;
    private View mInfoRoot;
    private TextView mWallCode;
    private TextView mShopInfo;
    private TextView mPickCount;
    private TextView mPickedCount;
    private TextView mWallPositionCode;
    private EditText mWallInput;
    private TextView mFinished;

    private TextView mDone;

    private Phase mPhase = Phase.START;

    //拣货框扫描
    private Response.Listener<BaseResponse> mReceiveResponse =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        mKuweiBianHao.requestFocus();
                        getWave();
                    } else {
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };

    //获取墙位信息
    private Response.Listener<ResponsePickWall> mPickWallRequestListener =
            new Response.Listener<ResponsePickWall>() {
                @Override
                public void onResponse(ResponsePickWall response) {
                    if (response != null && response.success) {
                        mPhase = Phase.WALL;
                        PickBasketActivity.this.mModel = response;
                        displayView();
                    } else {
                        //只有含有拣货任务的框才能成功获取墙位信息
                        ToastUtils.showToast("" + response.message);
                    }
                }
            };

    //扫描拣货墙编号 进行预绑定
    private Response.Listener<ResponseWallPosition> mWallPositionResponse =
            new Response.Listener<ResponseWallPosition>() {
                @Override
                public void onResponse(ResponseWallPosition response) {
                    if (response != null && response.isSuccess()) {
                        //获取这个接口中的数据
                        mWallPosition = response.target;
                        assignPickWall();
                    } else {
//                        ToastUtils.showToast("绑定失败");
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };

    //绑定成功后回调
    private Response.Listener<BaseResponse> mAssignResponse =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        mPhase = Phase.DONE;
                        displayView();
                    } else {
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };

    //点击完成后请求网络 完成波次  成功后回调到这里
    private Response.Listener<BaseResponse> mFinishWaveResponse =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        Toast.makeText(PickBasketActivity.this,
                                "波次完成", Toast.LENGTH_SHORT).show();
                        countList.clear();
                        mInfoRoot.setVisibility(View.INVISIBLE);
                        mPickInput.setText("");
                        mPickInput.setEnabled(true);
                        mPickInput.requestFocus();
                        mDone.setEnabled(false);
                        mPhase = Phase.START;

//            mWaveView.setVisibility(View.INVISIBLE);

                    } else {
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };
    private SQLiteHelper mHelper;
    private QiangweiDAO mDao;
    private Dialog mDialog;
    private List<PickVO> mVOs;
    private ViewGroup mWaveView;
    private NoInputEditText mKuweiBianHao;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_basket);

        mDialog = new Dialog(this, R.style.MyDialog);
        View view = getLayoutInflater().inflate(R.layout.dialog_order_cancled, null);
        Button ok = (Button) view.findViewById(R.id.btn_ok);

        mDialog.setCancelable(false);
        mDialog.setContentView(view);

        //后面给确定和取消按钮设置监听,onclicklistener;
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDialog.dismiss();
            }
        });

        //设置弹窗宽高
        mDialog.getWindow().setLayout(UiUtils.getScreenHeight(this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

        mHelper = new SQLiteHelper(this);
        mDao = new QiangweiDAO(mHelper);

        TextView module = (TextView) findViewById(R.id.item_tag);
        module.setText("拣货框编号:");

        mInfoRoot = findViewById(R.id.info_root);
        mPickInput = (EditText) findViewById(R.id.delivery_input).findViewById(item_input);
        mWallCode = (TextView) mInfoRoot.findViewById(R.id.item_name);
        mShopInfo = (TextView) mInfoRoot.findViewById(R.id.item_code);

        mWaveView = (ViewGroup) findViewById(R.id.wave_info_root);
        mKuweiBianHao = (NoInputEditText) mWaveView.findViewById(R.id.item_input);

        mWaveView.findViewById(R.id.back);

        mPickCount = (TextView) mInfoRoot.findViewById(R.id.pick_count);

        //已完成
        mPickedCount = (TextView) mInfoRoot.findViewById(R.id.picked_count);

        //拣货墙位
        mWallPositionCode = (TextView) mInfoRoot.findViewById(R.id.wall_code);

        //墙位编号
        mWallInput = (EditText) mInfoRoot.findViewById(item_input);

        mFinished = (TextView) mInfoRoot.findViewById(R.id.is_finished);
        mDone = (TextView) findViewById(done);
        mDone.setOnClickListener(this);

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.exchange).setOnClickListener(this);
        findViewById(R.id.quit).setOnClickListener(this);

        mInfoRoot.setVisibility(View.INVISIBLE);


        mDone.setEnabled(false);
    }

    //拣货框编号
    @Override
    public void onScanResult(String result) {
        if (mPhase == Phase.START)
            //校验输入的拣货框编号
            receiveBasket(result);
        else if (mPhase == Phase.WALL)

//            if (!StringUtils.isEmpty(mWallPositionCode.getText().toString().trim())) {
//                Toast.makeText(PickBasketActivity.this, "该拣货框已绑定过", Toast.LENGTH_LONG).show();
//                return;
//            }

            //根据录入的墙位编号   获取   墙位号
            getPickWallPosition(result);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
    }

    /**
     * Step 1 接受basket
     *
     * @param result
     */
    private void receiveBasket(String result) {

        result = result.trim();

        mBasketBarcode = result;
        mPickInput.setText(result);
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(result, "1"));
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.RECEIVE_PICK,
                            BaseResponse.class,
                            param,
                            mReceiveResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 2  根据输入的拣货框编号获取墙位(波次)信息    校验拣货框成功后才获取墙位
     * <p>
     * getWaveVOByPickingBasketBarcode
     */
    private void getWave() {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(mBasketBarcode, "1"));
            PickBasketRequest request =
                    new PickBasketRequest(param,
                            mPickWallRequestListener, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 3  获取拣货墙信息
     *
     * @param result getPickingWallPositionByBarcode
     */
    private void getPickWallPosition(String result) {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(result, "1"));
            BaseRequest<ResponseWallPosition> request =
                    new BaseRequest<>(RequestUtils.PICK_WALL_POSITION,
                            ResponseWallPosition.class,
                            param,
                            mWallPositionResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 4   绑定
     * <p>
     * assignPickingWallPosition
     */
    private void assignPickWall() {

        //进行绑定
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            JSONObject data = new JSONObject();
            data.put("wallPositionId", mWallPosition.id);//墙位编号   id 11
            data.put("assignmentId", mModel.target.id);//扫描拣货框  id  72
            param.put("data", data);
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.PICK_WALL,
                            BaseResponse.class,
                            param,
                            mAssignResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 5 结束波次   完成按钮的点击事件
     * <p>
     * waveFinished
     */
    private void finishWave() {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", mModel.target.id + "");
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.WAVE_FINISH,
                            BaseResponse.class,
                            param,
                            mFinishWaveResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private ArrayList<Integer> countList = new ArrayList<>();


    //显示view
    private void displayView() {

        //mModel就是返回来的json解析成的bean
        mInfoRoot.setVisibility(View.VISIBLE);
        mPickInput.setText(mBasketBarcode);

        mWallCode.setText("编号:" + mModel.target.id);
        mShopInfo.setText("门店:" + mModel.target.areaId);


        int count = 0;
        if (mModel.target.pickVOs == null) count = 0;//pickVOs是一个集合
        else count = mModel.target.pickVOs.size();

        int hasCount = 0;

        for (int i = 0; i < count; i++) {
            List<PickVO> pickVOs = mModel.target.pickVOs;
            PickVO vo = pickVOs.get(i);
            int statu = vo.statu;
            if (statu == 3) {
                hasCount++;
            }

        }

        mPickedCount.setText("已完成:" + hasCount);
        mPickCount.setText("拣货任务:" + count);


        if (mPhase == Phase.WALL) {//扫描拣货框编号

            PickWall target1 = mModel.target;
            List<PickingWallPosition> positions = target1.pickingWallPositions;
            int size = positions.size();

            if (size == 0) {
                //没有分配墙位,就绑定墙位
                //先扫一个墙位,获取墙位code成功后再将这个墙位id和拣货框id进行绑定
                mWallInput.requestFocus();
                Toast.makeText(PickBasketActivity.this, "该拣货框没有进行绑定", Toast.LENGTH_LONG).show();

                mVOs = target1.pickVOs;
                int size1 = mVOs.size();
                if (size1 == 1) {
                    //绑定后完成波次
                    mWallInput.requestFocus();
                }

            } else {

                //判断状态是否完成
                List<PickVO> os = target1.pickVOs;
                int taskSize = os.size();//剩余任务数

                for (int i = 0; i < taskSize; i++) {

                    PickVO pickVO = os.get(i);
                    int statu = pickVO.statu;

                    if (statu == 3) {
                        countList.add(1);
                    }
                }

                if (countList.size() == taskSize) {

                    //所有任务完成
                    mDone.setEnabled(true);
                    mDialog.show();
                    mPickInput.setEnabled(false);

                    //分配墙位了
                    String barcode = positions.get(0).barcode;
                    String code = positions.get(0).code;//墙位

                    mWallInput.setText(barcode);//墙位编号
                    mWallPositionCode.setText(code);//拣货墙位
//                    mWallPositionCode.setText("拣货墙位:" + code);//拣货墙位

                } else {
                    //还有未完成的拣货任务
                    mDone.setEnabled(false);

                    //分配墙位了
                    String barcode = positions.get(0).barcode;
                    String code = positions.get(0).code;//墙位

                    mWallInput.setText(barcode);//墙位编号
                    mWallPositionCode.setText(code);//拣货墙位
                }
            }

        } else if (mPhase == Phase.DONE) {//扫描墙位
            mWallInput.setEnabled(false);
            mWallInput.setText(mBasketBarcode);
            mWallPositionCode.setText("拣货墙位:" + mWallPosition.code);
            Toast.makeText(PickBasketActivity.this, "绑定成功", Toast.LENGTH_LONG).show();

//            mWaveView.setVisibility(View.INVISIBLE);
            mPickInput.setText("");

            if (mPhase == Phase.DONE) {
                mPhase = Phase.START;
            }

            if (mVOs != null && mVOs.size() == 1) {
                mDialog.show();
                mDone.setEnabled(true);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.done) {
//            if (mPhase == Phase.DONE) {
            finishWave();
//            }
        } else if (id == R.id.exchange) {

            Intent intent = new Intent(this, PickWallActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);

        } else if (id == R.id.quit) {
            Intent intent = new Intent(this, MenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (id == R.id.back) {
            finish();
        }
    }

    private enum Phase {
        START,
        WALL,
        DONE
    }
}
