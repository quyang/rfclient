package mobile.freshtime.com.freshtime.device;

/**
 * Created by orkid on 2016/9/13.
 */
public abstract class ScanDevice {

    public abstract void registerScanDevice();

    public abstract void unregisterScanDevice();

    public abstract void startScan();

    public abstract void stopScan();
}
