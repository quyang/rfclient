package mobile.freshtime.com.freshtime.function.tradeshelf.model;

/**
 * Created by orchid on 16/9/24.
 */

public class TradeShelfUnit {

    private String skuBarcode;
    private String spoilageNo;
    private String positionBarcode;

    public TradeShelfUnit() {
    }

    public TradeShelfUnit(String sku, String spoil, String position) {
        this.skuBarcode = sku;
        this.spoilageNo = spoil;
        this.positionBarcode = position;
    }

    public String getPositionBarcode() {
        return positionBarcode;
    }

    public String getSkuBarcode() {
        return skuBarcode;
    }

    public String getSpoilageNo() {
        return spoilageNo;
    }
}
