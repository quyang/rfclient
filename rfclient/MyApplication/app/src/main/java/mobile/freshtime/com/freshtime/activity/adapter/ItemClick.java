package mobile.freshtime.com.freshtime.activity.adapter;

import android.view.View;

/**
 * Created by orchid on 16/10/13.
 */

public class ItemClick implements View.OnClickListener {

    private int mPosition;
    private IClick mClick;

    public ItemClick(int position, IClick click) {
        this.mPosition = position;
        this.mClick = click;
    }

    @Override
    public void onClick(View v) {
        this.mClick.onItemClick(mPosition, v);
    }

    public interface IClick {
        void onItemClick(int position, View v);
    }
}
