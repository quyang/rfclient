package mobile.freshtime.com.freshtime.function.tradeshelf;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.tradeshelf.model.TradeShelfPage;
import mobile.freshtime.com.freshtime.function.tradeshelf.viewmodel.TradeShelfViewModel;

/**
 * Created by orchid on 16/9/24.
 * 验货
 * TradeShelfConfirmActivity
 */

public class TradeShelfActivity extends BaseActivity implements View.OnClickListener,
        ViewModelWatcher<TradeShelfPage>, PageListener<TradeShelfPage> {

    private View mInfoRoot;
    private ViewGroup mItemRoot;
    private ViewGroup mSkuRoot;
    private ViewGroup mQualityRoot;
    private ViewGroup mSourceRoot;

    private TextView mBefore;
    private TextView mNext;
    private TextView mDone;

    private TextView mItemName;
    private TextView mItemCode;

    private TextView mSkuName;
    private TextView mSkuCode;

    private TextView mQualityName;
    private TextView mQualityCode;

    private TextView mPostionName;
    private TextView mPostionCount;
    private TextView mCode69;

    private EditText mScanContent;
    private EditText mPositionInput;

    private TradeShelfViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tradeshelf);

        findViewById(R.id.back).setOnClickListener(this);

        mBefore = (TextView) findViewById(R.id.last);
        mBefore.setOnClickListener(this);
        mNext = (TextView) findViewById(R.id.next);
        mNext.setOnClickListener(this);
        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);

        mInfoRoot = findViewById(R.id.info_root);

        mItemRoot = (ViewGroup) findViewById(R.id.item_info_root);

        //sku
        mSkuRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        View ll = mSkuRoot.findViewById(R.id.new_code_ll);
        ll.setVisibility(View.GONE);

        //
        mSourceRoot = (ViewGroup) findViewById(R.id.source_info_root);

        mQualityRoot = (ViewGroup) findViewById(R.id.quality_info_root);
        mScanContent = (EditText) findViewById(R.id.scanContent);

        //商品69码
        mCode69 = (TextView) mItemRoot.findViewById(R.id.item_69code);
        TextView coder = (TextView) mItemRoot.findViewById(R.id.item_coder);
        coder.setVisibility(View.GONE);


        //规格
        mItemName = (TextView) mItemRoot.findViewById(R.id.item_name);
        //库位
        mItemCode = (TextView) mItemRoot.findViewById(R.id.item_code);

        mSkuName = (TextView) mSkuRoot.findViewById(R.id.item_name);
        mSkuCode = (TextView) mSkuRoot.findViewById(R.id.item_code);

        mQualityName = (TextView) mQualityRoot.findViewById(R.id.item_name);
        mQualityCode = (TextView) mQualityRoot.findViewById(R.id.item_code);

        mPostionName = (TextView) mSourceRoot.findViewById(R.id.item_name);
        mPostionCount = (TextView) mSourceRoot.findViewById(R.id.item_count);
        mPositionInput = (EditText) mSourceRoot.findViewById(R.id.position_input);

        mViewModel = new TradeShelfViewModel(this);
        mViewModel.addPageListener(this);
        mViewModel.registerContext(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.unregisterContext();
    }

    @Override
    public void onScanResult(String result) {
        result = result.trim();
        mViewModel.onScanResult(result);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {
            if (mInfoRoot.getVisibility() == View.VISIBLE) {
                if ("".equals(mPostionCount.getText().toString().trim()) || "0".equals(
                        mPostionCount.getText().toString().trim())) {
                    Toast.makeText(TradeShelfActivity.this, "请先输入数量", Toast.LENGTH_LONG).show();
                } else {
                    mViewModel.setCount(mPostionCount.getText().toString());
                    mViewModel.navNext();
                    requestDone();
                }
            }
//            requestDone();
        } else if (id == R.id.last) {
            mViewModel.navBefore();
        } else if (id == R.id.next) {
            if ("".equals(mPostionCount.getText().toString().trim()) || "0".equals(mPostionCount.getText().toString().trim())) {
                Toast.makeText(TradeShelfActivity.this, "请先输入商品数量", Toast.LENGTH_LONG).show();
            } else {
                mViewModel.setCount(mPostionCount.getText().toString());
                mViewModel.navNext();
            }
//            mViewModel.navNext();
        }
    }

    @Override
    public void onRequestStart() {

    }

    @Override
    public void onRequestStop() {

    }

    @Override
    public void onPageUpdate(TradeShelfPage page) {
        displayPage(page);
    }

    @Override
    public void onError(String errorCode) {

    }

    @Override
    public void onFinish() {

    }

    private void displayPage(TradeShelfPage page) {
        if (mViewModel.hasBefore()) {
            mBefore.setEnabled(true);
        } else {
            mBefore.setEnabled(false);
        }

        if (!mViewModel.hasNext() && page.getPhase() != TradeShelfPage.Phase.SKU) {
            mNext.setEnabled(false);
        } else {
            mNext.setEnabled(true);
        }

        TradeShelfPage.Phase phase = page.getPhase();
        if (phase == TradeShelfPage.Phase.START) {
            displayStart(page);
        } else if (phase == TradeShelfPage.Phase.SKU) {
            displaySku(page);
        }
//        else if (phase == TradeShelfPage.Phase.POSITION) {
//            displayPosition(page);
//        }
        else {
            displayCount(page);
        }
    }

    /**
     * 初始化的UI
     *
     * @param page
     */
    private void displayStart(TradeShelfPage page) {
        mInfoRoot.setVisibility(View.GONE);
        mScanContent.requestFocus();

        mItemName.setText("");
        mItemCode.setText("");

        mSkuName.setText("");
        mSkuCode.setText("");

        mQualityName.setText("");
        mQualityCode.setText("");

        mPositionInput.setText("");
        mPostionCount.setText("");

        mScanContent.setText("");
    }

    /**
     * 输入sku信息后展示的信息
     *
     * @param page
     */
    private void displaySku(TradeShelfPage page) {
        mInfoRoot.setVisibility(View.VISIBLE);

        SkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText("");
        mPostionCount.setText("");

        mPositionInput.requestFocus();


        mCode69.setText("商品69码:" + unit.getSku69Id());

    }

    /**
     * 输入库位信息后展示的信息
     *
     * @param page
     */
    private void displayPosition(TradeShelfPage page) {
        SkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText(unit.getPosition());
        mPostionCount.setText("");

        mPostionCount.requestFocus();
    }

    /**
     * 输入数量信息后展示的信息
     *
     * @param page
     */
    private void displayCount(TradeShelfPage page) {
        SkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());


//        mPositionInput.setText(unit.getPosition());
//        mPostionCount.setText(unit.getCount() + "");

        mPostionCount.clearFocus();
    }

    private void displayUnit(SkuModel unit) {

        mItemName.setText("品名：" + unit.getTitle());
        mItemCode.setText("编码：" + unit.getBarcode());
        mSkuName.setText("规格：" + unit.getUnStandardSpecification());
        mSkuCode.setText(unit.getKuwei());
        mQualityName.setText("保质期：暂无");
        mQualityCode.setText("有效期：暂无");

        if ("0".equals(unit.getCount() + "")) {
            mPostionCount.setText("");
        } else {
            mPostionCount.requestFocus();
            mPostionCount.length();
            mPostionCount.setText(unit.getCount() + "");
        }

        mCode69.setText("商品69码：" + unit.getSku69Id());
    }

    private void requestDone() {
        Intent intent = new Intent(this, TradeShelfConfirmActivity.class);

        List<TradeShelfPage> pages = mViewModel.getTradeShelfPages();
        ArrayList<SkuModel> modules = new ArrayList<>();
        for (int i = 0; i < pages.size(); i++) {
            TradeShelfPage page = pages.get(i);
            SkuModel model = page.getSkuModel();
            if (model != null && (page.getPhase() == TradeShelfPage.Phase.SKU
//                    (page.getPhase() == StoragePage.Phase.DONE
//                            ||
//                            page.getPhase() == StoragePage.Phase.POSITION
            )) {
//                pages.get(i).getSkuModel().setCount(Integer.parseInt(mPostionCount.getText().toString() + ""));
//                modules.add(pages.get(i).getSkuModel());

                modules.add(pages.get(i).getSkuModel());
            }

        }
        intent.putParcelableArrayListExtra("units", modules);
        startActivity(intent);
    }

    @Override
    public void onPageChanged(TradeShelfPage TradeShelfPage) {
        displayPage(TradeShelfPage);
    }
}