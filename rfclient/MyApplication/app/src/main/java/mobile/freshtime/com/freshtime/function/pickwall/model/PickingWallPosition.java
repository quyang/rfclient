package mobile.freshtime.com.freshtime.function.pickwall.model;

/**
 * Created by orchid on 16/10/14.
 */

public class PickingWallPosition {

    public String barcode;
    public String code;
    public long columnNo;
    public long dr;
    public long id;
    public long rowNo;
    public String ts;
    public long wallId;

    @Override
    public String toString() {
        return "PickingWallPosition{" +
                "barcode='" + barcode + '\'' +
                ", code='" + code + '\'' +
                ", columnNo=" + columnNo +
                ", dr=" + dr +
                ", id=" + id +
                ", rowNo=" + rowNo +
                ", ts='" + ts + '\'' +
                ", wallId=" + wallId +
                '}';
    }
}
