package mobile.freshtime.com.freshtime.function.handover.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;

/**
 * Created by orchid on 16/10/15.
 */
@Deprecated
public class DeliverTaskAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SkuModel> mLossUnits;

    public DeliverTaskAdapter(Context context, ArrayList<SkuModel> lossUnits) {
        this.mContext = context;
        this.mLossUnits = lossUnits;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
