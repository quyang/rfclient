package mobile.freshtime.com.freshtime.function.pick;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.pick.model.PickPage;
import mobile.freshtime.com.freshtime.function.pick.model.PickTask;
import mobile.freshtime.com.freshtime.function.pick.viewmodel.PickViewModel;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;

import static mobile.freshtime.com.freshtime.R.id.done;

/**
 * 开始拣货
 * Created by orchid on 16/10/6.
 */

public class PickActivity extends BaseActivity implements ViewModelWatcher<PickPage>,
        PageListener<PickPage>, View.OnClickListener {

    private PickTask mTask;

    private PickViewModel mViewModel;

    private TextView mBefore;

    private TextView mNext;

    private TextView mDone;

    private TextView mLack;

    private TextView mItemName;

    private TextView mItemCode;

    private TextView mSkuName;

    private TextView mSkuCode;

    private TextView mQualityName;

    private TextView mQualityCode;

    private EditText mItemInput;

    private EditText mPickPosition;

    private EditText mPickedQuantity;

    private TextView mUnpickedQuantity;

    private ArrayList<String> mBarCodeList;

    private ArrayList<String> mCode69List;

    private TextView mCode69;

    private TextView mCoder;

    private ArrayList<String> mCodeList;

    private ArrayList<String> mList = new ArrayList<>();

    private Dialog mMMDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick);

        mTask = getIntent().getParcelableExtra("task");

        //获取仓位码集合
        mBarCodeList = getIntent().getStringArrayListExtra("barcodeList");
        //获取coder
        mCodeList = getIntent().getStringArrayListExtra("codeList");
        //获取69码集合
        mCode69List = getIntent().getStringArrayListExtra("code69List");
        TextView itemTag = (TextView) findViewById(R.id.delivery_input).findViewById(R.id.item_tag);
        itemTag.setText("商品条码：");
        mBefore = (TextView) findViewById(R.id.last);
        mBefore.setOnClickListener(this);
        mNext = (TextView) findViewById(R.id.next);//下一页
        mNext.setOnClickListener(this);
        mDone = (TextView) findViewById(done);
        mDone.setOnClickListener(this);
        mLack = (TextView) findViewById(R.id.lack);
        mLack.setOnClickListener(this);

        findViewById(R.id.sku_info_root).findViewById(R.id.ll_container).setVisibility(View.GONE);

        findViewById(R.id.back).setOnClickListener(this);

//        new_code_ll
        ViewGroup skuRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        LinearLayout llNewCoder = (LinearLayout) skuRoot.findViewById(R.id.new_code_ll);
        llNewCoder.setVisibility(View.GONE);

        //品名
        mItemName = (TextView) findViewById(R.id.item_info_root).findViewById(R.id.item_name);
        //编码
        mItemCode = (TextView) findViewById(R.id.item_info_root).findViewById(R.id.item_code);
        //69code
        mCode69 = (TextView) findViewById(R.id.item_info_root).findViewById(R.id.item_69code);
        //QC
        mCoder = (TextView) findViewById(R.id.item_info_root).findViewById(R.id.item_coder);
        //规格
        mSkuName = (TextView) findViewById(R.id.sku_info_root).findViewById(R.id.item_name);
        //单位
        mSkuCode = (TextView) findViewById(R.id.sku_info_root).findViewById(R.id.item_code);
        mQualityName = (TextView) findViewById(R.id.quality_info_root).findViewById(R.id.item_name);
        mQualityCode = (TextView) findViewById(R.id.quality_info_root).findViewById(R.id.item_code);
        //条码框 上面
        mItemInput = (EditText) findViewById(R.id.item_input);
        //仓位
        mPickPosition = (EditText) findViewById(R.id.pick_info_root).findViewById(R.id.position_input);
        //已拣数
        mPickedQuantity = (EditText) findViewById(R.id.pick_info_root).findViewById(R.id.pick_count);
        //待拣数
        mUnpickedQuantity = (TextView) findViewById(R.id.pick_info_root).findViewById(R.id.unpick_count);
        //传递过来的仓位id
        long basketPosition = getIntent().getLongExtra("positionId", 0l);
        mViewModel = new PickViewModel(this, mTask, basketPosition, mBarCodeList);
        mViewModel.addPageListener(this);
        mViewModel.registerContext(this);

        //初始化第一页
        mCode69.setText("商品69码:" + mCode69List.get(0));
        mCoder.setText("库位:" + mCodeList.get(0));

        String code = "2210004100011".substring(2, 7);
    }

    public void setBarCode(String barCode) {
        mPickPosition.setText(barCode);
    }

    //仓位
    @Override
    public void onScanResult(String result) {
        mViewModel.onScanResult(result);
    }

    @Override
    public void onRequestStart() {
        // TODO nothing...
    }

    @Override
    public void onRequestStop() {
        // TODO nothing...
    }

    //扫码后将结果回调到这里
    @Override
    public void onPageUpdate(PickPage page) {
        displayPage(page);
    }

    //扫到的条码和实际的商品条码不一样时回调
    @Override
    public void onError(String errorCode) {
        Toast.makeText(PickActivity.this, "" + errorCode, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFinish() {
        mMMDialog.dismiss();
        finish();
    }


    //开始拣货按钮的回调/下一页
    @Override
    public void onPageChanged(PickPage page) {
        displayPage(page);
    }

    int index = 0;

    private void displayPage(PickPage page) {

        if (mViewModel.hasBefore()) {
            mBefore.setEnabled(true);
        } else {
            mBefore.setEnabled(false);
        }

        if (!mViewModel.hasNext()) {
            mNext.setEnabled(false);
        } else {
            mNext.setEnabled(true);

        }

        PickPage.Phase phase = page.getPhase();
        if (phase == PickPage.Phase.START) {
            displayStart();//上面的商品条码框显示
        } else if (phase == PickPage.Phase.POSITION) {
//            displayPosition();//显示仓位框
            displayCount();//显示已拣货框
        } else if (phase == PickPage.Phase.COUNT) {
            displayCount();//显示已拣货框
        } else {
            displayDone();
        }


        //商品69码：1
        mItemName.setText("品名:" + page.getTask().title);
        mItemCode.setText(page.getTask().skuBarcode);

        mSkuName.setText("规格:" + page.getTask().specification);
        mSkuCode.setText(page.getTask().subTitle);

        if (!TextUtils.isEmpty(page.getTask().skuBarcode)) {
            mItemInput.setText(page.getTask().skuBarcode);
        }

        if (!TextUtils.isEmpty(page.getTask().positionBarcode)) {
            mPickPosition.setText(page.getTask().positionBarcode);
        }

        //skuBarcode  商品条码
        if (page.getTask().skuBarcode == null) {
            mItemCode.setText("无");
        }


        mItemName.setText("品名:" + page.getTask().title);
        if (page.getTask().title == null) {
            mItemName.setText("品名:暂无");
        }


        mSkuName.setText("规格:" + page.getTask().specification);
        if (page.getTask().specification == null || "".equals(page.getTask().specification + "")) {
            mSkuName.setText("规格:暂无");
        }
        mSkuCode.setText(page.getTask().subTitle);
        if (page.getTask().subTitle == null || "".equals(page.getTask().subTitle + "")) {
            mSkuCode.setText("单位:暂无");
        }

        mQualityName.setText("保质期:" + "暂无");
        mQualityCode.setText("有效期:" + "暂无");

        //条码
        if (!TextUtils.isEmpty(page.getTask().skuBarcode)) {
            mItemInput.setText(page.getTask().skuBarcode);
        }

        if (page.getTask().pickedNo > page.getTask().orderNo) {
            return;
        }

        if ("0".equals(page.getTask().pickedNo + "")) {
            mPickedQuantity.setText("");
            mPickedQuantity.requestFocus();
        } else {
            mPickedQuantity.requestFocus();
            mPickedQuantity.setText(page.getTask().pickedNo + "");
            mPickedQuantity.length();
        }

        mUnpickedQuantity.setText(page.getTask().orderNo + "");
        if (!TextUtils.isEmpty(page.getTask().positionBarcode)) {
            mPickPosition.setText(page.getTask().positionBarcode);
        }
    }

    //已拣货框获取焦点
    public void getFoucus() {
        mPickedQuantity.requestFocus();
        mPickedQuantity.setEnabled(true);
    }

    //显示上面的条码输入框,获取焦点
    private void displayStart() {
        mItemInput.setEnabled(true);
        mItemInput.requestFocus();

        mPickPosition.setEnabled(false);
        mPickedQuantity.setEnabled(false);
    }

    //显示仓位框
    private void displayPosition() {
        mItemInput.setEnabled(false);
        mPickPosition.setEnabled(true);
        mPickedQuantity.setEnabled(false);
        mPickPosition.requestFocus();
    }

    //显示已拣货框
    private void displayCount() {
        mItemInput.setEnabled(false);
        mPickPosition.setEnabled(false);
        mPickedQuantity.setEnabled(true);
        mPickedQuantity.requestFocus();
    }

    //商品框.仓位框和已拣货框都不可用
    private void displayDone() {
        mItemInput.setEnabled(false);
        mPickPosition.setEnabled(false);
        mPickedQuantity.setEnabled(false);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            Intent intent = new Intent();
            intent.putExtra("task", mTask);
            setResult(Activity.RESULT_OK, intent);
            finish();
        } else if (id == done) {//完成
            if (!mViewModel.hasNext()) {//没有下一页的时候才能提交完成

                if ("".equals(mPickedQuantity.getText().toString())) {
                    Toast.makeText(PickActivity.this, "请先扫码", Toast.LENGTH_LONG).show();
                    return;
                }

                if (Integer.parseInt(mPickedQuantity.getText().toString())
                        == Integer.parseInt(mUnpickedQuantity.getText().toString())) {


                    mMMDialog = new Dialog(PickActivity.this, R.style.MyDialog);
                    View view = getLayoutInflater().inflate(R.layout.cancel, null);
                    TextView name = (TextView) view.findViewById(R.id.name);
                    name.setText("确定要提交吗?");
                    Button mNo = (Button) view.findViewById(R.id.no);
                    Button mYes = (Button) view.findViewById(R.id.yes);

                    mMMDialog.setCancelable(false);
                    mMMDialog.setContentView(view);

                    //设置弹窗宽高
                    mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(PickActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

                    mMMDialog.show();

                    mYes.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestDone();
                        }
                    });

                    mNo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mMMDialog.dismiss();
                        }
                    });


                } else {
                    Toast.makeText(PickActivity.this, "请检查数量", Toast.LENGTH_LONG).show();
                }

            } else {
                ToastUtils.showToast("未拣货完成");
            }
        } else if (id == R.id.last) {//上一页

            mList.add(mItemInput.getText().toString().trim() + "");

            mViewModel.navBefore();

            index--;

            String s = mList.get(index);

            mItemInput.setText(s);

            mCode69.setText("商品69码:" + mCode69List.get(index));
            mCoder.setText("QC:" + mCodeList.get(index));

        } else if (id == R.id.next) {  //下一页

            //判断已拣货数是否等于待拣货数
            String picked = mPickedQuantity.getText().toString().trim();
            String unPicked = mUnpickedQuantity.getText().toString().trim();

            if (StringUtils.isEmpty(picked)) {
                Toast.makeText(PickActivity.this, "请先输入数量", Toast.LENGTH_LONG).show();
                return;
            }

            if (Integer.parseInt(picked) > Integer.parseInt(unPicked)) {
                Toast.makeText(PickActivity.this, "已拣货数不能超过待拣货数", Toast.LENGTH_LONG).show();
                return;
            }

            if (Integer.parseInt(picked) == Integer.parseInt(unPicked)) {


                mList.add(mItemInput.getText().toString().trim() + "");

                mViewModel.navNext();

                index++;

                String s = null;
                try {
                    s = mList.get(index);
                    mItemInput.setText(s);

                } catch (Exception e) {
                    mItemInput.setText("");
                }

                mCode69.setText("商品69码:" + mCode69List.get(index));
                mCoder.setText("QC:" + mCodeList.get(index));

            } else {
                ToastUtils.showToast("拣货未完成");
            }
        }
    }

    private void requestDone() {
        mViewModel.requestDone();
        mList.clear();
    }

    public void setZero() {
        index = 0;
    }


    public long getDaiCount() {
        String trim = mUnpickedQuantity.getText().toString().trim();
        long aLong = Long.parseLong(trim);
        return aLong;
    }
}
