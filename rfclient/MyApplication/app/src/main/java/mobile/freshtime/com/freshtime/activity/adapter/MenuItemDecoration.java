package mobile.freshtime.com.freshtime.activity.adapter;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by orchid on 16/10/13.
 */

public class MenuItemDecoration extends RecyclerView.ItemDecoration {

    private int space;

    public MenuItemDecoration(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.top = space / 2;
        outRect.left = space;
        outRect.bottom = space;
        if (parent.getChildLayoutPosition(view) % 3 == 0) {
            outRect.left = 0;
        }
    }

}
