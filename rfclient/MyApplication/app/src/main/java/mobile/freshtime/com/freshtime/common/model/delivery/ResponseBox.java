package mobile.freshtime.com.freshtime.common.model.delivery;

/**
 * Created by orchid on 16/10/15.
 */

public class ResponseBox {

    public String message;
    public int return_code;
    public boolean success;
    public DeliverBox target;
}
