package mobile.freshtime.com.freshtime.function.restore.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;

/**
 * Created by orchid on 16/10/16.
 */

public class RestoreItemAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SkuModel> mSkuModels;
    private IQuantityWatcher mWatcher;

    public RestoreItemAdapter(Context context, ArrayList<SkuModel> list, IQuantityWatcher watcher) {
        this.mContext = context;
        this.mSkuModels = list;
        this.mWatcher = watcher;
    }

    @Override
    public int getCount() {
        return mSkuModels.size();
    }

    @Override
    public Object getItem(int position) {
        return mSkuModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ItemViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ItemViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_restore_item, null);
            convertView.setTag(viewHolder);
            viewHolder.mItemName = (TextView) convertView.findViewById(R.id.item_name);
            viewHolder.mMinusItem = (TextView) convertView.findViewById(R.id.minus);
            viewHolder.mItemCount = (TextView) convertView.findViewById(R.id.item_count);
            viewHolder.mAddItem = (TextView) convertView.findViewById(R.id.add);
        } else {
            viewHolder = (ItemViewHolder) convertView.getTag();
        }
        viewHolder.mAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWatcher.onItemAdd(position);
            }
        });
        viewHolder.mMinusItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWatcher.onItemMinus(position);
            }
        });
        viewHolder.mItemName.setText(mSkuModels.get(position).getTitle());
        viewHolder.mItemCount.setText("" + mSkuModels.get(position).getCount());
        return convertView;
    }

    public void updateData(ArrayList<SkuModel> list) {
        this.mSkuModels = list;
        this.notifyDataSetChanged();
    }

    private static class ItemViewHolder {
        public TextView mItemName;
        public TextView mAddItem;
        public TextView mItemCount;
        public TextView mMinusItem;
    }
}
