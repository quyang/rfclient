package mobile.freshtime.com.freshtime.function.search;

/**
 * Created by orchid on 16/10/10.
 */
public class KuweiPage {

    private Phase mPhase;

    public KuweiPage() {
        this.mPhase = Phase.START;
    }

    public Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == Phase.START) {
            mPhase = Phase.NEW_CODE;
        } else if (mPhase == Phase.NEW_CODE) {
            mPhase = Phase.BDING;
        } else if (mPhase == Phase.BDING) {
            mPhase = Phase.JIEBANG;
        } else if (mPhase == Phase.JIEBANG) {
            mPhase = Phase.DONE;
        }
    }

    public enum Phase {
        START,
        NEW_CODE,
        BDING,
        JIEBANG,
        DONE
    }
}
