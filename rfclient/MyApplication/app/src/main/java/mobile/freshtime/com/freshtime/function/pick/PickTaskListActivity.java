package mobile.freshtime.com.freshtime.function.pick;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.basket.ResponseBasket;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSkuList;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.function.pick.adapter.PickTaskAdatper;
import mobile.freshtime.com.freshtime.function.pick.model.PickTask;
import mobile.freshtime.com.freshtime.function.pick.model.ResponsePickTask;
import mobile.freshtime.com.freshtime.function.pick.model.Task;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuListRequest;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * 拣货
 * <p>
 * Created by orchid on 16/9/18.
 */
public class PickTaskListActivity extends BaseActivity implements View.OnClickListener, Response.ErrorListener,
        Response.Listener<ResponsePickTask> {


    private static final String TAG = "PickTaskListActivity";

    private static final int PICK = 1;

    private View mPickRoot;
    private ListView mListView;
    private PickTaskAdatper mAdatper;
    private PickTask mTasks;

    private TextView mStartText;
    private long mPositionId = 0l;
    private View mProgressBar;

    private EditText mBasketPosition;

    ArrayList<String> mListCode = new ArrayList<>();

    //仓位集合
    ArrayList<String> mListBarCode = new ArrayList<>();

    private Response.Listener<ResponseSkuList> mResponseSkuListener =
            new Response.Listener<ResponseSkuList>() {
                @Override
                public void onResponse(ResponseSkuList response) {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    //lv里面的信息列表获取
                    if (response != null && response.isSuccess()) {
                        displayDetail(response.getModule());
                    } else {

                        String msg = response == null ? "未获取到sku信息" : response.getErrorMsg();
                        //is empty
                        Toast.makeText(PickTaskListActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                    }
                }
            };

    private Response.Listener<ResponseBasket> mPositionListener =
            new Response.Listener<ResponseBasket>() {
                @Override
                public void onResponse(ResponseBasket response) {
                    mProgressBar.setVisibility(View.INVISIBLE);
                    if (response != null && response.isSuccess()) {
                        mPositionId = response.target.id;
                        if (mPickRoot.getVisibility() == View.VISIBLE) {
                            mStartText.setEnabled(true);
                        }
                    } else {
                        String msg = response == null ? "未获取到sku信息" : response.getErrorMsg();
                        //获取仓位错误
                        Toast.makeText(PickTaskListActivity.this, "" + msg, Toast.LENGTH_SHORT).show();
                    }
                }
            };

    private Handler mHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_task_list);

        EventBus.getDefault().register(this);

        mHandler = new Handler(Looper.getMainLooper());

        mPickRoot = findViewById(R.id.pick_root);
        mPickRoot.setVisibility(View.INVISIBLE);

        //listview
        mListView = (ListView) mPickRoot.findViewById(R.id.list);

        mStartText = (TextView) findViewById(R.id.start);
        mStartText.setOnClickListener(this);
        mStartText.setEnabled(false);
        findViewById(R.id.menu).setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);
        findViewById(R.id.get).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);

        //条码框
        mBasketPosition = (EditText) findViewById(R.id.position_input);

        mProgressBar = findViewById(R.id.progressbar);
        mProgressBar.setVisibility(View.INVISIBLE);

        TextView operator = (TextView) findViewById(R.id.operator);
        operator.setText("已登录用户：" + Login.getInstance().getUserId());
    }

    @Override
    public void onScanResult(String result) {

        if (mPickRoot.getVisibility() == View.VISIBLE) {

            result = result.trim();
            mBasketPosition.setText(result);
            JSONObject param = new JSONObject();
            try {
                param.put("token", Login.getInstance().getToken());
                param.put("data", result);
                BaseRequest<ResponseBasket> request =
                        new BaseRequest<>(RequestUtils.PICK_BASKET,
                                ResponseBasket.class,
                                param,
                                mPositionListener, this);
                FreshRequest.getInstance().addToRequestQueue(request, this.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(PickTaskListActivity.this, getResources().getString(R.string.please_get_pick_task_first), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.menu) {
            finish();
        } else if (id == R.id.done) {
        } else if (id == R.id.get) {
            requestPickList();
        } else if (id == R.id.start) {
            if ("".equals(mBasketPosition.getText().toString().trim())) {
                Toast.makeText(PickTaskListActivity.this, getResources().getString(R.string.please_scan_kuang), Toast.LENGTH_LONG).show();
            } else {
                startPick();//开始拣货
            }
        } else if (id == R.id.back) {
            finish();
        }
    }

    /**
     * 拉取拣货任务 jianxi
     */
    private void requestPickList() {
        mListCode.clear();
        mBasketPosition.setText("");
        mProgressBar.setVisibility(View.VISIBLE);
        JSONObject param = new JSONObject();

        try {
            param.put("token", Login.getInstance().getToken());
            BaseRequest<ResponsePickTask> request =
                    new BaseRequest<>(RequestUtils.PICK_TASK,
                            ResponsePickTask.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            mProgressBar.setVisibility(View.INVISIBLE);
            e.printStackTrace();
        }
    }

    //开始拣货
    private void startPick() {
        if (mPositionId == 0l) {
            //空的吐司
            Toast.makeText(this, getResources().getString(R.string.get_fail), Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(this, PickActivity.class);
            intent.putExtra("task", mTasks);
            intent.putExtra("positionId", mPositionId);
            intent.putStringArrayListExtra("barcodeList", barCodeLista);
            intent.putStringArrayListExtra("codeList", codeLista);
            intent.putStringArrayListExtra("code69List", sku69IdList);
            startActivityForResult(intent, PICK);
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mProgressBar.setVisibility(View.INVISIBLE);
        String message = error.getMessage();
        Toast.makeText(this, message + "", Toast.LENGTH_SHORT).show();
    }

    //存储positionId 仓位id
    List<Integer> mList = new ArrayList<Integer>();

    @Override
    public void onResponse(ResponsePickTask response) {
        if (response != null && response.isSuccess() && response.target != null) {

            ArrayList<Task> details = response.target.details;
            int size = details.size();

            if (size == 0) {
                return;
            }

            mList.clear();

            for (int i = 0; i < size; i++) {
                mList.add((int) details.get(i).positionId);
            }

            //显示上面的拣货框view
            mPickRoot.setVisibility(View.VISIBLE);
            mTasks = response.target;

            String ids = "";
            for (int i = 0; i < mTasks.details.size(); i++) {
                Task task = mTasks.details.get(i);
                if (i != mTasks.details.size() - 1) {
                    ids = ids + task.skuId + ",";
                } else {
                    ids = ids + task.skuId;
                }
            }

            JSONObject param = new JSONObject();
            try {
                param.put("token", Login.getInstance().getToken());
                //请你去lv里面的sku信息
                SkuListRequest request =
                        new SkuListRequest(ids, mResponseSkuListener, this);
                FreshRequest.getInstance().addToRequestQueue(request, this.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            } finally {
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        } else {
            String msg = (response == null || response.target == null) ? "未获取到任务信息" : response.getErrorMsg();
            mListView.setVisibility(View.INVISIBLE);
            mStartText.setEnabled(false);
//            mStartText.setVisibility(View.INVISIBLE);
            Toast.makeText(PickTaskListActivity.this, msg, Toast.LENGTH_SHORT).show();
            Log.e(TAG, "PickTaskListActivity.onResponse,msg=" + msg);//未获取到任何信息
            mProgressBar.setVisibility(View.INVISIBLE);
        }
    }

    ArrayList<String> sku69IdList = new ArrayList<String>();

    /**
     * 合并两次请求获得的详情数据
     *
     * @param details
     */
    private void displayDetail(List<SkuModel> details) {

        //获取商品69码
        for (int i = 0; i < details.size(); i++) {
            long sku69Id = details.get(i).getSku69Id();
            String s = String.valueOf(sku69Id);
            sku69IdList.add(s);
        }


        mListCode.clear();
        codeBeanList.clear();
        tagList.clear();
        urlTagList.clear();

        for (int i = 0; i < details.size(); i++) {
            for (int j = 0; j < mTasks.details.size(); j++) {
                long idSku = details.get(i).getSkuId();
                long idTask = mTasks.details.get(j).skuId;
                if (idSku == idTask) {
                    mTasks.details.get(j).merge(details.get(i));
                    break;
                }
            }
        }

        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .build();


        for (int i = 0; i < mList.size(); i++) {
            urlTagList.add(0);
        }

        requestNet(okHttpClient);
    }


    ArrayList<CodeBean> codeBeanList = new ArrayList<>();

    //请求网络
    private synchronized void requestNet(OkHttpClient okHttpClient) {

        String jihe = "";
        for (int p = 0; p < mList.size(); p++) {

            if (p == mList.size() - 1) {
                jihe = jihe + mList.get(p) + "";
            } else {
                jihe = jihe + mList.get(p) + "','";
            }
        }

        String urlCode = RequestUtils.PICK_POISITION + "?request={token:'" + Login.getInstance().getToken() + "',data:['" + jihe + "']}";
//        String urlCode = RequestUtils.PICK_POISITION + "?request={token:'" + Login.getInstance().getToken() + "',data:['" + mList.get(i) + "']}";
        final Request request = new Request.Builder()
                .get()
                .url(urlCode)
                .build();

        if (!NetUtils.hasNet(PickTaskListActivity.this)) {
            ToastUtils.showToast(getResources().getString(R.string.net_error));
            return;
        }

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                ToastUtils.showToast("" + e.getMessage());
            }

            @Override
            public void onResponse(Call call, okhttp3.Response response) throws IOException {

                final String string = response.body().string();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        adapterData(string);
                    }
                });

            }
        });
    }

    ArrayList<String> codeLista = new ArrayList<>();
    ArrayList<String> barCodeLista = new ArrayList<>();

    //适配数据
    private void adapterData(String string) {

        Gson gson = new Gson();
        CodeBean codeBean = gson.fromJson(string, CodeBean.class);

        if (codeBean.success) {
            List<CodeBean.TargetBean> targetBeanList = codeBean.target;

            for (int i = 0; i < targetBeanList.size(); i++) {
                CodeBean.TargetBean bean = targetBeanList.get(i);
                String barcode = bean.barcode;
                String code = bean.code;

                codeLista.add(code);
                barCodeLista.add(barcode);
            }

            mAdatper = new PickTaskAdatper(getApplicationContext(), mTasks.details, codeLista);
            mListView.setAdapter(mAdatper);
        } else {
            ToastUtils.showToast("" + codeBean.message);
        }


    }

    ArrayList<Integer> urlTagList = new ArrayList<>();
    ArrayList<Integer> tagList = new ArrayList<>();

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PICK && resultCode == Activity.RESULT_OK) {
            mTasks = data.getParcelableExtra("task");
            if (mAdatper != null) {
                mAdatper.updataData(mTasks.details);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void retryNet(Class<PickTaskListActivity> result) {
        requestPickList();
    }
}
