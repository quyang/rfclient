package mobile.freshtime.com.freshtime.function.stock;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.function.stock.module.OnePanzhuang;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPanInfo;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * 一盘任务详情页
 */
public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {


    private ListView mLv;
    private String mTaskId;
    private Dialog mMMDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        TextView back = (TextView) findViewById(R.id.back);
        TextView commit = (TextView) findViewById(R.id.commit);

        back.setOnClickListener(this);
        commit.setOnClickListener(this);


        mLv = (ListView) findViewById(R.id.lv);
        mLv.setOnItemClickListener(new MyOnItemClickListener());

        mTaskId = getIntent().getStringExtra("taskId");

        if (!StringUtils.isKongStr(mTaskId)) {
            getInfo(Integer.parseInt(mTaskId));
        }
    }

    //根据id获取对应的明细
    public void getInfo(int taskId) {

        JSONObject params = new JSONObject();
        try {
            params.put("token", Login.getInstance().getToken());
            params.put("data", taskId + "");

            GetProtocol protocol = new GetProtocol(RequestUtils.GET_STOCK + "?request=" + params);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("DetailsActivity.onSuccess" + "=" + json);

                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast("没有更多数据");
                        return;
                    }

                    if (!StringUtils.isEmpty(json)) {
                        Gson gson = new Gson();
                        TwoPanInfo info = gson.fromJson(json, TwoPanInfo.class);

                        if (info.isSuccess() && info.getTarget() != null && info.getTarget().getDetails() != null && info.getTarget().getDetails().size() > 0) {
                            List<TwoPanInfo.TargetBean.DetailsBean> details = info.getTarget().getDetails();
                            mLv.setAdapter(new MyBaseAdapter(details));
                        } else {
                            ToastUtils.showToast("" + info.getMessage());
                        }
                    }
                }

                @Override
                public void onFail() {
                    ToastUtils.showToast("请求失败");
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.commit://审核

                mMMDialog = new Dialog(DetailsActivity.this, R.style.MyDialog);
                View view = getLayoutInflater().inflate(R.layout.cancel, null);
                TextView name = (TextView) view.findViewById(R.id.name);
                name.setText(getResources().getString(R.string.submit_fuhe));
                Button mNo = (Button) view.findViewById(R.id.no);
                Button mYes = (Button) view.findViewById(R.id.yes);

                mMMDialog.setCancelable(false);
                mMMDialog.setContentView(view);

                //设置弹窗宽高
                mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(DetailsActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);
                mMMDialog.show();
                mYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        changeStatus(Integer.parseInt(mTaskId));

                    }
                });

                mNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMMDialog.dismiss();
                    }
                });


                break;
        }
    }

    //listview的item点击事件
    private class MyOnItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }
    }


    //改变状态 (一盘)
    private void changeStatus(final int taskId) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", taskId + "");

            GetProtocol protocol = new GetProtocol(RequestUtils.TO_CHECK + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    System.out.println("DetailsActivity.onSuccess" + "=" + json);

                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        if (jsonObject.getBoolean("success")) {
                            ToastUtils.showToast(getResources().getString(R.string.commit_success));

                            mMMDialog.dismiss();

                            EventBus.getDefault().post(new OnePanzhuang());

                            finish();

                        } else {
                            ToastUtils.showToast("" + jsonObject.getString("message"));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        ToastUtils.showToast(e.getMessage());
                    }
                }

                @Override
                public void onFail() {
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private class MyBaseAdapter extends BaseAdapter {

        private List<TwoPanInfo.TargetBean.DetailsBean> mDetails;

        public MyBaseAdapter(List<TwoPanInfo.TargetBean.DetailsBean> details) {
            mDetails = details;
        }

        @Override
        public int getCount() {
            return mDetails == null ? 0 : mDetails.size();
        }

        @Override
        public TwoPanInfo.TargetBean.DetailsBean getItem(int position) {
            return mDetails.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;

            if (convertView == null) {

                holder = new ViewHolder();

                convertView = View.inflate(DetailsActivity.this, R.layout.details_lv_item, null);

                holder.title = (TextView) convertView.findViewById(R.id.title);
                holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.count = (TextView) convertView.findViewById(R.id.count);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            TwoPanInfo.TargetBean.DetailsBean item = getItem(position);

            holder.title.setText(item.getSkuName() + "");
            holder.code.setText(item.getPositionCode() + "");
            holder.count.setText("一盘数: " + item.getFirstNo());

            return convertView;
        }
    }

    static class ViewHolder {

        public TextView title;
        public TextView code;
        public TextView count;
    }
}
