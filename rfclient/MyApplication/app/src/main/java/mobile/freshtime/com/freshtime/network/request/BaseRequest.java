package mobile.freshtime.com.freshtime.network.request;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;

/**
 * Created by orchid on 16/9/14.
 */
public class BaseRequest<ResponseObject> extends Request<ResponseObject> {

    private Gson mGson = new Gson();
    private Class<ResponseObject> mClass;
    private String mParam;
    private WeakReference<Response.Listener<ResponseObject>> mListener;

    public BaseRequest(String url,
                       Class<ResponseObject> clazz,
                       JSONObject param,
                       Response.Listener<ResponseObject> listener,
                       Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);
        this.mClass = clazz;
        this.mParam = (param == null) ? null : param.toString();
        this.mParam = "request=" + this.mParam;
        this.mListener = new WeakReference<>(listener);
    }

    @Override
    protected Response<ResponseObject> parseNetworkResponse(NetworkResponse response) {

        try {
            String jsonString =
                    new String(response.data, "UTF-8");
            Log.e("freshtime", "volley response : " + jsonString);
            ResponseObject result = mGson.fromJson(jsonString, mClass);
            return Response.success(
                    result,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        }
    }

    @Override
    protected void deliverResponse(ResponseObject response) {
        if (mListener != null && mListener.get() != null)
            mListener.get().onResponse(response);
        else
            Log.e("orkid", "request released!");
    }

    @Override
    public byte[] getBody() {
        try {
            return mParam == null ? null : mParam.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mParam, "utf-8");
            return null;
        }
    }
}
