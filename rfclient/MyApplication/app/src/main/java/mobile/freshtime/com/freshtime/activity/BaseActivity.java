package mobile.freshtime.com.freshtime.activity;

import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import mobile.freshtime.com.freshtime.device.DeviceManager;
import mobile.freshtime.com.freshtime.device.ScanDevice;
import mobile.freshtime.com.freshtime.function.search.ViewInterface;

/**
 * Created by orkid on 2016/8/14.
 */
public abstract class BaseActivity extends Activity implements ViewInterface {

    protected ScanDevice mDevice;
    protected MediaPlayer mediaPlayer = null;

    public abstract void onScanResult(String result);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //初始化声音
        initBeepSound();
        mDevice = DeviceManager.initDevice(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mDevice.registerScanDevice();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDevice.unregisterScanDevice();
    }

    private void initBeepSound() {
//
//        if (mediaPlayer == null) {
//            mediaPlayer = new MediaPlayer();
//            AssetFileDescriptor file = getResources().openRawResourceFd(
//                    R.raw.beep);
//            try {
//                if (mediaPlayer != null) {
//                    mediaPlayer.setDataSource(file.getFileDescriptor(),
//                            file.getStartOffset(), file.getLength());
//                    file.close();
//
//                    mediaPlayer.prepare();
//                }
//            } catch (IOException e) {
//                mediaPlayer = null;
//            }
//        }
    }

    public void playBeepSound() {
        if (mediaPlayer != null) {
            mediaPlayer.start();
        }
    }

    /**
     * for test
     *
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {


        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            onScanResult("10002");
            return true;
        } else if (keyCode == 142) {
            final View focus = getCurrentFocus();
            if (focus != null && focus instanceof EditText) {

                if (!(((EditText) focus).getText().toString().trim().isEmpty())) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onScanResult(((EditText) focus).getText().toString().trim());
//                            ((EditText) focus).setText("");
                        }
                    }, 50);

                }

//                onScanResult(((EditText) focus).getText().toString());
                return true;
            }
            return super.onKeyDown(keyCode, event);
        } else {
            return super.onKeyDown(keyCode, event);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        return super.dispatchKeyEvent(event);
    }
}
