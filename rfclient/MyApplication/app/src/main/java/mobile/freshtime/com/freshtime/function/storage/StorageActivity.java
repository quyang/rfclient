package mobile.freshtime.com.freshtime.function.storage;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.adapter.ReasonAdatper;
import mobile.freshtime.com.freshtime.common.model.reason.Reason;
import mobile.freshtime.com.freshtime.common.model.reason.ResponseReason;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.storage.model.InSkuModel;
import mobile.freshtime.com.freshtime.function.storage.model.StoragePage;
import mobile.freshtime.com.freshtime.function.storage.viewmodel.StorageViewModel;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;

import static mobile.freshtime.com.freshtime.R.id.item_code;
import static mobile.freshtime.com.freshtime.R.id.new_item_code;

/**
 * Created by orchid on 16/9/18.
 * <p>
 * 入库对应activity
 */
public class StorageActivity extends BaseActivity implements View.OnClickListener,
        PageListener<StoragePage>, ViewModelWatcher<StoragePage>, Response.ErrorListener, Response.Listener<ResponseReason>, DialogInterface.OnCancelListener {

    private View mInfoRoot;
    private ViewGroup mItemRoot;
    private ViewGroup mSkuRoot;
    private ViewGroup mQualityRoot;
    private ViewGroup mSourceRoot;

    private TextView mBefore;
    private TextView mNext;
    private TextView mDone;

    private TextView mItemName;
    public TextView mItemCode;

    private TextView mSkuName;
    public TextView mSkuCode;

    private TextView mQualityName;
    private TextView mQualityCode;

    private TextView mPostionName;
    public TextView mPostionCount;

    private EditText mScanContent;
    private EditText mPositionInput;

    private StorageViewModel mViewModel;
    private int mReason;
    private TextView mCode69;
    public TextView mBangdign;
    public NoInputEditText mNew_kuwei;
    private long mSkuId;
    private long newPid;
    private TextView mDanwei;
    private TextView mCheck;
    private TextView mCoder;
    private TextView mSafeKucun;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_storage);

        findViewById(R.id.back).setOnClickListener(this);

        mCheck = (TextView) findViewById(R.id.check);
        mCheck.setVisibility(View.VISIBLE);
        mCheck.setOnClickListener(this);

        mSafeKucun = (TextView) findViewById(R.id.safe_kucun);
        mSafeKucun.setVisibility(View.VISIBLE);

        mBefore = (TextView) findViewById(R.id.last);
        mBefore.setOnClickListener(this);
        mNext = (TextView) findViewById(R.id.next);
        mNext.setOnClickListener(this);
        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);

        mNext.setEnabled(false);

        //绑定
        mBangdign = (TextView) findViewById(R.id.bangding);
        mBangdign.setClickable(true);

        mInfoRoot = findViewById(R.id.info_root);
        mItemRoot = (ViewGroup) findViewById(R.id.item_info_root);
        mSkuRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        mSourceRoot = (ViewGroup) findViewById(R.id.source_info_root);
        mQualityRoot = (ViewGroup) findViewById(R.id.quality_info_root);
        mScanContent = (EditText) findViewById(R.id.scanContent);

        mScanContent.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        //单位
        mDanwei = (TextView) mSourceRoot.findViewById(R.id.danwei);

        //edittext 新库位
        mNew_kuwei = (NoInputEditText) findViewById(new_item_code);

        //商品69码
        mCode69 = (TextView) mItemRoot.findViewById(R.id.item_69code);
        mCoder = (TextView) mItemRoot.findViewById(R.id.item_coder);

        mItemName = (TextView) mItemRoot.findViewById(R.id.item_name);
        mItemCode = (TextView) mItemRoot.findViewById(item_code);

        mSkuName = (TextView) mSkuRoot.findViewById(R.id.item_name);

        //库位值
        mSkuCode = (TextView) mSkuRoot.findViewById(item_code);

        mQualityName = (TextView) mQualityRoot.findViewById(R.id.item_name);
        mQualityCode = (TextView) mQualityRoot.findViewById(item_code);

        mPostionName = (TextView) mSourceRoot.findViewById(R.id.item_name);

        //商品来源下的数量
        mPostionCount = (TextView) mSourceRoot.findViewById(R.id.item_count);
        mPostionCount.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        //仓位
        mPositionInput = (EditText) mSourceRoot.findViewById(R.id.position_input);

        mViewModel = new StorageViewModel(this);
        mViewModel.addPageListener(this);
        mViewModel.registerContext(this);


        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", 2 + "");
            BaseRequest<ResponseReason> request =
                    new BaseRequest<>(RequestUtils.STORAGE_REASON,
                            ResponseReason.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.unregisterContext();
    }

    @Override
    public void onScanResult(String result) {
        if (NetUtils.hasNet(this)) {

            if ("".equals(result)) {
                return;
            }

            result = result.trim();
            mViewModel.onScanResult(result);

        } else {
            ToastUtils.showToast(getResources().getString(R.string.net_error));
        }

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {
            if (mInfoRoot.getVisibility() == View.VISIBLE) {

                if ("".equals(mPostionCount.getText().toString().trim())) {
                    Toast.makeText(StorageActivity.this, getResources().getString(R.string.please_input_count), Toast.LENGTH_LONG).show();
                } else {
                    mViewModel.setCount(mPostionCount.getText().toString());
                    mViewModel.navNext();
                    requestDone();
                }
            }
        } else if (id == R.id.last) {
            mViewModel.navBefore();
        } else if (id == R.id.next) {

            if (StringUtils.isEmpty(mSkuCode.getText().toString().trim())) {
                Toast.makeText(StorageActivity.this, getResources().getString(R.string.no_kuwei_info), Toast.LENGTH_LONG).show();
                return;
            }

            if ("".equals(mPostionCount.getText().toString().trim())) {
                Toast.makeText(StorageActivity.this, getResources().getString(R.string.please_input_count), Toast.LENGTH_LONG).show();
                return;
            }

            mNew_kuwei.setText("");
            mViewModel.setCount(mPostionCount.getText().toString());
            mViewModel.navNext();
        }


        if (id == R.id.check) {
            if (!StringUtils.isEmpty(mScanContent.getText().toString().trim())) {
                if (mScanContent.hasFocus()) {
                    mViewModel.requestSku(mScanContent.getText().toString().trim());
                }
            } else {
                ToastUtils.showToast(getResources().getString(R.string.please_input_right_sku_code));
            }
        }
    }

    @Override
    public void onRequestStart() {
    }

    @Override
    public void onRequestStop() {
    }

    @Override
    public void onPageUpdate(StoragePage page) {
        displayPage(page);
    }

    @Override
    public void onError(String errorCode) {
    }

    @Override
    public void onFinish() {
    }

    private void displayPage(StoragePage page) {
        if (mViewModel.hasBefore()) {
            mBefore.setEnabled(true);
        } else {
            mBefore.setEnabled(false);
        }

        if (!mViewModel.hasNext() &&
                page.getPhase() != StoragePage.Phase.SKU) {
            mNext.setEnabled(false);
        } else {
//            mNext.setEnabled(false);
            mNext.setEnabled(true);
        }

        StoragePage.Phase phase = page.getPhase();
        if (phase == StoragePage.Phase.START) {
            displayStart(page);
        } else if (phase == StoragePage.Phase.SKU) {
            displaySku(page);//显示sku信息
        } else if (phase == StoragePage.Phase.POSITION) {
            displayPosition(page);//显示墙位position
        } else {
            displayCount(page);
        }
    }

    /**
     * 初始化的UI
     *
     * @param page
     */
    private void displayStart(StoragePage page) {
        mInfoRoot.setVisibility(View.GONE);
        mScanContent.requestFocus();

        mItemName.setText("品名：");
//        mItemCode.setText("SkuId：");

        mSkuName.setText("规格：");
//        mSkuCode.setText("库位：");

        mQualityName.setText("保质期：");
        mQualityCode.setText("有效期：");


        mPositionInput.setText("仓位：");
        mPostionCount.setText("");

        mScanContent.setText("");
    }

    /**
     * 输入sku信息后展示的信息
     *
     * @param page
     */
    private void displaySku(StoragePage page) {
        mInfoRoot.setVisibility(View.VISIBLE);

        InSkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());//设置扫描到的码
    }

    /**
     * 输入库位信息后展示的信息
     *
     * @param page
     */
    private void displayPosition(StoragePage page) {

        //是否有库位
        InSkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText(unit.getPosition());

        mPostionCount.requestFocus();
    }

    /**
     * 输入数量信息后展示的信息
     *
     * @param page
     */
    private void displayCount(StoragePage page) {
        InSkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPostionCount.clearFocus();
    }

    //显示商品属性
    private void displayUnit(InSkuModel unit) {
        mItemName.setText("品名：" + unit.getTitle());
        mItemCode.setText(unit.getSkuId() + "");
//        mItemCode.setText(unit.getBarcode());
        mSkuName.setText("规格：" + unit.getSpecification());

        mQualityName.setText("保质期：暂无");
        mQualityCode.setText("有效期：暂无");
        mCode69.setText("商品69码：" + unit.getSku69Id());
        mDanwei.setText(unit.saleUnitStr);

        if ("null".equals(unit.getCount() + "")) {
            mPostionCount.setText("");
//            mPostionCount.requestFocus();
        } else {
//            mPostionCount.requestFocus();
            mPostionCount.setText(unit.getCount() + "");
            mPostionCount.length();
        }

        //设置库位值
        mSkuCode.setText(unit.getKuwei());//空串或正式的值

        //扫描商品的skuid
        mSkuId = unit.getSkuId();

        String trim = mSkuCode.getText().toString().trim();
        if (StringUtils.isEmpty(trim)) {
            Toast.makeText(StorageActivity.this, getResources().getString(R.string.please_bangding_kuwei), Toast.LENGTH_LONG).show();
            mNew_kuwei.requestFocus();
        } else {
            mPostionCount.requestFocus();//扫完条码后数量获取焦点
        }

        if (StringUtils.isKongStr(unit.kucun)) {
            mCoder.setText("总库存:无");
        } else {
            mCoder.setText("总库存:" + unit.kucun);
        }

        if (StringUtils.isKongStr(unit.safeKucun)) {
            mSafeKucun.setText("安全库存：");
        } else {
            mSafeKucun.setText("安全库存：" + unit.safeKucun);
        }
    }


    //完成按钮
    private void requestDone() {
        try {

            Intent intent = new Intent(this, StorageConfirmActivity.class);
            List<StoragePage> pages = mViewModel.getOutgoPages();
            ArrayList<InSkuModel> modules = new ArrayList<>();
            for (int i = 0; i < pages.size(); i++) {

                StoragePage page = pages.get(i);
                InSkuModel model = page.getSkuModel();

                if (model != null && model.getPositionId() <= 0) {
                    ToastUtils.showToast(getResources().getString(R.string.has_no_kuwei));
                    return;
                }

                if (model != null && (page.getPhase() == StoragePage.Phase.SKU)) {
                    if (!"0".endsWith(pages.get(i).getSkuModel().getCount())) {
                        modules.add(pages.get(i).getSkuModel());
                    }
                }
            }

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("units", modules);
            bundle.putString("count", mPostionCount.getText().toString());
            bundle.putInt("reason", mReason);
            intent.putExtras(bundle);

            startActivity(intent);
        } catch (Exception e) {

        }
    }

    //获取sku后
    @Override
    public void onPageChanged(StoragePage outgoPage) {
        displayPage(outgoPage);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
    }

    @Override
    public void onResponse(ResponseReason response) {
        if (response != null && response.isSuccess()) {
            Log.e("test", response.toString());
            selectReason(response.target);
        } else {
            Toast.makeText(StorageActivity.this, "" + response.getErrorMsg(), Toast.LENGTH_LONG).show();
        }
    }

    private void selectReason(final List<Reason> list) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, android.R.style.Theme_Holo_Light_Dialog);
        builder.setTitle("选择入库原因");
        builder.setAdapter(new ReasonAdatper(this, list), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
//                mReason = list.get(which).opType;
                mReason = (int) list.get(which).id;
            }
        });
        builder.setOnCancelListener(this);
        builder.show();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Toast.makeText(this, "必须选择原因", Toast.LENGTH_SHORT).show();
        finish();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }


}
