package mobile.freshtime.com.freshtime.utils;

import com.google.gson.Gson;

/**
 * Created by Administrator on 2016/11/23.
 */

public class JsonUtils {

    public static final String TAG = "JsonUtils";

    private static JsonUtils instance = null;

    public static JsonUtils getInstance() {
        if (instance == null) {
            synchronized (JsonUtils.class) {
                if (instance == null) {
                    instance = new JsonUtils();
                }
            }
        }
        return instance;
    }

    private Gson mGson;

    public Gson getGson(){
        if (mGson == null) {
            synchronized (Gson.class) {
                if (mGson == null) {
                    mGson = new Gson();
                }
            }
        }
        return mGson;
    }

}
