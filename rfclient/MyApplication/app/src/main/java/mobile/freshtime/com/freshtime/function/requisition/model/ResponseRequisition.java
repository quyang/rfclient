package mobile.freshtime.com.freshtime.function.requisition.model;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orchid on 16/10/12.
 */

public class ResponseRequisition extends BaseResponse {

    private RequisitionModel target;

    public RequisitionModel getTarget() {
        return target;
    }

    public void setTarget(RequisitionModel target) {
        this.target = target;
    }
}
