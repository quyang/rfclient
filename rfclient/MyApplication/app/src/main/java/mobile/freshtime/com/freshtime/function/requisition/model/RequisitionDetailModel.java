package mobile.freshtime.com.freshtime.function.requisition.model;

/**
 * Created by orchid on 16/10/12.
 */

public class RequisitionDetailModel {

    public long agencyId;
    public String auditTime;
    public int auditor;
    public String createTime;
    public String details;
    public long dr;
    public long id;
    public String opTime;
    public int operate;
    public String outgoingAuditor;
    public int receiver;
    public String receiverNmae;
    public String sauditTime;
    public String sopTime;
    public long stallsId;
    public String stallsName;
    public int statu;
    public String ts;
}
