package mobile.freshtime.com.freshtime.function.handover;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Constants;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverBox;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverDetail;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverDistribution;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverTask;
import mobile.freshtime.com.freshtime.common.model.delivery.ResponseBox;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * Created by orchid on 16/10/15.
 */

public class HandoverCheckActivity extends BaseActivity implements Response.ErrorListener, Response.Listener<ResponseBox>, View.OnClickListener {

    private DeliverTask mDeliverTask;
    private EditText mBoxInput;
    private TextView mDeliverman;
    private TextView mProceedingTask;
    private TextView mDeliverSheet;
    private TextView mDeliverBox;
    private LinearLayout mContainer;

    private TextView mDone;

    private HashMap<Long, Boolean> mScannedBox;

    private Response.Listener<BaseResponse> mSubmit =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        Toast.makeText(HandoverCheckActivity.this, "交接任务成功", Toast.LENGTH_SHORT).show();
                        EventBus.getDefault().post(HandoverActivity.class);
                        finish();
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handover_check);
        mDeliverTask = getIntent().getParcelableExtra("task");


        findViewById(R.id.back).setOnClickListener(this);

        if (mDeliverTask == null) finish();

        ArrayList<DeliverBox> box = getIntent().getParcelableArrayListExtra("box");
        ArrayList<DeliverDetail> detail = getIntent().getParcelableArrayListExtra("detail");
        ArrayList<DeliverDistribution> distribution = getIntent().getParcelableArrayListExtra("distribution");
        mDeliverTask.boxes = box;
        mDeliverTask.details = detail;
        mDeliverTask.distributions = distribution;

        mScannedBox = new HashMap<>();
        for (int i = 0; i < box.size(); i++) {

            if (box.get(i) != null) {
                mScannedBox.put(box.get(i).id, false);
            } else {
//                Toast.makeText(HandoverCheckActivity.this,"HandoverCheckActivity box null",Toast.LENGTH_LONG).show();
            }
        }

        initView();
    }

    private void initView() {
        TextView title = (TextView) findViewById(R.id.item_tag);
        title.setText("配送箱:");
        mBoxInput = (EditText) findViewById(R.id.item_input);
        mDeliverman = (TextView) findViewById(R.id.delivery_name);
        mProceedingTask = (TextView) findViewById(R.id.task_count);
        mDeliverSheet = (TextView) findViewById(R.id.agency);
        mDeliverBox = (TextView) findViewById(R.id.area);

        mDeliverman.setText("配送员:" + mDeliverTask.operator);
        mDeliverSheet.setText("送货单:" + mDeliverTask.id);
        mDeliverBox.setText("配送箱:" + mDeliverTask.boxes.size() + "个");

        mDone = (TextView) findViewById(R.id.done);
//        mDone.setEnabled(false);
        mDone.setOnClickListener(this);
//        mProceedingTask.setText("进行中的任务" + mDeliverTask.);
        findViewById(R.id.back).setOnClickListener(this);
        initBoxContainer();
    }

    /**
     * 初始化UI,绘制配送箱信息里面全部的箱子信息
     */
    private void initBoxContainer() {
        int marginHorizental = (int) getResources().getDimension(R.dimen.common_subtitle_margin);
        int marginTop = (int) getResources().getDimension(R.dimen.common_margin);
        mContainer = (LinearLayout) findViewById(R.id.delivery_container);
        for (int i = 0; i < mDeliverTask.boxes.size(); i++) {
            if (mDeliverTask.boxes.get(i) != null) {
                DeliverBox deliverBox = mDeliverTask.boxes.get(i);
                TextView text = new TextView(this);
                text.setTag(deliverBox.id);
                text.setTextAppearance(this, R.style.Subtitle);
                text.setText("配送箱:" + deliverBox.barcode);
                LinearLayout.LayoutParams param
                        = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT);
                param.leftMargin = marginHorizental;
                param.rightMargin = marginHorizental;
                param.topMargin = marginTop;
                mContainer.addView(text, param);
            }
        }
    }

    @Override
    public void onScanResult(String result) {
        mBoxInput.setText(result);
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(result, "1"));
            BaseRequest<ResponseBox> request =
                    new BaseRequest<>(RequestUtils.DISTRI_BOX,
                            ResponseBox.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(ResponseBox response) {
        if (response != null && response.success) {
            if (response.target != null) {
                long id = response.target.id;
                if (mScannedBox.containsKey(id)) {
                    boolean scanned = mScannedBox.get(id);
                    if (!scanned) {
                        TextView scan = (TextView) mContainer.findViewWithTag(id);
                        if (scan != null) {
                            String orgin = scan.getText().toString();
                            scan.setText(orgin + Constants.MATCH);
                        }
                        mScannedBox.put(id, true);
                        watchScan();
                    }
                }
            }
        } else {
            ToastUtils.showToast("" + response.message);
        }
    }

    /**
     * 判断是否全部扫描过
     */
    private void watchScan() {
        Iterator<Long> iter = mScannedBox.keySet().iterator();
        boolean allScanned = true;
        while (iter.hasNext()) {
            long key = iter.next();
            boolean scanned = mScannedBox.get(key);
            allScanned &= scanned;
        }
        if (allScanned) {
            mDone.setEnabled(true);
        }
    }

    private void submitDeliver() {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            JSONObject data = new JSONObject();
            data.put("id", mDeliverTask.id);
            param.put("data", data);
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.HANDOVER_DELIVER,
                            BaseResponse.class,
                            param,
                            mSubmit, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.done) {
            submitDeliver();
        } else if (id == R.id.done) {
            finish();
        } else if (id == R.id.back) {
            finish();
        }
    }
}
