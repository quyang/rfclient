package mobile.freshtime.com.freshtime.function.stock;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobile.freshtime.com.freshtime.FreshTimeApplication;
import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ProgressDialogUtils;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.function.stock.jiekou.DetailsInfoListener;
import mobile.freshtime.com.freshtime.function.stock.module.InSkuModel;
import mobile.freshtime.com.freshtime.function.stock.module.MyTwoBaseAdapter;
import mobile.freshtime.com.freshtime.function.stock.module.SmallInfo;
import mobile.freshtime.com.freshtime.function.stock.module.StocktakingInProgressInfo;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPanClass;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPanForResult;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPanInfo;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPanListActivityPresenter;
import mobile.freshtime.com.freshtime.function.stock.module.TwoStatusInfo;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * 盘点任务列表(二盘)  任务详情
 */
public class TwoPanListActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, DetailsInfoListener {


    private boolean isOpen = false;

    @Bind(R.id.last)
    TextView mLast;
    @Bind(R.id.queren)
    TextView mQueren;
    @Bind(R.id.activity_pan_list)
    LinearLayout mActivityPanList;
    @Bind(R.id.expand_list)
    ListView mExpandList;

    private ArrayList<String> groupList = new ArrayList<>();
    private ViewGroup mBody;
    private ArrayList<InSkuModel> mSkuModels;
    private int Id;
    private String mTime;
    private TwoPanListActivityPresenter mPresenter;
    private int mTaskId;
    private MyTwoBaseAdapter mMyTwoBaseAdapter;
    private StocktakingInProgressInfo.TargetBean.ItemsBean mItem;
    private List<TwoPanInfo.TargetBean.DetailsBean> mDetails;
    private Dialog mProgressDialog;
    private Dialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan_list);
        ButterKnife.bind(this);
        EventBus.getDefault().register(this);

        Bundle bundle = getIntent().getExtras();
        Id = bundle.getInt("id");//索引


        mLast.setText("提交审核");
        mQueren.setText("提交二盘明细");

        mItem = (StocktakingInProgressInfo.TargetBean.ItemsBean) bundle.getSerializable("item");

        mLast.setClickable(true);
        mQueren.setClickable(true);
        mLast.setOnClickListener(this);
        mQueren.setOnClickListener(this);

        ViewGroup header = (ViewGroup) findViewById(R.id.header);
        TextView left = (TextView) header.findViewById(R.id.sku_info);
        TextView mid = (TextView) header.findViewById(R.id.sku_code);
        TextView right = (TextView) header.findViewById(R.id.item_detail);

        left.setText("任务id: " + mItem.getId());
        mid.setText("盘   点");

        header.setOnClickListener(this);

        mBody = (ViewGroup) findViewById(R.id.item_info_root);
        ViewGroup.LayoutParams params = mBody.getLayoutParams();
        params.height = 0;
        mBody.setLayoutParams(params);

        TextView fa = (TextView) mBody.findViewById(R.id.item_name);
        TextView time = (TextView) mBody.findViewById(R.id.item_count);
        TextView quyu = (TextView) mBody.findViewById(R.id.item_code);

        fa.setText("发起人: " + mItem.getOperate());
        time.setText("操作时间: " + mItem.getOpTime().replace("T", " "));
        quyu.setText("区域: " + mItem.getAgencyId());


//
        mPresenter = new TwoPanListActivityPresenter(this);
        mPresenter.setDetailsInfoListener(this);
        if (mItem.getId() != 0) {
            mPresenter.getInfo(mItem.getId());
        }
    }


    private Dialog mMMDialog;

    private Dialog mMMDialog2;

    @Override
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.header:
                changeBody();
                break;
            case R.id.queren://提交明细

                mMMDialog2 = new Dialog(TwoPanListActivity.this, R.style.MyDialog);
                View view2 = getLayoutInflater().inflate(R.layout.cancel, null);
                TextView name2 = (TextView) view2.findViewById(R.id.name);
                name2.setText(getResources().getString(R.string.commit_details));
                Button mNo2 = (Button) view2.findViewById(R.id.no);
                Button mYes2 = (Button) view2.findViewById(R.id.yes);

                mMMDialog2.setCancelable(false);
                mMMDialog2.setContentView(view2);

                //设置弹窗宽高
                mMMDialog2.getWindow().setLayout(UiUtils.getScreenHeight(TwoPanListActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);
                mMMDialog2.show();
                mYes2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMMDialog2.dismiss();
                        mProgressDialog = ProgressDialogUtils.getProgressDialog(TwoPanListActivity.this);
                        mProgressDialog.show();
                        submitTwoPanDetails();

                    }
                });

                mNo2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMMDialog2.dismiss();
                    }
                });


                break;
            case R.id.last://提交改变状态

                mMMDialog = new Dialog(TwoPanListActivity.this, R.style.MyDialog);
                View view = getLayoutInflater().inflate(R.layout.cancel, null);
                TextView name = (TextView) view.findViewById(R.id.name);
                name.setText(getResources().getString(R.string.commit_shenghe));
                final Button mNo = (Button) view.findViewById(R.id.no);
                Button mYes = (Button) view.findViewById(R.id.yes);

                mMMDialog.setCancelable(false);
                mMMDialog.setContentView(view);

                //设置弹窗宽高
                mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(TwoPanListActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

                mMMDialog.show();

                mYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMMDialog.dismiss();
                        mDialog = ProgressDialogUtils.getProgressDialog(TwoPanListActivity.this);
                        mDialog.show();
                        shenghe();
                    }
                });


                mNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMMDialog.dismiss();
                    }
                });

                break;
        }
    }

    MediaType STRING_TYPE = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

    //提交二盘任务(明细)
    private void submitTwoPanDetails() {

        if (mDetails == null) {
            return;
        }

        JSONObject object = new JSONObject();
        try {
            JSONArray array = new JSONArray();


            for (int i = 0; i < mDetails.size(); i++) {

                TwoPanInfo.TargetBean.DetailsBean bean = mDetails.get(i);

                JSONObject params = new JSONObject();
                //skuId\/positionId\/stocktakingId\/id
                params.put("id", bean.getId());//信息id
                params.put("skuId", bean.getSkuId());//skuid
                params.put("positionId", bean.getPositionId());
                params.put("stocktakingId", bean.getStocktakingId());

                if (!StringUtils.isEmpty(bean.getSecondNo() + "")) {
                    params.put("secondNo", bean.getSecondNo());
                }

                array.put(params);
            }


            object.put("token", Login.getInstance().getToken());
            object.put("data", array);

            String encode = null;
            try {
                encode = URLEncoder.encode(object.toString(), "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                ToastUtils.showToast(e.getMessage());
            }

            OkHttpClient okHttpClient = FreshTimeApplication.getOk();


            RequestBody body = RequestBody.create(STRING_TYPE, "request=" + encode);

            final Request request = new Request.Builder()
                    .post(body)
                    .url(RequestUtils.SUBMIT_STOCK)
                    .build();

            if (!NetUtils.hasNet(TwoPanListActivity.this)) {
                mProgressDialog.dismiss();
                ToastUtils.showToast(getResources().getString(R.string.net_error));
                return;
            }

            okHttpClient.newCall(request).enqueue(new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    mProgressDialog.dismiss();
                    ToastUtils.showToast(getResources().getString(R.string.commit_fail));
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    String json = response.body().string();
                    System.out.println("TwoPanActivity.submitTwoPanDetails====" + json);
                    if (!StringUtils.isEmpty(json)) {
                        try {
                            JSONObject object = new JSONObject(json);
                            if (object.getBoolean("success")) {
                                ToastUtils.showToast(getResources().getString(R.string.commit_success));

                            } else {
                                ToastUtils.showToast("" + object.getString("message"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            ToastUtils.showToast(e.getMessage());
                        }
                    } else {
                        ToastUtils.showToast("沒有更多数据");
                    }

                    mProgressDialog.dismiss();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            ToastUtils.showToast(e.getMessage());
        }
    }

    //审核 二盘
    private void shenghe() {

        JSONObject object = new JSONObject();

        if (mItem.getId() != 0) {
            try {
                object.put("token", Login.getInstance().getToken());
                object.put("data", mItem.getId() + "");//任务id

                GetProtocol protocol = new GetProtocol(RequestUtils.AUDIT + "?request=" + object);
                protocol.get();

                System.out.println("TwoPanListActivity.shenghe" + "=" + object);

                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                    @Override
                    public void onSuccess(String json) {

                        try {
                            JSONObject object1 = new JSONObject(json);

                            if (object1.getBoolean("success")) {
                                ToastUtils.showToast(getResources().getString(R.string.commit_success));

                                TwoStatusInfo info = new TwoStatusInfo();
                                EventBus.getDefault().post(info);

                                finish();

                            } else {
                                ToastUtils.showToast(getResources().getString(R.string.shenghe_fail));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            ToastUtils.showToast(e.getMessage());
                        }

                        mDialog.dismiss();
                    }

                    @Override
                    public void onFail() {
                        mDialog.dismiss();
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


    private void changeBody() {
        ValueAnimator animator = null;

        if (!isOpen) {
            //创建出ValueAnimator的对象
            animator = ValueAnimator.ofInt(0, getMaxHeight());
            isOpen = true;

        } else {
            animator = ValueAnimator.ofInt(getMaxHeight(), 0);
            isOpen = false;
        }

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int tempHeight = (Integer) animation.getAnimatedValue();//得到中间值
                System.out.println("PanListActivity.onAnimationUpdate=" + tempHeight);
                ViewGroup.LayoutParams layoutParams2 = mBody
                        .getLayoutParams();
                layoutParams2.height = tempHeight;
                mBody.setLayoutParams(layoutParams2);
            }
        });
        animator.setDuration(200);
        animator.start();
    }

    private int getMaxHeight() {

        List<Integer> screenHeight = UiUtils.getScreenHeight(TwoPanListActivity.this);
        Integer screenWidth = screenHeight.get(0);
        int childWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(screenWidth, View.MeasureSpec.AT_MOST);
        mBody.measure(childWidthMeasureSpec, 0);

        return mBody.getMeasuredHeight();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        TwoPanInfo.TargetBean.DetailsBean detailsBean = mDetails.get(position);

        Intent intent = new Intent(this, TwoPanActivity.class);
        SmallInfo info = new SmallInfo();

        info.itemName = detailsBean.getSkuName();
        info.guige = (String) detailsBean.getSaleUnit() + "";
        info.code69 = detailsBean.getPositionBarcode() + "";
        info.kuwei = detailsBean.getPositionCode() + "";
        info.jigou = detailsBean.getPositionId() + "";
        info.index = position;
        info.firstNo = "" + detailsBean.getFirstNo();
        info.stocktakingId = detailsBean.getStocktakingId();
        info.id = detailsBean.getId();
        info.skuId = detailsBean.getSkuId();
        info.pid = detailsBean.getPositionId();
        info.secondNo = (String) detailsBean.getSecondNo();

        intent.putExtra("info", info);
        startActivity(intent);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void Listener(Object info) {
        TwoPanInfo detail = (TwoPanInfo) info;

        mDetails = detail.getTarget().getDetails();

        mMyTwoBaseAdapter = new MyTwoBaseAdapter(mDetails, this);
        mExpandList.setAdapter(mMyTwoBaseAdapter);
        mExpandList.setOnItemClickListener(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(TwoPanClass info) {
        int index = info.index;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(TwoPanForResult info) {//二盘数
        int index = info.index;
        String count = info.mStringCount;

        TwoPanInfo.TargetBean.DetailsBean bean = mDetails.get(index);
        bean.setSecondNo(count);//  count;
        mMyTwoBaseAdapter.notifyDataSetChanged();

    }


}

