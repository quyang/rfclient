package mobile.freshtime.com.freshtime.common.model.delivery;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by orchid on 16/10/15.
 */

public class DeliverTask implements Parcelable {

    public long agencyId;
    public long areaId;
    public ArrayList<DeliverBox> boxes;
    public String createTime;
    public ArrayList<DeliverDetail> details;
    public ArrayList<DeliverDistribution> distributions;
    public long dr;
    public String finishTime;
    public long id;
    public long operator;
    public int statu;
    public String ts;
    public long waveId;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(agencyId);
        dest.writeLong(areaId);
//        DeliverBox[] box = new DeliverBox[boxes.size()];
//        for (int i = 0; i < boxes.size(); i++) {
//            box[i] = boxes.get(i);
//        }
//        dest.writeParcelableArray(box, flags);
        dest.writeString(createTime);

//        DeliverDetail[] detail = new DeliverDetail[details.size()];
//        for (int i = 0; i < details.size(); i++) {
//            detail[i] = details.get(i);
//        }
//        dest.writeParcelableArray(detail, flags);

//        DeliverDistribution[] distribution = new DeliverDistribution[details.size()];
//        for (int i = 0; i < distributions.size(); i++) {
//            distribution[i] = distributions.get(i);
//        }
//        dest.writeParcelableArray(distribution, flags);

        dest.writeLong(dr);
        dest.writeString(finishTime);
        dest.writeLong(id);
        dest.writeLong(operator);
        dest.writeInt(statu);
        dest.writeString(ts);
        dest.writeLong(waveId);
    }

    public static final Parcelable.Creator<DeliverTask> CREATOR =
            new Parcelable.Creator<DeliverTask>() {

                @Override
                public DeliverTask createFromParcel(Parcel source) {
                    DeliverTask model = new DeliverTask();
                    model.agencyId = source.readLong();
                    model.areaId = source.readLong();

//                    DeliverBox[] list = source.createTypedArray(DeliverBox.CREATOR);
//                    model.boxes = new ArrayList<>(list.length);
//                    for (int i = 0; i < list.length; i++) {
//                        model.boxes.add(i, list[i]);
//                    }

                    model.createTime = source.readString();

//                    DeliverDetail[] listDetail = source.createTypedArray(DeliverDetail.CREATOR);
//                    model.details = new ArrayList<>(listDetail.length);
//                    for (int i = 0; i < listDetail.length; i++) {
//                        model.details.add(i, listDetail[i]);
//                    }

//                    DeliverDistribution[] listDistribution = source.createTypedArray(DeliverDistribution.CREATOR);
//                    model.distributions = new ArrayList<>(listDistribution.length);
//                    for (int i = 0; i < listDistribution.length; i++) {
//                        model.distributions.add(i, listDistribution[i]);
//                    }

                    model.dr = source.readLong();
                    model.finishTime = source.readString();
                    model.id = source.readLong();
                    model.operator = source.readLong();
                    model.statu = source.readInt();
                    model.ts = source.readString();
                    model.waveId = source.readLong();
                    return model;
                }

                @Override
                public DeliverTask[] newArray(int size) {
                    return new DeliverTask[size];
                }
            };
}
