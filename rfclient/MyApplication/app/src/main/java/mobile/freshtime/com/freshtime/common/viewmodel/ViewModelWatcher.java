package mobile.freshtime.com.freshtime.common.viewmodel;

/**
 * Created by orchid on 16/9/21.
 * 监听请求回调和页面业务流程往下走的回调
 */

public interface ViewModelWatcher<Page> {

    void onRequestStart();

    void onRequestStop();

    void onPageUpdate(Page page);

    void onError(String errorCode);

    void onFinish();
}
