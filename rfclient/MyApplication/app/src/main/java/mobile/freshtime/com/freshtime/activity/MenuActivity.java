package mobile.freshtime.com.freshtime.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.adapter.ItemClick;
import mobile.freshtime.com.freshtime.activity.adapter.MenuAdapter;
import mobile.freshtime.com.freshtime.activity.adapter.MenuItemDecoration;
import mobile.freshtime.com.freshtime.function.guanli.GuanLiActivity;
import mobile.freshtime.com.freshtime.function.handover.HandoverActivity;
import mobile.freshtime.com.freshtime.function.kuweitiaozheng.KuweiTiaoZhengActivity;
import mobile.freshtime.com.freshtime.function.outgoing.OutgoActivity;
import mobile.freshtime.com.freshtime.function.pack.PackListActivity;
import mobile.freshtime.com.freshtime.function.pick.PickTaskListActivity;
import mobile.freshtime.com.freshtime.function.pickwall.PickBasketActivity;
import mobile.freshtime.com.freshtime.function.restore.RestoreListActivity;
import mobile.freshtime.com.freshtime.function.stock.FirstStockActivity;
import mobile.freshtime.com.freshtime.function.storage.StorageActivity;
import mobile.freshtime.com.freshtime.function.tradeshelf.TradeShelfActivity;
import mobile.freshtime.com.freshtime.function.transfer.TransferActivity;
import mobile.freshtime.com.freshtime.login.Login;

/**
 * new
 * Created by orchid on 16/9/26.
 */

public class MenuActivity extends BaseActivity implements ItemClick.IClick {

    private RecyclerView mRecyclerView;

    private long mLastTime = 0;

    private static final String[] FUNCTIONS = new String[]{"拣货", "分拣墙", "交接", "归还",
            "出库", "入库", "移库", "盘点", "上架", "验货", "库位管理",
            "配货台","库存调整"};

//    private static final String[] FUNCTIONS = new String[]{"出库", "入库", "移库", "盘点", "上架", "库位管理","库存调整"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        TextView operator = (TextView) findViewById(R.id.operator);
        operator.setText("已登录用户：" + Login.getInstance().getUserId());

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);
        mRecyclerView.setLayoutManager(gridLayoutManager);
        mRecyclerView.addItemDecoration(new MenuItemDecoration(8));
        mRecyclerView.setAdapter(new MenuAdapter(FUNCTIONS, this));

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onScanResult(String result) {
    }

    @Override
    public void onItemClick(int position, View v) {
        switch (position) {
            case 0://拣货
                startActivity(new Intent(this, PickTaskListActivity.class));
                break;
            case 1://拣货墙
                startActivity(new Intent(this, PickBasketActivity.class));
                break;
            case 2://交接
                startActivity(new Intent(this, HandoverActivity.class));
                break;
            case 3://归还
                startActivity(new Intent(this, RestoreListActivity.class));
                break;
            case 4:
                startActivity(new Intent(this, OutgoActivity.class));
                break;
            case 5:
                startActivity(new Intent(this, StorageActivity.class));
                break;
            case 6:
                startActivity(new Intent(this, TransferActivity.class));
                break;
            case 7:
                startActivity(new Intent(this, FirstStockActivity.class));
                break;
            case 8:
                startActivity(new Intent(this, mobile.freshtime.com.freshtime.function.shangjia.storage.StorageActivity .class));
                break;
            case 9:
                startActivity(new Intent(this, TradeShelfActivity.class));
                break;
            case 10:
                startActivity(new Intent(this, GuanLiActivity.class));
                break;
            case 11:
                startActivity(new Intent(this, PackListActivity.class));
                break;
            case 12:
                startActivity(new Intent(this, KuweiTiaoZhengActivity.class));
                break;
        }
    }

//    @Override
//    public void onItemClick(int position, View v) {
//        switch (position) {
//            case 0://出库
//                startActivity(new Intent(this, OutgoActivity.class));
//                break;
//            case 1://入库
//                startActivity(new Intent(this, StorageActivity.class));
//                break;
//            case 2://移库
//                startActivity(new Intent(this, TransferActivity.class));
//                break;
//            case 3://盘点
//                startActivity(new Intent(this, FirstStockActivity.class));
//                break;
//            case 4://上架
//                startActivity(new Intent(this, mobile.freshtime.com.freshtime.function.shangjia.storage.StorageActivity.class));
//                break;
//            case 5://库位管理
//                startActivity(new Intent(this, GuanLiActivity.class));
//                break;
//            case 6://库位调整
//                startActivity(new Intent(this, KuweiTiaoZhengActivity.class));
//                break;
//        }
//    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)// 主要是对这个函数的复写
    {
        if ((keyCode == KeyEvent.KEYCODE_BACK)
                && (event.getAction() == KeyEvent.ACTION_DOWN)) {
            if (System.currentTimeMillis() - mLastTime > 2000) // 2s内再次选择back键有效
            {
                System.out.println(Toast.LENGTH_LONG);
                Toast.makeText(this, "请再按一次返回退出", Toast.LENGTH_LONG).show();
                mLastTime = System.currentTimeMillis();
            } else {
                finish();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
