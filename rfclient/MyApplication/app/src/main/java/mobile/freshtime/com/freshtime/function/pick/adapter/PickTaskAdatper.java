package mobile.freshtime.com.freshtime.function.pick.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.function.pick.model.Task;

/**
 * Created by orchid on 16/10/6.
 */

public class PickTaskAdatper extends BaseAdapter {

    private static final String LACK = "缺货";
    private static final String NORMAL = "正常";

    private List<Task> mTasks;
    private Context mContext;
    private ArrayList<String> codeList;

    public PickTaskAdatper(Context context, List<Task> tasks, ArrayList<String> codeList) {
        this.mContext = context;
        this.mTasks = tasks;
        this.codeList = codeList;
        System.out.println("PickTaskAdatper.PickTaskAdatpe=" + codeList.size());
    }

    @Override
    public int getCount() {
        return mTasks.size();
    }

    @Override
    public Object getItem(int position) {
        return mTasks.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.listitem_pick_task, null);
            viewHolder.info = (TextView) convertView.findViewById(R.id.sku_info);
            //code
            viewHolder.code = (TextView) convertView.findViewById(R.id.sku_code);
            viewHolder.quantity = (TextView) convertView.findViewById(R.id.quantity);
            viewHolder.togo = (TextView) convertView.findViewById(R.id.quantity_togo);
            viewHolder.lack = (TextView) convertView.findViewById(R.id.lack);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Task task = mTasks.get(position);
        viewHolder.info.setText(task.title);
        viewHolder.code.setText(codeList.get(position));
//        viewHolder.code.setText(task.skuBarcode + "");
        viewHolder.quantity.setText(task.orderNo + "");
        viewHolder.togo.setText(task.pickedNo + "");
        if (task.oos == null || "1".equals(task.oos)) {
            viewHolder.lack.setText(LACK);
        } else {
            viewHolder.lack.setText(NORMAL);
        }
        return convertView;
    }

    public void updataData(List<Task> tasks) {
        this.mTasks = tasks;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        public TextView info, code, quantity, togo, lack;
    }
}
