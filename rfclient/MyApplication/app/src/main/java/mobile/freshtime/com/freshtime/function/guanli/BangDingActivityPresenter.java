package mobile.freshtime.com.freshtime.function.guanli;

import android.app.Dialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.function.search.PositionBarcodeInfo;
import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.protocol.PostProtocol;
import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/11/14.
 */
public class BangDingActivityPresenter {


    private ViewInterface mViewInterface;
    private BangDingActivity mBangDingActivity;

    public BangDingActivityPresenter(ViewInterface activity) {
        mViewInterface = activity;
        mBangDingActivity = (BangDingActivity) mViewInterface;
    }

    public void load(String result) {
        FormBody body = new FormBody.Builder()
                .add("skuCode", result)
                .build();
        PostProtocol protocol = new PostProtocol(body, RequestUtils.SKU);
        protocol.post();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                mViewInterface.success(json);
            }

            @Override
            public void onFail() {
                mViewInterface.fail();
            }
        });

    }

    //绑定新的库位
    public void bindNewCode(String input) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", input);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    if (!StringUtils.isEmpty(json)) {
                        Gson gson = new Gson();
                        final PositionBarcodeInfo positionBarcodeInfo = gson.fromJson(json, PositionBarcodeInfo.class);
                        if (positionBarcodeInfo.isSuccess()) {

                            {

                                final Dialog mDialog = new Dialog(mBangDingActivity, R.style.MyDialog);
                                View view = mBangDingActivity.getLayoutInflater().inflate(R.layout.cancel, null);
                                TextView name = (TextView) view.findViewById(R.id.name);
                                name.setText("确定要绑定库位吗?");
                                Button no = (Button) view.findViewById(R.id.no);
                                Button yes = (Button) view.findViewById(R.id.yes);

                                mDialog.setCancelable(false);
                                mDialog.setContentView(view);

                                //后面给确定和取消按钮设置监听,onclicklistener;
                                no.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        mDialog.dismiss();
                                    }
                                });

                                yes.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        bind(positionBarcodeInfo.getTarget().getId() + "", positionBarcodeInfo.getTarget().getBarcode(),mDialog);
                                    }
                                });

                                //设置弹窗宽高
                                mDialog.getWindow().setLayout(UiUtils.getScreenHeight(mBangDingActivity).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

                                mDialog.show();
                            }

                        } else {

                        }
                    }
                }

                @Override
                public void onFail() {

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void bind(String pid, final String barcode, final Dialog mDialog) {

        if (StringUtils.isEmpty(mBangDingActivity.mView4.getText().toString().trim())) {
            ToastUtils.showToast("请先扫描商品");
            return;
        }

        String url = RequestUtils.ADD_CODE + "?request={token:'" + Login.getInstance().getToken() + "',data:{skuId:" + mBangDingActivity.mView4.getText().toString().trim() + ",positionId:" + pid + "}}";

        GetProtocol protocol = new GetProtocol(url);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                if (!StringUtils.isEmpty(json)) {
                    try {
                        JSONObject object = new JSONObject(json);

                        if (object.getBoolean("success")) {
                            ToastUtils.showToast("绑定成功");
                            mBangDingActivity.mEtNew.setText(barcode);
                            mDialog.dismiss();
                            mBangDingActivity.bangOk();
                        } else {
                            ToastUtils.showToast("该商品已绑定过");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFail() {
            }
        });
    }


}
