package mobile.freshtime.com.freshtime.function.storage.model;

/**
 * Created by Administrator on 2016/11/7.
 */

public class PositionBean {

    /**
     * message : null
     * return_code : 0
     * success : true
     * target : 40039
     */

    private Object message;
    private int return_code;
    private boolean success;
    private int target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }
}
