package mobile.freshtime.com.freshtime.function.stock.presenter;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.function.stock.TwoPanActivity;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.PostProtocol;
import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/11/24.
 */
public class TwoPanActivityPresenter {

    private ViewInterface mViewInterface;
    private TwoPanActivity mTwoPanActivity;

    public TwoPanActivityPresenter(ViewInterface viewInterface) {
        mViewInterface = viewInterface;
        mTwoPanActivity = (TwoPanActivity) mViewInterface;
    }

    public void onScanResult(String input) {

        //商品条码
        if (mTwoPanActivity.getItemInputOne().hasFocus()) {
            requestSku(input);
        }

        //库位条码
        if (mTwoPanActivity.getItemInputTwo().hasFocus()) {

        }


    }

    /**
     * 获取sku信息
     *
     * @param code
     */
    private void requestSku(String code) {

        FormBody body = new FormBody.Builder()
                .add("skuCode", code)
                .build();
        PostProtocol protocol = new PostProtocol(body, RequestUtils.SKU);
        protocol.post();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {


                if (json == null || "null".equals(json) || "".equals(json)) {
                    return;
                }

                try {
                    JSONObject object = new JSONObject(json);
                    boolean success = object.getBoolean("success");
                    if (success) {
                        mViewInterface.success(json);
                    } else {
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail() {
            }
        });
    }


    //获取一盘明细
//    public void getMingxi(String id) {
//
//        if (NetUtils.hasNet(mTwoPanActivity)) {
//
//            JSONObject object = new JSONObject();
//            try {
//                object.put("token", Login.getInstance().getToken());
//                object.put("data", id);
//
//                GetProtocol protocol = new GetProtocol(RequestUtils.GET_STOCK + "?request=" + object);
//                System.out.println("TwoPanActivityPresenter.getMingxi=" + RequestUtils.GET_STOCK + "?request=" + object);
//                protocol.get();
//                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
//                    @Override
//                    public void onSuccess(String json) {
//                        if (StringUtils.isEmpty(json)) {
//                            return;
//                        }
//
//                        System.out.println("TwoPanActivityPresenter.onSuccess=" + json);
//
//                        Gson gson = new Gson();
//                        GetStocktakingInfo stocktakingInfo = gson.fromJson(json, GetStocktakingInfo.class);
//                        if (stocktakingInfo.isSuccess()) {
//                            ToastUtils.showToast("获取到了一盘明细");
//                        } else {
//                            ToastUtils.showToast("获取盘点明细失败");
//                        }
//                    }
//
//                    @Override
//                    public void onFail() {
//                    }
//                });
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//
//
//    }

}
