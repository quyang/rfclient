package mobile.freshtime.com.freshtime.function.stock.module;

import java.util.List;

/**
 * Created by Administrator on 2016/11/24.
 */
public class GetStocktakingInfo {

    /**
     * message : null
     * return_code : 0
     * success : true
     * target : {"agencyId":1,"auditTime":null,"auditor":null,"details":[],"dr":0,"id":13,"opTime":"2016-11-24T12:26:51","operate":55,"statu":1,"statuOld":null,"ts":"2016-11-24T20:40:54","type":null}
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * auditTime : null
     * auditor : null
     * details : []
     * dr : 0
     * id : 13
     * opTime : 2016-11-24T12:26:51
     * operate : 55
     * statu : 1
     * statuOld : null
     * ts : 2016-11-24T20:40:54
     * type : null
     */

    private TargetBean target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public TargetBean getTarget() {
        return target;
    }

    public void setTarget(TargetBean target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private Object auditTime;
        private Object auditor;
        private int dr;
        private int id;
        private String opTime;
        private int operate;
        private int statu;
        private Object statuOld;
        private String ts;
        private Object type;
        private List<Object> details;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public Object getAuditTime() {
            return auditTime;
        }

        public void setAuditTime(Object auditTime) {
            this.auditTime = auditTime;
        }

        public Object getAuditor() {
            return auditor;
        }

        public void setAuditor(Object auditor) {
            this.auditor = auditor;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOpTime() {
            return opTime;
        }

        public void setOpTime(String opTime) {
            this.opTime = opTime;
        }

        public int getOperate() {
            return operate;
        }

        public void setOperate(int operate) {
            this.operate = operate;
        }

        public int getStatu() {
            return statu;
        }

        public void setStatu(int statu) {
            this.statu = statu;
        }

        public Object getStatuOld() {
            return statuOld;
        }

        public void setStatuOld(Object statuOld) {
            this.statuOld = statuOld;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

        public List<?> getDetails() {
            return details;
        }

        public void setDetails(List<Object> details) {
            this.details = details;
        }
    }
}
