package mobile.freshtime.com.freshtime.function.search;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.protocol.PostProtocol;
import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/10/29.
 */
public class SearchActivityPresenter {

    private ViewInterface mViewInterface;
    private SearchActivity mSearchActivity;

    public SearchActivityPresenter(ViewInterface viewInterface) {
        mViewInterface = viewInterface;
        mSearchActivity = (SearchActivity) mViewInterface;
    }

    public void load(String result) {
        FormBody body = new FormBody.Builder()
                .add("skuCode", result)
                .build();
        PostProtocol protocol = new PostProtocol(body, RequestUtils.SKU);
        protocol.post();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                mViewInterface.success(json);
            }

            @Override
            public void onFail() {
                mViewInterface.fail();
            }
        });

    }


    public void onNewCuWeiResult(String barcode) {


        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", barcode);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {

                @Override
                public void onSuccess(String json) {
                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast("没有更多数据");
                        return;
                    }

                    Gson gson = new Gson();
                    PositionBarcodeInfo bean = gson.fromJson(json, PositionBarcodeInfo.class);
                    if (bean != null && bean.isSuccess() && bean.getTarget() != null) {
                        unBinging(bean);
                    } else {
                        ToastUtils.showToast("" + bean.getMessage());
                    }
                }

                @Override
                public void onFail() {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //unbing
    private void unBinging(final PositionBarcodeInfo bean) {

        if (StringUtils.isEmpty(mSearchActivity.mView4.getText().toString().trim())) {
            ToastUtils.showToast("请先扫描商品");
            return;
        }

        JSONObject object = new JSONObject();
        JSONObject object2 = new JSONObject();
        try {
            object.put("skuId", mSearchActivity.mView4.getText().toString().trim());
            object.put("positionId", bean.getTarget().getId());

            object2.put("token", Login.getInstance().getToken());
            object2.put("data", object.toString());


        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = RequestUtils.DEL_CODE +
                "?request={token:'" + Login.getInstance().getToken() +
                "',data:{skuId:'" + mSearchActivity.mView4.getText().toString().trim() + "',positionId:" + bean.getTarget().getId() + "}}";

//        String url = RequestUtils.DEL_CODE + "?request=" + object2.toString();
        System.out.println("SearchActivityPresenter.unBinging=" + url);

        GetProtocol protocol = new GetProtocol(url);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {

                try {
                    JSONObject object = new JSONObject(json);
                    if (object.getBoolean("success")) {
                        mSearchActivity.mEtNew.setText(bean.getTarget().getBarcode());
                        ToastUtils.showToast("解绑成功");
                        mSearchActivity.mView6.setText("库位: 无");
                        mSearchActivity.mBangding.setEnabled(true);
                        mSearchActivity.mBangding.setClickable(true);
                        mSearchActivity.mEtNew.requestFocus();
                    } else {
                        ToastUtils.showToast("解绑失败");
                        mSearchActivity.mBangding.setEnabled(false);
                        mSearchActivity.mBangding.setClickable(false);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFail() {

            }
        });

    }

    //绑定新的库位
    public void bindNewCode(String input) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", input);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    if (!StringUtils.isEmpty(json)) {
                        Gson gson = new Gson();
                        PositionBarcodeInfo positionBarcodeInfo = gson.fromJson(json, PositionBarcodeInfo.class);
                        if (positionBarcodeInfo.isSuccess()) {
                            bind(positionBarcodeInfo.getTarget().getId() + "", positionBarcodeInfo.getTarget().getBarcode());
                        } else {

                        }
                    }
                }

                @Override
                public void onFail() {

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void bind(String pid, final String barcode) {

        if (StringUtils.isEmpty(mSearchActivity.mView4.getText().toString().trim())) {
            ToastUtils.showToast("请先扫描商品");
            return;
        }


        String url = RequestUtils.ADD_CODE + "?request={token:'" + Login.getInstance().getToken() + "',data:{skuId:" + mSearchActivity.mView4.getText().toString().trim() + ",positionId:" + pid + "}}";

        GetProtocol protocol = new GetProtocol(url);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                if (!StringUtils.isEmpty(json)) {
                    try {
                        JSONObject object = new JSONObject(json);

                        if (object.getBoolean("success")) {
//                                System.out.println("hahaha2绑定成功=" + newPid);
                            ToastUtils.showToast("绑定成功");
                            mSearchActivity.mBind_new_kuwei.setText(barcode);

                        } else {
                            ToastUtils.showToast("该商品已绑定过");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
            }

            @Override
            public void onFail() {
            }
        });
    }
}
