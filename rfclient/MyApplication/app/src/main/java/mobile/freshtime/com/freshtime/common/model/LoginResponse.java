package mobile.freshtime.com.freshtime.common.model;

import org.json.JSONObject;

/**
 * Created by orchid on 16/9/30.
 */

public class LoginResponse extends BaseResponse {

    private String module;
    private String mEquipmentId;
    private String mUserId;
    private long mTimeStamp;

    public LoginResponse(JSONObject jsonObject) {
        return_code = jsonObject.optInt("resultCode", 0);
        success = jsonObject.optBoolean("success", false);
        message = jsonObject.optString("errorMsg", "");
        module = jsonObject.optString("module", "");
    }

    public String getModule() {
        return module;
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        mTimeStamp = timeStamp;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

    public String getEquipmentId() {
        return mEquipmentId;
    }

    public void setEquipmentId(String equipmentId) {
        mEquipmentId = equipmentId;
    }
}
