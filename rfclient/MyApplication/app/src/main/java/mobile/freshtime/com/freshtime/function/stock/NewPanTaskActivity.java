package mobile.freshtime.com.freshtime.function.stock;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.search.PositionBarcodeInfo;
import mobile.freshtime.com.freshtime.function.stock.module.InSkuModel;
import mobile.freshtime.com.freshtime.function.stock.module.StockPage;
import mobile.freshtime.com.freshtime.function.stock.presenter.NewPanTaskViewModule;

/**
 * 点击进入后的Activity  一盘  新盘点
 */
public class NewPanTaskActivity extends BaseActivity implements ViewModelWatcher<StockPage>, PageListener<StockPage> {

    @Bind(R.id.item_tag)
    TextView mItemTag;
    @Bind(R.id.item_tag_two)
    TextView mItemTagTwo;
    @Bind(R.id.activity_pan_task)
    LinearLayout mActivityPanTask;
    @Bind(R.id.onePan)
    TextView mOnePan;
    @Bind(R.id.last)
    TextView mLast;
    @Bind(R.id.done)
    TextView mDone;
    @Bind(R.id.next)
    TextView mNext;
    private NewPanTaskViewModule mModule;
    private TextView mItemName;
    private TextView mGuige;
    private TextView mItem69code;
    public TextView mItemInput;
    private TextView mOnePanGuige;
    private ViewGroup mItemInfroRoot;
    private ViewGroup mSkuInfoRoot;
    private TextView mCangwei;
    private TextView mJiegou;
    private ViewGroup mInputsRoor;
    private NoInputEditText mInput;
    private NoInputEditText mItemInputTwo;
    public TextView mItemCode;
    public TextView mItemCodeTwo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan_task);
        ButterKnife.bind(this);

        mInputsRoor = (ViewGroup) findViewById(R.id.inputs);
        mItemCode = (TextView) mInputsRoor.findViewById(R.id.item_input);
        mItemCodeTwo = (TextView) mInputsRoor.findViewById(R.id.item_input_two);

        mInput = (NoInputEditText) mInputsRoor.findViewById(R.id.item_input);
        mItemInputTwo = (NoInputEditText) mInputsRoor.findViewById(R.id.item_input_two);

        mItemInfroRoot = (ViewGroup) findViewById(R.id.item_info_root);
        mItemName = (TextView) mItemInfroRoot.findViewById(R.id.item_name);
        mGuige = (TextView) mItemInfroRoot.findViewById(R.id.guige);
        mItem69code = (TextView) mItemInfroRoot.findViewById(R.id.item_69code);
        mItemInput = (TextView) mItemInfroRoot.findViewById(R.id.item_input);
        mOnePanGuige = (TextView) mItemInfroRoot.findViewById(R.id.onePan_guige);

        mSkuInfoRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        mCangwei = (TextView) mSkuInfoRoot.findViewById(R.id.cangwei);
        mJiegou = (TextView) mSkuInfoRoot.findViewById(R.id.jiegou);

        mItemInfroRoot.setVisibility(View.INVISIBLE);
        mSkuInfoRoot.setVisibility(View.INVISIBLE);

        mInput.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
        mItemInputTwo.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        mItemTag.setText("商品条码: ");
        mItemTagTwo.setText("仓位编码: ");

        mModule = new NewPanTaskViewModule(this);
        mModule.addPageListener(this);
        mModule.registerContext(this);

    }

    @Override
    public void onScanResult(String result) {
        if (StringUtils.isEmpty(result)) {
            return;
        }

        mModule.onScanResult(result.trim());
    }

    @Override
    public void onRequestStart() {

    }

    @Override
    public void onRequestStop() {

    }

    @Override
    public void onPageUpdate(StockPage page) {
        displayPage(page);
    }


    @Override
    public void onError(String errorCode) {
    }

    @Override
    public void onFinish() {
    }

    @Override
    public void onPageChanged(StockPage page) {
        displayPage(page);
    }


    private void displayPage(StockPage page) {

        if (mModule.hasBefore()) {
            mLast.setEnabled(true);
        } else {
            mLast.setEnabled(false);
        }

        if (!mModule.hasNext()
//                &&
//                page.getPhase() != StockPage.Phase.SKU) {
//                page.getPhase() != StockPage.Phase.START
                ) {
            mNext.setEnabled(true);
        } else {
            mNext.setEnabled(true);
        }

        StockPage.Phase phase = page.getPhase();
        if (phase == StockPage.Phase.START) {
            displayStart(page);
        } else if (phase == StockPage.Phase.SKU) {
            displaySku(page);//显示sku信息
        } else if (phase == StockPage.Phase.COUNT) {
//            displayPosition(page);//显示墙位position
        } else {
//            displayCount(page);
        }
    }

    /**
     * 初始化的UI
     *
     * @param page
     */
    private void displayStart(StockPage page) {

        mItemInfroRoot.setVisibility(View.INVISIBLE);
        mSkuInfoRoot.setVisibility(View.INVISIBLE);

        mItemCode.setText("");
        mItemCodeTwo.setText("");

        mItemCode.requestFocus();

    }

    //显示sku信息
    private void displaySku(StockPage page) {
        InSkuModel unit = page.getSkuModel();
        displayUnit(unit);

    }

    private void displayUnit(InSkuModel unit) {

        mItemInfroRoot.setVisibility(View.VISIBLE);
        mSkuInfoRoot.setVisibility(View.VISIBLE);

        mItemName.setText("商品: " + unit.getTitle());
        mGuige.setText("规格: " + unit.getSpecification());
        mItem69code.setText("69码: " + unit.getSku69Id() + "");
        mOnePanGuige.setText(unit.getSpecification().substring(1));
        mItemCode.setText(unit.getBarcode());//设置扫描到的码
        mItemCodeTwo.requestFocus();

        if ("null".equals(unit.getCount() + "")) {
            mItemInput.setText("");//设置数量
        } else {
            mItemInput.setText(String.valueOf(unit.getCount()));//设置数量
        }
    }

    @OnClick({R.id.last, R.id.done, R.id.next})
    public void onClick(View view) {

        int id = view.getId();

        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {
            if (mItemInfroRoot.getVisibility() == View.INVISIBLE || mSkuInfoRoot.getVisibility() == View.INVISIBLE) {
                ToastUtils.showToast(getResources().getString(R.string.please_scan_tight_tiaoma_kuwei));
                return;
            }

            if (StringUtils.isEmpty(mItemInput.getText().toString().trim())) {
                Toast.makeText(NewPanTaskActivity.this, getResources().getString(R.string.please_input_count), Toast.LENGTH_LONG).show();
                return;
            }

            mModule.setCount(mItemInput.getText().toString());
            mModule.navNext();
            requestDone();
        } else if (id == R.id.last) {
            mModule.navBefore();
        } else if (id == R.id.next) {

            if (mItemInfroRoot.getVisibility() == View.INVISIBLE || mSkuInfoRoot.getVisibility() == View.INVISIBLE) {
                Toast.makeText(NewPanTaskActivity.this, getResources().getString(R.string.please_item_or_kuwei), Toast.LENGTH_LONG).show();
                return;
            }

            if (StringUtils.isEmpty(mItemInput.getText().toString().trim())) {
                Toast.makeText(NewPanTaskActivity.this, getResources().getString(R.string.please_input_count), Toast.LENGTH_LONG).show();
                return;
            }

            mModule.setCount(mItemInput.getText().toString());
            mItemInput.setText("");
            mModule.navNext();
        }
    }

    //提交一盘数据
    private void requestDone() {

        ArrayList<StockPage> stockPages = mModule.getStockPages();
        ArrayList<InSkuModel> modules = new ArrayList<>();

        Intent intent = new Intent(NewPanTaskActivity.this, PanListActivity.class);
        for (int i = 0; i < stockPages.size(); i++) {

            StockPage page = stockPages.get(i);
            InSkuModel model = page.getSkuModel();

            if (model != null && (page.getPhase() == StockPage.Phase.SKU)) {
                modules.add(stockPages.get(i).getSkuModel());
            }
        }

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("units", modules);
        bundle.putInt("id", 1);//索引
        bundle.putInt("taskId", 1);
        bundle.putInt("faqiren", 1);
        bundle.putInt("quyu", 1);
        bundle.putString("time", "");

        intent.putExtras(bundle);
        startActivity(intent);

    }

    //库位信息
    public void onCangwei(PositionBarcodeInfo info) {
        PositionBarcodeInfo.TargetBean target = info.getTarget();
        if (target != null) {

            String barcode = target.getBarcode();
            String code = target.getCode();
            int areaId = target.getAreaId();
            int id = target.getId();

            mCangwei.setText(code);
            mJiegou.setText(areaId + "");
            mItemCodeTwo.setText(barcode);

            mItemInput.requestFocus();

        }
    }
}
