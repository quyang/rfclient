package mobile.freshtime.com.freshtime.function.pickwall;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.activity.MenuActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.model.wallposition.ResponseWallPosition;
import mobile.freshtime.com.freshtime.common.model.wallposition.WallPosition;
import mobile.freshtime.com.freshtime.function.pickwall.model.ResponsePickWall;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.PickWallRequest;

import static mobile.freshtime.com.freshtime.R.id.info_root;

/**
 * 拣货墙
 * <p>
 * Created by orchid on 16/9/20.
 */
public class PickWallActivity extends BaseActivity implements Response.ErrorListener, View.OnClickListener {

    private String mWallBarcode;
    private WallPosition mWallPosition;
    private ResponsePickWall mModel;

    private EditText mPickInput;
    private View mInfoRoot;
    private TextView mWallCode;
    private TextView mShopInfo;
    private TextView mPickCount;
    private TextView mPickedCount;
    private TextView mWallPositionCode;
    private EditText mWallInput;
    private TextView mFinished;

    private TextView mDone;

    private Phase mPhase = Phase.START;

    private Response.Listener<BaseResponse> mReceiveResponse =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        getWave();
                    } else {
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };

    private Response.Listener<ResponsePickWall> mPickWallRequestListener =
            new Response.Listener<ResponsePickWall>() {
                @Override
                public void onResponse(ResponsePickWall response) {
                    if (response != null && response.success) {
                        mPhase = Phase.WALL;
                        PickWallActivity.this.mModel = response;
                        displayView();
                    } else {
                        ToastUtils.showToast("" + response.message);
                    }
                }
            };

    private Response.Listener<ResponseWallPosition> mWallPositionResponse =
            new Response.Listener<ResponseWallPosition>() {
                @Override
                public void onResponse(ResponseWallPosition response) {
                    if (response != null && response.isSuccess()) {
                        mWallPosition = response.target;
                        assignPickWall();
                    } else {
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };

    private Response.Listener<BaseResponse> mAssignResponse =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        mPhase = Phase.DONE;
                        displayView();
                        mDone.setEnabled(true);
                    } else {
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };

    private Response.Listener<BaseResponse> mFinishWaveResponse =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        Toast.makeText(PickWallActivity.this,
                                "波次完成", Toast.LENGTH_SHORT).show();

                        //影藏view
                        mInfoRoot.setVisibility(View.INVISIBLE);

                        finish();
                    } else {
                        ToastUtils.showToast("" + response.getErrorMsg());
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pickwall);


        TextView module = (TextView) findViewById(R.id.item_tag);
        module.setText("拣货墙编号:");

        mInfoRoot = findViewById(info_root);
        mPickInput = (EditText) findViewById(R.id.delivery_input).findViewById(R.id.item_input);

        mWallCode = (TextView) mInfoRoot.findViewById(R.id.item_name);
        mShopInfo = (TextView) mInfoRoot.findViewById(R.id.item_code);
        mPickCount = (TextView) mInfoRoot.findViewById(R.id.pick_count);
        mPickedCount = (TextView) mInfoRoot.findViewById(R.id.picked_count);
        mWallPositionCode = (TextView) mInfoRoot.findViewById(R.id.wall_code);
        mWallInput = (EditText) mInfoRoot.findViewById(R.id.item_input);
        mFinished = (TextView) mInfoRoot.findViewById(R.id.is_finished);
        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);
        mDone.setEnabled(false);

        findViewById(R.id.back).setOnClickListener(this);

        //切换
        findViewById(R.id.exchange).setOnClickListener(this);
        findViewById(R.id.quit).setOnClickListener(this);

        mInfoRoot.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onScanResult(String result) {
        result = result.trim();
        if (mPhase == Phase.START) {
            receiveBasket(result);
        } else if (mPhase == Phase.WALL)
            getPickWallPosition(result);
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    /**
     * Step 1 接受basket
     *
     * @param result
     */
    private void receiveBasket(String result) {
        mWallBarcode = result;
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(result, "1"));
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.RECEIVE_PICK,
                            BaseResponse.class,
                            param,
                            mReceiveResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 2 获取波次信息
     */
    private void getWave() {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(mWallBarcode, "1"));
            PickWallRequest request =
                    new PickWallRequest(param,
                            mPickWallRequestListener, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    /**
     * Step 3
     *
     * @param result
     */
    private void getPickWallPosition(String result) {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(result, "1"));
            BaseRequest<ResponseWallPosition> request =
                    new BaseRequest<>(RequestUtils.PICK_WALL_POSITION,
                            ResponseWallPosition.class,
                            param,
                            mWallPositionResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 4 校对拣货墙位
     */
    private void assignPickWall() {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            JSONObject data = new JSONObject();
            data.put("wallPositionId", mWallPosition.id);
            data.put("assignmentId", mModel.target.id);
            param.put("data", data);
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.PICK_WALL,
                            BaseResponse.class,
                            param,
                            mAssignResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * Step 5 结束波次
     */
    private void finishWave() {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", mModel.target.id);
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.WAVE_FINISH,
                            BaseResponse.class,
                            param,
                            mFinishWaveResponse, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void displayView() {
        mPickInput.setEnabled(false);
        mInfoRoot.setVisibility(View.VISIBLE);
        mPickInput.setText(mWallBarcode);
        mWallCode.setText("编号:" + mModel.target.id);
        mShopInfo.setText("门店:" + mModel.target.areaId);
        int count = 0;
        if (mModel.target.pickVOs == null) count = 0;
        else count = mModel.target.pickVOs.size();
        mPickCount.setText("拣货任务:" + count);
        mPickedCount.setText("已完成:" + count);

        if (mPhase == Phase.WALL) {
            mWallInput.requestFocus();
        } else if (mPhase == Phase.DONE) {
            mWallInput.setEnabled(false);
            mWallInput.setText(mWallBarcode);
            mFinished.setText("门店:" + mWallPosition.wallId);
            mWallPositionCode.setText("拣货墙位:" + mWallPosition.code);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.done) {
            if (mPhase == Phase.DONE) {
                finishWave();
            }
        } else if (id == R.id.exchange) {
            Intent intent = new Intent(this, PickBasketActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (id == R.id.quit) {
            Intent intent = new Intent(this, MenuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else if (id == R.id.back) {
            finish();
        }
    }

    private enum Phase {
        START,
        WALL,
        DONE
    }
}
