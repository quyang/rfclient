package mobile.freshtime.com.freshtime.function.stock.presenter;

import android.view.View;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.function.stock.FirstStockActivity;
import mobile.freshtime.com.freshtime.function.stock.module.StocktakingInProgressInfo;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by Administrator on 2016/11/15.
 */
public class FirstStockActivityPresenter {

    private ViewInterface mActivity;
    private FirstStockActivity mFirstStockActivity;

    public FirstStockActivityPresenter(ViewInterface activity) {
        mActivity = activity;
        mFirstStockActivity = (FirstStockActivity) mActivity;
    }

    public void onScanResult(String input) {

        if (StringUtils.isEmpty(input)) {
            throw new IllegalArgumentException("onScanResult 获取盘点任务失败");
        }

        String url = "http://www.baidu.com";
        GetProtocol protocol = new GetProtocol(url);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                if (!StringUtils.isEmpty(json)) {
                    mActivity.success(json);
                }
            }

            @Override
            public void onFail() {
                mActivity.fail();
            }
        });
    }

    //创建任务
    public void submitStocktakingDetail(String url) {
        checkTask();
    }

    //查询任务 一盘任务
    public void checkTask() {
        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            GetProtocol protocol = new GetProtocol(RequestUtils.STOCK + "?request=" + object.toString());
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("FirstStockActivityPresenter.onSuccess=" + json);
                    try {
                        JSONObject object = new JSONObject(json);
                        if (object.getBoolean("success")) {
                            Gson gson = new Gson();
                            StocktakingInProgressInfo info = gson.fromJson(json, StocktakingInProgressInfo.class);
                            if (info.getTarget() != null && info.getTarget().getItems() != null && info.getTarget().getItems().size() > 0) {
                                gotoTwoPan(info.getTarget());
                            } else {
                                ToastUtils.showToast("没有盘点任务");
                                mFirstStockActivity.getNewPanDian().setClickable(true);
                                mFirstStockActivity.mList.setVisibility(View.INVISIBLE);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
//                    ToastUtils.showToast("查询失败");
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //进行一盘
    private void gotoTwoPan(StocktakingInProgressInfo.TargetBean target) {
        List<StocktakingInProgressInfo.TargetBean.ItemsBean> items = target.getItems();
        mFirstStockActivity.onTwoPan(items);


//        JSONObject object = new JSONObject();
//        try {
//            object.put("token", Login.getInstance().getToken());
//            GetProtocol protocol = new GetProtocol(RequestUtils.TO_CHECK + "?request=" + object.toString());
//            protocol.get();
//            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
//                @Override
//                public void onSuccess(String json) {
//                    System.out.println("FirstStockActivityPresenter.onSuccess=" + json);
//                    try {
//                        JSONObject object = new JSONObject(json);
//                        if (object.getBoolean("success")) {
//                            Gson gson = new Gson();
//                            StocktakingInProgressInfo info = gson.fromJson(json, StocktakingInProgressInfo.class);
//                            if (info.getTarget() != null && info.getTarget().getItems() != null) {
//                                gotoTwoPan(info.getTarget());
//                            }
//                        } else {
//                            ToastUtils.showToast("查询失败");
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                @Override
//                public void onFail() {
//                    ToastUtils.showToast("查询失败");
//                }
//            });
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }
}
