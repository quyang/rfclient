package mobile.freshtime.com.freshtime.common.model.viewmodel;

/**
 * Created by orchid on 16/9/14.
 * 监听用户更换上下页的事件
 */
public interface PageListener<Page> {

    void onPageChanged(Page page);
}
