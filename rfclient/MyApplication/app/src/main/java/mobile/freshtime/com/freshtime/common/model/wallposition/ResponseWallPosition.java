package mobile.freshtime.com.freshtime.common.model.wallposition;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orkid on 2016/10/17.
 */
public class ResponseWallPosition extends BaseResponse {

    public WallPosition target;
}
