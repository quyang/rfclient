package mobile.freshtime.com.freshtime.function.kuweitiaozheng.module;

/**
 * Created by Administrator on 2016/12/13.
 */
public class ItemKucunInfo {


    /**
     * message : null
     * return_code : 0
     * success : true
     * target : {"agencyId":1,"areaId":13,"areaType":null,"dr":0,"guardBit":null,"id":49847,"maySale":null,"number":-56,"numberReal":-56000,"numberRealTotal":null,"numberShow":"-56","numberShowTotal":null,"numberTotal":null,"positionCode":"QC02803","positionId":573,"saleUnit":"只","saleUnitId":68,"saleUnitType":1,"skuId":40088,"skuName":"鲜鲍鱼（2只起售）","skuPlu":null,"slottingId":null,"slottingType":null,"spec":null,"specUnit":null,"specUnitId":null,"ts":"2016-12-17T15:32:24"}
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * areaId : 13
     * areaType : null
     * dr : 0
     * guardBit : null
     * id : 49847
     * maySale : null
     * number : -56
     * numberReal : -56000
     * numberRealTotal : null
     * numberShow : -56
     * numberShowTotal : null
     * numberTotal : null
     * positionCode : QC02803
     * positionId : 573
     * saleUnit : 只
     * saleUnitId : 68
     * saleUnitType : 1
     * skuId : 40088
     * skuName : 鲜鲍鱼（2只起售）
     * skuPlu : null
     * slottingId : null
     * slottingType : null
     * spec : null
     * specUnit : null
     * specUnitId : null
     * ts : 2016-12-17T15:32:24
     */

    private TargetBean target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public TargetBean getTarget() {
        return target;
    }

    public void setTarget(TargetBean target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private int areaId;
        private Object areaType;
        private int dr;
        private Object guardBit;
        private int id;
        private Object maySale;
        private int number;
        private int numberReal;
        private Object numberRealTotal;
        private String numberShow;
        private Object numberShowTotal;
        private Object numberTotal;
        private String positionCode;
        private int positionId;
        private String saleUnit;
        private int saleUnitId;
        private int saleUnitType;
        private int skuId;
        private String skuName;
        private Object skuPlu;
        private Object slottingId;
        private Object slottingType;
        private Object spec;
        private Object specUnit;
        private Object specUnitId;
        private String ts;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public Object getAreaType() {
            return areaType;
        }

        public void setAreaType(Object areaType) {
            this.areaType = areaType;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public Object getGuardBit() {
            return guardBit;
        }

        public void setGuardBit(Object guardBit) {
            this.guardBit = guardBit;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getMaySale() {
            return maySale;
        }

        public void setMaySale(Object maySale) {
            this.maySale = maySale;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getNumberReal() {
            return numberReal;
        }

        public void setNumberReal(int numberReal) {
            this.numberReal = numberReal;
        }

        public Object getNumberRealTotal() {
            return numberRealTotal;
        }

        public void setNumberRealTotal(Object numberRealTotal) {
            this.numberRealTotal = numberRealTotal;
        }

        public String getNumberShow() {
            return numberShow;
        }

        public void setNumberShow(String numberShow) {
            this.numberShow = numberShow;
        }

        public Object getNumberShowTotal() {
            return numberShowTotal;
        }

        public void setNumberShowTotal(Object numberShowTotal) {
            this.numberShowTotal = numberShowTotal;
        }

        public Object getNumberTotal() {
            return numberTotal;
        }

        public void setNumberTotal(Object numberTotal) {
            this.numberTotal = numberTotal;
        }

        public String getPositionCode() {
            return positionCode;
        }

        public void setPositionCode(String positionCode) {
            this.positionCode = positionCode;
        }

        public int getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public String getSaleUnit() {
            return saleUnit;
        }

        public void setSaleUnit(String saleUnit) {
            this.saleUnit = saleUnit;
        }

        public int getSaleUnitId() {
            return saleUnitId;
        }

        public void setSaleUnitId(int saleUnitId) {
            this.saleUnitId = saleUnitId;
        }

        public int getSaleUnitType() {
            return saleUnitType;
        }

        public void setSaleUnitType(int saleUnitType) {
            this.saleUnitType = saleUnitType;
        }

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public String getSkuName() {
            return skuName;
        }

        public void setSkuName(String skuName) {
            this.skuName = skuName;
        }

        public Object getSkuPlu() {
            return skuPlu;
        }

        public void setSkuPlu(Object skuPlu) {
            this.skuPlu = skuPlu;
        }

        public Object getSlottingId() {
            return slottingId;
        }

        public void setSlottingId(Object slottingId) {
            this.slottingId = slottingId;
        }

        public Object getSlottingType() {
            return slottingType;
        }

        public void setSlottingType(Object slottingType) {
            this.slottingType = slottingType;
        }

        public Object getSpec() {
            return spec;
        }

        public void setSpec(Object spec) {
            this.spec = spec;
        }

        public Object getSpecUnit() {
            return specUnit;
        }

        public void setSpecUnit(Object specUnit) {
            this.specUnit = specUnit;
        }

        public Object getSpecUnitId() {
            return specUnitId;
        }

        public void setSpecUnitId(Object specUnitId) {
            this.specUnitId = specUnitId;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }
    }
}
