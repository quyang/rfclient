package mobile.freshtime.com.freshtime.function.purchase;

import android.os.Bundle;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.FTLog;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * Created by orchid on 16/9/26.
 */

public class PurchaseListActivity extends BaseActivity implements View.OnClickListener, Response.ErrorListener, Response.Listener<ResponseSku> {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_list);

        findViewById(R.id.last).setOnClickListener(this);
        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onScanResult(String result) {

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.last) {
            JSONObject param = new JSONObject();
            try {
                param.put("token", Login.getInstance().getToken());
//                param.put("data", 1);
                BaseRequest<ResponseSku> request =
                        new BaseRequest<>(RequestUtils.BOOK,
                                ResponseSku.class,
                                param,
                                this, this);
                FreshRequest.getInstance().addToRequestQueue(request, this.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        if (id == R.id.back) {
            finish();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        FTLog.e("error");
    }

    @Override
    public void onResponse(ResponseSku response) {
        FTLog.e("success");
    }
}
