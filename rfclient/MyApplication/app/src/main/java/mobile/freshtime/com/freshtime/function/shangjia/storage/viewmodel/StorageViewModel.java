package mobile.freshtime.com.freshtime.function.shangjia.storage.viewmodel;

import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.position.ResponsePosition;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.viewmodel.ModelList;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.BaseViewModel;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.shangjia.storage.StorageActivity;
import mobile.freshtime.com.freshtime.function.shangjia.storage.StorageBean;
import mobile.freshtime.com.freshtime.function.shangjia.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.function.shangjia.storage.model.InSkuRequest;
import mobile.freshtime.com.freshtime.function.shangjia.storage.model.ResponseSkuIn;
import mobile.freshtime.com.freshtime.function.shangjia.storage.model.StoragePage;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuRequest;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by orchid on 16/9/22.
 */
public class StorageViewModel extends BaseViewModel<StoragePage> implements ModelList<StoragePage>,
        Response.ErrorListener {

    private List<StoragePage> mStoragePages;
    private int mCurrentPageIndex;

    private PageListener mPageListener;
    private ViewModelWatcher mModelWatcher;

    private boolean mIsRequesting;
    private Request<ResponseSkuIn> mSkuRequest;
    /**
     * sku请求回调
     */
    private Response.Listener<ResponseSkuIn> mSkuModelListener = new Response.Listener<ResponseSkuIn>() {
        @Override
        public void onResponse(ResponseSkuIn response) {
            StorageViewModel.this.onResponse(response);
        }
    };

    /**
     * sku请求回调2，负责添加数量
     */
    private Response.Listener<ResponseSku> mSkuCountListener = new Response.Listener<ResponseSku>() {
        @Override
        public void onResponse(ResponseSku response) {
            StorageViewModel.this.onCountResponse(response);
        }
    };

    private Response.Listener<ResponsePosition> mPositionListener =
            new Response.Listener<ResponsePosition>() {
                @Override
                public void onResponse(ResponsePosition response) {
                    if (response != null && response.isSuccess()) {
                        StoragePage page = getCurrentPage();
                        page.getSkuModel().setPositionId(response.target.id);
                        page.advance();
                        mModelWatcher.onPageUpdate(page);
                    } else {
                        String msg = response == null ? "未获取到仓位信息" : response.getErrorMsg();
                        mModelWatcher.onError(msg);
                    }
                }
            };
    private final StorageActivity mWatcher;

    public StorageViewModel(ViewModelWatcher watcher) {
        this.mStoragePages = new ArrayList<>();
        this.mCurrentPageIndex = 0;
        this.mModelWatcher = watcher;
        mWatcher = (StorageActivity) mModelWatcher;

        //初始化时为start
        StoragePage page = new StoragePage(null);
        mStoragePages.add(mCurrentPageIndex, page);

    }

    public void addPageListener(PageListener pageListener) {
        this.mPageListener = pageListener;
        StoragePage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    /**
     * --------------------- BaseViewModel start ---------------------
     **/
    @Override
    public void onScanResult(String input) {

        StoragePage page = getCurrentPage();
        StoragePage.Phase phase = page.getPhase();
        if (phase == StoragePage.Phase.START) {
            requestSku(input);
        } else if (phase == StoragePage.Phase.SKU) {
//            fillPosition(input);
            if (mWatcher.mNew_kuwei.hasFocus()) {
                fillCuKei(input);
            }
        } else if (phase == StoragePage.Phase.POSITION) {
            fillCount(input);
        }
    }

    //获取库位和pid
    private void fillCuKei(String data) {

        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", data);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    if (StringUtils.isEmpty(json)) {
                        return;
                    }

                    Gson gson = new Gson();
                    StorageBean bean = gson.fromJson(json, StorageBean.class);
                    if (bean != null && bean.isSuccess() && bean.getTarget() != null && bean.getTarget() != null) {
                        StoragePage currentPage = getCurrentPage();

                        mWatcher.mBangdign.setEnabled(true);
                        //请求成功后才能绑定
                        mWatcher.mBangdign.setOnClickListener(new MyOnClickListener(bean.getTarget().getId() + ""));

                        mWatcher.mSkuCode.setText(bean.getTarget().getCode());
                        mWatcher.mNew_kuwei.setText(bean.getTarget().getBarcode());

                        System.out.println("StorageViewModel.onSuccess=fillCuKeiID=" + bean.getTarget().getId());

                        //设置code和pid
                        currentPage.getSkuModel().setCuwei(bean.getTarget().getCode());
                        currentPage.getSkuModel().setPositionId(bean.getTarget().getId());

                        System.out.println("StorageViewModel.onSuccess=fillCuKei=cuwei=" + bean.getTarget().getCode());

                    } else {
//                        ToastUtils.showToast("请扫描正确的库位");
                    }
                }

                @Override
                public void onFail() {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //邦定
    private class MyOnClickListener implements View.OnClickListener {

        private String id;

        public MyOnClickListener(String id) {
            this.id = id;
        }

        @Override
        public void onClick(View v) {

            String code = mWatcher.mNew_kuwei.getText().toString().trim();//code
            if (StringUtils.isEmpty(code)) {
                ToastUtils.showToast("请先输入目标库位");
                return;
            }

            String url = RequestUtils.ADD_CODE + "?request={token:'" +
                    Login.getInstance().getToken() + "',data:{skuId:" +
                    getCurrentPage().getSkuModel().getSkuId() +
                    ",positionId:" + id + "}}";

            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    if (!StringUtils.isEmpty(json)) {
                        try {
                            JSONObject object = new JSONObject(json);

                            if (object.getBoolean("success")) {
//                                System.out.println("hahaha2绑定成功=" + newPid);
                                ToastUtils.showToast("绑定成功");
                                mWatcher.mPostionCount.requestFocus();

                            } else {
                                ToastUtils.showToast("该商品已绑定过");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                }

                @Override
                public void onFail() {
                }
            });
        }
    }

    @Override
    public void onInterrupt() {
        mIsRequesting = false;
        if (mSkuRequest != null)
            FreshRequest.getInstance().cancel(mSkuRequest.toString());
    }

    @Override
    public boolean isRequesting() {
        return mIsRequesting;
    }

    @Override
    public void registerContext(BaseActivity activity) {

    }

    @Override
    public void unregisterContext() {
        this.mPageListener = null;
        this.mModelWatcher = null;
        this.mSkuRequest = null;
    }

    /**
     * --------------------- ModelList start ---------------------
     **/

    @Override
    public boolean hasBefore() {
        return mCurrentPageIndex != 0;
    }

    @Override
    public boolean hasNext() {
        return mCurrentPageIndex != mStoragePages.size() - 1;
    }

    @Override
    public boolean isReady() {
        for (int i = 0; i < mStoragePages.size(); i++) {
            StoragePage page = mStoragePages.get(i);
            if (page.getPhase() != StoragePage.Phase.DONE || page.getPhase() != StoragePage.Phase.POSITION)
                return false;
        }

        return true;
    }

    @Override
    public void navNext() {

//        ToastUtils.showToast(getCurrentPage().getSkuModel().getSkuId() + "=" + mCurrentPageIndex);

        int len = mStoragePages.size();

        StoragePage currentPage = getCurrentPage();
        currentPage.getSkuModel().setCount(count);//设置数量

        if (mCurrentPageIndex++ == len - 1) {
            // 新增一页
            StoragePage page = new StoragePage(null);
            mStoragePages.add(mCurrentPageIndex, page);
        }

        StoragePage page = getCurrentPage();
        mPageListener.onPageChanged(page); //更新界面

    }

    @Override
    public void navBefore() {
        if (mCurrentPageIndex != 0) {
            mCurrentPageIndex--;
            StoragePage page = getCurrentPage();
            mPageListener.onPageChanged(page);
        }
    }

    @Override
    public StoragePage getCurrentPage() {
        return mStoragePages.get(mCurrentPageIndex);
    }
    /**
     * --------------------- ModelList end ---------------------
     **/

    /**
     * 获取sku信息
     *
     * @param code
     */
    private void requestSku(String code) {
        if (!mIsRequesting) {
            mIsRequesting = true;
            mSkuRequest = new InSkuRequest(code, mSkuModelListener, this);
            FreshRequest.getInstance().addToRequestQueue(mSkuRequest, this.toString());
        }
    }

    /**
     * 指定新的库位
     *
     * @param code
     */
    private void fillPosition(String code) {

        StoragePage page = getCurrentPage();
        page.getSkuModel().setPosition(code);
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(code, "1"));
            BaseRequest<ResponsePosition> request =
                    new BaseRequest<>(RequestUtils.POSITION_BARCODE,
                            ResponsePosition.class,
                            param,
                            mPositionListener, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    /**
     * @param code
     */
    private void fillCount(String code) {
        StoragePage page = getCurrentPage();
        if (Utils.filterInput(code.equals(page.getSkuModel().getBarcode()), false)) {
            addItem(page, 1, code);
        } else if (Utils.isDigital(code)) {
            try {
                int count = Integer.parseInt(code);
                setItem(page, count);
            } catch (Exception e) {
                SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                        mSkuCountListener, this);
                FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
            }
        } else {
            SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                    mSkuCountListener, this);
            FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
        }
    }

    private void addItem(StoragePage page, int count, String code) {
//        int current = page.getSkuModel().getCount();
//        page.getSkuModel().setCount(count + current);
        page.getSkuModel().setCount(code);
        mModelWatcher.onPageUpdate(page);
    }

    private void setItem(StoragePage page, int count) {
//        page.getSkuModel().setCount(Integer.parseInt(count));
        page.getSkuModel().setCount(StringUtils.getString(count));
//        page.getSkuModel().setPositionId(Long.parseLong(positionId));
        mModelWatcher.onPageUpdate(page);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        clearRequestState();
    }


    //扫描商品条码回调
    public void onResponse(final ResponseSkuIn response) {

        clearRequestState();
        if (response != null && response.isSuccess()) {

            String url = RequestUtils.VALUE_SERVICE + "?idlist="
                    + response.getModule().skuSpecificationUnit
                    + "," + response.getModule().skuUnit;

            System.out.println("StorageViewModel.onResponse===" + url);

            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    if (StringUtils.isEmpty(json)) {
                        return;
                    }

                    try {
                        JSONObject object = new JSONObject(json);

                        if (object.getBoolean("success")) {

                            JSONObject module = object.getJSONObject("module");

//                            String saleUnitTypeString = module.getString("" + response.getModule().saleUnitType);
                            String skuSpecificationUnitString = module.getString("" + response.getModule().skuSpecificationUnit);
                            String skuUnitString = module.getString("" + response.getModule().skuUnit);

                            response.getModule().setSpecUnitZhuan(skuSpecificationUnitString);//规格单位
//                            response.getModule().setSaleUnitTypeZhuan(saleUnitTypeString);//销售方式
                            response.getModule().setSaleUnitZhuan(skuUnitString);//销售类型

                            getIntoStorage(response);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFail() {

                }
            });


        }
    }

    //获取推荐库位
    private void getIntoStorage(final ResponseSkuIn response) {
        long skuId = response.getModule().getSkuId();

        //根据skuid获取库位
        String s = RequestUtils.INTO_STORAGE + "request=" +
                "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + skuId + "\"}";

        //获取库位的请求
        GetProtocol protocol = new GetProtocol(s);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                linkedData(response, json);
            }

            @Override
            public void onFail() {
            }
        });
    }


    /**
     * @param response 商品的基本信息
     * @param json     包含商品positionID的json
     */
    private void linkedData(ResponseSkuIn response, String json) {

        if (json == null || "".equals(json) || "null".equals(json)) {
            return;
        }


        Gson gson = new Gson();
        CuWeiBean bean = gson.fromJson(json, CuWeiBean.class);

        StoragePage page = getCurrentPage();
        page.setSkuModel(response.getModule());

        if (bean.target != null && bean.target.size() > 0) {
            //有库位
            //获取仓位
            CuWeiBean.TargetBean targetBean = bean.target.get(0);
            if (targetBean != null && targetBean.pisitionVO != null) {
                //有库位
                //设置positionId
                page.getSkuModel().setPositionId(targetBean.pisitionVO.id);

                //有库位
                page.getSkuModel().setCuwei(targetBean.pisitionVO.code);//设置库位信息

                System.out.println("StorageViewModel.linkedData=cuwei=" + targetBean.pisitionVO.code);
                System.out.println("StorageViewModel.linkedData=pid=" + targetBean.pisitionVO.id);

            } else {
                page.getSkuModel().setCuwei("");
                page.getSkuModel().setPositionId(-1);
            }

        } else {
            page.getSkuModel().setCuwei("");
            page.getSkuModel().setPositionId(-1);
        }

        page.advance();//sku ,
        mPageListener.onPageChanged(page);

    }


    public void onCountResponse(ResponseSku response) {
        clearRequestState();
        if (response != null && response.isSuccess()) {
            StoragePage page = getCurrentPage();
            if (page.getSkuModel().getSkuId() == response.getModule().getSkuId()) {
                addItem(page, 1, "");
            } else {
                mModelWatcher.onError("请输入正确的商品条码或者数量");
            }
        } else {
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }

    private void clearRequestState() {
        mIsRequesting = false;
    }

    public List<StoragePage> getOutgoPages() {
        return mStoragePages;
    }

    private String count;

    public void setCount(String count) {
        this.count = count;
    }

    private String positionId;

    public void setPositionId(String positionId) {
        this.positionId = positionId;
    }

}
