package mobile.freshtime.com.freshtime.function.tradeshelf.viewmodel;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.viewmodel.ModelList;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.BaseViewModel;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.function.tradeshelf.model.TradeShelfPage;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuRequest;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by orchid on 16/9/24.
 */
public class TradeShelfViewModel extends BaseViewModel<TradeShelfPage> implements ModelList<TradeShelfPage>, Response.ErrorListener {

    private List<TradeShelfPage> mTradeShelfPages;
    private int mCurrentPageIndex;

    private PageListener mPageListener;
    private ViewModelWatcher mModelWatcher;

    private boolean mIsRequesting;
    private Request<ResponseSku> mSkuRequest;
    /**
     * sku请求回调
     */
    private Response.Listener<ResponseSku> mSkuModelListener = new Response.Listener<ResponseSku>() {
        @Override
        public void onResponse(ResponseSku response) {
            TradeShelfViewModel.this.onResponse(response);
        }
    };

    /**
     * sku请求回调2，负责添加数量
     */
    private Response.Listener<ResponseSku> mSkuCountListener = new Response.Listener<ResponseSku>() {
        @Override
        public void onResponse(ResponseSku response) {
            TradeShelfViewModel.this.onCountResponse(response);
        }
    };


    public void onCountResponse(ResponseSku response) {
        clearRequestState();
        if (response != null && response.isSuccess()) {
            TradeShelfPage page = getCurrentPage();
            if (page.getSkuModel().getSkuId() == response.getModule().getSkuId()) {
                addItem(page, 1);
            } else {
                mModelWatcher.onError("请输入正确的商品条码或者数量");
            }
        }
    }

    private void setItem(TradeShelfPage page, int count) {
        page.getSkuModel().setCount(count);
        mModelWatcher.onPageUpdate(page);
    }

    private void addItem(TradeShelfPage page, int count) {
        int current = page.getSkuModel().getCount();
        page.getSkuModel().setCount(count + current);
        mModelWatcher.onPageUpdate(page);
    }

    public TradeShelfViewModel(ViewModelWatcher watcher) {
        this.mTradeShelfPages = new ArrayList<>();
        this.mCurrentPageIndex = 0;
        this.mModelWatcher = watcher;

        TradeShelfPage page = new TradeShelfPage(null);
        mTradeShelfPages.add(mCurrentPageIndex, page);

    }

    public void addPageListener(PageListener pageListener) {
        this.mPageListener = pageListener;
        TradeShelfPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    /**
     * --------------------- BaseViewModel start ---------------------
     **/
    @Override
    public void onScanResult(String input) {
        TradeShelfPage page = getCurrentPage();
        TradeShelfPage.Phase phase = page.getPhase();
        if (phase == TradeShelfPage.Phase.START) {
            requestSku(input);
        } else if (phase == TradeShelfPage.Phase.SKU) {
            fillPosition(input);
        }

//        else if (phase == TradeShelfPage.Phase.POSITION) {
//            fillCount(input);
//        }
    }

    @Override
    public void onInterrupt() {
        mIsRequesting = false;
        if (mSkuRequest != null)
            FreshRequest.getInstance().cancel(mSkuRequest.toString());
    }

    @Override
    public boolean isRequesting() {
        return mIsRequesting;
    }

    @Override
    public void registerContext(BaseActivity activity) {

    }

    @Override
    public void unregisterContext() {
        this.mPageListener = null;
        this.mModelWatcher = null;
        this.mSkuRequest = null;
    }
    /**
     * --------------------- BaseViewModel end ---------------------
     **/

    /**
     * --------------------- ModelList start ---------------------
     **/

    @Override
    public boolean hasBefore() {
        return mCurrentPageIndex != 0;
    }

    @Override
    public boolean hasNext() {
        return mCurrentPageIndex != mTradeShelfPages.size() - 1;
    }

    @Override
    public boolean isReady() {
        for (int i = 0; i < mTradeShelfPages.size(); i++) {
            TradeShelfPage page = mTradeShelfPages.get(i);
            if (page.getPhase() != TradeShelfPage.Phase.DONE)
                return false;
        }
        return true;
    }

    @Override
    public void navNext() {
        int len = mTradeShelfPages.size();

        TradeShelfPage currentPage = getCurrentPage();
        currentPage.getSkuModel().setCount(Integer.parseInt(count));


        if (mCurrentPageIndex++ == len - 1) {
            // 新增一页
            TradeShelfPage page = new TradeShelfPage(null);
            mTradeShelfPages.add(mCurrentPageIndex, page);
        }

        TradeShelfPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    @Override
    public void navBefore() {
        if (mCurrentPageIndex != 0) {
            mCurrentPageIndex--;
            TradeShelfPage page = getCurrentPage();
            mPageListener.onPageChanged(page);
        }
    }

    @Override
    public TradeShelfPage getCurrentPage() {
        return mTradeShelfPages.get(mCurrentPageIndex);
    }
    /**
     * --------------------- ModelList end ---------------------
     **/

    /**
     * 获取sku信息
     *
     * @param code
     */
    private void requestSku(String code) {
        if (!mIsRequesting) {
            mIsRequesting = true;
            mSkuRequest = new SkuRequest(code, mSkuModelListener, this);
            FreshRequest.getInstance().addToRequestQueue(mSkuRequest, this.toString());
        }
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillPosition(String code) {
        TradeShelfPage page = getCurrentPage();
        page.getSkuModel().setPosition(code);
        page.advance();
        mModelWatcher.onPageUpdate(page);
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillCount(String code) {
        TradeShelfPage page = getCurrentPage();
        if (Utils.filterInput(code.equals(page.getSkuModel().getBarcode()), false)) {
            addItem(page, 1);
        } else if (Utils.isDigital(code)) {
            try {
                int count = Integer.parseInt(code);
                setItem(page, count);
            } catch (Exception e) {
                SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                        mSkuCountListener, this);
                FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
            }
        } else {
            SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                    mSkuCountListener, this);
            FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
        }


        //-----------------------------------------------------------------------------
////        if (TextUtils.isDigitsOnly(code))
//        try {
//            int count = Integer.parseInt(code);
//            TradeShelfPage page = getCurrentPage();
//            page.getSkuModel().setCount(count);
//            page.advance();
//            mModelWatcher.onPageUpdate(page);
//        } catch (Exception e) {
//            e.printStackTrace();
//            mModelWatcher.onError("请输入数字");
//        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        clearRequestState();
    }

    public void onResponse(final ResponseSku response) {
        clearRequestState();

        if (response != null && response.isSuccess()) {

            long skuId = response.getModule().getSkuId();
            String token = Login.getInstance().getToken();
            String s = RequestUtils.INTO_STORAGE + "request=" +
                    "{token:\"" + token + "\",data:\"" + skuId + "\"}";

            //获取库位的请求
            GetProtocol protocol = new GetProtocol(s);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    linkedData(response, json);
                }

                @Override
                public void onFail() {
                }
            });
        }

    }

    private void linkedData(ResponseSku response, String json) {


        Gson gson = new Gson();
        CuWeiBean bean = gson.fromJson(json, CuWeiBean.class);

        if (bean != null && bean.target.size() > 0) {

            TradeShelfPage page = getCurrentPage();
            page.setSkuModel(response.getModule());
            if (bean.target.get(0).pisitionVO != null) {

                String code = bean.target.get(0).pisitionVO.code;
                page.getSkuModel().setCuwei(code + "");
            }


            page.advance();
            mPageListener.onPageChanged(page);
        } else {
            ToastUtils.showToast("没有更多数据");
        }


    }

    private void clearRequestState() {
        mIsRequesting = false;
    }

    public List<TradeShelfPage> getTradeShelfPages() {
        return mTradeShelfPages;
    }

    private String count;

    public void setCount(String count) {
        this.count = count;
    }
}
