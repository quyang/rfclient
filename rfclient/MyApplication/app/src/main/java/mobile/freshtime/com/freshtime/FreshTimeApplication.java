package mobile.freshtime.com.freshtime;

import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;

import java.util.concurrent.TimeUnit;

import mobile.freshtime.com.freshtime.utils.GsonObject;
import okhttp3.OkHttpClient;

/**
 * Created by orkid on 2016/9/12.
 */
public class FreshTimeApplication extends Application {

    public static Context context;

    public static OkHttpClient mOkHttpClient;

    //便携式打印机
    public mobile.freshtime.com.freshtime.function.shangjia.storage.printer.JQPrinter printer = null;

    public mobile.freshtime.com.freshtime.function.storage.printer.JQPrinter printer1 = null;

    public BluetoothAdapter btAdapter = null;

    @Override
    public void onCreate() {
        super.onCreate();
        context = this.getApplicationContext();

        init();

    }

    public static OkHttpClient getOk() {
        return mOkHttpClient;
    }

    public void init() {
        mOkHttpClient = new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
    }


}
