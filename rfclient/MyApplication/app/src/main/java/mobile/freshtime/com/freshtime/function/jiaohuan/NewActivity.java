package mobile.freshtime.com.freshtime.function.jiaohuan;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.function.search.SearchBean;
import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * 库位移动
 */
public class NewActivity extends BaseActivity implements ViewInterface {

    private NoInputEditText mInput;
    private TextView mBack;
    private LinearLayout mLlContainer;
    private NewActivityPresenter mPresenter;
    private TextView mName;
    private TextView mKuwei;
    private TextView mKuwei_input;
    private Button mYiku;
    private Button mBangding;

    private boolean isFirst = true;
    private Button mRetry;
    private TextView mCode69;
    private TextView mSku;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new);

        mInput = (NoInputEditText) findViewById(R.id.item_input);
        mBack = (TextView) findViewById(R.id.back);
        mLlContainer = (LinearLayout) findViewById(R.id.ll_container);
        mName = (TextView) findViewById(R.id.name_input);
        //现库位
        mKuwei = (TextView) findViewById(R.id.kuwei);
        //目标库位
        mKuwei_input = (TextView) findViewById(R.id.kuwei_input);
        //移库
        mYiku = (Button) findViewById(R.id.button);
        //绑定
        mBangding = (Button) findViewById(R.id.button2);
        //重置
        mRetry = (Button) findViewById(R.id.button3);
        mCode69 = (TextView) findViewById(R.id.item_69code);
        mSku = (TextView) findViewById(R.id.sku);

        mRetry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mInput.setText("");
                mKuwei_input.setText("");
                mInput.requestFocus();
                isFirst = true;
            }
        });

        mPresenter = new NewActivityPresenter(this);
        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onScanResult(String result) {

        if (isFirst) {
            mInput.setText(result.trim());

            if (!NetUtils.hasNet(this)) {
                Toast.makeText(NewActivity.this, "请检查网络环境", Toast.LENGTH_LONG).show();
                return;
            }
            mPresenter.load(result.trim());

            isFirst = false;
        } else {
            mKuwei_input.setText(result.trim());

        }

    }

    @Override
    public void success(String json) {

//        try {
//            JSONObject object = new JSONObject(json);
//
//            boolean aBoolean = object.getBoolean("success");
//            if(aBoolean){
//
//                 Gson gson = new Gson();
//                CuWeiBean weiBean = gson.fromJson(json, CuWeiBean.class);
//
//                List<CuWeiBean.TargetBean> target = weiBean.target;
//                if (target.size() == 0) {
//                    //没有库位
//
//                } else {
//                    //有库位
//
//
//                }
//
//
//            }else{
//
//            }
//
//
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        //-----------------------------------------------------------------------------

        if (StringUtils.isEmpty(json)) {
            return;
        }

        Gson gson = new Gson();
        final SearchBean bean = gson.fromJson(json, SearchBean.class);

        if (bean == null) {
            return;
        }

        if (!bean.success) {
            Toast.makeText(NewActivity.this, "" + bean.errorMsg, Toast.LENGTH_LONG).show();
            return;
        }

        SearchBean.ModuleBean module = bean.module;
        String skuId = module.skuId;

        String s = RequestUtils.ADD_CODE + "request=" +
                "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + skuId + "\"}";

        if (!NetUtils.hasNet(NewActivity.this)) {
            Toast.makeText(NewActivity.this, "请检查网络环境", Toast.LENGTH_LONG).show();
            return;
        }

        GetProtocol protocol = new GetProtocol(s);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                linkData(bean, json);
            }

            @Override
            public void onFail() {
            }
        });

    }

    @Override
    public void fail() {
        Toast.makeText(NewActivity.this, "请求服务器失败", Toast.LENGTH_LONG).show();
    }

    private void linkData(SearchBean bean, String json) {


        mKuwei_input.requestFocus();

        SearchBean.ModuleBean module = bean.module;
        String title = module.title;//品名
        String skuId = module.skuId;
        mCode69.setText("69码: " + module.sku69Id);
        mSku.setText("skuId: " + module.skuId);
        mName.setText("商品: " + title);

        if (StringUtils.isEmpty(json)) {
            Toast.makeText(NewActivity.this, "没有更多数据", Toast.LENGTH_LONG).show();
            return;
        }

//        Gson gon = new Gson();
//        JiaoHanBean kuCun = gon.fromJson(json, JiaoHanBean.class);
//
//
//        //库存
//        if (kuCun == null) {
//            return;
//        }
//
//        if (kuCun != null && kuCun.getTarget() != null) {
//
//            Object pisitionVO = kuCun.getTarget().get(0).getPisitionVO();//.code;
//            if (pisitionVO != null) {
//                //没有库位
//                JiaoHanBean.TargetBean targetBean = kuCun.getTarget().get(0);
//                if (targetBean != null) {
//
//                    Toast.makeText(NewActivity.this, "没有绑定过库位", Toast.LENGTH_LONG).show();
//
//                    int skuId1 = targetBean.getSkuId();
//                    long positionId = targetBean.getPositionId();
//                    bangdign(skuId1 + "", positionId + "");
//                }
//
//            } else {
//                //有库位
//
//            }
////                //只有该商品有库位的时候才设置移库功能
////                if (NetUtils.hasNet(this)) {
////                    //绑定
////                    mYiku.setOnClickListener(new MyOnClickListener(skuId));
////                } else {
////                    Toast.makeText(NewActivity.this, "请检查网络环境", Toast.LENGTH_LONG).show();
////                }
//
//
//        }
    }

    //绑定库位
    private void bangdign(String skuid, String positionId) {
        mBangding.setOnClickListener(new BangDingClickListener(skuid, positionId));
    }

    //移库 解绑
    private class MyOnClickListener implements View.OnClickListener {

        private String skuId;

        public MyOnClickListener(String skuid) {
            this.skuId = skuid;
        }

        @Override
        public void onClick(View v) {

            if (StringUtils.isEmpty(mYiku.getText().toString().trim())) {
                Toast.makeText(NewActivity.this, "请输入目标库位", Toast.LENGTH_LONG).show();
                return;
            }

            String url = RequestUtils.DEL_CODE + "?request=" + "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + skuId + "\"}";
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    if (StringUtils.isEmpty(json)) {
                        return;
                    }

                    try {
                        JSONObject object = new JSONObject(json);
                        if (object.getBoolean("success")) {
                            Toast.makeText(NewActivity.this, "解绑成功", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(NewActivity.this, "解绑失败", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


                @Override
                public void onFail() {
                    Toast.makeText(NewActivity.this, "请求网络失败", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    //绑定
    private class BangDingClickListener implements View.OnClickListener {

        private String sk;
        private String positionId;

        public BangDingClickListener(String sku, String positionId) {
            this.sk = sku;
            this.positionId = positionId;
        }

        @Override
        public void onClick(View v) {

            String code = mKuwei_input.getText().toString().trim();//code
            if (StringUtils.isEmpty(code)) {
                Toast.makeText(NewActivity.this, "请先输入目标库位", Toast.LENGTH_LONG).show();
                return;
            }
            String url = RequestUtils.ADD_CODE + "?request={token:'" + Login.getInstance().getToken() + "',data:{skuId:" + sk + ",positionId:" + positionId + "}}";

            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    try {
                        JSONObject object = new JSONObject(json);
                        if (object.getBoolean("success")) {
                            Toast.makeText(NewActivity.this, "绑定成功", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(NewActivity.this, "绑定失败", Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
                    Toast.makeText(NewActivity.this, "请求网络失败", Toast.LENGTH_LONG).show();
                }
            });

        }
    }
}
