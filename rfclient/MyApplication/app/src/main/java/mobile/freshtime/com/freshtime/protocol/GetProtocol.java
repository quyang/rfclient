package mobile.freshtime.com.freshtime.protocol;

import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/10/28.
 */

public class GetProtocol extends BaseProtocal {


    public String url;

    public GetProtocol(String url) {
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public FormBody getFormBody() {
        return null;
    }




}
