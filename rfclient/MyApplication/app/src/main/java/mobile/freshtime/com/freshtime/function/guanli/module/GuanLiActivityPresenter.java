package mobile.freshtime.com.freshtime.function.guanli.module;

import com.google.gson.Gson;

import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.function.guanli.GuanLiActivity;
import mobile.freshtime.com.freshtime.function.search.SearchBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.protocol.PostProtocol;
import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/11/14.
 */
public class GuanLiActivityPresenter {


    private GuanLiActivity mActivity;

    public GuanLiActivityPresenter(GuanLiActivity activity) {
        mActivity = activity;
    }

    public void onScanResult(String input) {
        //获取skuid
        FormBody body = new FormBody.Builder()
                .add("skuCode", input)
                .build();
        PostProtocol protocol = new PostProtocol(body, RequestUtils.SKU);
        protocol.post();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                getgetCodes(json);
            }

            @Override
            public void onFail() {

            }
        });

    }

    //获取商品code
    private void getgetCodes(String json) {
        if (StringUtils.isEmpty(json)) {
            ToastUtils.showToast("没有更多数据");
            return;
        }

        Gson gson = new Gson();
        final SearchBean bean = gson.fromJson(json, SearchBean.class);

        if (bean == null) {
            return;
        }

        if (!bean.success) {
            ToastUtils.showToast("" + bean.errorMsg);
            return;
        }

        SearchBean.ModuleBean module = bean.module;

        if (module != null) {
            String skuId = module.skuId;

            String s = RequestUtils.INTO_STORAGE + "request=" +
                    "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + skuId + "\"}";

            GetProtocol protocol = new GetProtocol(s);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    mActivity.setCodes(bean, json);
                }

                @Override
                public void onFail() {
                }
            });
        }

    }
}
