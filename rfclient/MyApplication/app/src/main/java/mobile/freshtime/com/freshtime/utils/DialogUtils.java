package mobile.freshtime.com.freshtime.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import mobile.freshtime.com.freshtime.R;

/**
 * quyang
 * Created by Administrator on 2016/10/11.
 */

public class DialogUtils {

    private static DialogUtils instance = null;
    private Dialog mMDialog;
    private Button mYes;
    private Button mNo;

    public static DialogUtils getInstance(String content, Activity activity) {
        if (instance == null) {
            synchronized (DialogUtils.class) {
                if (instance == null) {
                    instance = new DialogUtils(content, activity);
                }
            }
        }
        return instance;
    }

    private DialogUtils(String content, Activity activity) {
        newAnotherDialog(activity, content);
    }

    public void showDialog() {
        mMDialog.show();
    }

    public void hideDialog() {
        mMDialog.dismiss();
    }

    public Button getOkButton() {
        return mYes;
    }

    public Button getNoButton() {
        return mNo;
    }

    private void newAnotherDialog(Activity mActivity, String content) {

        mMDialog = new Dialog(mActivity, R.style.MyDialog);
        View view = mActivity.getLayoutInflater().inflate(R.layout.cancel, null);
        TextView name = (TextView) view.findViewById(R.id.name);
        name.setText(content);
        mNo = (Button) view.findViewById(R.id.no);
        mYes = (Button) view.findViewById(R.id.yes);

        mMDialog.setCancelable(false);
        mMDialog.setContentView(view);

        //设置弹窗宽高
        mMDialog.getWindow().setLayout(UiUtils.getScreenHeight(mActivity).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

    }
}
