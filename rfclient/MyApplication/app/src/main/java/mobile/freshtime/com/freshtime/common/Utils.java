package mobile.freshtime.com.freshtime.common;

import android.text.TextUtils;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;

/**
 * Created by orchid on 16/10/16.
 */

public class Utils {

    public static boolean isTesting = false;
    public static boolean isOnline = true;

    public static int hasSku(SkuModel model, ArrayList<SkuModel> list) {
        for (int i = 0; i < list.size(); i++) {
            SkuModel target = list.get(i);
            if (target.getId() == model.getId())
                return i;
        }
        return -1;
    }

    public static String filterInput(String barcode, String defaultValue) {
        if (isTesting) return defaultValue;
        else return barcode;
    }

    public static boolean filterInput(boolean real, boolean defaultValue) {
        if (isTesting) return defaultValue;
        else return real;
    }

    public static String get69Code(String code) {
        if (TextUtils.isEmpty(code) &&
                code.startsWith("22") &&
                code.length() == 15) {
            return code.substring(2, 7);
        }
        return code;
    }

    public static boolean isDigital(String code) {
        return TextUtils.isDigitsOnly(code) && code.length() <= 4;
    }
}
