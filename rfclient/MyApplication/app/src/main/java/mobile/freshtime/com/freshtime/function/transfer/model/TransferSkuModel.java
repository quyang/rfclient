package mobile.freshtime.com.freshtime.function.transfer.model;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONObject;

import java.util.List;

import mobile.freshtime.com.freshtime.KuWeiInfo;
import mobile.freshtime.com.freshtime.function.search.PositionBarcodeInfo;

/**
 * Created by orchid on 16/9/21.
 */

public class TransferSkuModel implements Parcelable {


    private long id;
    private String title;
    private String subTitle;
    private long catId;
    private String picList;
    private String introduction;
    private String propertyVO;
    private String supplierName;
    private String purchasingAgentName;
    private int status;
    private int purchaseFlowStatus;
    private String checkReason;
    private String checkUserName;
    private String titletags;
    private String displayCheckInfo;
    private String displayCheckButton;
    private long skuId;
    private long sku69Id;
    private int productId;
    private double price;
    private String unStandardSpecification;
    private String specification;

    private String barcode;
    private String position;
    private String count;
    private long positionId;
    private long position2Id;
    public String kuwei; //商品的库位
    public int storageType;// 79


    //新加字段
    public int saleUnitType;//销售方式  6
    public String saleUnitStr;//销售单位 线下
    public int skuSpecificationUnit;//规格单位 6
    public int skuSpecificationNum;//规格数量
    public int skuUnit;//销售类型(单位)线下 6


    //转型
    public String specUnitZhuan;//规格单位
    public String saleUnitTypeZhuan;//销售方式
    public String saleUnitZhuan;//销售类型

    //移库的库位
    public String outKuwei;
    public String inKuwei;

    public String kucun;
    private String safeKucun;



    public List<KuWeiInfo> kuweiBeanList;
    private PositionBarcodeInfo mInfo;


    public String getSafeKucun() {
        return safeKucun;
    }

    public void setSafeKucun(String safeKucun) {
        this.safeKucun = safeKucun;
    }

    public String getInKuwei() {
        return inKuwei;
    }

    public void setInKuwei(String inKuwei) {
        this.inKuwei = inKuwei;
    }

    public String getOutKuwei() {
        return outKuwei;
    }

    public void setOutKuwei(String outKuwei) {
        this.outKuwei = outKuwei;
    }

    public String getSaleUnitTypeZhuan() {
        return saleUnitTypeZhuan;
    }

    public void setSaleUnitTypeZhuan(String saleUnitTypeZhuan) {
        this.saleUnitTypeZhuan = saleUnitTypeZhuan;
    }

    public String getSaleUnitZhuan() {
        return saleUnitZhuan;
    }

    public void setSaleUnitZhuan(String saleUnitZhuan) {
        this.saleUnitZhuan = saleUnitZhuan;
    }

    public String getSpecUnitZhuan() {
        return specUnitZhuan;
    }

    public void setSpecUnitZhuan(String specUnitZhuan) {
        this.specUnitZhuan = specUnitZhuan;
    }

    public PositionBarcodeInfo getInfo() {
        return mInfo;
    }

    public void setInfo(PositionBarcodeInfo info) {
        mInfo = info;
    }


    public List<KuWeiInfo> getKuweiBeanList() {
        return kuweiBeanList;
    }

    public void setKuweiBeanList(List<KuWeiInfo> kuweiBeanList) {
        this.kuweiBeanList = kuweiBeanList;
    }


    public int getStorageType() {
        return storageType;
    }

    public void setStorageType(int storageType) {
        this.storageType = storageType;
    }

    @Override
    public String toString() {
        return "SkuModel{" +
                "kuwei='" + kuwei + '\'' +
                '}';
    }

    public void setCuwei(String kuwei) {
        this.kuwei = kuwei;
    }

    public String getKuwei() {
        return kuwei;
    }

    public long getPosition2Id() {
        return position2Id;
    }

    public void setPosition2Id(long position2Id) {
        this.position2Id = position2Id;
    }


    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public TransferSkuModel() {

    }

    public TransferSkuModel(JSONObject jsonObject) {
        id = jsonObject.optLong("id", 0l);
        title = jsonObject.optString("title", "");
        subTitle = jsonObject.optString("subTitle", "");
        catId = jsonObject.optLong("catId", 0l);
        picList = jsonObject.optString("picList", "");
        introduction = jsonObject.optString("introduction", "");
        propertyVO = jsonObject.optString("propertyVO", "");
        supplierName = jsonObject.optString("supplierName", "");
        purchasingAgentName = jsonObject.optString("purchasingAgentName", "");
        status = jsonObject.optInt("status", 0);
        purchaseFlowStatus = jsonObject.optInt("purchaseFlowStatus", 0);
        checkReason = jsonObject.optString("checkReason", "");
        checkUserName = jsonObject.optString("checkUserName", "");
        titletags = jsonObject.optString("titletags", "");
        displayCheckInfo = jsonObject.optString("displayCheckInfo", "");
        displayCheckButton = jsonObject.optString("displayCheckButton", "");
        skuId = jsonObject.optLong("skuId", 0l);
        sku69Id = jsonObject.optLong("sku69Id", 0l);
        productId = jsonObject.optInt("productId", 0);
        price = jsonObject.optDouble("price", 0d);
        unStandardSpecification = jsonObject.optString("unStandardSpecification", "");
        specification = jsonObject.optString("specification", "");
        kuwei = jsonObject.optString("kuwei", "");
        storageType = jsonObject.optInt("storageType", 79);
        saleUnitType = jsonObject.optInt("saleUnitType", 0);
        saleUnitStr = jsonObject.optString("saleUnitStr", "");
        skuSpecificationUnit = jsonObject.optInt("skuSpecificationUnit", 0);
        skuSpecificationNum = jsonObject.optInt("skuSpecificationNum", 0);
        skuUnit = jsonObject.optInt("skuUnit", 0);
        specUnitZhuan = jsonObject.optString("specUnitZhuan", "");
        saleUnitTypeZhuan = jsonObject.optString("saleUnitTypeZhuan", "");
        saleUnitZhuan = jsonObject.optString("saleUnitZhuan", "");
        outKuwei = jsonObject.optString("outKuwei", "");
        inKuwei = jsonObject.optString("inKuwei", "");
        kucun = jsonObject.optString("kucun", "");
        safeKucun = jsonObject.optString("safeKucun", "");
    }

    public int getPurchaseFlowStatus() {
        return purchaseFlowStatus;
    }

    public void setPurchaseFlowStatus(int purchaseFlowStatus) {
        this.purchaseFlowStatus = purchaseFlowStatus;
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }


    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public long getCatId() {
        return catId;
    }

    public void setCatId(long catId) {
        this.catId = catId;
    }

    public String getCheckReason() {
        return checkReason;
    }

    public void setCheckReason(String checkReason) {
        this.checkReason = checkReason;
    }

    public String getCheckUserName() {
        return checkUserName;
    }

    public void setCheckUserName(String checkUserName) {
        this.checkUserName = checkUserName;
    }

    public String getDisplayCheckButton() {
        return displayCheckButton;
    }

    public void setDisplayCheckButton(String displayCheckButton) {
        this.displayCheckButton = displayCheckButton;
    }

    public long getSku69Id() {
        return sku69Id;
    }

    public void setSku69Id(long sku69Id) {
        this.sku69Id = sku69Id;
    }

    public String getDisplayCheckInfo() {
        return displayCheckInfo;
    }

    public void setDisplayCheckInfo(String displayCheckInfo) {
        this.displayCheckInfo = displayCheckInfo;
    }

    public long getId() {
        return id;
    }

    public long getPositionId() {
        return positionId;
    }

    public void setPositionId(long positionId) {
        this.positionId = positionId;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getPicList() {
        return picList;
    }

    public void setPicList(String picList) {
        this.picList = picList;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getPropertyVO() {
        return propertyVO;
    }

    public void setPropertyVO(String propertyVO) {
        this.propertyVO = propertyVO;
    }

    public String getPurchasingAgentName() {
        return purchasingAgentName;
    }

    public void setPurchasingAgentName(String purchasingAgentName) {
        this.purchasingAgentName = purchasingAgentName;
    }

    public long getSkuId() {
        return skuId;
    }

    public void setSkuId(long skuId) {
        this.skuId = skuId;
    }

    public String getUnStandardSpecification() {
        return unStandardSpecification;
    }

    public void setUnStandardSpecification(String unStandardSpecification) {
        this.unStandardSpecification = unStandardSpecification;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitletags() {
        return titletags;
    }

    public void setTitletags(String titletags) {
        this.titletags = titletags;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(id);
        dest.writeString(title);
        dest.writeString(subTitle);
        dest.writeLong(catId);
        dest.writeString(picList);
        dest.writeString(introduction);
        dest.writeString(propertyVO);
        dest.writeString(supplierName);
        dest.writeString(purchasingAgentName);
        dest.writeInt(status);
        dest.writeInt(purchaseFlowStatus);
        dest.writeString(checkReason);
        dest.writeString(checkUserName);
        dest.writeString(titletags);
        dest.writeString(displayCheckInfo);
        dest.writeString(displayCheckButton);
        dest.writeLong(skuId);
        dest.writeLong(sku69Id);
        dest.writeInt(productId);
        dest.writeDouble(price);
        dest.writeString(specification);
        dest.writeString(unStandardSpecification);
        dest.writeString(barcode);
        dest.writeString(position);
        dest.writeString(count);
        dest.writeLong(positionId);
        dest.writeLong(position2Id);
        dest.writeString(kuwei);
        dest.writeInt(storageType);
        dest.writeInt(saleUnitType);
        dest.writeString(saleUnitStr);
        dest.writeInt(skuSpecificationUnit);
        dest.writeInt(skuSpecificationNum);
        dest.writeInt(skuUnit);
        dest.writeString(specUnitZhuan);
        dest.writeString(saleUnitTypeZhuan);
        dest.writeString(saleUnitZhuan);
        dest.writeString(outKuwei);
        dest.writeString(inKuwei);
        dest.writeString(kucun);
        dest.writeString(safeKucun);


    }

    public static final Creator<TransferSkuModel> CREATOR =
            new Creator<TransferSkuModel>() {

                @Override
                public TransferSkuModel createFromParcel(Parcel source) {
                    TransferSkuModel model = new TransferSkuModel();
                    model.setId(source.readLong());
                    model.setTitle(source.readString());
                    model.setSubTitle(source.readString());
                    model.setCatId(source.readLong());
                    model.setPicList(source.readString());
                    model.setIntroduction(source.readString());
                    model.setPropertyVO(source.readString());
                    model.setSupplierName(source.readString());
                    model.setPurchasingAgentName(source.readString());
                    model.setStatus(source.readInt());
                    model.setPurchaseFlowStatus(source.readInt());
                    model.setCheckReason(source.readString());
                    model.setCheckUserName(source.readString());
                    model.setTitletags(source.readString());
                    model.setDisplayCheckInfo(source.readString());
                    model.setDisplayCheckButton(source.readString());
                    model.setSkuId(source.readLong());
                    model.setSku69Id(source.readLong());
                    model.setProductId(source.readInt());
                    model.setPrice(source.readDouble());
                    model.setSpecification(source.readString());
                    model.setUnStandardSpecification(source.readString());
                    model.setBarcode(source.readString());
                    model.setPosition(source.readString());
                    model.setCount(source.readString());
                    model.setPositionId(source.readLong());
                    model.setPosition2Id(source.readLong());
                    model.kuwei = source.readString();
                    model.storageType = source.readInt();
                    model.saleUnitType = source.readInt();
                    model.saleUnitStr = source.readString();
                    model.skuSpecificationUnit = source.readInt();
                    model.skuSpecificationNum = source.readInt();
                    model.skuUnit = source.readInt();
                    model.specUnitZhuan = source.readString();
                    model.saleUnitTypeZhuan = source.readString();
                    model.saleUnitZhuan = source.readString();
                    model.outKuwei = source.readString();
                    model.inKuwei = source.readString();
                    model.kucun = source.readString();
                    model.safeKucun = source.readString();

                    return model;
                }

                @Override
                public TransferSkuModel[] newArray(int size) {
                    return new TransferSkuModel[size];
                }
            };


}
