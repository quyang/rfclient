package mobile.freshtime.com.freshtime.function.reportloss.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by orkid on 2016/9/13.
 */
public class LossUnit implements Parcelable {

    private String skuBarcode;
    private String spoilageNo;
    private String positionBarcode;

    public LossUnit() {
    }

    public LossUnit(String sku, String spoil, String position) {
        this.skuBarcode = sku;
        this.spoilageNo = spoil;
        this.positionBarcode = position;
    }

    public String getPositionBarcode() {
        return positionBarcode;
    }

    public String getSkuBarcode() {
        return skuBarcode;
    }

    public String getSpoilageNo() {
        return spoilageNo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(skuBarcode);
        dest.writeString(spoilageNo);
        dest.writeString(positionBarcode);
    }

    public static final Parcelable.Creator<LossUnit> CREATOR =
            new Parcelable.Creator<LossUnit>() {

                @Override
                public LossUnit createFromParcel(Parcel source) {
                    return null;
                }

                @Override
                public LossUnit[] newArray(int size) {
                    return new LossUnit[0];
                }
            };
}
