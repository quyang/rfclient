package mobile.freshtime.com.freshtime.function.kuweitiaozheng.module;

import java.util.List;

/**
 * Created by Administrator on 2016/12/13.
 */

public class PositonBean {


    /**
     * message : null
     * return_code : 0
     * success : true
     * target : [{"agencyId":1,"areaId":13,"areaType":null,"dr":0,"id":41716,"maySale":null,"number":5,"numberReal":5000,"numberShow":"5","positionCode":null,"positionId":388,"saleUnit":null,"saleUnitId":null,"saleUnitType":null,"skuId":40088,"skuName":null,"skuPlu":null,"slottingId":null,"slottingType":null,"spec":null,"specUnit":null,"specUnitId":null,"ts":"2016-11-22T17:04:53"},{"agencyId":1,"areaId":5,"areaType":null,"dr":0,"id":42794,"maySale":null,"number":1,"numberReal":1000,"numberShow":"1","positionCode":"ZC04","positionId":875,"saleUnit":"只","saleUnitId":68,"saleUnitType":1,"skuId":40088,"skuName":"鲜鲍鱼（2只起售）","skuPlu":null,"slottingId":null,"slottingType":null,"spec":null,"specUnit":null,"specUnitId":null,"ts":"2016-12-09T14:33:09"},{"agencyId":1,"areaId":13,"areaType":null,"dr":0,"id":42795,"maySale":null,"number":7,"numberReal":7000,"numberShow":"7","positionCode":"QC02803","positionId":573,"saleUnit":"只","saleUnitId":68,"saleUnitType":1,"skuId":40088,"skuName":"鲜鲍鱼（2只起售）","skuPlu":null,"slottingId":null,"slottingType":null,"spec":null,"specUnit":null,"specUnitId":null,"ts":"2016-12-13T14:21:49"},{"agencyId":1,"areaId":13,"areaType":null,"dr":0,"id":42797,"maySale":null,"number":2,"numberReal":2000,"numberShow":"2","positionCode":"QC02805","positionId":575,"saleUnit":"只","saleUnitId":68,"saleUnitType":1,"skuId":40088,"skuName":"鲜鲍鱼（2只起售）","skuPlu":null,"slottingId":null,"slottingType":null,"spec":null,"specUnit":null,"specUnitId":null,"ts":"2016-12-09T14:33:15"}]
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * areaId : 13
     * areaType : null
     * dr : 0
     * id : 41716
     * maySale : null
     * number : 5
     * numberReal : 5000
     * numberShow : 5
     * positionCode : null
     * positionId : 388
     * saleUnit : null
     * saleUnitId : null
     * saleUnitType : null
     * skuId : 40088
     * skuName : null
     * skuPlu : null
     * slottingId : null
     * slottingType : null
     * spec : null
     * specUnit : null
     * specUnitId : null
     * ts : 2016-11-22T17:04:53
     */

    private List<TargetBean> target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<TargetBean> getTarget() {
        return target;
    }

    public void setTarget(List<TargetBean> target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private int areaId;
        private Object areaType;
        private int dr;
        private int id;
        private Object maySale;
        private int number;
        private int numberReal;
        private String numberShow;
        private Object positionCode;
        private int positionId;
        private Object saleUnit;
        private Object saleUnitId;
        private Object saleUnitType;
        private int skuId;
        private Object skuName;
        private Object skuPlu;
        private Object slottingId;
        private Object slottingType;
        private Object spec;
        private Object specUnit;
        private Object specUnitId;
        private String ts;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public Object getAreaType() {
            return areaType;
        }

        public void setAreaType(Object areaType) {
            this.areaType = areaType;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getMaySale() {
            return maySale;
        }

        public void setMaySale(Object maySale) {
            this.maySale = maySale;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getNumberReal() {
            return numberReal;
        }

        public void setNumberReal(int numberReal) {
            this.numberReal = numberReal;
        }

        public String getNumberShow() {
            return numberShow;
        }

        public void setNumberShow(String numberShow) {
            this.numberShow = numberShow;
        }

        public Object getPositionCode() {
            return positionCode;
        }

        public void setPositionCode(Object positionCode) {
            this.positionCode = positionCode;
        }

        public int getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public Object getSaleUnit() {
            return saleUnit;
        }

        public void setSaleUnit(Object saleUnit) {
            this.saleUnit = saleUnit;
        }

        public Object getSaleUnitId() {
            return saleUnitId;
        }

        public void setSaleUnitId(Object saleUnitId) {
            this.saleUnitId = saleUnitId;
        }

        public Object getSaleUnitType() {
            return saleUnitType;
        }

        public void setSaleUnitType(Object saleUnitType) {
            this.saleUnitType = saleUnitType;
        }

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public Object getSkuName() {
            return skuName;
        }

        public void setSkuName(Object skuName) {
            this.skuName = skuName;
        }

        public Object getSkuPlu() {
            return skuPlu;
        }

        public void setSkuPlu(Object skuPlu) {
            this.skuPlu = skuPlu;
        }

        public Object getSlottingId() {
            return slottingId;
        }

        public void setSlottingId(Object slottingId) {
            this.slottingId = slottingId;
        }

        public Object getSlottingType() {
            return slottingType;
        }

        public void setSlottingType(Object slottingType) {
            this.slottingType = slottingType;
        }

        public Object getSpec() {
            return spec;
        }

        public void setSpec(Object spec) {
            this.spec = spec;
        }

        public Object getSpecUnit() {
            return specUnit;
        }

        public void setSpecUnit(Object specUnit) {
            this.specUnit = specUnit;
        }

        public Object getSpecUnitId() {
            return specUnitId;
        }

        public void setSpecUnitId(Object specUnitId) {
            this.specUnitId = specUnitId;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }
    }
}
