package mobile.freshtime.com.freshtime.common.model.delivery;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by orchid on 16/10/15.
 */

public class DeliverDetail implements Parcelable {

    public long agencyId;
    public long boxId;
    public long delivId;
    public long delivNo;
    public long distributionId;
    public long distributionNo;
    public long dr;
    public long id;
    public long orderNo;
    public long remainNo;
    public long skuId;
    public String ts;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(agencyId);
        dest.writeLong(boxId);
        dest.writeLong(delivId);
        dest.writeLong(delivNo);
        dest.writeLong(distributionId);
        dest.writeLong(distributionNo);
        dest.writeLong(dr);
        dest.writeLong(id);
        dest.writeLong(orderNo);
        dest.writeLong(remainNo);
        dest.writeLong(skuId);
        dest.writeString(ts);
    }

    public static final Parcelable.Creator<DeliverDetail> CREATOR =
            new Parcelable.Creator<DeliverDetail>() {

                @Override
                public DeliverDetail createFromParcel(Parcel source) {
                    DeliverDetail model = new DeliverDetail();
                    model.agencyId = source.readLong();
                    model.boxId = source.readLong();
                    model.delivId = source.readLong();
                    model.delivNo = source.readLong();
                    model.distributionId = source.readLong();
                    model.distributionNo = source.readLong();
                    model.dr = source.readLong();
                    model.id = source.readLong();
                    model.orderNo = source.readLong();
                    model.remainNo = source.readLong();
                    model.skuId = source.readLong();
                    model.ts = source.readString();
                    return model;
                }

                @Override
                public DeliverDetail[] newArray(int size) {
                    return new DeliverDetail[size];
                }
            };
}
