package mobile.freshtime.com.freshtime.function.pickwall.model;

/**
 * Created by orchid on 16/10/14.
 */

public class PickDetail {

    public String agencyId;
    public String areaId;
    public String basketBarcode;
    public long basketId;
    public long dr;
    public long id;
    public String oos;
    public long orderNo;
    public long pickId;
    public long pickedNo;
    public String positionBarcode;
    public long positionId;
    public String skuBarcode;
    public long skuId;
    public String ts;

}
