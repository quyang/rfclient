package mobile.freshtime.com.freshtime.utils;

import android.text.format.DateFormat;

/**
 * Created by Administrator on 2016/10/12.
 */

public class DateUtils {

    /**
     * 格式化当前系统时间,只包括时间
     * @return
     */
    public static String getFormedTime(){
         return (String) DateFormat.format("HH:mm", System.currentTimeMillis());
    }

    /**
     * 格式化传入的事件 时分
     * @return
     */
    public static String getFormedTime(String time){
         return (String) DateFormat.format("HH:mm", Integer.parseInt(time));
    }


    /**
     * 格式化当前系统时间,包括年月
     * @return
     */
    public static String getFormedTimeWitheData(String ab){
//         return (String) DateFormat.format("HH:mm", Long.parseLong(ab));
         return (String) DateFormat.format("yyyy-MM-dd HH:mm", Long.parseLong(ab));
    }


}
