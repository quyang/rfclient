package mobile.freshtime.com.freshtime.common.model.sku;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orchid on 16/9/28.
 */

public class ResponseSkuList extends BaseResponse {

    private List<SkuModel> module;

    public ResponseSkuList(JSONObject jsonObject) {
        return_code = jsonObject.optInt("resultCode", 0);
        success = jsonObject.optBoolean("success", false);
        message = jsonObject.optString("errorMsg", "");
        module = new ArrayList<>();
        try {
            JSONArray array = jsonObject.optJSONArray("module");
            if (array != null) {//quyang
                for (int i = 0; i < array.length(); i++) {
                    SkuModel model = new SkuModel(array.getJSONObject(i));
                    module.add(model);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<SkuModel> getModule() {
        return module;
    }

    public void setModule(List<SkuModel> module) {
        this.module = module;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
