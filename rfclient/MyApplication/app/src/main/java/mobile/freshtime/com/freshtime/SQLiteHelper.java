package mobile.freshtime.com.freshtime;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Administrator on 2016/11/1.
 */

public class SQLiteHelper extends SQLiteOpenHelper {


    private final static String TABLE_NAME = "qiangwei";

    public SQLiteHelper(Context context) {
        super(context, Contants.QIANGWEI_DB, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE fengjianqiang (_id integer primary key autoincrement, barcode varchar(20), code varchar(20))");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
