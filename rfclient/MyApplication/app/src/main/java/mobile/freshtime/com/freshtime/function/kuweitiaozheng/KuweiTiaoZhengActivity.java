package mobile.freshtime.com.freshtime.function.kuweitiaozheng;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.function.guanli.module.ItemInfo;
import mobile.freshtime.com.freshtime.function.kuweitiaozheng.module.KuweiTiaoZhengActivityModule;
import mobile.freshtime.com.freshtime.utils.ProgressDialogUtils;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;

import static mobile.freshtime.com.freshtime.R.id.item_info_root;

/**
 * 库存调整
 */
public class KuweiTiaoZhengActivity extends BaseActivity implements View.OnClickListener {


    private KuweiTiaoZhengActivityModule mModule;
    public TextView mItemCode;
    private TextView mName;
    private TextView mSkuId;
    private TextView mPid;
    private TextView mKucun;
    private TextView mCoder;
    public TextView mCodeTiaoMa;
    private TextView mNewKucun;
    private TextView mLast;
    private TextView mTiaozheng;
    private ViewGroup mItem_info_root;
    public TextView mNewCount;
    private TextView mBack;
    private TextView mSafeKucun;
    private TextView mGetInfo;
    private Dialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kuwei_tiao_zheng);

        mItem_info_root = (ViewGroup) findViewById(item_info_root);

        mItem_info_root.setVisibility(View.INVISIBLE);

        mBack = (TextView) findViewById(R.id.back);
        mName = (TextView) findViewById(R.id.item_name);
        mSkuId = (TextView) findViewById(R.id.item_code);
        mPid = (TextView) findViewById(R.id.item_69code);
        mKucun = (TextView) findViewById(R.id.item_coder);
        mCoder = (TextView) findViewById(R.id.coder);
        mNewCount = (TextView) findViewById(R.id.new_count);
        mNewCount.setInputType(InputType.TYPE_CLASS_NUMBER);
        mCoder.setVisibility(View.VISIBLE);
        mGetInfo = (TextView) findViewById(R.id.get);

        mGetInfo.setOnClickListener(this);

        mSafeKucun = (TextView) findViewById(R.id.safe_kucun);
        mSafeKucun.setVisibility(View.VISIBLE);

        mLast = (TextView) findViewById(R.id.last);
        mTiaozheng = (TextView) findViewById(R.id.bangding);

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mLast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mTiaozheng.setOnClickListener(new MyOnClickListener());

        mKucun.setText("原总库存:");

        mCodeTiaoMa = (TextView) findViewById(R.id.code_tiaoma);
        mNewKucun = (TextView) findViewById(R.id.new_kucun);
        mItemCode = (TextView) findViewById(R.id.scanContent);
        mItemCode.setInputType(InputType.TYPE_CLASS_NUMBER);

        mItemCode.requestFocus();

        mModule = new KuweiTiaoZhengActivityModule(this);
        mModule.setOnPageListener(this);

    }


    @Override
    public void onScanResult(String result) {
        mModule.onScanResult(result.trim());
    }


    public void onPageChange(ItemInfo mInfo) {

        mItem_info_root.setVisibility(View.VISIBLE);

        mItemCode.setText("" + mInfo.itemCode);
        mName.setText(mInfo.title + "");
        mSkuId.setText(mInfo.skuId + "");
        mPid.setText("pid: " + mInfo.pid + "");
        mKucun.setText("原库存数量: " + mInfo.oldNum);
        mCoder.setText("库位: " + mInfo.coder);
        mCodeTiaoMa.setText("" + mInfo.barcode);
        mSafeKucun.setText("安全库存" + mInfo.safeKucun);

    }


    private Dialog mMMDialog;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.get:
                if (StringUtils.isKongStr(mItemCode.getText().toString().trim())) {
                    Toast.makeText(getApplicationContext(), "请先输入条码", Toast.LENGTH_LONG).show();
                } else {
                    mModule.getAllKuCun(mItemCode.getText().toString().trim());
                }
                break;
        }
    }

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            mMMDialog = new Dialog(KuweiTiaoZhengActivity.this, R.style.MyDialog);
            View view = getLayoutInflater().inflate(R.layout.cancel, null);
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(getResources().getString(R.string.submit_adust));
            Button mNo = (Button) view.findViewById(R.id.no);
            Button mYes = (Button) view.findViewById(R.id.yes);

            mMMDialog.setCancelable(false);
            mMMDialog.setContentView(view);

            //设置弹窗宽高
            mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(KuweiTiaoZhengActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

            mMMDialog.show();

            mYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMMDialog.dismiss();
                    mProgressDialog = ProgressDialogUtils.getProgressDialog(KuweiTiaoZhengActivity.this);
                    mProgressDialog.show();
                    mModule.update();
                }
            });

            mNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMMDialog.dismiss();
                }
            });
        }
    }

    public Dialog getMMDialog() {
        return mProgressDialog;
    }


}
