package mobile.freshtime.com.freshtime.function.tradeshelf;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.Contants;
import mobile.freshtime.com.freshtime.utils.DateUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.SPUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.common.view.expandable.ExpandableLayoutListView;
import mobile.freshtime.com.freshtime.common.adapter.ExpandableAdapter;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.RequestUtils;

/**
 *
 * Created by orchid on 16/9/29.
 */

public class TradeShelfConfirmActivity extends BaseActivity implements View.OnClickListener,
        Response.ErrorListener, Response.Listener<BaseResponse> {

    private ArrayList<SkuModel> mSkuModels;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        ViewGroup topView = (ViewGroup) findViewById(R.id.top_root);
        TextView time = (TextView) topView.findViewById(R.id.time);
        TextView operator = (TextView) topView.findViewById(R.id.operator);

        operator.setText("操作人: " + SPUtils.getStringValue(this, Contants.SP_FILE_NAME, Contants.OPERATER, null));
        time.setText("操作时间: " + DateUtils.getFormedTimeWitheData(System.currentTimeMillis() + ""));

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);

        mSkuModels = getIntent().getParcelableArrayListExtra("units");
        ExpandableAdapter adapter = new ExpandableAdapter(this, mSkuModels,"呵呵");
        ExpandableLayoutListView listView = (ExpandableLayoutListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {
            submitLoss();
        }
    }

    @Override
    public void onScanResult(String result) {
        // TODO Nothing...
    }

    /**
     * 提交数据
     */
    private void submitLoss() {
        JSONArray skus = new JSONArray();
        JSONObject param = new JSONObject();
        try {
            param.put("token", 1);
            for (int i = 0; i < mSkuModels.size(); i++) {
                JSONObject sku = new JSONObject();
                sku.put("skuBarcode", 1);
                sku.put("spoilageNo", 1);
                sku.put("positionBarcode", 1);
                skus.put(sku);
                param.put("data", skus);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        BaseRequest<BaseResponse> mRequest = new BaseRequest<>(
                RequestUtils.LOSS,
                BaseResponse.class,
                param,
                this, this);
        FreshRequest.getInstance().addToRequestQueue(mRequest, this.toString());
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(BaseResponse response) {
        if (response != null && response.isSuccess()) {
            finish();
        }
    }
}