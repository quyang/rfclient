package mobile.freshtime.com.freshtime.function.stock.module;

import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.function.stock.FirstStockActivity;
import mobile.freshtime.com.freshtime.function.stock.TwoPanListActivity;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * 盘点列表适配器
 * Created by Administrator on 2016/11/15.
 */
public class TwoPandianListAdapter extends BaseAdapter {

    private List<StocktakingInProgressInfo.TargetBean.ItemsBean> mList;

    private FirstStockActivity mActivity;

    private Dialog mMMDialog;

    private static final int KEY = R.id.expandableLayout;

    public TwoPandianListAdapter(List<StocktakingInProgressInfo.TargetBean.ItemsBean> list, FirstStockActivity activity) {
        mList = list;
        mActivity = activity;
        System.out.println("TwoPandianListAdapter.TwoPandianListAdapter=" + mList.size());
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public StocktakingInProgressInfo.TargetBean.ItemsBean getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            convertView = View.inflate(mActivity, R.layout.listitem_pandian, null);
            holder = new ViewHolder();

            //body
            holder.faqiren = (TextView) convertView.findViewById(R.id.item_name);
            holder.time = (TextView) convertView.findViewById(R.id.item_count);
            holder.pandianAdress = (TextView) convertView.findViewById(R.id.item_code);

            //header
            holder.pandianTask = (TextView) convertView.findViewById(R.id.sku_info);
            holder.pandianQuyu = (TextView) convertView.findViewById(R.id.sku_code);
            holder.enter = (TextView) convertView.findViewById(R.id.item_detail);
            holder.commit = (TextView) convertView.findViewById(R.id.commit);

            convertView.setTag(KEY, holder);

        } else {
            holder = (ViewHolder) convertView.getTag(KEY);
        }

        final StocktakingInProgressInfo.TargetBean.ItemsBean item = getItem(position);

        holder.faqiren.setText("发起人: " + item.getOperate());
        holder.time.setText("发起时间: " + item.getOpTime().replace("T", " "));
        holder.pandianAdress.setText("盘点区域: " + item.getAgencyId());
        holder.pandianTask.setText("盘点任务: " + item.getId());

        holder.enter.setVisibility(View.VISIBLE);

        holder.commit.setVisibility(View.GONE);

        holder.enter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, TwoPanListActivity.class);
                intent.putExtra("id", position);
                intent.putExtra("item", item);
                mActivity.startActivity(intent);
            }
        });

//        if (item.isChanged()) {
//            holder.commit.setVisibility(View.GONE);
//        } else {
//            holder.commit.setVisibility(View.GONE);
//        }

//        holder.commit.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                mMMDialog = new Dialog(mActivity, R.style.MyDialog);
//                View view = mActivity.getLayoutInflater().inflate(R.layout.cancel, null);
//                TextView name = (TextView) view.findViewById(R.id.name);
//                name.setText("确定要提交吗?");
//                Button mNo = (Button) view.findViewById(R.id.no);
//                Button mYes = (Button) view.findViewById(R.id.yes);
//
//                mMMDialog.setCancelable(false);
//                mMMDialog.setContentView(view);
//
//                //设置弹窗宽高
//                mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(mActivity).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);
//
//                mMMDialog.show();
//
//                mYes.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        shenghe(item.getId());
//                    }
//                });
//
//                mNo.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        mMMDialog.dismiss();
//                    }
//                });
//
//
//            }
//        });

        return convertView;
    }


    public static class ViewHolder {

        public TextView faqiren;
        public TextView time;
        public TextView pandianAdress;
        public TextView pandianTask;
        public TextView pandianQuyu;
        public TextView enter;
        public TextView commit;

    }


    //审核 二盘
    private void shenghe(int id) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", id + "");//任务id

            GetProtocol protocol = new GetProtocol(RequestUtils.AUDIT + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("TwoPandianListAdapter.onSuccess" + "=" + json);
                    try {
                        JSONObject object1 = new JSONObject(json);

                        if (object1.getBoolean("success")) {

                            mMMDialog.dismiss();

                            mActivity.getTwoPanTask();
//                                TwoStatusInfo info = new TwoStatusInfo();
//                                EventBus.getDefault().post(info);
//
//                                Intent intent = new Intent(TwoPanListActivity.this, FirstStockActivity.class);
//                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                startActivity(intent);

                        } else {
                            ToastUtils.showToast("提交审核失败");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

