package mobile.freshtime.com.freshtime.function.pickwall.model;

import java.util.List;

/**
 * Created by orchid on 16/10/14.
 */

public class PickWall {

    public int agencyId;
    public int areaId;
    public String createTime;
    public String details;
    public int dr;
    public String finishTime;
    public int id;
    public int operator;
    public List<PickDetail> pickDetails;
    public List<PickVO> pickVOs;
    public String pickingBaskets;
    public String pickingWallPositionBarcode;
    public String pickingWallPositionCode;
    public String pickingWallPositionID;
    public List<PickingWallPosition> pickingWallPositions;
    public int pickup;
    public int single;
    public int statu;
    public String ts;

}
