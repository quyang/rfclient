package mobile.freshtime.com.freshtime.function.stock.module;

import mobile.freshtime.com.freshtime.function.pick.model.Task;

/**
 * Created by Administrator on 2016/11/15.
 */

public class StockPage {


    private InSkuModel mSkuModel;
    private StockPage.Phase mPhase;

    public InSkuModel getSkuModel() {
        return mSkuModel;
    }

    public void setSkuModel(InSkuModel skuModel) {
        mSkuModel = skuModel;
    }


    public StockPage(Task task) {
        this.mPhase = StockPage.Phase.START;

    }

    public StockPage.Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == StockPage.Phase.START) {
            mPhase = Phase.SKU;
        } else if (mPhase == Phase.SKU) {
            mPhase = Phase.COUNT;
        } else if(mPhase == Phase.COUNT){
            mPhase = Phase.DONE;
        }
    }

    public enum Phase {
        START,
        SKU,
        COUNT,
        DONE
    }
}
