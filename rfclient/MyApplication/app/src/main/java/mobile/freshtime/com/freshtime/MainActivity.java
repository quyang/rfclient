package mobile.freshtime.com.freshtime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import mobile.freshtime.com.freshtime.function.handover.HandoverActivity;
import mobile.freshtime.com.freshtime.function.outgoing.OutgoActivity;
import mobile.freshtime.com.freshtime.function.pack.PackListActivity;
import mobile.freshtime.com.freshtime.function.pick.PickTaskListActivity;
import mobile.freshtime.com.freshtime.function.purchase.PurchaseListActivity;
import mobile.freshtime.com.freshtime.function.reportloss.ReportLossActivity;
import mobile.freshtime.com.freshtime.function.requisition.RequisitionListActivity;
import mobile.freshtime.com.freshtime.function.restore.RestoreListActivity;
import mobile.freshtime.com.freshtime.function.storage.StorageActivity;
import mobile.freshtime.com.freshtime.function.tradeshelf.TradeShelfActivity;
import mobile.freshtime.com.freshtime.function.transfer.TransferActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



//        findViewById(R.id.login).setOnClickListener(this);
        findViewById(R.id.reportLoss).setOnClickListener(this);
        findViewById(R.id.purchase).setOnClickListener(this);
//        findViewById(R.id.menu).setOnClickListener(this);
        findViewById(R.id.export).setOnClickListener(this);
        findViewById(R.id.handback).setOnClickListener(this);
        findViewById(R.id.pick).setOnClickListener(this);
        findViewById(R.id.pickWall).setOnClickListener(this);
        findViewById(R.id.handover).setOnClickListener(this);
//        findViewById(R.id.storage).setOnClickListener(this);
        findViewById(R.id.lendExport).setOnClickListener(this);
        findViewById(R.id.stock).setOnClickListener(this);
        findViewById(R.id.inventory).setOnClickListener(this);
        findViewById(R.id.inventoryShelf).setOnClickListener(this);
        findViewById(R.id.delivery).setOnClickListener(this);
        findViewById(R.id.imports).setOnClickListener(this);
        findViewById(R.id.deliveryGoods).setOnClickListener(this);
        findViewById(R.id.shelf).setOnClickListener(this);
        findViewById(R.id.check).setOnClickListener(this);
        findViewById(R.id.transport).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
//            case R.id.login:
//                startActivity(new Intent(this, LoginActivity.class));
//                break;
//            case R.id.menu:
//                startActivity(new Intent(this, MenuActivity.class));
//                break;
            case R.id.reportLoss:
                startActivity(new Intent(this, ReportLossActivity.class));
                break;
            case R.id.export:
                startActivity(new Intent(this, OutgoActivity.class));
                break;
            case R.id.imports:
                startActivity(new Intent(this, StorageActivity.class));
                break;
            case R.id.pick:
                startActivity(new Intent(this, PickTaskListActivity.class));
                break;
            case R.id.handback:
                startActivity(new Intent(this, RestoreListActivity.class));
                break;
            case R.id.handover:
                startActivity(new Intent(this, HandoverActivity.class));
                break;
            case R.id.shelf:
                startActivity(new Intent(this, TradeShelfActivity.class));
                break;
            case R.id.purchase:
                startActivity(new Intent(this, PurchaseListActivity.class));
                break;
            case R.id.transport:
                startActivity(new Intent(this, TransferActivity.class));
                break;
            case R.id.lendExport:
                startActivity(new Intent(this, RequisitionListActivity.class));
                break;
            case R.id.inventoryShelf:
                startActivity(new Intent(this, PackListActivity.class));
                break;
        }
    }
}
