package mobile.freshtime.com.freshtime.common.model.delivery;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by orchid on 16/10/15.
 */

public class ResponseUserTask implements Parcelable{

    public String message;
    public int return_code;
    public boolean success;
    public DeliverTask target;

    protected ResponseUserTask(Parcel in) {
        message = in.readString();
        return_code = in.readInt();
        success = in.readByte() != 0;
        target = in.readParcelable(DeliverTask.class.getClassLoader());
    }

    public static final Creator<ResponseUserTask> CREATOR = new Creator<ResponseUserTask>() {
        @Override
        public ResponseUserTask createFromParcel(Parcel in) {
            return new ResponseUserTask(in);
        }

        @Override
        public ResponseUserTask[] newArray(int size) {
            return new ResponseUserTask[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
        dest.writeInt(return_code);
        dest.writeByte((byte) (success ? 1 : 0));
        dest.writeParcelable(target, flags);
    }
}
