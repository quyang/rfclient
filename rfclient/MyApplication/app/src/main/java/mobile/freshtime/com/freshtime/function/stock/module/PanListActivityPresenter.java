package mobile.freshtime.com.freshtime.function.stock.module;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import mobile.freshtime.com.freshtime.FreshTimeApplication;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.function.stock.PanListActivity;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/11/24.
 */
public class PanListActivityPresenter {

    private PanListActivity mActivity;

    MediaType STRING_TYPE = MediaType.parse("application/x-www-form-urlencoded; charset=utf-8");

    public PanListActivityPresenter(PanListActivity activity) {
        mActivity = activity;
    }

    //提交一盘信息
    public void submitInfo(ArrayList<InSkuModel> skuModels, final int taskId, final int id) {

        if (!NetUtils.hasNet(mActivity)) {
            ToastUtils.showToast(mActivity.getResources().getString(R.string.net_error));
            return;
        }


        if (skuModels.size() == 0) {
            ToastUtils.showToast("提交数据为空");
            return;
        }

        try {
            JSONArray array = new JSONArray();

            if (skuModels != null) {
                for (int i = 0; i < skuModels.size(); i++) {

                    JSONObject params = new JSONObject();

                    InSkuModel skuModel = skuModels.get(i);

                    params.put("skuId", skuModel.getSkuId());
                    params.put("positionId", skuModel.getPositionId());
                    params.put("stocktakingId", taskId);
                    params.put("saleUnitId", skuModel.skuUnit);
                    params.put("firstNo", skuModel.getCount());

                    array.put(params);
                }

                JSONObject object = new JSONObject();
                try {
                    object.put("token", Login.getInstance().getToken());
                    object.put("data", array);


                    System.out.println("PanListActivityPresenter.submitInfo=" + object);

                    String encode = null;
                    try {
                        encode = URLEncoder.encode(object.toString(), "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }


                    OkHttpClient okHttpClient = FreshTimeApplication.getOk();

                    RequestBody body = RequestBody.create(STRING_TYPE, "request=" + encode);
                    final Request request = new Request.Builder()
                            .post(body)
                            .url(RequestUtils.SUBMIT_STOCK)
                            .build();

                    okHttpClient.newCall(request).enqueue(new Callback() {
                        @Override
                        public void onFailure(Call call, IOException e) {
                            ToastUtils.showToast(mActivity.getResources().getString(R.string.net_error));
                        }

                        @Override
                        public void onResponse(Call call, Response response) throws IOException {
                            String json = response.body().string();
                            System.out.println("PanListActivityPresenter.onSuccess=" + json);
                            try {
                                JSONObject jsonObject = new JSONObject(json);
                                if (jsonObject.getBoolean("success")) {

                                    ToastUtils.showToast("提交成功");

                                    mActivity.getMMDialog().dismiss();

                                    TaskOver info = new TaskOver();
                                    EventBus.getDefault().post(info);

                                    mActivity.finish();
                                } else {
                                    mActivity.getMMDialog().dismiss();
                                    ToastUtils.showToast("" + jsonObject.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
