package mobile.freshtime.com.freshtime.function.transfer;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.Contants;
import mobile.freshtime.com.freshtime.utils.DateUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ProgressDialogUtils;
import mobile.freshtime.com.freshtime.utils.SPUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.view.expandable.ExpandableLayoutListView;
import mobile.freshtime.com.freshtime.function.transfer.model.TransferExpandableAdapter;
import mobile.freshtime.com.freshtime.function.transfer.model.TransferSkuModel;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * 移库确认
 * Created by orkid on 2016/10/19.
 */
public class TransferConfirmActivity extends BaseActivity implements View.OnClickListener,
        Response.ErrorListener, Response.Listener<BaseResponse> {

    private ArrayList<TransferSkuModel> mSkuModels;

    private Dialog mMMDialog;
    private Dialog mProgressDialog;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        ViewGroup topView = (ViewGroup) findViewById(R.id.top_root);
        TextView time = (TextView) topView.findViewById(R.id.time);
        TextView operator = (TextView) topView.findViewById(R.id.operator);

        operator.setText("操作人: " + SPUtils.getStringValue(this, Contants.SP_FILE_NAME, Contants.OPERATER, null));
        time.setText("操作时间: " + DateUtils.getFormedTimeWitheData(System.currentTimeMillis() + ""));


        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);
        TextView title = (TextView) findViewById(R.id.title);
        title.setText("移库确认");

        mSkuModels = getIntent().getParcelableArrayListExtra("units");
        String count = getIntent().getStringExtra("count");
        TransferExpandableAdapter adapter = new TransferExpandableAdapter(this, mSkuModels, count);
        ExpandableLayoutListView listView = (ExpandableLayoutListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {

            mMMDialog = new Dialog(TransferConfirmActivity.this, R.style.MyDialog);
            View view = getLayoutInflater().inflate(R.layout.cancel, null);
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText("确定要提交吗?");
            Button mNo = (Button) view.findViewById(R.id.no);
            Button mYes = (Button) view.findViewById(R.id.yes);

            mMMDialog.setCancelable(false);
            mMMDialog.setContentView(view);

            //设置弹窗宽高
            mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(TransferConfirmActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

            mMMDialog.show();

            mYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mSkuModels != null) {
                        mMMDialog.dismiss();
                        mProgressDialog = ProgressDialogUtils.getProgressDialog(TransferConfirmActivity.this);
                        mProgressDialog.show();
                        submit();
                    }
                }
            });

            mNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMMDialog.dismiss();
                }
            });
        }
    }

    @Override
    public void onScanResult(String result) {
        // TODO Nothing...
    }

    /**
     * 提交数据
     */
    private void submit() {

        if (mSkuModels.size() == 0) {
            ToastUtils.showToast(getResources().getString(R.string.no_data));
            return;
        }

        JSONArray skus = new JSONArray();
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            for (int i = 0; i < mSkuModels.size(); i++) {
                TransferSkuModel model = mSkuModels.get(i);
                JSONObject sku = new JSONObject();
                sku.put("skuId", model.getSkuId());
                sku.put("number", model.getCount());
                sku.put("positionOld", model.getPositionId());
                sku.put("positionNew", model.getPosition2Id());
                sku.put("skuUnit", model.skuUnit);
                skus.put(sku);
            }
            param.put("data", skus);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("TransferConfirmActivity.submit===" + param.toString());

        BaseRequest<BaseResponse> mRequest = new BaseRequest<>(
                RequestUtils.TRANSFER,
                BaseResponse.class,
                param,
                this, this);
        FreshRequest.getInstance().addToRequestQueue(mRequest, this.toString());
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(BaseResponse response) {
        if (response != null && response.isSuccess()) {
            Toast.makeText(this, "移库提交成功", Toast.LENGTH_SHORT).show();

            mProgressDialog.dismiss();

            Intent intent = new Intent(this, TransferActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            mProgressDialog.dismiss();
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }
}
