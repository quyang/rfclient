package mobile.freshtime.com.freshtime.function.outgoing.model;

import org.json.JSONObject;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orchid on 16/9/28.
 */

public class ResponseSkuOut extends BaseResponse {

    private OutSkuModel module;

    public ResponseSkuOut(JSONObject jsonObject) {
        return_code = jsonObject.optInt("resultCode", 0);
        success = jsonObject.optBoolean("success", false);
        message = jsonObject.optString("errorMsg", "");
        module = new OutSkuModel(jsonObject.optJSONObject("module"));
    }

    public OutSkuModel getModule() {
        return module;
    }

    public void setModule(OutSkuModel module) {
        this.module = module;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
