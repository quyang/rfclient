package mobile.freshtime.com.freshtime.function.pickwall.model;

/**
 * Created by orchid on 16/10/14.
 */

public class ResponsePickWall {

    public PickWall target;
    public int return_code;
    public String message;
    public boolean success;
}
