package mobile.freshtime.com.freshtime.function.search;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.function.storage.StorageBean;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * 库位管理
 */
public class SearchActivity extends BaseActivity implements ViewInterface, View.OnClickListener {


    @Bind(R.id.back)
    TextView mBack;
    @Bind(R.id.ll)
    LinearLayout mLl;
    @Bind(R.id.et_old)
    NoInputEditText mEtOld;
    @Bind(R.id.input_ll_old_code)
    LinearLayout mInputLlOldCode;
    @Bind(R.id.et_new)
    NoInputEditText mEtNew;
    @Bind(R.id.input_ll_new_code)
    LinearLayout mInputLlNewCode;

    @Bind(R.id.search)
    RadioButton mSearch;
    @Bind(R.id.bangding)
    RadioButton mBangding;
    @Bind(R.id.jiebang)
    RadioButton mJiebang;
    @Bind(R.id.rg)
    RadioGroup mRg;
    @Bind(R.id.activity_search)
    RelativeLayout mActivitySearch;
    @Bind(R.id.view1)
    TextView mView1;
    @Bind(R.id.view2)
    TextView mView2;
    @Bind(R.id.view3)
    TextView mView3;
    @Bind(R.id.view4)
    TextView mView4;
    @Bind(R.id.view5)
    TextView mView5;
    @Bind(R.id.view6)
    TextView mView6;
    @Bind(R.id.view7)
    TextView mView7;

    @Bind(R.id.view9)
    TextView mView9;
    @Bind(R.id.btn_ok)
    RadioButton mBtnOk;
    @Bind(R.id.sku_name)
    TextView mSkuName;

    public SearchActivityPresenter mPresenter;
    public NoInputEditText mBind_new_kuwei;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        mEtOld.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        mBind_new_kuwei = (NoInputEditText) findViewById(R.id.et_bind_new);
        findViewById(R.id.btn_ok).setOnClickListener(this);

        mPresenter = new SearchActivityPresenter(this);

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        MyClick click = new MyClick(this);
        mJiebang.setOnClickListener(click);
        mBack.setOnClickListener(click);
//        mBangding.setOnClickListener(click);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public void onScanResult(String result) {
        result = result.trim();

        boolean empty = result.trim().isEmpty();
        if (empty) {
            return;
        }

        if (!NetUtils.hasNet(this)) {
            Toast.makeText(SearchActivity.this, "连接网络失败", Toast.LENGTH_LONG).show();
        }

        if (mEtNew.hasFocus()) {
            //解绑
            mPresenter.onNewCuWeiResult(result);
            return;
        }

        if (mBind_new_kuwei.hasFocus()) {
            mPresenter.bindNewCode(result);
            return;
        }

        mPresenter.load(result);

    }


    private void realBD(final StorageBean bean) {
        if (StringUtils.isEmpty(Login.getInstance().getToken()) || StringUtils.isEmpty(mView4.getText().toString().trim()) || StringUtils.isEmpty(mView4.getText().toString().trim()) || StringUtils.isEmpty(bean.getTarget().getId() + "")) {
            throw new IllegalArgumentException("realBD 参数异常");
        }

        String url = RequestUtils.ADD_CODE + "?request={token:'" + Login.getInstance().getToken()
                + "',data:{skuId:" + mView4.getText().toString().trim() + ",positionId:" + bean.getTarget().getId() + "}}";

        if (!NetUtils.hasNet(this)) {
            Toast.makeText(SearchActivity.this, "请检查网络环境", Toast.LENGTH_LONG).show();
            return;
        }

        GetProtocol protocol = new GetProtocol(url);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                if (StringUtils.isEmpty(json)) {
                } else {
                    try {
                        JSONObject object = new JSONObject(json);
                        if (object.getBoolean("success")) {
                            ToastUtils.showToast("绑定成功");
                            mInputLlNewCode.setVisibility(View.GONE);
                            mView6.setText("库位: " + bean.getTarget().getCode());

                        } else {
                            ToastUtils.showToast("已绑定过");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFail() {
            }
        });
    }


    @Override
    public void success(String json) {

        if (json == null || "null".equals(json) || "".equals(json)) {
            return;
        }

        try {
            JSONObject object = new JSONObject(json);
            boolean success = object.getBoolean("success");
            if (success) {
                parseJson(json);
            } else {
//                ToastUtils.showToast("请扫描正确的条码");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void parseJson(final String json1) {

        if (StringUtils.isEmpty(json1)) {
            return;
        }

        Gson gson = new Gson();
        final SearchBean bean = gson.fromJson(json1, SearchBean.class);

        if (bean == null) {
            return;
        }

        SearchBean.ModuleBean module = bean.module;
        String skuId = module.skuId;

        mEtOld.setText(module.sku69Id);

        String s = RequestUtils.INTO_STORAGE + "request=" +
                "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + skuId + "\"}";

        GetProtocol protocol = new GetProtocol(s);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                linkData(bean, json);
            }

            @Override
            public void onFail() {
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void linkData(SearchBean bean, String json) {

        SearchBean.ModuleBean module = bean.module;
        String title = module.title;//品名
        String skuId = module.skuId;//skuId
        String price = module.unitPrice;//价格
        String code69 = module.sku69Id;//商品69码
        String guige = module.unStandardSpecification;//规格
        int storageType = module.storageType;//存储类型
        String changjia = (String) module.supplierName;//厂家


        mView1.setText("品名: " + title);
        mSkuName.setText("SkuId: ");
        mView4.setText(skuId);
        mView3.setText("价格: " + price);
        mView6.setText("69码: " + code69);
        mView5.setText("规格: " + guige);
        mView9.setText("厂家: " + changjia);
        mView2.setText("库存:" + title);

        String leixin = "";
        switch (storageType) {
            case 78:
                leixin = "活物";
                break;
            case 79:
                leixin = "常温";
                break;
            case 80:
                leixin = "恒温";
                break;
            case 81:
                leixin = "冷藏";
                break;
            case 82:
                leixin = "冷冻";
                break;
        }

        mView7.setText("存储类型: " + leixin);


        //-----------------------------------------------------------------------------

        Gson gon = new Gson();
        CuWeiBean kuCun = gon.fromJson(json, CuWeiBean.class);
        LinearLayout mLlContainer = (LinearLayout) findViewById(R.id.ll_container);

        mLlContainer.removeAllViews();

        //库存
        if ((kuCun.success == true) && kuCun != null && kuCun.target != null && kuCun.target.size() > 0) {


            for (int i = 0; i < kuCun.target.size(); i++) {

                if (kuCun.target.get(i).pisitionVO != null) {

                    TextView textView = new TextView(SearchActivity.this);
                    textView.setTextColor(Color.BLACK);
                    textView.setTextSize(12);

                    String code = kuCun.target.get(i).pisitionVO.code;

                    textView.setText("库位" + ": " + code);

                    long pid = kuCun.target.get(i).pisitionVO.id;

                    mLlContainer.addView(textView);
                }
            }
        } else {
            TextView textView = new TextView(SearchActivity.this);
            textView.setTextColor(Color.BLACK);
            textView.setTextSize(12);
            textView.setText("库位: 无");
            mLlContainer.addView(textView);
            mInputLlNewCode.setVisibility(View.VISIBLE);

            mInputLlNewCode.requestFocus();
        }


        if (mLlContainer.getChildCount() == 0) {
            mBangding.setClickable(true);
            mJiebang.setClickable(false);
        } else {
            mBangding.setClickable(false);
            mJiebang.setClickable(true);
        }
    }

    @Override
    public void fail() {
        ToastUtils.showToast("请扫描正确的条码");
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_ok) {
            finish();
        }
    }

    static class SearchPage {

        private Phase mPhase;

        public SearchPage() {
            mPhase = Phase.START;
        }

        public void advance() {
            if (mPhase == Phase.START) {
                mPhase = Phase.NEW_POSITION;
            } else if (mPhase == Phase.NEW_POSITION) {
                mPhase = Phase.BDING;
            } else if (mPhase == Phase.BDING) {
                mPhase = Phase.JBANG;
            } else if (mPhase == Phase.DONE) {

            }
        }

        public enum Phase {
            START,
            NEW_POSITION,
            BDING,
            JBANG,
            DONE
        }
    }


}
