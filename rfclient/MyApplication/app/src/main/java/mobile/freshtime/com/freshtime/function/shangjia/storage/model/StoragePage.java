package mobile.freshtime.com.freshtime.function.shangjia.storage.model;

/**
 * Created by orchid on 16/9/22.
 */

public class StoragePage {


    public boolean isNull;
    private InSkuModel mSkuModel;
//    private SkuModel mSkuModel;
    private StoragePage.Phase mPhase;

    public StoragePage(InSkuModel skuModel) {
        this.mPhase = StoragePage.Phase.START;
        this.mSkuModel = skuModel;
    }

    public InSkuModel getSkuModel() {
        return mSkuModel;
    }

    public void setSkuModel(InSkuModel outgoUnit) {
        mSkuModel = outgoUnit;
    }

    public StoragePage.Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == StoragePage.Phase.START) {
            mPhase = StoragePage.Phase.SKU;
        } else if (mPhase == StoragePage.Phase.SKU) {
            mPhase = StoragePage.Phase.POSITION;
        } else if (mPhase == StoragePage.Phase.POSITION){
            mPhase = StoragePage.Phase.DONE;
        }
    }

    public enum Phase {
        START,
        SKU,
        POSITION,
        DONE

    }
}
