package mobile.freshtime.com.freshtime.common.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by orkid on 2016/8/15.
 */
public class FreshTimeInput extends EditText {

    public FreshTimeInput(Context context) {
        super(context);
    }

    public FreshTimeInput(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
}
