package mobile.freshtime.com.freshtime.function.guanli;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.function.guanli.module.GuanLIItemInfo;
import mobile.freshtime.com.freshtime.function.guanli.module.GuanLiActivityPresenter;
import mobile.freshtime.com.freshtime.function.search.SearchBean;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;

/**
 * 新库位管理
 */
public class GuanLiActivity extends BaseActivity {

    @Bind(R.id.item_input)
    NoInputEditText mItemInput;
    @Bind(R.id.lv)
    ListView mLv;
    @Bind(R.id.item)
    TextView mItem;
    @Bind(R.id.kuwei)
    TextView mKuwei;
    @Bind(R.id.setup)
    TextView mSetup;
    @Bind(R.id.activity_guan_li)
    LinearLayout mActivityGuanLi;
    private GuanLiActivityPresenter mPresenter;

    private ListView mLvKuwei;

    List<GuanLIItemInfo> list = new ArrayList<>();

//    private MyBaseAdapter mItemAdapter = new MyBaseAdapter(list);

    private MyBaseAdapter mMyBaseAdapter = new MyBaseAdapter(list);

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guan_li);
        ButterKnife.bind(this);

        mItemInput.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        //库位lv
        mLvKuwei = (ListView) findViewById(R.id.lv_kuwei);

        mLv.setVisibility(View.VISIBLE);
        mLvKuwei.setVisibility(View.INVISIBLE);
        mItem.setSelected(true);
        mKuwei.setSelected(false);

        mPresenter = new GuanLiActivityPresenter(this);

    }


    @Override
    public void onScanResult(String result) {
        mPresenter.onScanResult(result.trim());
    }

    @OnClick({R.id.item, R.id.kuwei, R.id.setup})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.item:
                mItem.setSelected(true);
                mKuwei.setSelected(false);
                mLv.setVisibility(View.VISIBLE);
                mLvKuwei.setVisibility(View.INVISIBLE);

                break;
            case R.id.kuwei:
                mItem.setSelected(false);
                mKuwei.setSelected(true);
                mLv.setVisibility(View.INVISIBLE);
                mLvKuwei.setVisibility(View.VISIBLE);

                break;
            case R.id.setup:
                Intent intent = new Intent(this, BangDingActivity.class);
                startActivity(intent);
                break;
        }
    }


    //商品  给list适配数据 code
    public void setCodes(SearchBean bean, String json) {

        list.clear();

        String skuId = bean.module.skuId;

        if (StringUtils.isEmpty(json)) {
            return;
        }

        Gson gon = new Gson();
        CuWeiBean kuCun = gon.fromJson(json, CuWeiBean.class);

        //库存
        if ((kuCun.success == true) && kuCun != null && kuCun.target != null && kuCun.target.size() > 0) {

            for (int i = 0; i < kuCun.target.size(); i++) {

                if (kuCun.target.get(i).pisitionVO != null) {
                    GuanLIItemInfo itemInfo = new GuanLIItemInfo();
                    itemInfo.itemName = bean.module.title;

                    itemInfo.pid = kuCun.target.get(i).pisitionVO.id;
                    itemInfo.kuwei = kuCun.target.get(i).pisitionVO.code;
                    itemInfo.id = kuCun.target.get(i).id;

                    //商品skuid
                    itemInfo.skuId = bean.module.skuId;

                    list.add(itemInfo);
                }
            }

            if (mItem.isSelected()) {
                //适配数据
                mLv.setAdapter(mMyBaseAdapter);
            }

            if (mKuwei.isSelected()) {
                //适配数据
                mLvKuwei.setAdapter(mMyBaseAdapter);
            }

        } else {
            ToastUtils.showToast("" + kuCun.message);
        }
    }

    private class MyBaseAdapter extends BaseAdapter {

        public List<GuanLIItemInfo> list1;

        public MyBaseAdapter(List<GuanLIItemInfo> list) {
            this.list1 = list;
        }

        @Override
        public int getCount() {
            return list1 == null ? 0 : list.size();
        }

        @Override
        public GuanLIItemInfo getItem(int position) {
            return list1.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if (convertView == null) {
                if (mItem.isSelected()) {
                    convertView = View.inflate(GuanLiActivity.this, R.layout.guanli_item, null);
                }

                if (mKuwei.isSelected()) {
                    convertView = View.inflate(GuanLiActivity.this, R.layout.guanli_item_kuwei, null);
                }
            }

            TextView itemName = (TextView) convertView.findViewById(R.id.item_name);
            TextView itemCuwei = (TextView) convertView.findViewById(R.id.kuwei);
            TextView delete = (TextView) convertView.findViewById(R.id.delete);

            final GuanLIItemInfo itemInfo = getItem(position);

            itemName.setText(itemInfo.itemName);
            itemCuwei.setText(itemInfo.kuwei);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final Dialog mDialog = new Dialog(GuanLiActivity.this, R.style.MyDialog);
                    View view = getLayoutInflater().inflate(R.layout.cancel, null);
                    Button no = (Button) view.findViewById(R.id.no);
                    Button yes = (Button) view.findViewById(R.id.yes);

                    mDialog.setCancelable(false);
                    mDialog.setContentView(view);

                    //后面给确定和取消按钮设置监听,onclicklistener;
                    no.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            mDialog.dismiss();
                        }
                    });

                    yes.setOnClickListener(new MyOnClickListener(mDialog, itemInfo, position));

                    //设置弹窗宽高
                    mDialog.getWindow().setLayout(UiUtils.getScreenHeight(GuanLiActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

                    mDialog.show();
                }
            });

            return convertView;
        }

    }


    private class MyOnClickListener implements View.OnClickListener {

        private Dialog mDialog;
        private GuanLIItemInfo itemInfo;
        private int positon;

        public MyOnClickListener(Dialog dialog, GuanLIItemInfo itemInfo, int positon) {
            mDialog = dialog;
            this.itemInfo = itemInfo;
            this.positon = positon;
        }

        @Override
        public void onClick(View view) {
            deleteKuwei(itemInfo, positon);
            mDialog.dismiss();
        }
    }

    //点击lv的item删除库位
    private void deleteKuwei(GuanLIItemInfo itemInfo, final int position) {

        JSONObject object = new JSONObject();
        try {
            object.put("data", itemInfo.id + "");
            object.put("token", Login.getInstance().getToken());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = RequestUtils.DEL_CODE +
                "?request=" + object.toString();

        GetProtocol protocol = new GetProtocol(url);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                System.out.println("GuanLiActivity.onSuccess=" + json);

                try {
                    JSONObject object = new JSONObject(json);
                    if (object.getBoolean("success")) {
                        ToastUtils.showToast("解绑成功");
                        list.remove(position);
                        mMyBaseAdapter.notifyDataSetChanged();
                    } else {
                        ToastUtils.showToast("" + object.getString("message"));
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail() {
            }
        });

    }


}
