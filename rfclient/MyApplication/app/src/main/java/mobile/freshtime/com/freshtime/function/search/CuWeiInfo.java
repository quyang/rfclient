package mobile.freshtime.com.freshtime.function.search;

import java.util.List;

/**
 * 获取商品对应的所有的库位信息实体类
 * Created by Administrator on 2016/11/11.
 */

public class CuWeiInfo {


    /**
     * message : null
     * return_code : 0
     * success : true
     * target : [{"agencyId":1,"dr":0,"guardBit":0,"id":40629,"pisitionVO":{"agencyId":1,"areaId":13,"areaType":null,"barcode":"02800100000000003178","code":"QC02702","columnNo":null,"dr":0,"id":565,"rowNo":null,"slottingId":null,"slottingType":null,"ts":"2016-11-06T18:15:33"},"positionId":565,"skuId":20426,"slottingId":null,"ts":"2016-11-07T23:26:51"},{"agencyId":1,"dr":0,"guardBit":0,"id":41389,"pisitionVO":{"agencyId":1,"areaId":13,"areaType":null,"barcode":"02800100000000003186","code":"QC02803","columnNo":null,"dr":0,"id":573,"rowNo":null,"slottingId":null,"slottingType":null,"ts":"2016-11-06T18:15:34"},"positionId":573,"skuId":20426,"slottingId":null,"ts":"2016-11-08T23:45:28"},{"agencyId":1,"dr":0,"guardBit":0,"id":41395,"pisitionVO":{"agencyId":1,"areaId":13,"areaType":null,"barcode":"02800100000000003185","code":"QC02802","columnNo":null,"dr":0,"id":572,"rowNo":null,"slottingId":null,"slottingType":null,"ts":"2016-11-06T18:15:34"},"positionId":572,"skuId":20426,"slottingId":null,"ts":"2016-11-08T23:59:39"},{"agencyId":1,"dr":0,"guardBit":0,"id":41439,"pisitionVO":{"agencyId":1,"areaId":13,"areaType":null,"barcode":"02800100000000003188","code":"QC02805","columnNo":null,"dr":0,"id":575,"rowNo":null,"slottingId":null,"slottingType":null,"ts":"2016-11-06T18:15:34"},"positionId":575,"skuId":20426,"slottingId":null,"ts":"2016-11-09T10:15:13"},{"agencyId":1,"dr":0,"guardBit":0,"id":41441,"pisitionVO":{"agencyId":1,"areaId":13,"areaType":null,"barcode":"02800100000000003187","code":"QC02804","columnNo":null,"dr":0,"id":574,"rowNo":null,"slottingId":null,"slottingType":null,"ts":"2016-11-06T18:15:34"},"positionId":574,"skuId":20426,"slottingId":null,"ts":"2016-11-09T10:16:18"}]
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * dr : 0
     * guardBit : 0
     * id : 40629
     * pisitionVO : {"agencyId":1,"areaId":13,"areaType":null,"barcode":"02800100000000003178","code":"QC02702","columnNo":null,"dr":0,"id":565,"rowNo":null,"slottingId":null,"slottingType":null,"ts":"2016-11-06T18:15:33"}
     * positionId : 565
     * skuId : 20426
     * slottingId : null
     * ts : 2016-11-07T23:26:51
     */

    private List<TargetBean> target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<TargetBean> getTarget() {
        return target;
    }

    public void setTarget(List<TargetBean> target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private int dr;
        private int guardBit;
        private int id;
        /**
         * agencyId : 1
         * areaId : 13
         * areaType : null
         * barcode : 02800100000000003178
         * code : QC02702
         * columnNo : null
         * dr : 0
         * id : 565
         * rowNo : null
         * slottingId : null
         * slottingType : null
         * ts : 2016-11-06T18:15:33
         */

        private PisitionVOBean pisitionVO;
        private int positionId;
        private int skuId;
        private Object slottingId;
        private String ts;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public int getGuardBit() {
            return guardBit;
        }

        public void setGuardBit(int guardBit) {
            this.guardBit = guardBit;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public PisitionVOBean getPisitionVO() {
            return pisitionVO;
        }

        public void setPisitionVO(PisitionVOBean pisitionVO) {
            this.pisitionVO = pisitionVO;
        }

        public int getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public Object getSlottingId() {
            return slottingId;
        }

        public void setSlottingId(Object slottingId) {
            this.slottingId = slottingId;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public static class PisitionVOBean {
            private int agencyId;
            private int areaId;
            private Object areaType;
            private String barcode;
            private String code;
            private Object columnNo;
            private int dr;
            private int id;
            private Object rowNo;
            private Object slottingId;
            private Object slottingType;
            private String ts;

            public int getAgencyId() {
                return agencyId;
            }

            public void setAgencyId(int agencyId) {
                this.agencyId = agencyId;
            }

            public int getAreaId() {
                return areaId;
            }

            public void setAreaId(int areaId) {
                this.areaId = areaId;
            }

            public Object getAreaType() {
                return areaType;
            }

            public void setAreaType(Object areaType) {
                this.areaType = areaType;
            }

            public String getBarcode() {
                return barcode;
            }

            public void setBarcode(String barcode) {
                this.barcode = barcode;
            }

            public String getCode() {
                return code;
            }

            public void setCode(String code) {
                this.code = code;
            }

            public Object getColumnNo() {
                return columnNo;
            }

            public void setColumnNo(Object columnNo) {
                this.columnNo = columnNo;
            }

            public int getDr() {
                return dr;
            }

            public void setDr(int dr) {
                this.dr = dr;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public Object getRowNo() {
                return rowNo;
            }

            public void setRowNo(Object rowNo) {
                this.rowNo = rowNo;
            }

            public Object getSlottingId() {
                return slottingId;
            }

            public void setSlottingId(Object slottingId) {
                this.slottingId = slottingId;
            }

            public Object getSlottingType() {
                return slottingType;
            }

            public void setSlottingType(Object slottingType) {
                this.slottingType = slottingType;
            }

            public String getTs() {
                return ts;
            }

            public void setTs(String ts) {
                this.ts = ts;
            }
        }
    }
}
