package mobile.freshtime.com.freshtime.login;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import mobile.freshtime.com.freshtime.Contants;
import mobile.freshtime.com.freshtime.FreshTimeApplication;
import mobile.freshtime.com.freshtime.utils.SPUtils;
import mobile.freshtime.com.freshtime.common.model.LoginResponse;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.LoginRequest;

/**
 * Created by orchid on 16/9/29.
 */
public class Login implements Response.ErrorListener, Response.Listener<LoginResponse> {

    private static Login instance = null;

    public static Login getInstance() {
        if (instance == null) {
            synchronized (Login.class) {
                if (instance == null) {
                    instance = new Login();
                }
            }
        }
        return instance;
    }

    private Login() {
    }

    private String mToken;
    private String mEquipmentId;
    private String mUserId;
    private long mTimeStamp;

    public String getEquipmentId() {
        return mEquipmentId;
    }

    public long getTimeStamp() {
        return mTimeStamp;
    }

    public String getToken() {
        String value = SPUtils.getStringValue(FreshTimeApplication.context, Contants.SP_FILE_NAME, Contants.TOKEN, null);
//        return Utils.filterInput(mToken, "717cc74885b940a0a2893cbb8bae8965");
        System.out.println("Login.getToken=" + value);
        return value;
    }

    public String getUserId() {
        return mUserId;
    }

    private LoginListener mLoginListener;

    public void login(String userId, String password, LoginListener listener) {
        this.login(userId, password, "hardcode", listener);
    }

    public void login(String userId, String password, String equipmentId, LoginListener listener) {
        this.mLoginListener = listener;
        LoginRequest mRequest = new LoginRequest(userId, password, equipmentId, this, this);
        FreshRequest.getInstance().addToRequestQueue(mRequest, this.toString());
    }

    public boolean isTokenValidate() {
        SharedPreferences tokenSP = FreshTimeApplication.context.getSharedPreferences("token", Context.MODE_PRIVATE);
        if (tokenSP != null && tokenSP.contains("time")) {
            long time = tokenSP.getLong("time", 0l);
            long now = System.currentTimeMillis();
            if (now - time > 0 && now - time < GAP) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        if (this.mLoginListener != null) {
            mLoginListener.onLoginFail(error.getMessage());

            if (error.networkResponse == null) {
                return;
            }

            String log = new String(error.networkResponse.data);
            Log.e("login", log);
        }
    }

    @Override
    public void onResponse(LoginResponse response) {
        if (response.isSuccess()) {

            SPUtils.putStringValue(FreshTimeApplication.context,
                    Contants.SP_FILE_NAME, Contants.TOKEN, response.getModule());

            this.mToken = response.getModule();
            this.mEquipmentId = response.getEquipmentId();
            this.mUserId = response.getUserId();
            this.mTimeStamp = response.getTimeStamp();
//            storeToken();
            if (this.mLoginListener != null) {
                mLoginListener.onLoginSuccess();
            }
        } else {
            if (this.mLoginListener != null) {
                mLoginListener.onLoginFail(response.getErrorMsg());
            }
        }
    }

    private static final int GAP = 1000 * 60 * 60 * 12 * 2 * 30;

    private void storeToken() {
        SharedPreferences tokenSP = FreshTimeApplication.context.getSharedPreferences("token", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = tokenSP.edit();
        editor.putString("token", mToken);
        editor.putString("equipmentId", mEquipmentId);
        editor.putString("userId", mUserId);
        editor.putLong("timeStamp", mTimeStamp);
        editor.putLong("time", System.currentTimeMillis());
        editor.commit();

    }

    private void restoreToken() {
        System.out.println("Login.restoreToken=");
        SharedPreferences tokenSP = FreshTimeApplication.context.getSharedPreferences("token", Context.MODE_PRIVATE);
        if (tokenSP != null && tokenSP.contains("time")) {
            long time = tokenSP.getLong("time", 0l);
            long now = System.currentTimeMillis();
            if (now - time > 0 && now - time < GAP) {
                mToken = tokenSP.getString("token", null);
                mEquipmentId = tokenSP.getString("equipmentId", null);
                mUserId = tokenSP.getString("userId", null);
                mTimeStamp = tokenSP.getLong("timeStamp", 0l);
            }
        }
    }

    public static final String MD5(String content) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(content.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2) {
                    h = "0" + h;
                }
                hexString.append(h);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * 根据{@link #MD5(String)}来获取价签密码
     *
     * @param password
     * @param data
     * @param equipmentId
     * @return
     */
    public static final String MIX(String password, long data, String equipmentId) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(MD5(password));
        stringBuilder.append("_" + data + "_");
        stringBuilder.append(MD5(equipmentId));
        String finalOne = MD5(stringBuilder.toString());
        return finalOne;
    }

    public interface LoginListener {
        void onLoginSuccess();

        void onLoginFail(String reason);
    }
}
