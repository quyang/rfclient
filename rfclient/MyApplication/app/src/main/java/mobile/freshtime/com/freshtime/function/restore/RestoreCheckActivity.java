package mobile.freshtime.com.freshtime.function.restore;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Constants;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverBox;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverDetail;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverDistribution;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverTask;
import mobile.freshtime.com.freshtime.common.model.delivery.ResponseBox;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.function.restore.adapter.RestoreListActivityBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * 归还商品确认
 * Created by orchid on 16/10/16.
 */

public class RestoreCheckActivity extends BaseActivity implements Response.ErrorListener,
        Response.Listener<ResponseBox>, View.OnClickListener {

    private DeliverTask mDeliverTask;
    private ArrayList<SkuModel> mSkuModels;
    private EditText mBoxInput;
    private TextView mDeliverman;
    private TextView mProceedingTask;
    private TextView mDeliverSheet;
    private TextView mDeliverBox;
    private LinearLayout mContainer;

    private TextView mDone;

    private HashMap<Long, Boolean> mScannedBox = new HashMap<>();

    private Response.Listener<BaseResponse> mSubmit =
            new Response.Listener<BaseResponse>() {
                @Override
                public void onResponse(BaseResponse response) {
                    if (response != null && response.isSuccess()) {
                        Toast.makeText(RestoreCheckActivity.this, "归还完成", Toast.LENGTH_SHORT).show();
                        EventBus.getDefault().post(new RestoreListActivityBean());
                        finish();
                    } else {
                        Toast.makeText(RestoreCheckActivity.this, "" + response.getErrorMsg(), Toast.LENGTH_LONG).show();
                    }
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handover_check);

        TextView title = (TextView) findViewById(R.id.title);
        title.setText("归还配送箱");

        mDeliverTask = getIntent().getParcelableExtra("task");
        if (mDeliverTask == null) finish();

        findViewById(R.id.back).setOnClickListener(this);

        ArrayList<DeliverBox> box = getIntent().getParcelableArrayListExtra("box");
        ArrayList<DeliverDetail> detail = getIntent().getParcelableArrayListExtra("detail");
        ArrayList<DeliverDistribution> distribution = getIntent().getParcelableArrayListExtra("distribution");
        mDeliverTask.boxes = box;
        mDeliverTask.details = detail;
        mDeliverTask.distributions = distribution;

        mSkuModels = getIntent().getParcelableArrayListExtra("sku");

        if (mSkuModels.size() > mDeliverTask.details.size()) {
            Toast.makeText(RestoreCheckActivity.this, "提交商品超过配送时的商品数", Toast.LENGTH_LONG).show();
            return;
        }

        for (int i = 0; i < box.size(); i++) {
            mScannedBox.put(box.get(i).id, false);
        }

        initView();
    }


    private void initView() {

        TextView title = (TextView) findViewById(R.id.item_tag);
        title.setText("配送箱:");
        mBoxInput = (EditText) findViewById(R.id.item_input);
        mDeliverman = (TextView) findViewById(R.id.delivery_name);
        mProceedingTask = (TextView) findViewById(R.id.task_count);
        mDeliverSheet = (TextView) findViewById(R.id.agency);
        mDeliverBox = (TextView) findViewById(R.id.area);

        mDeliverman.setText("配送员:" + mDeliverTask.operator);
        mDeliverSheet.setText("送货单:" + mDeliverTask.id);
        mDeliverBox.setText("配送箱:" + mDeliverTask.boxes.size() + "个");

        mDone = (TextView) findViewById(R.id.done);
        mDone.setEnabled(false);
        mDone.setOnClickListener(this);
//        mProceedingTask.setText("进行中的任务" + mDeliverTask.);
        initBoxContainer();
    }

    /**
     * 初始化UI,绘制配送箱信息里面全部的箱子信息
     */
    private void initBoxContainer() {
        int marginHorizental = (int) getResources().getDimension(R.dimen.common_subtitle_margin);
        int marginTop = (int) getResources().getDimension(R.dimen.common_margin);
        mContainer = (LinearLayout) findViewById(R.id.delivery_container);
        for (int i = 0; i < mDeliverTask.boxes.size(); i++) {
            DeliverBox deliverBox = mDeliverTask.boxes.get(i);
            TextView text = new TextView(this);//创建textview
            text.setTag(deliverBox.id);
            text.setTextAppearance(this, R.style.Subtitle);
            text.setText("配送箱:" + deliverBox.barcode);
            LinearLayout.LayoutParams param
                    = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT);
            param.leftMargin = marginHorizental;
            param.rightMargin = marginHorizental;
            param.topMargin = marginTop;
            mContainer.addView(text, param);//添加子view
        }
    }

    @Override
    public void onScanResult(String result) {
        result = result.trim();
        mBoxInput.setText(result);

        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(result, "1"));
            BaseRequest<ResponseBox> request =
                    new BaseRequest<>(RequestUtils.DISTRI_BOX,
                            ResponseBox.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(ResponseBox response) {
        if (response != null && response.success) {
            if (response.target != null) {
                long id = response.target.id;
                if (mScannedBox.containsKey(id)) {
                    boolean scanned = mScannedBox.get(id);
                    if (!scanned) {
                        TextView scan = (TextView) mContainer.findViewWithTag(id);
                        if (scan != null) {
                            String orgin = scan.getText().toString();
                            scan.setText(orgin + Constants.MATCH);
                        }
                        mScannedBox.put(id, true);
                        watchScan();
                    }
                } else {
                    Toast.makeText(RestoreCheckActivity.this, "不含该配送框", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(RestoreCheckActivity.this, "" + response.message, Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 判断是否全部扫描过
     */
    private void watchScan() {
        Iterator<Long> iter = mScannedBox.keySet().iterator();
        boolean allScanned = true;
        while (iter.hasNext()) {
            long key = iter.next();
            boolean scanned = mScannedBox.get(key);
            allScanned &= scanned;
        }
        if (allScanned) {
            mDone.setEnabled(true);
        }
    }

    //提交数据
    private void submitDeliver() {
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            JSONObject data = new JSONObject();

            //获取参数
            generateParam(data);

            param.put("data", data);
            BaseRequest<BaseResponse> request =
                    new BaseRequest<>(RequestUtils.RETURN_BOX,
                            BaseResponse.class,
                            param,
                            mSubmit, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public long getId(long a) {

        ArrayList<DeliverDetail> details = mDeliverTask.details;
        for (int i = 0; i < details.size(); i++) {
            long skuId = mDeliverTask.details.get(i).skuId;

            if (a == skuId) {
                long id = mDeliverTask.details.get(i).id;

                return id;
            }
        }

        return -1;

    }


    public long getRemainNo(long a) {

        ArrayList<DeliverDetail> details = mDeliverTask.details;
        for (int i = 0; i < details.size(); i++) {
            long skuId = mDeliverTask.details.get(i).skuId;

            if (a == skuId) {
                long remainNo = mDeliverTask.details.get(i).remainNo;
                return remainNo;
            }
        }

        return -1;

    }

    //给json里面添加参数
    private void generateParam(JSONObject data) throws JSONException {
        data.put("id", mDeliverTask.id);
        JSONArray boxes = new JSONArray();
        for (int i = 0; i < mDeliverTask.boxes.size(); i++) {
            JSONObject box = new JSONObject();
            box.put("boxId", mDeliverTask.boxes.get(i).id);
            boxes.put(box);
        }

        data.put("boxes", boxes);
        JSONArray details = new JSONArray();
        for (int i = 0; i < mSkuModels.size(); i++) {
            long skuId1 = mSkuModels.get(i).getSkuId();

            JSONObject detail = new JSONObject();
            detail.put("id", getId(skuId1));
//            detail.put("id", mSkuModels.get(i).getId());
            detail.put("remainNo", getRemainNo(skuId1));
//            detail.put("remainNo", mSkuModels.get(i).getCount());
            details.put(detail);
        }
        data.put("details", details);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.done) {
            submitDeliver();
        } else if (id == R.id.back) {
            finish();
        }
    }
}
