package mobile.freshtime.com.freshtime.function.restore.adapter;

/**
 * Created by orchid on 16/10/16.
 */

public interface IQuantityWatcher {

    void onItemAdd(int position);
    void onItemMinus(int position);
}
