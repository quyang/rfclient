package mobile.freshtime.com.freshtime.function.pick.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by orchid on 16/10/6.
 */

public class PickTask implements Parcelable {

    public long agencyId;
    public long areaId;
    public String createTime;
    public long dr;
    public String finishTime;
    public long id;
    public long operator;
    public int statu;
    public String ts;
    public long waveId;

    public ArrayList<Task> details;

    public PickTask() {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(agencyId);
        dest.writeLong(areaId);
        dest.writeString(createTime);
        dest.writeLong(dr);
        dest.writeString(finishTime);
        dest.writeLong(id);
        dest.writeLong(operator);
        dest.writeInt(statu);
        dest.writeString(ts);
        dest.writeLong(waveId);
        Task[] tasks = new Task[details.size()];
        for (int i = 0; i < details.size(); i++) {
            tasks[i] = details.get(i);
        }
        dest.writeTypedArray(tasks, flags);
    }

    public static final Parcelable.Creator<PickTask> CREATOR =
            new Parcelable.Creator<PickTask>() {

                @Override
                public PickTask createFromParcel(Parcel source) {
                    PickTask model = new PickTask();
                    model.agencyId = source.readLong();
                    model.areaId = source.readLong();
                    model.createTime = source.readString();
                    model.dr = source.readLong();
                    model.finishTime = source.readString();
                    model.id = source.readLong();
                    model.operator = source.readLong();
                    model.statu = source.readInt();
                    model.ts = source.readString();
                    model.waveId = source.readLong();
                    Task[] list = source.createTypedArray(Task.CREATOR);
                    model.details = new ArrayList<>(list.length);
                    for (int i = 0; i < list.length; i++) {
                        model.details.add(i, list[i]);
                    }
                    return model;
                }

                @Override
                public PickTask[] newArray(int size) {
                    return new PickTask[size];
                }
            };
}
