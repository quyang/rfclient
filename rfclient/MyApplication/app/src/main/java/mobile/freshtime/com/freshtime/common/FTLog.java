package mobile.freshtime.com.freshtime.common;

import android.util.Log;

/**
 * Created by orchid on 16/9/21.
 */

public class FTLog {

    public static final String TAG = "fresh";

    public static final void e(String info) {
        Log.e(TAG, info);
    }

    public static final void i(String info) {
        Log.i(TAG, info);
    }
}
