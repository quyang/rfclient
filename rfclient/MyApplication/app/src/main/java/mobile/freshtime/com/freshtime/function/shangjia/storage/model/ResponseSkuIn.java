package mobile.freshtime.com.freshtime.function.shangjia.storage.model;

import org.json.JSONObject;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orchid on 16/9/28.
 */

public class ResponseSkuIn extends BaseResponse {

    private InSkuModel module;

    public ResponseSkuIn(JSONObject jsonObject) {
        return_code = jsonObject.optInt("resultCode", 0);
        success = jsonObject.optBoolean("success", false);
        message = jsonObject.optString("errorMsg", "");
        module = new InSkuModel(jsonObject.optJSONObject("module"));
    }

    public InSkuModel getModule() {
        return module;
    }

    public void setModule(InSkuModel module) {
        this.module = module;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
