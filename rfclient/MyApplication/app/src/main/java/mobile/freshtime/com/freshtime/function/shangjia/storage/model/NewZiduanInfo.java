package mobile.freshtime.com.freshtime.function.shangjia.storage.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Administrator on 2016/11/18.
 */

public class NewZiduanInfo {


    /**
     * resultCode : 1
     * success : true
     * errorMsg :
     * module : {"3":"kg","68":"只"}
     * succcess : true
     */

    private int resultCode;
    private boolean success;
    private String errorMsg;
    /**
     * 3 : kg
     * 68 : 只
     */

    private ModuleBean module;
    private boolean succcess;

    public int getResultCode() {
        return resultCode;
    }

    public void setResultCode(int resultCode) {
        this.resultCode = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }

    public ModuleBean getModule() {
        return module;
    }

    public void setModule(ModuleBean module) {
        this.module = module;
    }

    public boolean isSucccess() {
        return succcess;
    }

    public void setSucccess(boolean succcess) {
        this.succcess = succcess;
    }

    public static class ModuleBean {
        @SerializedName("3")
        private String value3;
        @SerializedName("68")
        private String value68;

        public String getValue3() {
            return value3;
        }

        public void setValue3(String value3) {
            this.value3 = value3;
        }

        public String getValue68() {
            return value68;
        }

        public void setValue68(String value68) {
            this.value68 = value68;
        }
    }
}
