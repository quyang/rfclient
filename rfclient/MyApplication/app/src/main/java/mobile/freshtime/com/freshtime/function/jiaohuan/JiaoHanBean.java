package mobile.freshtime.com.freshtime.function.jiaohuan;

import java.util.List;

/**
 * Created by Administrator on 2016/11/7.
 */

public class JiaoHanBean {

    /**
     * message : null
     * return_code : 0
     * success : true
     * target : [{"agencyId":1,"dr":0,"guardBit":0,"id":40010,"pisitionVO":null,"positionId":2,"skuId":20019,"slottingId":null,"ts":"2016-11-07T10:34:59"},{"agencyId":1,"dr":0,"guardBit":0,"id":40008,"pisitionVO":null,"positionId":2800100000000003000,"skuId":20019,"slottingId":null,"ts":"2016-11-07T10:30:21"}]
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * dr : 0
     * guardBit : 0
     * id : 40010
     * pisitionVO : null
     * positionId : 2
     * skuId : 20019
     * slottingId : null
     * ts : 2016-11-07T10:34:59
     */

    private List<TargetBean> target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<TargetBean> getTarget() {
        return target;
    }

    public void setTarget(List<TargetBean> target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private int dr;
        private int guardBit;
        private int id;
        private Object pisitionVO;
        private long positionId;
        private int skuId;
        private Object slottingId;
        private String ts;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public int getGuardBit() {
            return guardBit;
        }

        public void setGuardBit(int guardBit) {
            this.guardBit = guardBit;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getPisitionVO() {
            return pisitionVO;
        }

        public void setPisitionVO(Object pisitionVO) {
            this.pisitionVO = pisitionVO;
        }

        public long getPositionId() {
            return positionId;
        }

        public void setPositionId(int positionId) {
            this.positionId = positionId;
        }

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public Object getSlottingId() {
            return slottingId;
        }

        public void setSlottingId(Object slottingId) {
            this.slottingId = slottingId;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }
    }
}
