package mobile.freshtime.com.freshtime.device.urovo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import mobile.freshtime.com.freshtime.activity.BaseActivity;

/**
 * Created by orchid on 16/9/15.
 */
public class UrovoBroadcastReceiver extends BroadcastReceiver {

    private BaseActivity mContext;

    public UrovoBroadcastReceiver(BaseActivity context) {
        this.mContext = context;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        byte[] barcode = intent.getByteArrayExtra("barocode");
        int barocodelen = intent.getIntExtra("length", 0);
        String barcodeStr = new String(barcode, 0, barocodelen);
        mContext.onScanResult(barcodeStr);
        mContext.playBeepSound();
    }
}