package mobile.freshtime.com.freshtime.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import mobile.freshtime.com.freshtime.FreshTimeApplication;

/**
 * quyang
 * <p/>
 * Created by Administrator on 2016/9/3.
 */
public class ToastUtils {

    /**
     * 弹吐司
     *
     * @param msg
     */
    public static void showToast(final String msg) {

        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                showMsg(FreshTimeApplication.context, msg, Toast.LENGTH_SHORT);
            }
        });
    }

    private static String oldMsg;
    private static long time;

    private static void showMsg(Context context, String msg, int duration) {
        if (msg != null && !msg.equals(oldMsg)) { // 当显示的内容不一样时，即断定为不是同一个Toast
            Toast.makeText(context, msg, duration).show();
            time = System.currentTimeMillis();
        } else {

            // 显示内容一样时，只有间隔时间大于2秒时才显示
            if (System.currentTimeMillis() - time > 2500) {
                Toast.makeText(context, msg, duration).show();
                time = System.currentTimeMillis();
            }
        }
        oldMsg = msg;

        Toast.makeText(context, msg + "", Toast.LENGTH_SHORT);
    }
}
