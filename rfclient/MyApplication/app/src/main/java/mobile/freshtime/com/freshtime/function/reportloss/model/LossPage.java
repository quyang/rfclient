package mobile.freshtime.com.freshtime.function.reportloss.model;

import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;

/**
 * Created by orchid on 16/9/14.
 */
public class LossPage {

    private SkuModel mSkuModel;
    private Phase mPhase;

    public LossPage(SkuModel skuModel) {
        this.mPhase = Phase.START;
        this.mSkuModel = skuModel;
    }

    public SkuModel getSkuModel() {
        return mSkuModel;
    }

    public void setSkuModel(SkuModel lossUnit) {
        mSkuModel = lossUnit;
    }

    public Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == Phase.START) {
            mPhase = Phase.SKU;
        } else if (mPhase == Phase.SKU) {
            mPhase = Phase.POSITION;
        } else if (mPhase == Phase.POSITION){
            mPhase = Phase.DONE;
        }
    }

    public enum Phase {
        START,
        SKU,
        POSITION,
        DONE
    }
}
