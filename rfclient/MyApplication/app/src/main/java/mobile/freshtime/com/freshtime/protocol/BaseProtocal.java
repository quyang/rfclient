package mobile.freshtime.com.freshtime.protocol;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import mobile.freshtime.com.freshtime.FreshTimeApplication;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Administrator on 2016/10/27.
 */

public abstract class BaseProtocal {


    public Handler mHandler;

    public BaseProtocal() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    public abstract String getUrl();

    public abstract FormBody getFormBody();


    public void get() {

        OkHttpClient okHttpClient = FreshTimeApplication.getOk();
        Request request = new Request.Builder()
                .get()
                .url(getUrl())
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFail();
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String string = response.body().string();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onSuccess(string);
                    }
                });

            }
        });

    }


    /**
     * post请求
     */
    public void post() {
        OkHttpClient okHttpClient = FreshTimeApplication.getOk();
        Request request = new Request.Builder()
                .post(getFormBody())
                .url(getUrl())
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onFail();
                    }
                });
            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {
                final String string = response.body().string();
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mListener.onSuccess(string);
                    }
                });
            }
        });
    }


    public interface OnDataListener {

        void onSuccess(String json);

        void onFail();
    }

    private OnDataListener mListener;

    public void setOnDataListener(OnDataListener listener) {
        mListener = listener;
    }


}
