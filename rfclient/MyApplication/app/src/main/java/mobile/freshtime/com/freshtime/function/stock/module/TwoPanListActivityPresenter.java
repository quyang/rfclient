package mobile.freshtime.com.freshtime.function.stock.module;

import android.content.Intent;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.function.stock.TwoPanActivity;
import mobile.freshtime.com.freshtime.function.stock.TwoPanListActivity;
import mobile.freshtime.com.freshtime.function.stock.jiekou.DetailsInfoListener;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by Administrator on 2016/11/24.
 */
public class TwoPanListActivityPresenter {

    private TwoPanListActivity mActivity;

    public TwoPanListActivityPresenter(TwoPanListActivity activity) {
        mActivity = activity;
    }

    //提交一盘信息
    public void submitInfo(ArrayList<InSkuModel> skuModels, final int taskId, final int id) {

        if (!NetUtils.hasNet(mActivity)) {
            ToastUtils.showToast("请求网络失败,请先检查网络环境");
            return;
        }

        try {
            JSONObject params = new JSONObject();
            JSONArray array = new JSONArray();

            for (int i = 0; i < skuModels.size(); i++) {
                InSkuModel skuModel = skuModels.get(i);
                params.put("skuId", skuModel.getSkuId());
                params.put("positionId", skuModel.getPositionId());
                params.put("firstNo", skuModel.getCount());
                params.put("stocktakingId", taskId);
                params.put("saleUnitId", skuModel.skuUnit);
            }

            JSONObject object = new JSONObject();
            try {
                array.put(params);
                object.put("token", Login.getInstance().getToken());
                object.put("data", array);

                GetProtocol protocol = new GetProtocol(RequestUtils.SUBMIT_STOCK + "?request=" + object);
                protocol.get();
                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                    @Override
                    public void onSuccess(String json) {
                        System.out.println("PanListActivityPresenter.onSuccess=" + json);
                        try {
                            JSONObject jsonObject = new JSONObject(json);
                            if (jsonObject.getBoolean("success")) {
                                changeStatus(id, taskId);
                            } else {
                                ToastUtils.showToast("提交失败");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFail() {
                        ToastUtils.showToast("提交失败");
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //改变状态
    private void changeStatus(final int id, final int taskId) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", taskId + "");

            GetProtocol protocol = new GetProtocol(RequestUtils.TO_CHECK + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    System.out.println("PanListActivityPresenter.onSuccess=" + json);
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        if (jsonObject.getBoolean("success")) {

                            Intent intent = new Intent(mActivity, TwoPanActivity.class);
                            intent.putExtra("id", taskId);
                            mActivity.startActivity(intent);
//                            gotoNextActivity(id);
                        } else {
                            ToastUtils.showToast("提交失败");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    //进入二盘
    private void gotoNextActivity(int id) {

        Intent intent = new Intent(mActivity, TwoPanActivity.class);
        mActivity.startActivity(intent);

//        OnePanInfo info = new OnePanInfo();
//        info.id = StringUtils.getString(id);
//        EventBus.getDefault().post(info);
//
//        Intent intent = new Intent(mActivity, FirstStockActivity.class);
//        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        mActivity.startActivity(intent);
    }

    //提交二盘明细
    public void subTwoPanDetails() {

        JSONObject params = new JSONObject();
        JSONObject object = new JSONObject();
        try {

            JSONArray array = new JSONArray();

            params.put("skuId", 1);
            params.put("positionId", 1);
            params.put("firstNo", "");
            params.put("secondNo", "");
            params.put("stocktakingId", 1);
            params.put("saleUnitId", 1);

            array.put(params);

            object.put("token", Login.getInstance().getToken());
            object.put("data", array);

            GetProtocol protocol = new GetProtocol(RequestUtils.CHECK_STOCK + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("TwoPanActivity.submitTwoPanDetails=" + json);
                    if (!StringUtils.isEmpty(json)) {
                        try {
                            JSONObject object = new JSONObject(json);
                            if (object.getBoolean("success")) {
                                ToastUtils.showToast("提交二盘明细成功");
                                shenghe();
                            } else {
                                ToastUtils.showToast("提交二盘明细失败");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFail() {
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //审核
    private void shenghe() {

        ToastUtils.showToast("审核");
        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", 1 + "");//任务id

            GetProtocol protocol = new GetProtocol(RequestUtils.AUDIT + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("TwoPanActivity.shenghe=" + json);
                    JSONObject object1 = new JSONObject();
                    try {
                        if (object1.getBoolean("success")) {
                            ToastUtils.showToast("提交审核成功了");
                        } else {
                            ToastUtils.showToast("提交审核失败了");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
                    ToastUtils.showToast("审核 fail");
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //根据id获取对应的明细
    public void getInfo(int taskId) {

        JSONObject params = new JSONObject();
        try {
            params.put("token", Login.getInstance().getToken());
            params.put("data", taskId + "");

            GetProtocol protocol = new GetProtocol(RequestUtils.GET_STOCK + "?request=" + params);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("TwoPanListActivityPresenter.onSuccess=" + json);

                    if (!StringUtils.isEmpty(json)) {
                        Gson gson = new Gson();
                        TwoPanInfo info = gson.fromJson(json, TwoPanInfo.class);

                        if (info.isSuccess() && info.getTarget() != null && info.getTarget().getDetails() != null && info.getTarget().getDetails().size() > 0) {
                            mlisenter.Listener(info);
                        }
                    }
                }

                @Override
                public void onFail() {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    private DetailsInfoListener mlisenter;

    public void setDetailsInfoListener(DetailsInfoListener activity) {
        mlisenter = activity;
    }
}
