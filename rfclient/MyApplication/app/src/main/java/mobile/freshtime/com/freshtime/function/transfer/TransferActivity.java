package mobile.freshtime.com.freshtime.function.transfer;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.position.ResponsePosition;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.transfer.model.TransferPage;
import mobile.freshtime.com.freshtime.function.transfer.model.TransferSkuModel;
import mobile.freshtime.com.freshtime.function.transfer.viewmodel.TransferViewModel;

/**
 * Created by orchid on 16/10/11.
 * 移库功能入口Activity
 */

public class TransferActivity extends BaseActivity implements ViewModelWatcher<TransferPage>,
        PageListener<TransferPage>, View.OnClickListener, Response.ErrorListener, Response.Listener<ResponsePosition> {

    private View mInfoRoot;
    private ViewGroup mItemRoot;
    private ViewGroup mQualityRoot;
    private ViewGroup mSourceRoot;

    private TextView mBefore;
    private TextView mNext;
    private TextView mDone;

    private TextView mItemName;
    private TextView mItemCode;

    private TextView mQualityName;
    private EditText mQuanlityInput;

    private TextView mOutPostion;
    private EditText mOutPositionInput;

    private TextView mInPosition;
    private EditText mInPositionInput;

    private EditText mScanContent;

    private TransferViewModel mViewModel;
    private TextView mCode69;
    private View mOut;
    private View mIn;
    private TextView mCoder;
    private TextView mSafeKucun;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer);

        findViewById(R.id.back).setOnClickListener(this);

        mBefore = (TextView) findViewById(R.id.last);
        mBefore.setOnClickListener(this);
        mNext = (TextView) findViewById(R.id.next);
        mNext.setOnClickListener(this);
        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);


        mInfoRoot = findViewById(R.id.info_root);

        mItemRoot = (ViewGroup) findViewById(R.id.item_info_root);

        mSafeKucun = (TextView) mItemRoot.findViewById(R.id.safe_kucun);
        mSafeKucun.setVisibility(View.VISIBLE);


        //商品69码
        mCode69 = (TextView) mItemRoot.findViewById(R.id.item_69code);

        mCoder = (TextView) mItemRoot.findViewById(R.id.item_coder);

        mQualityRoot = (ViewGroup) findViewById(R.id.tranfer_count_root);
        mScanContent = (EditText) findViewById(R.id.scanContent);

        mItemName = (TextView) mItemRoot.findViewById(R.id.item_name);
        mItemCode = (TextView) mItemRoot.findViewById(R.id.item_code);

        mQualityName = (TextView) mQualityRoot.findViewById(R.id.item_spec);

        //下面的商品数量
        mQuanlityInput = (EditText) mQualityRoot.findViewById(R.id.count_input);

        mOut = findViewById(R.id.out_info_root);
        mOutPostion = (TextView) mOut.findViewById(R.id.item_organ);

        //输出仓位
        mOutPositionInput = (EditText) mOut.findViewById(R.id.item_position);

        mIn = findViewById(R.id.in_info_root);
        mInPosition = (TextView) mIn.findViewById(R.id.item_organ);

        //输入仓位
        mInPositionInput = (EditText) mIn.findViewById(R.id.item_position);

        mViewModel = new TransferViewModel(this);
        mViewModel.addPageListener(this);
        mViewModel.registerContext(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.unregisterContext();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {
            if (mInfoRoot.getVisibility() == View.VISIBLE) {

                if (StringUtils.isEmpty(mOutPositionInput.getText().toString().trim()) || StringUtils.isEmpty(mInPositionInput.getText().toString().trim())) {
                    ToastUtils.showToast("请先输入仓位");
                    return;
                }

                if ("".equals(mQuanlityInput.getText().toString().trim())) {
                    Toast.makeText(TransferActivity.this, "请先输入数量", Toast.LENGTH_LONG).show();
                } else {
                    mViewModel.setCount(mQuanlityInput.getText().toString());
                    mViewModel.navNext();
                    requestDone();
                }
            }
        } else if (id == R.id.last) {
            mViewModel.navBefore();
        } else if (id == R.id.next) {

            if (mInfoRoot.getVisibility() == View.GONE) {
                Toast.makeText(TransferActivity.this, getResources().getString(R.string.please_item_or_kuwei), Toast.LENGTH_LONG).show();
                return;
            }

            if ("".equals(mQuanlityInput.getText().toString().trim())) {
                Toast.makeText(TransferActivity.this, "请先输入商品数量", Toast.LENGTH_LONG).show();
            } else {
                mViewModel.setCount(mQuanlityInput.getText().toString());
                mViewModel.navNext();
            }
        }
    }

    //扫描开始
    @Override
    public void onScanResult(String result) {
        if (NetUtils.hasNet(this)) {
            mViewModel.onScanResult(result);
        } else {
            ToastUtils.showToast(getResources().getString(R.string.net_error));
        }
    }

    @Override
    public void onRequestStart() {

    }

    @Override
    public void onRequestStop() {

    }

    @Override
    public void onPageUpdate(TransferPage page) {
        displayPage(page);
    }

    @Override
    public void onError(String errorCode) {

    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onPageChanged(TransferPage page) {
        displayPage(page);
    }

    private void displayPage(TransferPage page) {
        if (mViewModel.hasBefore()) {
            mBefore.setEnabled(true);
        } else {
            mBefore.setEnabled(false);
        }

        if (!mViewModel.hasNext()
//                &&
//                page.getPhase() != TransferPage.Phase.DONE &&
//                page.getPhase() != TransferPage.Phase.COUNT
                ) {
            mNext.setEnabled(true);
        } else {
            mNext.setEnabled(true);
        }

        TransferPage.Phase phase = page.getPhase();
        if (phase == TransferPage.Phase.START) {
            displayStart(page);
        } else if (phase == TransferPage.Phase.COUNT) {
            displayCount(page);
//            displayOut2(page);
        } else if (phase == TransferPage.Phase.OUT) {
            displayOut(page);//显示sku信息
        } else if (phase == TransferPage.Phase.IN) {
            displayIn(page);
        } else {
//            displayDone(page);
        }
    }


    private void displayOut2(TransferPage page) {

        mInPositionInput.setText(page.getSkuModel().getBarcode());
        mInPosition.setText("库位: " + page.getSkuModel().getKuwei());

        mQuanlityInput.requestFocus();
    }

    /**
     * 初始化的UI
     *
     * @param page
     */
    private void displayStart(TransferPage page) {

        mInfoRoot.setVisibility(View.GONE);

        mScanContent.requestFocus();

        mItemName.setText("品名：");
        mItemCode.setText("编码：");

        mQualityName.setText("规格：");

        mOutPostion.setText("机构：");
        mOutPositionInput.setText("");

        mInPosition.setText("机构：");
        mInPositionInput.setText("");

        mScanContent.setText("");
    }

    /**
     * 输入库位信息后展示的信息
     *
     * @param page
     */
    private void displayOut(TransferPage page) {
        mInfoRoot.setVisibility(View.VISIBLE);

        TransferSkuModel unit = page.getSkuModel();

        //显示sku信息
        displayUnit(page);

        mScanContent.setText(unit.getBarcode());

        mOutPositionInput.requestFocus();
    }

    /**
     * 输入数量信息后展示的信息
     *
     * @param page
     */
    private void displayIn(TransferPage page) {
        TransferSkuModel unit = page.getSkuModel();
        displayUnit(page);

        mScanContent.setText(unit.getBarcode());

        mInPositionInput.requestFocus();
    }

    /**
     * 输入sku信息后展示的信息
     *
     * @param page
     */
    private void displayCount(TransferPage page) {
        TransferSkuModel unit = page.getSkuModel();
        displayUnit(page);

        mScanContent.setText(unit.getBarcode());

        mQuanlityInput.requestFocus();


    }

    /**
     * 输入数量信息后展示的信息
     *
     * @param page
     */
    private void displayDone(TransferPage page) {
        TransferSkuModel unit = page.getSkuModel();
        displayUnit(page);

        mScanContent.setText(unit.getBarcode());
    }

    //显示sku信息
    private void displayUnit(TransferPage page) {

        mInfoRoot.setVisibility(View.VISIBLE);

        TransferSkuModel unit = page.getSkuModel();
        mItemName.setText("品名：" + unit.getTitle());
        mItemCode.setText(unit.getSkuId() + "");
        mQualityName.setText("规格：" + unit.getUnStandardSpecification());
        if ("null".equals(unit.getCount() + "")) {
            mQuanlityInput.setText("");
        } else {
            mQuanlityInput.setText(unit.getCount() + "");
        }
        mOutPostion.setText("库位：" + unit.getOutKuwei());
        mInPosition.setText("库位：" + unit.getInKuwei());
        mCode69.setText("商品69码：" + unit.getSku69Id());

        mOutPositionInput.setText(page.getOutBarcode());
        mInPositionInput.setText(page.getInBarcode());

        mCode69.setText("商品69码:" + unit.getSku69Id());

        if (StringUtils.isKongStr(unit.kucun)) {
            mCoder.setText("总库存: 无");
        } else {
            mCoder.setText("总库存: " + unit.kucun);
        }

        if (StringUtils.isKongStr(unit.getSafeKucun())) {
            mSafeKucun.setText("安全库存： ");
        } else {
            mSafeKucun.setText("安全库存：" + unit.getSafeKucun());
        }
    }

    private void requestDone() {
        Intent intent = new Intent(this, TransferConfirmActivity.class);
        List<TransferPage> pages = mViewModel.getTransferPages();
        ArrayList<TransferSkuModel> modules = new ArrayList<>();
        for (int i = 0; i < pages.size(); i++) {
            TransferPage page = pages.get(i);
            TransferSkuModel model = page.getSkuModel();
            if (model != null &&
                    (page.getPhase() == TransferPage.Phase.DONE ||
                            page.getPhase() == TransferPage.Phase.COUNT))
                if (!"0".equals(pages.get(i).getSkuModel().getCount())) {
                    modules.add(pages.get(i).getSkuModel());
                }
        }
        intent.putParcelableArrayListExtra("units", modules);
        intent.putExtra("count", mQuanlityInput.getText().toString() + "");
        startActivity(intent);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        ToastUtils.showToast("" + error.getMessage());
    }

    @Override
    public void onResponse(ResponsePosition response) {

    }
}
