package mobile.freshtime.com.freshtime.device;

import android.os.Build;
import android.text.TextUtils;

import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.device.emulator.Emulator;
import mobile.freshtime.com.freshtime.device.urovo.UrovoDevice;

/**
 * Created by orkid on 2016/9/13.
 */
public class DeviceManager {

    public static ScanDevice initDevice(BaseActivity activity) {
        Devices device = Devices.getBrand(Build.BRAND);
        switch (device) {
            case UROVO:
                return new UrovoDevice(activity);
            default:
                return new Emulator();
        }
    }

    public enum Devices {
        NONE("emulator"),
        UROVO("qcom");

        private String mBrand;

        Devices(String brand) {
            this.mBrand = brand;
        }

        public static Devices getBrand(String brand) {
            if (TextUtils.isEmpty(brand)) return NONE;
            else if (brand.equals("qcom")) return UROVO;
            return NONE;
        }
    }
}
