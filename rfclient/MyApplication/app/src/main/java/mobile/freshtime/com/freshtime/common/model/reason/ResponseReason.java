package mobile.freshtime.com.freshtime.common.model.reason;

import java.util.List;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orkid on 2016/10/19.
 */
public class ResponseReason extends BaseResponse {

    public List<Reason> target;
}
