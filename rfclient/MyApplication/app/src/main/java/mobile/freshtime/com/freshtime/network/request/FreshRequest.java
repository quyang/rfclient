package mobile.freshtime.com.freshtime.network.request;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import mobile.freshtime.com.freshtime.FreshTimeApplication;

/**
 * Created by orkid on 2016/9/12.
 */
public class FreshRequest {

    public static final String TAG = "fresh_time";

    private RequestQueue mRequestQueue;

    private static FreshRequest ourInstance = new FreshRequest();

    public static FreshRequest getInstance() {
        return ourInstance;
    }

    private FreshRequest() {
        mRequestQueue = Volley.newRequestQueue(FreshTimeApplication.context);
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        // set the default tag if tag is empty
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        mRequestQueue.add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        mRequestQueue.add(req);
    }

    public void cancel(String tag) {
        mRequestQueue.cancelAll(tag);
    }
}
