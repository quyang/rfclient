package mobile.freshtime.com.freshtime.function.outgoing.model;

import java.io.Serializable;

/**
 * Created by Administrator on 2016/10/27.
 */

public class CommitBean implements Serializable{

    //品名
    public String itemName;

    //编码
    public String itemCode;

    //保质期
    public String baozhiqi;

    //有效期
    public String youxiaoqi;

    //规格
    public String guige;

    //单位
    public String danwei;

    //数量
    public String counts;

    //原因
    public int yuanying;

    //skuId
    public long skuId;

}
