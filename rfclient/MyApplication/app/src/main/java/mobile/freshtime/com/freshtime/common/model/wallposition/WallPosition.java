package mobile.freshtime.com.freshtime.common.model.wallposition;

/**
 * Created by orkid on 2016/10/17.
 */
public class WallPosition {

    public String barcode;
    public String code;
    public int columnNo;
    public long dr;
    public long id;
    public int rowNo;
    public String ts;
    public long wallId;
}
