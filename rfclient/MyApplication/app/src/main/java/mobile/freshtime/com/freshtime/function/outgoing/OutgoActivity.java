package mobile.freshtime.com.freshtime.function.outgoing;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.adapter.ReasonAdatper;
import mobile.freshtime.com.freshtime.common.model.reason.Reason;
import mobile.freshtime.com.freshtime.common.model.reason.ResponseReason;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.outgoing.model.OutSkuModel;
import mobile.freshtime.com.freshtime.function.outgoing.model.OutgoPage;
import mobile.freshtime.com.freshtime.function.outgoing.viewmodel.OutgoViewModel;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * 出库
 * Created by orchid on 16/9/16.
 */
public class OutgoActivity extends BaseActivity implements View.OnClickListener,
        ViewModelWatcher<OutgoPage>, PageListener<OutgoPage>, Response.ErrorListener, Response.Listener<ResponseReason>, DialogInterface.OnCancelListener {

    private View mInfoRoot;
    private ViewGroup mItemRoot;
    private ViewGroup mSkuRoot;
    private ViewGroup mQualityRoot;
    private ViewGroup mSourceRoot;

    private TextView mBefore;
    private TextView mNext;
    private TextView mDone;

    private TextView mItemName;
    private TextView mItemCode;

    private TextView mSkuName;
    public TextView mSkuCode;

    private TextView mQualityName;
    private TextView mQualityCode;

    private TextView mPostionName;
    public TextView mPostionCount;

    private EditText mScanContent;
    private EditText mPositionInput;

    private OutgoViewModel mViewModel;
    private int mReason;
    private TextView mCode69;
    private LinearLayout mLl_new_code;
    public NoInputEditText mNewInput;
    public TextView mBangDing;
    private TextView mDanwei;
    private TextView mKucun;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient mClient;
    private TextView mSafeKucun;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outgo);

        findViewById(R.id.back).setOnClickListener(this);

        mBangDing = (TextView) findViewById(R.id.bangding);
        mBangDing.setClickable(true);

        mBefore = (TextView) findViewById(R.id.last);
        mBefore.setOnClickListener(this);
        mNext = (TextView) findViewById(R.id.next);
        mNext.setOnClickListener(this);
        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);
        mSafeKucun = (TextView) findViewById(R.id.safe_kucun);
        mSafeKucun.setVisibility(View.VISIBLE);

        mInfoRoot = findViewById(R.id.info_root);
        mItemRoot = (ViewGroup) findViewById(R.id.item_info_root);
        mSkuRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        mSourceRoot = (ViewGroup) findViewById(R.id.source_info_root);
        mQualityRoot = (ViewGroup) findViewById(R.id.quality_info_root);
        mScanContent = (EditText) findViewById(R.id.scanContent);

        mNewInput = (NoInputEditText) mSkuRoot.findViewById(R.id.new_item_code);

        mDanwei = (TextView) mSourceRoot.findViewById(R.id.danwei);

        mLl_new_code = (LinearLayout) mSkuRoot.findViewById(R.id.new_code_ll);

        mSkuRoot.findViewById(R.id.item_code);

        mKucun = (TextView) mItemRoot.findViewById(R.id.item_coder);

        //69码
        mCode69 = (TextView) mInfoRoot.findViewById(R.id.item_69code);

        mItemName = (TextView) mItemRoot.findViewById(R.id.item_name);
        mItemCode = (TextView) mItemRoot.findViewById(R.id.item_code);

        mSkuName = (TextView) mSkuRoot.findViewById(R.id.item_name);
        //库位
        mSkuCode = (TextView) mSkuRoot.findViewById(R.id.item_code);

        mQualityName = (TextView) mQualityRoot.findViewById(R.id.item_name);
        mQualityCode = (TextView) mQualityRoot.findViewById(R.id.item_code);

        mPostionName = (TextView) mSourceRoot.findViewById(R.id.item_name);

        //商品数量
        mPostionCount = (TextView) mSourceRoot.findViewById(R.id.item_count);
        //仓位
        mPositionInput = (EditText) mSourceRoot.findViewById(R.id.position_input);

        mViewModel = new OutgoViewModel(this);
        mViewModel.addPageListener(this);
        mViewModel.registerContext(this);

        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", 1 + "");
            BaseRequest<ResponseReason> request =
                    new BaseRequest<>(RequestUtils.STORAGE_REASON,
                            ResponseReason.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mClient = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public TextView getKucun() {
        return mKucun;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.unregisterContext();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {
            if (mInfoRoot.getVisibility() == View.VISIBLE) {
                if ("".equals(mPostionCount.getText().toString().trim())) {
                    Toast.makeText(OutgoActivity.this, "请先输入数量", Toast.LENGTH_LONG).show();
                } else {
                    mViewModel.setCount(mPostionCount.getText().toString());
                    mViewModel.navNext();
                    requestDone();
                }
            }
        } else if (id == R.id.last) {
            mViewModel.navBefore();
        } else if (id == R.id.next) {
            if ("".equals(mPostionCount.getText().toString().trim())) {
                Toast.makeText(OutgoActivity.this, "请先输入数量", Toast.LENGTH_LONG).show();
            } else {
                mViewModel.setCount(mPostionCount.getText().toString());
                mViewModel.navNext();
            }
        }
    }

    @Override
    public void onScanResult(String result) {
        tiaoma = result;
        result = result.trim();
        mViewModel.onScanResult(result);
    }

    @Override
    public void onPageChanged(OutgoPage page) {
        displayPage(page);
    }

    private void displayPage(OutgoPage page) {
        if (mViewModel.hasBefore()) {
            mBefore.setEnabled(true);
        } else {
            mBefore.setEnabled(false);//影藏上一个按钮
        }

        if (!mViewModel.hasNext() &&
                page.getPhase() != OutgoPage.Phase.SKU
//                &&
//                page.getPhase() != OutgoPage.Phase.POSITION
                ) {
            mNext.setEnabled(false);
        } else {
            mNext.setEnabled(true);
        }

        OutgoPage.Phase phase = page.getPhase();
        if (phase == OutgoPage.Phase.START) {//原因
            displayStart(page);
        } else if (phase == OutgoPage.Phase.SKU) {//扫描商品条码
            displaySku(page);
        } else if (phase == OutgoPage.Phase.POSITION) {//扫描仓位
            displayPosition(page);
        } else {
            displayCount(page);//都不是
        }
    }

    /**
     * 初始化的UI
     *
     * @param page
     */
    private void displayStart(OutgoPage page) {
        mInfoRoot.setVisibility(View.GONE);
        mScanContent.requestFocus();

        mItemName.setText("品名：");
        mItemCode.setText("编码：");

        mSkuName.setText("规格：");
        mSkuCode.setText("库位：");

        mQualityName.setText("保质期：");
        mQualityCode.setText("有效期：");

        mPositionInput.setText("仓位：");
        mPostionCount.setText("");

        mScanContent.setText("");
    }

    /**
     * 扫到商品条码后  sku信息后展示的信息
     *
     * @param page
     */
    private void displaySku(OutgoPage page) {
        mInfoRoot.setVisibility(View.VISIBLE);

        OutSkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText("");

        mPositionInput.requestFocus();
    }

    /**
     * 输入库位信息后展示的信息
     *
     * @param page
     */
    private void displayPosition(OutgoPage page) {
        OutSkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText(unit.getPosition());

        mPostionCount.requestFocus();
    }

    /**
     * 输入数量信息后展示的信息
     *
     * @param page
     */
    private void displayCount(OutgoPage page) {
        OutSkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText(unit.getPosition());

        mPostionCount.clearFocus();
    }

    private void displayUnit(OutSkuModel unit) {
        mItemName.setText("品名：" + unit.getTitle());
        mItemCode.setText(unit.getSkuId() + "");
        mSkuName.setText("规格：" + unit.getUnStandardSpecification());
        mDanwei.setText(unit.saleUnitStr);

        mQualityName.setText("保质期：暂无");
        mQualityCode.setText("有效期：暂无");

        if ("null".equals(unit.getCount() + "")) {
            mPostionCount.requestFocus();
            mPostionCount.setText("");
        } else {
            mPostionCount.requestFocus();
            mPostionCount.length();
            mPostionCount.setText(unit.getCount() + "");
        }

        mSkuCode.setText(unit.kuwei);

        if (StringUtils.isEmpty(mSkuCode.getText().toString().trim())) {
            mNewInput.requestFocus();
        } else {
            mPostionCount.requestFocus();
        }

        mCode69.setText("商品69码:" + unit.getSku69Id());

        if (StringUtils.isKongStr(unit.kucun)) {
            mKucun.setText("总库存: 无");
        } else {
            mKucun.setText("总库存: " + unit.kucun);
        }

        if (StringUtils.isKongStr(unit.getSafeKucun())) {
            mSafeKucun.setText("安全库存：");
        } else {
            mSafeKucun.setText("安全库存：" + unit.getSafeKucun());
        }

    }

    //提交完成
    private void requestDone() {

        Intent intent = new Intent(this, OutgoConfirmActivity.class);
        List<OutgoPage> pages = mViewModel.getOutgoPages();
        ArrayList<OutSkuModel> modules = new ArrayList<>();
        for (int i = 0; i < pages.size(); i++) {
            OutgoPage page = pages.get(i);
            OutSkuModel model = page.getSkuModel();

            if (model != null && model.getPositionId() <= 0) {
                ToastUtils.showToast(getResources().getString(R.string.has_no_kuwei));
                return;
            }

            if (model != null && (page.getPhase() == OutgoPage.Phase.SKU)) {
                if (!"0".equals(pages.get(i).getSkuModel().getCount())) {
                    modules.add(pages.get(i).getSkuModel());
                }
            }
            intent.putParcelableArrayListExtra("units", modules);
            intent.putExtra("reason", mReason);
            intent.putExtra("count", mPostionCount.getText().toString() + "");

        }
        startActivity(intent);
    }

    @Override
    public void onRequestStart() {

    }

    @Override
    public void onRequestStop() {

    }

    @Override
    public void onPageUpdate(OutgoPage page) {
        displayPage(page);
    }

    @Override
    public void onError(String errorCode) {
        ToastUtils.showToast("" + errorCode);
    }

    @Override
    public void onFinish() {

    }

    @Override
    public void onErrorResponse(VolleyError error) {
        ToastUtils.showToast("" + error.getMessage());
    }

    @Override
    public void onResponse(ResponseReason response) {
        if (response != null && response.isSuccess()) {
            selectReason(response.target);
        }
    }

    private void selectReason(final List<Reason> list) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, android.R.style.Theme_Holo_Light_Dialog);
        builder.setTitle("选择入库原因");
        builder.setAdapter(new ReasonAdatper(this, list), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mReason = (int) list.get(which).id;
            }
        });
        builder.setOnCancelListener(this);
        builder.show();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        Toast.makeText(this, "必须选择原因", Toast.LENGTH_SHORT).show();
        finish();
    }

    public String tiaoma;

    public String getTiaoma() {
        return tiaoma;
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Outgo Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        mClient.connect();
        AppIndex.AppIndexApi.start(mClient, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(mClient, getIndexApiAction());
        mClient.disconnect();
    }
}
