package mobile.freshtime.com.freshtime.function.pick.model;

/**
 * Created by orchid on 16/10/10.
 */

public class PickPage {

    private Task mTask;
    private Phase mPhase;

    public PickPage(Task task) {
        this.mPhase = Phase.START;
        this.mTask = task;
    }

    public Task getTask() {
        return mTask;
    }

    public Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == Phase.START) {
            mPhase = Phase.COUNT;
        }
        else if (mPhase == Phase.COUNT) {
            mPhase = Phase.DONE;
        }
//
//        else if (mPhase == Phase.COUNT) {
//            mPhase = Phase.DONE;
//        }
//
//        if (mPhase == Phase.START) {
//            mPhase = Phase.POSITION;
//        }
//        else if (mPhase == Phase.POSITION) {
//            mPhase = Phase.COUNT;
//        }
//        else if (mPhase == Phase.COUNT) {
//            mPhase = Phase.DONE;
//        }
    }

    public enum Phase {
        START,
        POSITION,
        COUNT,
        DONE
    }
}
