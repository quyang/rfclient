package mobile.freshtime.com.freshtime.protocol;

import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/10/28.
 */

public class PostProtocol extends BaseProtocal {

    private FormBody mBody;
    private String url;

    public PostProtocol(FormBody body, String url) {
        mBody = body;
        this.url = url;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public FormBody getFormBody() {
        return mBody;
    }
}
