package mobile.freshtime.com.freshtime.function.outgoing.model;

/**
 * Created by orchid on 16/9/22.
 */
public class OutgoPage {

//    private SkuModel mSkuModel;
    private OutSkuModel mSkuModel;
    private OutgoPage.Phase mPhase;

    public OutgoPage(OutSkuModel skuModel) {
        this.mPhase = OutgoPage.Phase.START;
        this.mSkuModel = skuModel;
    }

    public OutSkuModel getSkuModel() {
        return mSkuModel;
    }

    public void setSkuModel(OutSkuModel outgoUnit) {
        mSkuModel = outgoUnit;
    }

    public OutgoPage.Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == OutgoPage.Phase.START) {
            mPhase = OutgoPage.Phase.SKU;
        } else if (mPhase == OutgoPage.Phase.SKU) {
            mPhase = OutgoPage.Phase.POSITION;
        } else if (mPhase == OutgoPage.Phase.POSITION){
            mPhase = OutgoPage.Phase.DONE;
        }
    }

    public enum Phase {
        START,
        SKU,
        POSITION,
        DONE
    }
}
