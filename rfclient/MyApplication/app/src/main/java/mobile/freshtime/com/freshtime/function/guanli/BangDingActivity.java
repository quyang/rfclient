package mobile.freshtime.com.freshtime.function.guanli;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.function.guanli.module.KuCunInfo;
import mobile.freshtime.com.freshtime.function.kuweitiaozheng.module.PositonBean;
import mobile.freshtime.com.freshtime.function.search.SearchBean;
import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.protocol.PostProtocol;
import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import okhttp3.FormBody;

/**
 * 创建activity
 */
public class BangDingActivity extends BaseActivity implements ViewInterface, BangListener, View.OnClickListener {

    @Bind(R.id.back)
    TextView mBack;
    @Bind(R.id.ll)
    LinearLayout mLl;
    @Bind(R.id.et_old)
    NoInputEditText mEtOld;
    @Bind(R.id.input_ll_old_code)
    LinearLayout mInputLlOldCode;
    @Bind(R.id.et_new)
    NoInputEditText mEtNew;
    @Bind(R.id.input_ll_new_code)
    LinearLayout mInputLlNewCode;
    @Bind(R.id.input_bing_new_code)
    LinearLayout mInputBingNewCode;
    @Bind(R.id.view1)
    TextView mView1;
    @Bind(R.id.view2)
    TextView mView2;
    @Bind(R.id.ll_container)
    LinearLayout mLlContainer;
    @Bind(R.id.view3)
    TextView mView3;
    @Bind(R.id.sku_name)
    TextView mSkuName;
    @Bind(R.id.view4)
    TextView mView4;
    @Bind(R.id.view5)
    TextView mView5;
    @Bind(R.id.view6)
    TextView mView6;
    @Bind(R.id.view7)
    TextView mView7;
    @Bind(R.id.view9)
    TextView mView9;
    @Bind(R.id.search)
    RadioButton mSearch;
    @Bind(R.id.bangding)
    RadioButton mBangding;
    @Bind(R.id.btn_ok)
    RadioButton mBtnOk;
    @Bind(R.id.jiebang)
    RadioButton mJiebang;
    @Bind(R.id.rg)
    RadioGroup mRg;
    @Bind(R.id.activity_search)
    RelativeLayout mActivitySearch;
    private BangDingActivityPresenter mPresenter;
    private NoInputEditText mEtShouDong;
    private List<PositonBean.TargetBean> mTarget;
    private TextView mSafeKucun;
    private LinearLayout mLinearLayout;
    private EditText mKucun;
    private RadioButton mRbSafeKucun;
    private Dialog mMDialog;
    private String mSkuId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);

        mEtShouDong = (NoInputEditText) findViewById(R.id.et_shoudong);
        mEtShouDong.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
        mJiebang.setOnClickListener(this);

        mSafeKucun = (TextView) findViewById(R.id.view10);
        mKucun = (NoInputEditText) findViewById(R.id.sf_kucun);
        mRbSafeKucun = (RadioButton) findViewById(R.id.commit_safe_kucun);

        mEtNew.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
        mEtOld.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
        mKucun.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
        mKucun.setVisibility(View.GONE);

        mBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mBtnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mPresenter = new BangDingActivityPresenter(this);
        mRbSafeKucun.setOnClickListener(this);

    }


    @Override
    public void onScanResult(String result) {

        if (mEtNew.hasFocus()) {
            mPresenter.bindNewCode(result.trim());
            return;
        }

        mPresenter.load(result.trim());

    }

    @Override
    public void success(String json) {

        if (json == null || "null".equals(json) || "".equals(json)) {
            return;
        }

        try {
            JSONObject object = new JSONObject(json);
            boolean success = object.getBoolean("success");
            if (success) {
                parseJson(json);
            } else {
                ToastUtils.showToast("" + object.getString("errorMsg"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void parseJson(final String json1) {

        if (StringUtils.isEmpty(json1)) {
            return;
        }

        Gson gson = new Gson();
        final SearchBean bean = gson.fromJson(json1, SearchBean.class);

        if (bean == null) {
            return;
        }

        SearchBean.ModuleBean module = bean.module;
        String skuId = module.skuId;

        mEtOld.setText(module.sku69Id);

        String s = RequestUtils.INTO_STORAGE + "request=" +
                "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + skuId + "\"}";

        GetProtocol protocol = new GetProtocol(s);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                linkData(bean, json);
            }

            @Override
            public void onFail() {
            }
        });
    }


    @TargetApi(Build.VERSION_CODES.M)
    private void linkData(SearchBean bean, String json) {

        SearchBean.ModuleBean module = bean.module;
        String title = module.title;//品名
        //skuId
        mSkuId = module.skuId;
        String price = module.unitPrice;//价格
        String code69 = module.sku69Id;//商品69码
        String guige = module.unStandardSpecification;//规格
        int storageType = module.storageType;//存储类型
        String changjia = (String) module.supplierName;//厂家


        mView1.setText("品名: " + title);
        mSkuName.setText("SkuId: ");
        mView4.setText(mSkuId);
        mView3.setText("价格: " + price);
        mView6.setText("69码: " + code69);
        mView5.setText("规格: " + guige);
        mView9.setText("厂家: " + changjia);


        String leixin = "";
        switch (storageType) {
            case 78:
                leixin = "活物";
                break;
            case 79:
                leixin = "常温";
                break;
            case 80:
                leixin = "恒温";
                break;
            case 81:
                leixin = "冷藏";
                break;
            case 82:
                leixin = "冷冻";
                break;
        }

        mView7.setText("存储类型: " + leixin);


        //-----------------------------------------------------------------------------

        final Gson gon = new Gson();
        CuWeiBean kuCun = gon.fromJson(json, CuWeiBean.class);

        mLlContainer.removeAllViews();


        //获取商品对应的库存
        getAllKuCun2(mSkuId);

        //获取总库存
        getAllKuCun(mSkuId);

        mKucun.setVisibility(View.VISIBLE);
        mKucun.setText("");

    }

    private void getAllKuCun2(final String input) {
        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", input);
            GetProtocol protocol = new GetProtocol(RequestUtils.POSITION + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("KuweiTiaoZhengActivityModule.onSuccess=和=fosiodfho=" + json);
                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast(getResources().getString(R.string.no_more_msg));
                        return;
                    }

                    try {
                        Gson gson = new Gson();
                        PositonBean info = gson.fromJson(json, PositonBean.class);
                        if (info != null && info.isSuccess()) {
                            mTarget = info.getTarget();

                            if (mTarget != null) {
                                for (int i = 0; i < mTarget.size(); i++) {

                                    View inflate = View.inflate(BangDingActivity.this, R.layout.kuwei, null);
                                    TextView kuwei = (TextView) inflate.findViewById(R.id.kuwei);
                                    TextView kucun = (TextView) inflate.findViewById(R.id.kucun);
                                    kuwei.setTextColor(Color.BLACK);
                                    kucun.setTextColor(Color.BLACK);
                                    kuwei.setTextSize(12);
                                    kucun.setTextSize(12);

                                    String code = (String) mTarget.get(i).getPositionCode();
                                    int pid = mTarget.get(i).getPositionId();

                                    if (code == null) {
                                        kuwei.setText("库位" + ": " + pid);
                                    } else {
                                        kuwei.setText("库位" + ": " + code);
                                    }

                                    kucun.setText("库存: " + mTarget.get(i).getNumberShow());

                                    mLlContainer.addView(inflate);
                                }
                            }


                        } else {
                            ToastUtils.showToast("" + info.getMessage());
                        }
                    } catch (Exception e) {
                        ToastUtils.showToast("" + e.getMessage());
                    }
                }

                @Override
                public void onFail() {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getAllKuCun(String skuId) {
        //GET_KUCUN
        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", skuId);

            String url = RequestUtils.GET_KUCUN + "?request=" + object;
            GetProtocol protocol = new GetProtocol(url);

            System.out.println("BangDingActivity.linkData=" + url);

            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("BangDingActivity.onSuccessdddd=" + json);
                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast(getResources().getString(R.string.no_more_msg));
                    } else {
                        try {
                            Gson gson = new Gson();
                            KuCunInfo info = gson.fromJson(json, KuCunInfo.class);
                            if (info.isSuccess()) {
                                String numberShow = info.getTarget().getNumberShowTotal();
                                mView2.setText("总库存:" + numberShow);
                                mSafeKucun.setText("安全库存： " + info.getTarget().getGuardBit());

                            } else {
                                ToastUtils.showToast("" + info.getMessage());
                                mView2.setText("库存: 获取失败");
                            }
                        } catch (Exception e) {
                            ToastUtils.showToast(e.getMessage());
                            mView2.setText(getResources().getString(R.string.kucun));
                        }
                    }
                }

                @Override
                public void onFail() {
                    mView2.setText("库存: 获取失败");

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void fail() {
    }

    @Override
    public void bangOk() {
        if (!StringUtils.isEmpty(mEtOld.getText().toString().trim())) {
            mPresenter.load(mEtOld.getText().toString().trim());
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.jiebang || mEtShouDong.hasFocus()) {
            String skuId = mEtShouDong.getText().toString().trim();
            if (!StringUtils.isEmpty(skuId)) {
                FormBody body = new FormBody.Builder()
                        .add("skuCode", skuId)
                        .build();
                PostProtocol protocol = new PostProtocol(body, RequestUtils.SKU);
                protocol.post();
                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                    @Override
                    public void onSuccess(String json) {
                        success(json);
                    }

                    @Override
                    public void onFail() {
                    }
                });
            }

        }


        if (v.getId() == R.id.commit_safe_kucun) {
            if (StringUtils.isKongStr(mKucun.getText().toString().trim())) {
                Toast.makeText(getApplicationContext(), "请先输入安全库存数量", Toast.LENGTH_SHORT).show();
                return;
            }

            mMDialog = new Dialog(this, R.style.MyDialog);
            View view = getLayoutInflater().inflate(R.layout.cancel, null);
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText("确定要重新设置安全库位吗?");
            Button no = (Button) view.findViewById(R.id.no);
            Button yes = (Button) view.findViewById(R.id.yes);

            mMDialog.setCancelable(false);
            mMDialog.setContentView(view);

            //后面给确定和取消按钮设置监听,onclicklistener;
            no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mMDialog.dismiss();
                }
            });

            yes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    commitSafeKucun();
                }
            });

            //设置弹窗宽高
            mMDialog.getWindow().setLayout(UiUtils.getScreenHeight(this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

            mMDialog.show();
        }
    }

    private void commitSafeKucun() {

        if (NetUtils.hasNet(this)) {

            JSONObject params = new JSONObject();
            JSONObject obj = new JSONObject();
            try {
                params.put("token", Login.getInstance().getToken());

                obj.put("skuId", mSkuId);
                obj.put("coutn", (mKucun.getText().toString().trim()));

                params.put("data", obj);

            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            }


            String url = RequestUtils.SAFEKUCUN + "?request=" + params;
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    if (UiUtils.isEmpty(json)) {
                        return;
                    }

                    try {
                        JSONObject object = new JSONObject(json);
                        if (object.getBoolean("success")) {
                            Toast.makeText(BangDingActivity.this, "提交成功", Toast.LENGTH_SHORT).show();
                            mKucun.setText("");
                            mMDialog.dismiss();
                        } else {
                            Toast.makeText(BangDingActivity.this, "" + object.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(BangDingActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFail() {

                }
            });
        } else {
            Toast.makeText(this, getResources().getString(R.string.net_error), Toast.LENGTH_SHORT).show();
        }
    }


}
