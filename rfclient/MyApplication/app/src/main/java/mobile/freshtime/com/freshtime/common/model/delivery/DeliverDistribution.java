package mobile.freshtime.com.freshtime.common.model.delivery;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by orchid on 16/10/15.
 */

public class DeliverDistribution implements Parcelable {

    public long agencyId;
    public long deliverId;
    public long distributionId;
    public long dr;
    public long id;
    public String ts;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(agencyId);
        dest.writeLong(deliverId);
        dest.writeLong(distributionId);
        dest.writeLong(dr);
        dest.writeLong(id);
        dest.writeString(ts);
    }

    public static final Parcelable.Creator<DeliverDistribution> CREATOR =
            new Parcelable.Creator<DeliverDistribution>() {

                @Override
                public DeliverDistribution createFromParcel(Parcel source) {
                    DeliverDistribution model = new DeliverDistribution();
                    model.agencyId = source.readLong();
                    model.deliverId = source.readLong();
                    model.distributionId = source.readLong();
                    model.dr = source.readLong();
                    model.id = source.readLong();
                    model.ts = source.readString();
                    return model;
                }

                @Override
                public DeliverDistribution[] newArray(int size) {
                    return new DeliverDistribution[size];
                }
            };
}
