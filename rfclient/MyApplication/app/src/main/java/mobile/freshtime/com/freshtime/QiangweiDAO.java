package mobile.freshtime.com.freshtime;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Administrator on 2016/11/1.
 */

public class QiangweiDAO {


    public SQLiteHelper mHelper;

    public QiangweiDAO(SQLiteHelper helper) {
        mHelper = helper;
    }

    /**
     * 往数据库添加一条数据
     */
    public void add(String barcode, String code) {
//        boolean result = find(barcode);
//        if (result)
//            return;


        SQLiteDatabase db = mHelper.getWritableDatabase();
        if (db.isOpen()) {
            db.execSQL("insert into fengjianqiang (barcode,code) values (?,?)",
                    new Object[]{barcode, code});
            // 关闭数据库 释放数据库的链接
            db.close();
        }
    }


    /**
     * 查找数据库的操作
     */
    public boolean find(String barcode) {
        boolean result = false;
        SQLiteDatabase db = mHelper.getReadableDatabase();
//        if (db.isOpen()) {
            Cursor cursor = db.rawQuery("select * from fengjianqiang where barcode=?",
                    new String[]{barcode});
//            if (cursor.moveToFirst()) {
//                int index = cursor.getColumnIndex("code"); // 得到phone在表中是第几列
//                String phone = cursor.getString(index);
//                result = true;
//
        if (cursor.moveToNext()) {
            result = true;
        }
//            }
            // 记得关闭掉 cursor
            cursor.close();
            result = false;
            // 释放数据库的链接
            db.close();
//        }
        return result;
    }

    /**
     * 删除一条记录
     *
     * @param barcode
     */
    public void delete(String barcode) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        if (db.isOpen()) {
            db.execSQL("delete from fengjianqiang where barcode =?",
                    new Object[]{barcode});
            db.close();
        }
    }

    /**
     * 更新一条记录
     */
    public void update(String barcode, String newbarcode, String newcode) {
        SQLiteDatabase db = mHelper.getWritableDatabase();
        if (db.isOpen()) {
            db.execSQL("update fengjianqiang set barcode=? , code=? where barcode=?",
                    new Object[]{newbarcode, newcode, barcode});
            db.close();
        }
    }

    /**
     * 查找全部
     */
    public int getAll(String barcode) {
        SQLiteDatabase db = mHelper.getReadableDatabase();
//        if (db.isOpen()) {
            Cursor cursor = db.rawQuery("select count(*) from fengjianqiang where barcode=?", new String[]{barcode});
//            while (cursor.moveToNext()) {
//
//            }
            return cursor.getCount();
//            cursor.close();
//            db.close();

//        }
    }
}
