package mobile.freshtime.com.freshtime.utils;

import com.google.gson.Gson;

/**
 * Created by Administrator on 2016/12/16.
 */

public class GsonObject {


    private static Gson instance = null;

    public static Gson getInstance() {
        if (instance == null) {
            synchronized (GsonObject.class) {
                if (instance == null) {
                    instance = new Gson();
                }
            }
        }
        return instance;
    }
}
