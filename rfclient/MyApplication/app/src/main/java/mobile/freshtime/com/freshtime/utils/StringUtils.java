package mobile.freshtime.com.freshtime.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Administrator on 2016/10/29.
 */
public class StringUtils {


    public static boolean isEmpty(String s) {

        if ("".equals(s) || "null".equals(s) || s == null || "0".equals(s)) {
            return true;
        } else {
            return false;
        }
    }


    public static boolean isKongStr(String s) {

        if ("".equals(s) || s == null || "null".equals(s)) {
            return true;
        } else {
            return false;
        }
    }


    public static String getString(int aInt) {
        return String.valueOf(aInt);
    }

    public static Long getLong(String str) {
        return Long.parseLong(str);
    }


    public static boolean isEmpty2(String s) {

        if ("".equals(s) || "null".equals(s) || s == null) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * judge whether the string is null or ""
     *
     * @param str String wo input
     * @return String wo get
     */
    public static boolean juege(String str) {

        if (str != null || !"".equals(str)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * get the right length String wo want
     *
     * @param txt String wo put
     */
    public static String getString(String txt) {
        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(txt);
        if (m.matches()) {
            //数字
        }

        p = Pattern.compile("[a-zA-Z]");
        m = p.matcher(txt);
        if (m.matches()) {
            //字母
            txt = txt.substring(0, 20);
        }

        p = Pattern.compile("[\u4e00-\u9fa5]");
        m = p.matcher(txt);
        if (m.matches()) {
            //汉字
            txt = txt.substring(0, 10);
        }

        return txt;
    }


    /**
     * 判断一个字符串是否含有汉字
     *
     * @param s 要判断的字符串
     * @return
     */
    public static boolean isHasHanzi(String s) {

        boolean has = false;

        for (int i = 0; i < s.length(); i++) {
            Pattern pa = Pattern.compile("^[\u4e00-\u9fa5]*$");
            Matcher matcher = pa.matcher(s.charAt(i) + "");
            if (matcher.find()) {
                has = true;
            }
        }

        return has;
    }


    /**
     * judge is number
     *
     * @param txt String wo put
     */
    public static boolean isNumber(String txt) {


        Pattern p = Pattern.compile("[0-9]*");
        Matcher m = p.matcher(txt);
        if (m.matches()) {
            //数字
            return true;
        }
//
//        p = Pattern.compile("[a-zA-Z]");
//        m = p.matcher(txt);
//        if (m.matches()) {
//            //字母
//            txt = txt.substring(0, 20);
//        }
//
//        p = Pattern.compile("[\u4e00-\u9fa5]");
//        m = p.matcher(txt);
//        if (m.matches()) {
//            //汉字
//            txt = txt.substring(0, 10);
//        }

        return false;
    }


}
