package mobile.freshtime.com.freshtime.function.stock.module;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.function.stock.TwoPanListActivity;

/**
 * Created by Administrator on 2016/11/18.
 */
public class MyTwoBaseAdapter extends BaseAdapter {

    public static final int TOP = 1;
    public static final int NORMAL = 2;

    private List<TwoPanInfo.TargetBean.DetailsBean> mList;

    private TwoPanListActivity mactivity;


    public MyTwoBaseAdapter(List<TwoPanInfo.TargetBean.DetailsBean> list, TwoPanListActivity activity) {
        mList = list;
        mactivity = activity;

    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public TwoPanInfo.TargetBean.DetailsBean getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {


        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(mactivity, R.layout.pandian_lv_head_two, null);

            holder = new ViewHolder();

            holder.item_name = (TextView) convertView.findViewById(R.id.item_name);
            holder.kuwei = (TextView) convertView.findViewById(R.id.kuwei);
            holder.guige_left = (TextView) convertView.findViewById(R.id.guige_left);
            holder.guige_mid = (TextView) convertView.findViewById(R.id.guige_mid);
            holder.guige_right = (TextView) convertView.findViewById(R.id.guige_right);
            holder.choujian = (TextView) convertView.findViewById(R.id.choujian);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        final TwoPanInfo.TargetBean.DetailsBean item = getItem(position);

        holder.item_name.setText(item.getSkuName());
        holder.kuwei.setText(item.getPositionCode());
        holder.guige_left.setText(item.getFirstNo());//一盘数量

        if (StringUtils.isEmpty2(item.getSecondNo() + "")) {
            holder.guige_mid.setText("");
        } else {
            holder.guige_mid.setText(item.getSecondNo() + "");
        }

        if (StringUtils.isEmpty(item.count + "")) {
            holder.guige_right.setText("");
        } else {
            holder.guige_right.setText(item.count + "");
        }

        return convertView;
    }

    static class ViewHolder {


        public TextView item_name;
        public TextView kuwei;
        public TextView guige_left;
        public TextView guige_mid;
        public TextView guige_right;
        public TextView choujian;


    }
}
