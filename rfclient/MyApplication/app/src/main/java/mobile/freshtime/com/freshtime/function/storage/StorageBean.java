package mobile.freshtime.com.freshtime.function.storage;

/**
 * Created by Administrator on 2016/11/7.
 */
public class StorageBean {


    /**
     * message : null
     * return_code : 0
     * success : true
     * target : {"agencyId":1,"areaId":9,"areaType":null,"barcode":"02800100000000003481","code":"HC00901","columnNo":null,"dr":0,"id":868,"rowNo":null,"slottingId":null,"slottingType":null,"ts":"2016-11-06T18:15:40"}
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * areaId : 9
     * areaType : null
     * barcode : 02800100000000003481
     * code : HC00901
     * columnNo : null
     * dr : 0
     * id : 868
     * rowNo : null
     * slottingId : null
     * slottingType : null
     * ts : 2016-11-06T18:15:40
     */

    private TargetBean target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public TargetBean getTarget() {
        return target;
    }

    public void setTarget(TargetBean target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private int areaId;
        private Object areaType;
        private String barcode;
        private String code;
        private Object columnNo;
        private int dr;
        private long id;
        private Object rowNo;
        private Object slottingId;
        private Object slottingType;
        private String ts;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public Object getAreaType() {
            return areaType;
        }

        public void setAreaType(Object areaType) {
            this.areaType = areaType;
        }

        public String getBarcode() {
            return barcode;
        }

        public void setBarcode(String barcode) {
            this.barcode = barcode;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public Object getColumnNo() {
            return columnNo;
        }

        public void setColumnNo(Object columnNo) {
            this.columnNo = columnNo;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public long getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public Object getRowNo() {
            return rowNo;
        }

        public void setRowNo(Object rowNo) {
            this.rowNo = rowNo;
        }

        public Object getSlottingId() {
            return slottingId;
        }

        public void setSlottingId(Object slottingId) {
            this.slottingId = slottingId;
        }

        public Object getSlottingType() {
            return slottingType;
        }

        public void setSlottingType(Object slottingType) {
            this.slottingType = slottingType;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }
    }
}
