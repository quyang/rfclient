package mobile.freshtime.com.freshtime.function.search;

import android.view.View;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by Administrator on 2016/11/11.
 */
public class MyClick implements View.OnClickListener {

    private SearchActivity mActivity;

    public MyClick(SearchActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.jiebang://商品与指定库位解绑
                //根据库位barcode获取pid
                getPid();
                break;
            case R.id.back:
                mActivity.finish();
            case R.id.bangding://绑定
                bandingStorage();
                break;
        }
    }

    //绑定到指定库位
    private void bandingStorage() {

    }

    private void getPid() {
        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", mActivity.mView4.getText().toString().trim());
            String url = RequestUtils.INTO_STORAGE + "request=" + object.toString();

            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    if (StringUtils.isEmpty(json)) {
                        return;
                    }

                    Gson gson = new Gson();
                    CuWeiBean bean = gson.fromJson(json, CuWeiBean.class);
                    if (bean.success && bean.target != null && bean.target.size() > 0 && bean.target.get(0).pisitionVO != null) {
                        CuWeiBean.TargetBean.PisitionVOBean pisitionVO = bean.target.get(0).pisitionVO;


                    } else {

                    }


//
//                    Gson gson = new Gson();
//                    StorageBean bean = gson.fromJson(json, StorageBean.class);
//                    if (bean != null && bean.isSuccess() && bean.getTarget() != null && bean.getTarget() != null) {
//
//                        System.out.println("MyClick.onSuccess pid=" + bean.getTarget().getId());
//                        System.out.println("MyClick.onSuccess cuwei=" + bean.getTarget().getCode());
//
//                        //设置code和pid
////                        currentPage.getSkuModel().setCuwei(bean.getTarget().getCode());
////                        currentPage.getSkuModel().setPositionId(bean.getTarget().getId());
//
//                        jieBang(bean.getTarget().getId());
//
//                    } else {
////                        ToastUtils.showToast("请扫描正确的库位");
//                    }
                }

                @Override
                public void onFail() {

                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //解绑库位
    private void jieBang(long pid) {

        if (StringUtils.isEmpty(mActivity.mView4.getText().toString().trim())) {
            return;
        }

        if (!NetUtils.hasNet(mActivity)) {
            ToastUtils.showToast("连接网络失败,请检查网络环境");
            return;
        }

        if (StringUtils.isEmpty(Login.getInstance().getToken()) || StringUtils.isEmpty(pid + "") || StringUtils.isEmpty(mActivity.mView4.getText().toString().trim())) {
            throw new IllegalArgumentException("jiebang 解绑参数异常");
        }


        String url = RequestUtils.DEL_CODE +
                "?request={token:'" + Login.getInstance().getToken() +
                "',data:{skuId:'" + mActivity.mView4.getText().toString().trim() + "',positionId:" + pid + "}}";

        GetProtocol protocol = new GetProtocol(url);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {

                try {
                    JSONObject object = new JSONObject(json);
                    if (object.getBoolean("success")) {
                        ToastUtils.showToast("解绑成功");
                        mActivity.mView6.setText("库位: 无");
                        mActivity.mBangding.setEnabled(true);
                        mActivity.mBangding.setClickable(true);
                        mActivity.mEtNew.setVisibility(View.VISIBLE);
                        mActivity.mEtNew.requestFocus();
                    } else {
                        ToastUtils.showToast("解绑失败");
                        mActivity.mBangding.setEnabled(false);
                        mActivity.mBangding.setClickable(false);
                        mActivity.mEtNew.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFail() {

            }
        });
    }
}
