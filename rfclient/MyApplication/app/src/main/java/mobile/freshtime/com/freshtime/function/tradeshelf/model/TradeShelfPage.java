package mobile.freshtime.com.freshtime.function.tradeshelf.model;

import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;

/**
 * Created by orchid on 16/9/24.
 */

public class TradeShelfPage  {

    private SkuModel mSkuModel;
    private TradeShelfPage.Phase mPhase;

    public TradeShelfPage(SkuModel skuModel) {
        this.mPhase = TradeShelfPage.Phase.START;
        this.mSkuModel = skuModel;
    }

    public SkuModel getSkuModel() {
        return mSkuModel;
    }

    public void setSkuModel(SkuModel outgoUnit) {
        mSkuModel = outgoUnit;
    }

    public TradeShelfPage.Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == TradeShelfPage.Phase.START) {
            mPhase = TradeShelfPage.Phase.SKU;
        } else if (mPhase == TradeShelfPage.Phase.SKU) {
            mPhase = TradeShelfPage.Phase.POSITION;
        } else if (mPhase == TradeShelfPage.Phase.POSITION){
            mPhase = TradeShelfPage.Phase.DONE;
        }
    }

    public enum Phase {
        START,
        SKU,
        POSITION,
        DONE
    }
}
