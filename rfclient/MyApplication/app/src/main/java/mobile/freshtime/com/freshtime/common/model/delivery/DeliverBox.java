package mobile.freshtime.com.freshtime.common.model.delivery;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by orchid on 16/10/15.
 */

public class DeliverBox implements Parcelable {

    public long agencyId;
    public String barcode;
    public String code;
    public long dr;
    public long id;
    public int storeType;
    public String ts;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(agencyId);
        dest.writeString(barcode);
        dest.writeString(code);
        dest.writeLong(dr);
        dest.writeLong(id);
        dest.writeInt(storeType);
        dest.writeString(ts);
    }

    public static final Parcelable.Creator<DeliverBox> CREATOR =
            new Parcelable.Creator<DeliverBox>() {

                @Override
                public DeliverBox createFromParcel(Parcel source) {
                    DeliverBox model = new DeliverBox();
                    model.agencyId = source.readLong();
                    model.barcode = source.readString();
                    model.code = source.readString();
                    model.dr = source.readLong();
                    model.id = source.readLong();
                    model.storeType = source.readInt();
                    model.ts = source.readString();
                    return model;
                }

                @Override
                public DeliverBox[] newArray(int size) {
                    return new DeliverBox[size];
                }
            };
}
