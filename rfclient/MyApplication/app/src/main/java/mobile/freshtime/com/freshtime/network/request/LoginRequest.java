package mobile.freshtime.com.freshtime.network.request;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.net.URLEncoder;

import mobile.freshtime.com.freshtime.common.model.LoginResponse;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;

/**
 * Created by orchid on 16/9/29.
 */

public class LoginRequest extends Request<LoginResponse> {

    private static final String TAG = "LoginRequest";

    private String mUserId;
    private String mEquipmentId;
    private long mTimeStamp;
    private String mParam;
    private WeakReference<Response.Listener<LoginResponse>> mListener;

    public LoginRequest(String userId,
                        String password,
                        String equipmentId,
                        Response.Listener<LoginResponse> listener,
                        Response.ErrorListener errorListener) {
        super(Method.POST, RequestUtils.LOGIN, errorListener);
        this.mUserId = userId;
        this.mEquipmentId = equipmentId;
        JSONObject param = new JSONObject();
        try {
            mTimeStamp = System.currentTimeMillis();
            param.put("userId", userId);
            param.put("equipmentId", Login.MD5("hardcode"));
            param.put("sysTime", mTimeStamp);
            param.put("sign", Login.MIX(password, mTimeStamp, equipmentId));
            this.mParam = "data=" + URLEncoder.encode(param.toString(), "UTF-8");
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        this.mListener = new WeakReference<>(listener);
    }

    @Override
    protected Response<LoginResponse> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =
                    new String(response.data, "UTF-8");
            Log.e("freshtime", "volley response : " + jsonString);
            LoginResponse result = new LoginResponse(new JSONObject(jsonString));
            result.setUserId(this.mUserId);
            result.setEquipmentId(this.mEquipmentId);
            result.setTimeStamp(this.mTimeStamp);
            return Response.success(
                    result,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(LoginResponse response) {
        if (mListener != null && mListener.get() != null)
            mListener.get().onResponse(response);
        else
            Log.e("orkid", "request released!");
    }

    @Override
    public byte[] getBody() {
        try {
            return mParam == null ? null : mParam.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mParam, "utf-8");
            return null;
        }
    }
}