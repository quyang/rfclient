package mobile.freshtime.com.freshtime.function.kuweitiaozheng.module;

import android.content.Intent;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.function.guanli.module.ItemInfo;
import mobile.freshtime.com.freshtime.function.kuweitiaozheng.KuweiTiaoZhengActivity;
import mobile.freshtime.com.freshtime.function.search.PositionBarcodeInfo;
import mobile.freshtime.com.freshtime.function.search.SearchBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.protocol.PostProtocol;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/12/13.
 */
public class KuweiTiaoZhengActivityModule {

    private KuweiTiaoZhengActivity mActivity;
    private final ItemInfo mInfo;
    private List<PositonBean.TargetBean> mTarget;


    public KuweiTiaoZhengActivityModule(KuweiTiaoZhengActivity activity) {
        mActivity = activity;
        mInfo = new ItemInfo();
    }

    public void onScanResult(String input) {

        if (mActivity.mCodeTiaoMa.hasFocus()) {
            if (!StringUtils.isKongStr(mActivity.mItemCode.getText().toString().trim())) {
                bindNewCode(input);
            } else {
                ToastUtils.showToast(mActivity.getResources().getString(R.string.scan_item_first));
            }
        } else if (mActivity.mItemCode.hasFocus()) {

            getAllKuCun(input);
        }
    }

    public void getAllKuCun(final String input) {
        getIitemInfo(input);
    }

    private void getIitemInfo(String input) {


        mInfo.itemCode = input;


        //获取skuId
        FormBody body = new FormBody.Builder()
                .add("skuCode", input)
                .build();
        PostProtocol protocol = new PostProtocol(body, RequestUtils.SKU);
        protocol.post();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {

                System.out.println("KuweiTiaoZhengActivityModule.onSuccess=" + json);

                if (StringUtils.isEmpty(json)) {
                    return;
                }

                Gson gson = new Gson();
                final SearchBean bean = gson.fromJson(json, SearchBean.class);

                if (bean == null) {
                    return;
                }

                SearchBean.ModuleBean module = bean.module;
                mInfo.skuId = module.skuId;
                mInfo.title = module.title;
                mInfo.code69 = module.sku69Id + "";

                mlistener.onPageChange(mInfo);

                mActivity.mCodeTiaoMa.requestFocus();

            }

            @Override
            public void onFail() {
            }
        });
    }

    private void getInventoryByPositionAndSku() {

        if (StringUtils.isEmpty(mInfo.skuId) || StringUtils.isEmpty(mInfo.pid)) {
            return;
        }

        JSONObject object = new JSONObject();
        JSONObject obj = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            obj.put("skuId", mInfo.skuId);
            obj.put("positionId", mInfo.pid);
            object.put("data", obj);

            GetProtocol protocol = new GetProtocol(RequestUtils.GET_COUNT_IN_ONE_KUWEI + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    System.out.println("KuweiTiaoZhengActivityModule.onSuccess=" + json);

                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast(mActivity.getResources().getString(R.string.no_more_msg));
                        return;
                    }
                    Gson gson = new Gson();
                    ItemKucunInfo info = gson.fromJson(json, ItemKucunInfo.class);
                    if (info.isSuccess()) {
                        ItemKucunInfo.TargetBean target = info.getTarget();
                        if (target != null) {
                            int positionId = target.getPositionId();

                            mInfo.oldNum = target.getNumberShowTotal() + "";
                            mInfo.safeKucun = target.getGuardBit() + "";
                            mlistener.onPageChange(mInfo);
                            mActivity.mNewCount.requestFocus();

                        } else {
                            ToastUtils.showToast(mActivity.getResources().getString(R.string.no_count_in_the_kuwei));
                        }

                    } else {
                        ToastUtils.showToast("" + info.getMessage());
                    }
                }

                @Override
                public void onFail() {
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean hasPid(int id) {
        if (mTarget != null) {
            int size = mTarget.size();

            for (int i = 0; i < size; i++) {
                PositonBean.TargetBean bean = mTarget.get(i);
                int positionId = bean.getPositionId();
                if (positionId == id) {
                    return true;
                }
            }
        }
        return false;
    }


    //获取库位
    public void bindNewCode(String input) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", input);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    System.out.println("KuweiTiaoZhengActivityModule.onSuccess=====" + json);

                    if (!StringUtils.isEmpty(json)) {
                        Gson gson = new Gson();
                        final PositionBarcodeInfo positionBarcodeInfo = gson.fromJson(json, PositionBarcodeInfo.class);
                        if (positionBarcodeInfo.isSuccess()) {
                            mInfo.pid = positionBarcodeInfo.getTarget().getId() + "";
                            mInfo.barcode = positionBarcodeInfo.getTarget().getBarcode();
                            mInfo.coder = positionBarcodeInfo.getTarget().getCode();

                            getInventoryByPositionAndSku();

                        } else {
                            ToastUtils.showToast("" + positionBarcodeInfo.getMessage());
                        }
                    }
                }

                @Override
                public void onFail() {

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private KuweiTiaoZhengActivity mlistener;

    public void setOnPageListener(KuweiTiaoZhengActivity activity) {
        mlistener = activity;
    }

    //库位调整
    public void update() {

        if (mInfo != null && !StringUtils.isKongStr(mInfo.skuId)) {
//
//            if (StringUtils.isKongStr(mInfo.oldNum)) {
//                ToastUtils.showToast(mActivity.getResources().getString(R.string.input_right_kuwei));
//                return;
//            }

            if (StringUtils.isKongStr(mActivity.mNewCount.getText().toString().trim())) {
                ToastUtils.showToast(mActivity.getResources().getString(R.string.please_input_count));
                return;
            }

            JSONObject object = new JSONObject();
            JSONObject data = new JSONObject();
            JSONArray details = new JSONArray();
            final JSONObject inner = new JSONObject();

            try {
                object.put("token", Login.getInstance().getToken());

                inner.put("skuId", mInfo.skuId);
                inner.put("positionId", mInfo.pid);
                inner.put("number", mActivity.mNewCount.getText().toString().trim());
                inner.put("numberOld", mInfo.oldNum);

                details.put(inner);

                data.put("details", details);
                object.put("data", data);

                GetProtocol protocol = new GetProtocol(RequestUtils.POSITION_UPDATE + "?request=" + object);
                System.out.println("KuweiTiaoZhengActivityModule.update=" + object);
                protocol.get();
                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                    @Override
                    public void onSuccess(String json) {

                        if (StringUtils.isEmpty(json)) {
                            mActivity.getMMDialog().dismiss();
                            ToastUtils.showToast("没有更多数据");
                            return;
                        }
                        try {
                            JSONObject object1 = new JSONObject(json);
                            if (object1.getBoolean("success")) {
                                ToastUtils.showToast("调整成功");

                                mActivity.getMMDialog().dismiss();

                                Intent intent = new Intent(mActivity, KuweiTiaoZhengActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                mActivity.startActivity(intent);

                            } else {
                                mActivity.getMMDialog().dismiss();
                                ToastUtils.showToast("" + object1.getString("message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            ToastUtils.showToast("" + e.getMessage());
                            mActivity.getMMDialog().dismiss();
                        }
                    }

                    @Override
                    public void onFail() {

                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
