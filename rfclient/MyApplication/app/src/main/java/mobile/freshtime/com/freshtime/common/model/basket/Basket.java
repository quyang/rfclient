package mobile.freshtime.com.freshtime.common.model.basket;

/**
 * Created by Administrator on 2016/10/20.
 */

public class Basket {

    public long agencyId;
    public long areaId;
    public String barcode;
    public String code;
    public long dr;
    public long id;
    public String ts;
}
