package mobile.freshtime.com.freshtime.common.viewmodel;

import mobile.freshtime.com.freshtime.activity.BaseActivity;

/**
 * Created by orchid on 16/9/21.
 */

public abstract class BaseViewModel<Page> {

    public abstract void onScanResult(String input);

    public abstract void onInterrupt();

    public abstract boolean isRequesting();

    public abstract Page getCurrentPage();

    public abstract void registerContext(BaseActivity activity);

    public abstract void unregisterContext();
}
