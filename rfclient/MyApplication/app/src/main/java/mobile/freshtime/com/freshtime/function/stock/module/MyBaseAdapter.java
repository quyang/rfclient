package mobile.freshtime.com.freshtime.function.stock.module;

import android.app.Dialog;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.function.stock.PanListActivity;

import static mobile.freshtime.com.freshtime.R.id.delete;

/**
 * Created by Administrator on 2016/11/18.
 */
public class MyBaseAdapter extends BaseAdapter {

    public static final int TOP = 1;
    public static final int NORMAL = 2;

    private ArrayList<InSkuModel> mList;

    private PanListActivity mactivity;


    public MyBaseAdapter(ArrayList<InSkuModel> list, PanListActivity activity) {
        mList = list;
        mactivity = activity;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public InSkuModel getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            convertView = View.inflate(mactivity, R.layout.pandian_lv_head_two, null);

            holder = new ViewHolder();

            holder.item_name = (TextView) convertView.findViewById(R.id.item_name);
            holder.kuwei = (TextView) convertView.findViewById(R.id.kuwei);
            holder.guige_left = (TextView) convertView.findViewById(R.id.guige_left);
            holder.guige_mid = (TextView) convertView.findViewById(R.id.guige_mid);
            holder.guige_right = (TextView) convertView.findViewById(R.id.guige_right);
            holder.choujian = (TextView) convertView.findViewById(R.id.choujian);
            holder.delete = (TextView) convertView.findViewById(delete);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        InSkuModel item = getItem(position);

        holder.item_name.setText(item.getTitle());
        holder.kuwei.setText(item.kuwei);
        holder.guige_left.setText(item.getCount());

        holder.choujian.setVisibility(View.GONE);
        holder.delete.setVisibility(View.VISIBLE);

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog mMDialog = new Dialog(mactivity, R.style.MyDialog);
                View view = mactivity.getLayoutInflater().inflate(R.layout.cancel, null);
                TextView name = (TextView) view.findViewById(R.id.name);
                name.setText("确定要删除吗?");
                Button mNo = (Button) view.findViewById(R.id.no);
                Button mYes = (Button) view.findViewById(R.id.yes);

                mMDialog.setCancelable(false);
                mMDialog.setContentView(view);

                //设置弹窗宽高
                mMDialog.getWindow().setLayout(UiUtils.getScreenHeight(mactivity).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);
                mMDialog.show();
                mYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mList.remove(position);
                        notifyDataSetChanged();
                        mMDialog.dismiss();
                    }
                });

                mNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMDialog.dismiss();
                    }
                });



            }
        });


        return convertView;
    }

    static class ViewHolder {


        public TextView item_name;
        public TextView kuwei;
        public TextView guige_left;
        public TextView guige_mid;
        public TextView guige_right;
        public TextView choujian;
        public TextView delete;


    }
}
