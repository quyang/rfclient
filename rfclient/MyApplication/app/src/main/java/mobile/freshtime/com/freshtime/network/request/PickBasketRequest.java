package mobile.freshtime.com.freshtime.network.request;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;

import mobile.freshtime.com.freshtime.function.pickwall.model.ResponsePickWall;
import mobile.freshtime.com.freshtime.network.RequestUtils;

/**
 * Created by orchid on 16/10/14.
 */

public class PickBasketRequest extends Request<ResponsePickWall> {

    private Gson mGson = new Gson();
    private String mParam;

    private WeakReference<Response.Listener<ResponsePickWall>> mListener;

    public PickBasketRequest(JSONObject param,
                             Response.Listener<ResponsePickWall> listener,
                             Response.ErrorListener errorListener) {
        super(Method.POST, RequestUtils.GET_WAVE, errorListener);
        this.mParam = (param == null) ? null : param.toString();
        this.mParam = "request=" + this.mParam;
        this.mListener = new WeakReference<>(listener);
    }

    //获取拣货墙回调到这
    @Override
    protected Response<ResponsePickWall> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =
                    new String(response.data, "UTF-8");
            Log.e("freshtime", "volley response : " + jsonString);
            JSONObject obj = new JSONObject(jsonString);
//            JSONObject target = obj.getJSONObject("target");
            ResponsePickWall result = mGson.fromJson(obj.toString(), ResponsePickWall.class);
            return Response.success(
                    result,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(ResponsePickWall response) {
        if (mListener != null && mListener.get() != null)
            mListener.get().onResponse(response);
        else
            Log.e("orkid", "request released!");
    }

    @Override
    public byte[] getBody() {
        try {
            return mParam == null ? null : mParam.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mParam, "utf-8");
            return null;
        }
    }
}
