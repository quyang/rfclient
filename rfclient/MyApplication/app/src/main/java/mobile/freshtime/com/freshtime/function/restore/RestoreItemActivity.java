package mobile.freshtime.com.freshtime.function.restore;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverDetail;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.function.restore.adapter.IQuantityWatcher;
import mobile.freshtime.com.freshtime.function.restore.adapter.RestoreItemAdapter;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuRequest;

/**
 * 归还商品页
 * Created by orchid on 16/10/16.
 */

public class RestoreItemActivity extends BaseActivity implements IQuantityWatcher, Response.ErrorListener, Response.Listener<ResponseSku>, View.OnClickListener {

    private ListView mListView;
    private EditText mItemInput;
    private ArrayList<SkuModel> mSkuModels = new ArrayList<>();
    ;
    private RestoreItemAdapter mAdapter;
    private ArrayList<DeliverDetail> mTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore_item);

        TextView tag = (TextView) findViewById(R.id.item_tag);

        tag.setText("商品条码:");
        mItemInput = (EditText) findViewById(R.id.item_input);

        findViewById(R.id.back).setOnClickListener(this);

        mTask = getIntent().getParcelableArrayListExtra("task");

//        Toast.makeText(RestoreItemActivity.this, "task =" + mTask.size(), Toast.LENGTH_LONG).show();

        ArrayList<SkuModel> param = getIntent().getParcelableArrayListExtra("sku");
        if (param != null)
            mSkuModels.addAll(param);

        mListView = (ListView) findViewById(R.id.item_list);
        mAdapter = new RestoreItemAdapter(this, mSkuModels, this);
        mListView.setAdapter(mAdapter);

        findViewById(R.id.done).setOnClickListener(this);
    }

    public boolean isHas(String input) {
        boolean is = false;

        for (int i = 0; i < mTask.size(); i++) {
            long skuId = mTask.get(i).skuId;
            is = input.contains(skuId + "");
        }

        return is;
    }

    @Override
    public void onScanResult(String result) {
        result = result.trim();
        mItemInput.setText(result);

//        boolean has = isHas(result);
//        if (!has) {
//            Toast.makeText(RestoreItemActivity.this, "不含该商品", Toast.LENGTH_LONG).show();
//            return;
//        }

        //请求网络 获取sku信息
        SkuRequest request = new SkuRequest(Utils.filterInput(result, "10002"), this, this);
        FreshRequest.getInstance().addToRequestQueue(request, this.toString());
    }

    @Override
    public void onItemAdd(int position) {
        int count = mSkuModels.get(position).getCount();
        mSkuModels.get(position).setCount(count + 1);
        mAdapter.updateData(mSkuModels);
    }

    @Override
    public void onItemMinus(int position) {
        int count = mSkuModels.get(position).getCount();
        if (count == 1)
            mSkuModels.remove(position);
        else
            mSkuModels.get(position).setCount(count - 1);
        mAdapter.updateData(mSkuModels);
    }

    //获取sku信息失败
    @Override
    public void onErrorResponse(VolleyError error) {

    }

    //获取sku信息成功
    @Override
    public void onResponse(ResponseSku response) {
        if (response != null && response.isSuccess()) {
            SkuModel model = response.getModule();
            int index = Utils.hasSku(model, mSkuModels);
            if (index >= 0) {
                int count = mSkuModels.get(index).getCount();
                mSkuModels.get(index).setCount(count + 1);
                mAdapter.updateData(mSkuModels);
            } else {
                model.setCount(1);
                mSkuModels.add(model);
                mAdapter.updateData(mSkuModels);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.done) {//提交商品
            Intent intent = new Intent();
            intent.putParcelableArrayListExtra("sku", mSkuModels);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }

        if (id == R.id.back) {
            finish();
        }
    }
}
