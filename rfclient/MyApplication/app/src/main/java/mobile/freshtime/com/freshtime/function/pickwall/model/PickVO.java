package mobile.freshtime.com.freshtime.function.pickwall.model;

/**
 * Created by orchid on 16/10/14.
 */

public class PickVO {

    public long agencyId;
    public long areaId;
    public String createTime;
    public String details;
    public long dr;
    public String finishTime;
    public long id;
    public String operator;
    public int statu;
    public String ts;
    public long waveId;

}
