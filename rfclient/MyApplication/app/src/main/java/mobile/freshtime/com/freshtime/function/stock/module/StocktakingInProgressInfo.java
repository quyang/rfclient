package mobile.freshtime.com.freshtime.function.stock.module;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Administrator on 2016/11/24.
 */

public class StocktakingInProgressInfo implements Serializable {

    /**
     * message : null
     * return_code : 0
     * success : true
     * target : {"firstPage":true,"items":[{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":6,"opTime":"2016-11-24T11:43:43","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:43:43","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":7,"opTime":"2016-11-24T11:43:48","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:43:48","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":8,"opTime":"2016-11-24T11:46:01","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:46:01","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":9,"opTime":"2016-11-24T11:50:52","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:50:52","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":10,"opTime":"2016-11-24T11:51:21","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:51:21","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":11,"opTime":"2016-11-24T11:52:57","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:52:57","type":null}],"lastPage":false,"nextPage":2,"page":1,"pageCount":0,"previousPage":1,"size":10,"totalCount":6}
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * firstPage : true
     * items : [{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":6,"opTime":"2016-11-24T11:43:43","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:43:43","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":7,"opTime":"2016-11-24T11:43:48","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:43:48","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":8,"opTime":"2016-11-24T11:46:01","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:46:01","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":9,"opTime":"2016-11-24T11:50:52","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:50:52","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":10,"opTime":"2016-11-24T11:51:21","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:51:21","type":null},{"agencyId":1,"auditTime":null,"auditor":null,"details":null,"dr":0,"id":11,"opTime":"2016-11-24T11:52:57","operate":55,"statu":0,"statuOld":null,"ts":"2016-11-24T11:52:57","type":null}]
     * lastPage : false
     * nextPage : 2
     * page : 1
     * pageCount : 0
     * previousPage : 1
     * size : 10
     * totalCount : 6
     */

    private TargetBean target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public TargetBean getTarget() {
        return target;
    }

    public void setTarget(TargetBean target) {
        this.target = target;
    }

    public static class TargetBean implements Serializable {
        private boolean firstPage;
        private boolean lastPage;
        private int nextPage;
        private int page;
        private int pageCount;
        private int previousPage;
        private int size;
        private int totalCount;
        /**
         * agencyId : 1
         * auditTime : null
         * auditor : null
         * details : null
         * dr : 0
         * id : 6  任务id
         * opTime : 2016-11-24T11:43:43
         * operate : 55
         * statu : 0
         * statuOld : null
         * ts : 2016-11-24T11:43:43
         * type : null
         */

        private List<ItemsBean> items;

        public boolean isFirstPage() {
            return firstPage;
        }

        public void setFirstPage(boolean firstPage) {
            this.firstPage = firstPage;
        }

        public boolean isLastPage() {
            return lastPage;
        }

        public void setLastPage(boolean lastPage) {
            this.lastPage = lastPage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getPage() {
            return page;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public int getPageCount() {
            return pageCount;
        }

        public void setPageCount(int pageCount) {
            this.pageCount = pageCount;
        }

        public int getPreviousPage() {
            return previousPage;
        }

        public void setPreviousPage(int previousPage) {
            this.previousPage = previousPage;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean implements Serializable {
            private int agencyId;
            private Object auditTime;
            private Object auditor;
            private Object details;
            private int dr;
            private int id;//任务id
            private String opTime;
            private int operate;
            private int statu;
            private Object statuOld;
            private String ts;
            private Object type;
            private boolean isChanged;

            public boolean isChanged() {
                return isChanged;
            }

            public void setChanged(boolean changed) {
                isChanged = changed;
            }

            public int getAgencyId() {
                return agencyId;
            }

            public void setAgencyId(int agencyId) {
                this.agencyId = agencyId;
            }

            public Object getAuditTime() {
                return auditTime;
            }

            public void setAuditTime(Object auditTime) {
                this.auditTime = auditTime;
            }

            public Object getAuditor() {
                return auditor;
            }

            public void setAuditor(Object auditor) {
                this.auditor = auditor;
            }

            public Object getDetails() {
                return details;
            }

            public void setDetails(Object details) {
                this.details = details;
            }

            public int getDr() {
                return dr;
            }

            public void setDr(int dr) {
                this.dr = dr;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getOpTime() {
                return opTime;
            }

            public void setOpTime(String opTime) {
                this.opTime = opTime;
            }

            public int getOperate() {
                return operate;
            }

            public void setOperate(int operate) {
                this.operate = operate;
            }

            public int getStatu() {
                return statu;
            }

            public void setStatu(int statu) {
                this.statu = statu;
            }

            public Object getStatuOld() {
                return statuOld;
            }

            public void setStatuOld(Object statuOld) {
                this.statuOld = statuOld;
            }

            public String getTs() {
                return ts;
            }

            public void setTs(String ts) {
                this.ts = ts;
            }

            public Object getType() {
                return type;
            }

            public void setType(Object type) {
                this.type = type;
            }
        }
    }
}
