package mobile.freshtime.com.freshtime.function.jiaohuan;

import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.PostProtocol;
import okhttp3.FormBody;

/**
 * Created by Administrator on 2016/11/5.
 */
public class NewActivityPresenter {


    private ViewInterface mViewInterface;

    public NewActivityPresenter(ViewInterface viewInterface) {
        mViewInterface = viewInterface;
    }

    public void load(String result) {

//        String s = RequestUtils.INTO_STORAGE + "request=" +
//                "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + result + "\"}";
//
//        GetProtocol protocol = new GetProtocol(s);
//        protocol.get();

        FormBody body = new FormBody.Builder()
                .add("skuCode", result)
                .build();
        PostProtocol protocol = new PostProtocol(body, RequestUtils.SKU);
        protocol.post();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                mViewInterface.success(json);
            }

            @Override
            public void onFail() {
                mViewInterface.fail();
            }
        });

    }


}
