
package mobile.freshtime.com.freshtime.function.stock;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.function.search.PositionBarcodeInfo;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;

/**
 * 补录界面
 */
public class BuLuActivity extends BaseActivity {


    private NoInputEditText mInput;
    private int mId;
    private String mPosition;
    private String mCode;
    private TextView mCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bu_lu);

        mPosition = getIntent().getStringExtra("index");

        mCount = (TextView) findViewById(R.id.new_count);

        TextView back = (TextView) findViewById(R.id.back);
        back.setClickable(true);
        back.setOnClickListener(new MyOnClickListener());

        mInput = (NoInputEditText) findViewById(R.id.et_new);
        mInput.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

    }


    @Override
    public void onScanResult(String result) {
        if (!StringUtils.isEmpty(result.trim())) {
            if (mInput.hasFocus()) {
                bindNewCode(result.trim());
            } else {
                ToastUtils.showToast("请先获取焦点");
            }
        }
    }


    //绑定新的库位
    public void bindNewCode(String input) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", input);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    try {
                        if (!StringUtils.isEmpty(json)) {

                            Gson gson = new Gson();
                            final PositionBarcodeInfo positionBarcodeInfo = gson.fromJson(json, PositionBarcodeInfo.class);
                            if (positionBarcodeInfo.isSuccess()) {
                                PositionBarcodeInfo.TargetBean target = positionBarcodeInfo.getTarget();
                                mInput.setText(target.getBarcode() + "");
                                mId = target.getId();
                                mCode = target.getCode();

                            } else {
                                ToastUtils.showToast("" + positionBarcodeInfo.getMessage());
                            }
                        }
                    } catch (Exception e) {
                        ToastUtils.showToast(getResources().getString(R.string.json_exception));
                    }

                }

                @Override
                public void onFail() {

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            Intent intent = new Intent();

            if (mId != 0 && mId > 0) {
                intent.putExtra("pid", mId + "");//pid
                intent.putExtra("code", mCode + "");//coder
            }

            intent.putExtra("index", mPosition + "");
            intent.putExtra("newCount", mCount.getText().toString().trim() + "");//新的数量

            setResult(RESULT_OK, intent);
            finish();
        }
    }
}
