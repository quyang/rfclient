package mobile.freshtime.com.freshtime.function.stock;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;

public class FirstStockActivityTwo extends AppCompatActivity {


    private ArrayList<String> mGroupArray = new ArrayList();
    private ArrayList<ArrayList<String>> mChildArray = new ArrayList();
    private ArrayList<String> items = new ArrayList();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        mGroupArray.add("如何成为鲜在时会员");
        mGroupArray.add("如何成为鲜在时会员");
        mGroupArray.add("如何成为鲜在时会员");
        mGroupArray.add("如何成为鲜在时会员");
        mGroupArray.add("如何成为鲜在时会员");

        items.add("hahahaha3");
        items.add("hahahaha3");
        items.add("hahahaha3");
        items.add("hahahaha3");
        items.add("hahahaha3");
        mChildArray.add(items);

        ExpandableListView mList = (ExpandableListView) findViewById(R.id.expand_list);
        mList.setAdapter(new MyBaseExpandableListAdapter());

    }


    private class MyBaseExpandableListAdapter extends BaseExpandableListAdapter {

        @Override
        public int getGroupCount() {
            return mGroupArray.size();
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return mChildArray.size();
        }

        @Override
        public String getGroup(int groupPosition) {
            return mGroupArray.get(groupPosition);
        }

        @Override
        public String getChild(int groupPosition, int childPosition) {
            return mChildArray.get(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }


        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            View view = View.inflate(getApplicationContext(), R.layout.widget_pandian_header, null);
            TextView task = (TextView) view.findViewById(R.id.sku_info);
            task.setText(mGroupArray.get(groupPosition));
            return view;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            View inflate = View.inflate(getApplicationContext(), R.layout.widget_pandian_body, null);
            TextView name = (TextView) inflate.findViewById(R.id.item_name);
//            ArrayList<String> list = mChildArray.get(groupPosition);
//            int size = list.size();
////            String s = list.get(childPosition);
////            name.setText(s);
            return inflate;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }
    }
}
