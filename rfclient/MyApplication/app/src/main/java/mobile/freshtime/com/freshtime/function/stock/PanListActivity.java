package mobile.freshtime.com.freshtime.function.stock;

import android.animation.ValueAnimator;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ProgressDialogUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.function.stock.module.InSkuModel;
import mobile.freshtime.com.freshtime.function.stock.module.MyBaseAdapter;
import mobile.freshtime.com.freshtime.function.stock.module.PanListActivityPresenter;

/**
 * 盘点任务列表 s商品数(一盘)
 */
public class PanListActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {


    private boolean isOpen = false;

    @Bind(R.id.last)
    TextView mLast;
    @Bind(R.id.queren)
    TextView mQueren;
    @Bind(R.id.activity_pan_list)
    LinearLayout mActivityPanList;
    @Bind(R.id.expand_list)
    ListView mExpandList;


    private ArrayList<String> groupList = new ArrayList<>();

    private ViewGroup mBody;

    private ArrayList<InSkuModel> mSkuModels;

    private int Id;

    private String mTime;

    private PanListActivityPresenter mPresenter;

    private int mTaskId;

    private Dialog mMMDialog;

    private MyBaseAdapter mBaseAdapter;
    private Dialog mProgressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan_list);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        Id = bundle.getInt("id");//索引
        mTaskId = bundle.getInt("taskId");
        mTime = bundle.getString("time");
        int faqiren1 = bundle.getInt("faqiren");
        int quyu1 = bundle.getInt("quyu");
        mSkuModels = bundle.getParcelableArrayList("units");

        mLast.setClickable(true);
        mQueren.setClickable(true);
        mLast.setOnClickListener(this);
        mQueren.setOnClickListener(this);

        ViewGroup header = (ViewGroup) findViewById(R.id.header);
        TextView left = (TextView) header.findViewById(R.id.sku_info);
        TextView mid = (TextView) header.findViewById(R.id.sku_code);
        TextView right = (TextView) header.findViewById(R.id.item_detail);

        left.setText("任务id: " + mTaskId);
        mid.setText("区域盘点");
        right.setText("");

        header.setOnClickListener(this);

        mBody = (ViewGroup) findViewById(R.id.item_info_root);
        ViewGroup.LayoutParams params = mBody.getLayoutParams();
        params.height = 0;
        mBody.setLayoutParams(params);

        TextView fa = (TextView) mBody.findViewById(R.id.item_name);
        TextView time = (TextView) mBody.findViewById(R.id.item_count);
        TextView quyu = (TextView) mBody.findViewById(R.id.item_code);

        fa.setText("发起人: " + faqiren1);
        time.setText("操作时间: " + mTime);
        quyu.setText("区域: " + quyu1);

        mBaseAdapter = new MyBaseAdapter(mSkuModels, this);
        mExpandList.setAdapter(mBaseAdapter);
        mExpandList.setOnItemClickListener(this);

        mPresenter = new PanListActivityPresenter(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.header:
                changeBody();
                break;
            case R.id.last:
                finish();
                break;
            case R.id.queren://提交一盘明细

                mMMDialog = new Dialog(PanListActivity.this, R.style.MyDialog);
                View view = getLayoutInflater().inflate(R.layout.cancel, null);
                TextView name = (TextView) view.findViewById(R.id.name);
                name.setText(getResources().getString(R.string.submit_one_pan_details));
                Button mNo = (Button) view.findViewById(R.id.no);
                Button mYes = (Button) view.findViewById(R.id.yes);

                mMMDialog.setCancelable(false);
                mMMDialog.setContentView(view);

                //设置弹窗宽高
                mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(PanListActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

                mMMDialog.show();

                mYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMMDialog.dismiss();
                        mProgressDialog = ProgressDialogUtils.getProgressDialog(PanListActivity.this);
                        mProgressDialog.show();
                        mPresenter.submitInfo(mSkuModels, mTaskId, Id);
                    }
                });

                mNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mMMDialog.dismiss();
                    }
                });
                break;
        }
    }


    private void changeBody() {
        ValueAnimator animator = null;

        if (!isOpen) {
            //创建出ValueAnimator的对象
            animator = ValueAnimator.ofInt(0, getMaxHeight());
            isOpen = true;

        } else {
            animator = ValueAnimator.ofInt(getMaxHeight(), 0);
            isOpen = false;
        }

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int tempHeight = (Integer) animation.getAnimatedValue();//得到中间值
                System.out.println("PanListActivity.onAnimationUpdate=" + tempHeight);
                ViewGroup.LayoutParams layoutParams2 = mBody
                        .getLayoutParams();
                layoutParams2.height = tempHeight;
                mBody.setLayoutParams(layoutParams2);
            }
        });
        animator.setDuration(200);
        animator.start();
    }

    private int getMaxHeight() {

        List<Integer> screenHeight = UiUtils.getScreenHeight(PanListActivity.this);
        Integer screenWidth = screenHeight.get(0);
        int childWidthMeasureSpec = View.MeasureSpec.makeMeasureSpec(screenWidth, View.MeasureSpec.AT_MOST);
        mBody.measure(childWidthMeasureSpec, 0);

        return mBody.getMeasuredHeight();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Intent intent = new Intent(PanListActivity.this, BuLuActivity.class);
        intent.putExtra("index", position + "");
        startActivityForResult(intent, 1);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            String pid = data.getStringExtra("pid");
            String index = data.getStringExtra("index");
            String code = data.getStringExtra("code");
            String newCount = data.getStringExtra("newCount");

            InSkuModel model = mSkuModels.get(Integer.parseInt(index));

            if (model != null && pid != null) {
                model.setPositionId(Integer.parseInt(pid));
            }

            if (code != null) {
                model.setCuwei(code);
            }

            if (newCount != null && !"".equals(newCount)) {
                model.setCount(newCount);
            }

            mBaseAdapter.notifyDataSetChanged();
        }
    }

    public Dialog getMMDialog() {
        return mProgressDialog;
    }
}

