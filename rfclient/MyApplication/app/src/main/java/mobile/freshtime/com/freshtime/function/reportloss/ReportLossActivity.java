package mobile.freshtime.com.freshtime.function.reportloss;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.reportloss.model.LossPage;
import mobile.freshtime.com.freshtime.function.reportloss.viewmodel.ReportLossViewModel;

/**
 * 退换货上架
 * Created by orkid on 2016/8/14.
 */
public class ReportLossActivity extends BaseActivity implements View.OnClickListener,
        PageListener<LossPage>, ViewModelWatcher<LossPage> {

    private ViewGroup mInfoRoot;
    private ViewGroup mItemRoot;
    private ViewGroup mSkuRoot;
    private ViewGroup mSourceRoot;

    private TextView mBefore;
    private TextView mNext;
    private TextView mDone;

    private TextView mItemName;
    private TextView mItemCode;
    private TextView mSkuName;
    private TextView mSkuCode;
    private TextView mPostionName;
    private TextView mPostionCount;
    private EditText mPositionInput;

    private EditText mScanContent;

    private View mProgressView;

    private ReportLossViewModel mViewModel;
    private TextView mCode69;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reportloss);

        findViewById(R.id.back).setOnClickListener(this);

        mScanContent = (EditText) findViewById(R.id.scanContent);

        mBefore = (TextView) findViewById(R.id.last);
        mBefore.setOnClickListener(this);
        mNext = (TextView) findViewById(R.id.next);
        mNext.setOnClickListener(this);
        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);

        mInfoRoot = (ViewGroup) findViewById(R.id.info_root);
        mInfoRoot.findViewById(R.id.new_code_ll).setVisibility(View.GONE);

        mItemRoot = (ViewGroup) findViewById(R.id.item_info_root);
        mSkuRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        mSourceRoot = (ViewGroup) findViewById(R.id.source_info_root);

        mCode69 = (TextView) mItemRoot.findViewById(R.id.item_69code);
        TextView coder = (TextView) mItemRoot.findViewById(R.id.item_coder);
        coder.setVisibility(View.GONE);

        mItemName = (TextView) mItemRoot.findViewById(R.id.item_name);
        mItemCode = (TextView) mItemRoot.findViewById(R.id.item_code);

        mSkuName = (TextView) mSkuRoot.findViewById(R.id.item_name);
        mSkuCode = (TextView) mSkuRoot.findViewById(R.id.item_code);

        mPostionName = (TextView) mSourceRoot.findViewById(R.id.item_name);
        mPostionCount = (TextView) mSourceRoot.findViewById(R.id.item_count);
        mPositionInput = (EditText) mSourceRoot.findViewById(R.id.position_input);

        mProgressView = findViewById(R.id.progressbar);
        mProgressView.setVisibility(View.INVISIBLE);
//        displayPage(mLossList.getCurrentPage());

        mViewModel = new ReportLossViewModel(this);
        mViewModel.addPageListener(this);
        mViewModel.registerContext(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mViewModel.unregisterContext();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {

            if (mInfoRoot.getVisibility() == View.VISIBLE) {
                if ("".equals(mPostionCount.getText().toString().trim()) || "0".equals(
                        mPostionCount.getText().toString().trim())) {
                    Toast.makeText(ReportLossActivity.this, "请先输入数量", Toast.LENGTH_LONG).show();
                } else {
                    mViewModel.setCount(mPostionCount.getText().toString());
                    mViewModel.navNext();
                    requestDone();
                }
            }

        } else if (id == R.id.last) {
            mViewModel.navBefore();
        } else if (id == R.id.next) {
            if ("".equals(mPostionCount.getText().toString().trim()) || "0".equals(mPostionCount.getText().toString().trim())) {
                Toast.makeText(ReportLossActivity.this, "请先输入商品数量", Toast.LENGTH_LONG).show();
            } else {
                mViewModel.setCount(mPostionCount.getText().toString());
                mViewModel.navNext();
            }
//            mViewModel.navNext();
        }
    }

    @Override
    public void onScanResult(String result) {
        result = result.trim();
        mViewModel.onScanResult(result);
    }

    @Override
    public void onPageChanged(LossPage page) {
        displayPage(page);
    }

    private void displayPage(LossPage page) {
        if (mViewModel.hasBefore()) {
            mBefore.setEnabled(true);
        } else {
            mBefore.setEnabled(false);
        }

        if (!mViewModel.hasNext() && page.getPhase() != LossPage.Phase.SKU) {
            mNext.setEnabled(false);
        } else {
            mNext.setEnabled(true);
        }

        LossPage.Phase phase = page.getPhase();
        if (phase == LossPage.Phase.START) {
            displayStart(page);
        } else if (phase == LossPage.Phase.SKU) {
            displaySku(page);
        }
//        else if (phase == LossPage.Phase.POSITION) {
//            displayPosition(page);
//        }
        else {
            displayCount(page);
        }
    }

    /**
     * 初始化的UI
     *
     * @param page
     */
    private void displayStart(LossPage page) {
        mInfoRoot.setVisibility(View.GONE);
        mScanContent.requestFocus();

        mItemName.setText("");
        mItemCode.setText("");
        mSkuName.setText("");
        mSkuCode.setText("");

        mScanContent.setText("");
        mPositionInput.setText("");
        mPostionCount.setText("");
    }

    /**
     * 输入sku信息后展示的信息
     *
     * @param page
     */
    private void displaySku(LossPage page) {
        mInfoRoot.setVisibility(View.VISIBLE);

        SkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText("");
        mPostionCount.setText("");

        mPositionInput.requestFocus();
    }

    /**
     * 输入库位信息后展示的信息
     *
     * @param page
     */
    private void displayPosition(LossPage page) {
        SkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText(unit.getPosition());
        mPostionCount.setText("");

        mPostionCount.requestFocus();
    }

    /**
     * 输入数量信息后展示的信息
     *
     * @param page
     */
    private void displayCount(LossPage page) {
        SkuModel unit = page.getSkuModel();
        displayUnit(unit);

        mScanContent.setText(unit.getBarcode());
        mPositionInput.setText(unit.getPosition());
        mPostionCount.setText(unit.getCount() + "");

        mPostionCount.clearFocus();
    }

    private void displayUnit(SkuModel unit) {

        mItemName.setText("品名：" + unit.getTitle());
        mItemCode.setText("编码：" + unit.getBarcode());
        mSkuName.setText("规格：" + unit.getUnStandardSpecification());
        mSkuCode.setText("" + unit.getKuwei());


        if ("0".equals(unit.getCount() + "")) {
            mPostionCount.setText(unit.getCount() + "");
            mPostionCount.requestFocus();

        } else {
            mPostionCount.requestFocus();
            mPostionCount.length();
            mPostionCount.setText(unit.getCount() + "");

        }
        mCode69.setText("商品69码:" + unit.getSku69Id());

    }

    private void requestDone() {
        Intent intent = new Intent(this, LossConfirmActivity.class);
        List<LossPage> pages = mViewModel.getLossPages();
        ArrayList<SkuModel> modules = new ArrayList<>();
        for (int i = 0; i < pages.size(); i++) {
            LossPage page = pages.get(i);
            SkuModel model = page.getSkuModel();
            if (model != null && (page.getPhase() == LossPage.Phase.SKU
//                    (page.getPhase() == StoragePage.Phase.DONE
//                            ||
//                            page.getPhase() == StoragePage.Phase.POSITION
            )) {
//                pages.get(i).getSkuModel().setCount(Integer.parseInt(mPostionCount.getText().toString() + ""));
//                modules.add(pages.get(i).getSkuModel());

                modules.add(pages.get(i).getSkuModel());
            }
        }
        intent.putParcelableArrayListExtra("units", modules);
        startActivity(intent);
    }

    @Override
    public void onRequestStart() {
        mProgressView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onRequestStop() {
        mProgressView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onPageUpdate(LossPage page) {
        displayPage(page);
    }

    @Override
    public void onError(String errorCode) {
        mProgressView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onFinish() {

    }
}
