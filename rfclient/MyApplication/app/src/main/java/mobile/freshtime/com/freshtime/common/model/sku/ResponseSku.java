package mobile.freshtime.com.freshtime.common.model.sku;

import org.json.JSONObject;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orchid on 16/9/28.
 */

public class ResponseSku extends BaseResponse {

    private SkuModel module;

    public ResponseSku(JSONObject jsonObject) {
        return_code = jsonObject.optInt("resultCode", 0);
        success = jsonObject.optBoolean("success", false);
        message = jsonObject.optString("errorMsg", "");
        module = new SkuModel(jsonObject.optJSONObject("module"));
    }

    public SkuModel getModule() {
        return module;
    }

    public void setModule(SkuModel module) {
        this.module = module;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
