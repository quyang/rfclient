package mobile.freshtime.com.freshtime.network.request;

import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonSyntaxException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;

import mobile.freshtime.com.freshtime.common.model.sku.ResponseSkuList;
import mobile.freshtime.com.freshtime.network.RequestUtils;

/**
 * Created by orchid on 16/10/7.
 */

public class SkuListRequest extends Request<ResponseSkuList> {

    private String mParam;
    private WeakReference<Response.Listener<ResponseSkuList>> mListener;

    public SkuListRequest(String barcode,
                          Response.Listener<ResponseSkuList> listener,
                          Response.ErrorListener errorListener) {
        super(Method.POST, RequestUtils.SKU_LIST, errorListener);
        this.mParam = "idlist=" + barcode;
        this.mListener = new WeakReference<>(listener);
    }

    @Override
    protected Response<ResponseSkuList> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =
                    new String(response.data, "UTF-8");
            Log.e("freshtime", "volley response : " + jsonString);
            ResponseSkuList result = new ResponseSkuList(new JSONObject(jsonString));
//            result.getModule().setBarcode(this.mBarcode);
            return Response.success(
                    result,
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (JsonSyntaxException je) {
            return Response.error(new ParseError(je));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(ResponseSkuList response) {
        if (mListener != null && mListener.get() != null)
            mListener.get().onResponse(response);
        else
            Log.e("orkid", "request released!");
    }

    @Override
    public byte[] getBody() {
        try {
            return mParam == null ? null : mParam.getBytes("utf-8");
        } catch (UnsupportedEncodingException uee) {
            VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s",
                    mParam, "utf-8");
            return null;
        }
    }
}