package mobile.freshtime.com.freshtime.common.model;

/**
 * Created by orchid on 16/9/29.
 */

public class BaseResponse {

    protected int return_code;
    protected boolean success;
    protected String message;

    public String getErrorMsg() {
        return message;
    }

    public void setErrorMsg(String errorMsg) {
        this.message = errorMsg;
    }

    public int getResultCode() {
        return return_code;
    }

    public void setResultCode(int resultCode) {
        this.return_code = resultCode;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
