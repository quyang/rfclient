package mobile.freshtime.com.freshtime.function.transfer.model;

import org.json.JSONObject;

import mobile.freshtime.com.freshtime.common.model.BaseResponse;

/**
 * Created by orchid on 16/9/28.
 */

public class ResponseSkuTransfer extends BaseResponse {

    private TransferSkuModel module;

    public ResponseSkuTransfer(JSONObject jsonObject) {
        return_code = jsonObject.optInt("resultCode", 0);
        success = jsonObject.optBoolean("success", false);
        message = jsonObject.optString("errorMsg", "");
        module = new TransferSkuModel(jsonObject.optJSONObject("module"));
    }

    public TransferSkuModel getModule() {
        return module;
    }

    public void setModule(TransferSkuModel module) {
        this.module = module;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
