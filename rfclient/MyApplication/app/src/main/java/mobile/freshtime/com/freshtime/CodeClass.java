package mobile.freshtime.com.freshtime;

/**
 * Created by Administrator on 2016/11/1.
 */
public class CodeClass {

    public long barcode;
    public String code;

    public long getBarcode()     {
        return barcode;
    }

    public void setBarcode(long barcode) {
        this.barcode = barcode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
