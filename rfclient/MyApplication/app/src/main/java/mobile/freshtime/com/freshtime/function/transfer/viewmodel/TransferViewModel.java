package mobile.freshtime.com.freshtime.function.transfer.viewmodel;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.position.ResponsePosition;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.viewmodel.ModelList;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.BaseViewModel;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.guanli.module.KuCunInfo;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.function.transfer.TransferActivity;
import mobile.freshtime.com.freshtime.function.transfer.model.ResponseSkuTransfer;
import mobile.freshtime.com.freshtime.function.transfer.model.TransferPage;
import mobile.freshtime.com.freshtime.function.transfer.model.TransferSkuRequest;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuRequest;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by orchid on 16/10/11.
 */
public class TransferViewModel extends BaseViewModel<TransferPage> implements ModelList<TransferPage>,
        Response.ErrorListener {

    private List<TransferPage> mTransferPages;

    private int mCurrentPageIndex;

    private PageListener mPageListener;

    private ViewModelWatcher mModelWatcher;

    private boolean mIsRequesting;

    private Request<ResponseSkuTransfer> mSkuRequest;

    private String mBarcode;

    private final TransferActivity mActivity;


    /**
     * sku请求回调
     */
    private Response.Listener<ResponseSkuTransfer> mSkuModelListener = new Response.Listener<ResponseSkuTransfer>() {
        @Override
        public void onResponse(ResponseSkuTransfer response) {
            TransferViewModel.this.onResponse(response);
        }
    };

    /**
     * sku请求回调2，负责添加数量
     */
    private Response.Listener<ResponseSku> mSkuCountListener = new Response.Listener<ResponseSku>() {
        @Override
        public void onResponse(ResponseSku response) {
            TransferViewModel.this.onCountResponse(response);
        }
    };

    private Response.Listener<ResponsePosition> mPositionListener =
            new Response.Listener<ResponsePosition>() {
                @Override
                public void onResponse(ResponsePosition response) {
                    if (response != null && response.isSuccess()) {

                        TransferPage page = getCurrentPage();
                        if (page.getPhase() == TransferPage.Phase.OUT) {

                            page.getSkuModel().setPositionId(response.target.id);
                            page.getSkuModel().setOutKuwei(response.target.code);

                        } else {
                            page.getSkuModel().setPosition2Id(response.target.id);
                            page.getSkuModel().setInKuwei(response.target.code);

                        }

                        page.advance();
                        mModelWatcher.onPageUpdate(page);

                    } else {
                        String msg = response == null ? "未获取到仓位信息" : response.getErrorMsg();
                        mModelWatcher.onError(msg);
                    }
                }
            };


    public TransferViewModel(ViewModelWatcher watcher) {
        this.mTransferPages = new ArrayList<>();
        this.mCurrentPageIndex = 0;
        this.mModelWatcher = watcher;
        mActivity = (TransferActivity) watcher;

        TransferPage page = new TransferPage(null);
        mTransferPages.add(mCurrentPageIndex, page);

    }

    public void addPageListener(PageListener pageListener) {
        this.mPageListener = pageListener;
        TransferPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    /**
     * --------------------- BaseViewModel start ---------------------
     **/
    @Override
    public void onScanResult(String input) {
        TransferPage page = getCurrentPage();
        TransferPage.Phase phase = page.getPhase();
        if (phase == TransferPage.Phase.START) {
            requestSku(input);//开始扫条码
        } else if (phase == TransferPage.Phase.OUT) {
            fillOut(input);
        } else if (phase == TransferPage.Phase.IN) {
            fillIn(input);
        } else if (phase == TransferPage.Phase.COUNT) {
            fillCount(input);
        }
    }

    @Override
    public void onInterrupt() {
        mIsRequesting = false;
        if (mSkuRequest != null)
            FreshRequest.getInstance().cancel(mSkuRequest.toString());
    }

    @Override
    public boolean isRequesting() {
        return mIsRequesting;
    }

    @Override
    public void registerContext(BaseActivity activity) {

    }

    @Override
    public void unregisterContext() {
        this.mPageListener = null;
        this.mModelWatcher = null;
        this.mSkuRequest = null;
    }
    /**
     * --------------------- BaseViewModel end ---------------------
     **/

    /**
     * --------------------- ModelList start ---------------------
     **/

    @Override
    public boolean hasBefore() {
        return mCurrentPageIndex != 0;
    }

    @Override
    public boolean hasNext() {
        return mCurrentPageIndex != mTransferPages.size() - 1;
    }

    @Override
    public boolean isReady() {
        for (int i = 0; i < mTransferPages.size(); i++) {
            TransferPage page = mTransferPages.get(i);
            if (page.getPhase() != TransferPage.Phase.DONE ||
                    page.getPhase() != TransferPage.Phase.COUNT)
                return false;
        }
        return true;
    }

    @Override
    public void navNext() {

        int len = mTransferPages.size();

        TransferPage currentPage = getCurrentPage();
        currentPage.getSkuModel().setCount(count1);

        if (mCurrentPageIndex++ == len - 1) {
            // 新增一页
            TransferPage page = new TransferPage(null);
            mTransferPages.add(mCurrentPageIndex, page);
        }


        TransferPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }


    private void getAllKuCun(String skuId) {


        //GET_KUCUN
        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", skuId);

            String url = RequestUtils.GET_KUCUN + "?request=" + object;
            GetProtocol protocol = new GetProtocol(url);

            System.out.println("BangDingActivity.linkData=" + url);

            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("BangDingActivity.onSuccessdddd=" + json);
                    if (StringUtils.isEmpty(json)) {

                    } else {
                        try {
                            Gson gson = new Gson();
                            KuCunInfo info = gson.fromJson(json, KuCunInfo.class);
                            if (info.isSuccess()) {
                                getCurrentPage().getSkuModel().kucun = info.getTarget().getNumberShowTotal();
                                getCurrentPage().getSkuModel().setSafeKucun(info.getTarget().getGuardBit());
                            } else {
                                ToastUtils.showToast("" + info.getMessage());
                            }
                        } catch (Exception e) {
                            ToastUtils.showToast(e.getMessage());
                        }
                    }

                    getCurrentPage().advance();
                    mModelWatcher.onPageUpdate(getCurrentPage());
                }

                @Override
                public void onFail() {
                    getCurrentPage().advance();
                    mModelWatcher.onPageUpdate(getCurrentPage());

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void navBefore() {
        if (mCurrentPageIndex != 0) {
            mCurrentPageIndex--;
            TransferPage page = getCurrentPage();
            mPageListener.onPageChanged(page);
        }
    }

    @Override
    public TransferPage getCurrentPage() {
        return mTransferPages.get(mCurrentPageIndex);
    }
    /**
     * --------------------- ModelList end ---------------------
     **/

    /**
     * 获取sku信息   更具商品的code获取sku信息
     *
     * @param code
     */
    private void requestSku(String code) {
        if (!mIsRequesting) {
            mIsRequesting = true;
            mSkuRequest = new TransferSkuRequest(Utils.filterInput(code, "10002"), mSkuModelListener, this);
            FreshRequest.getInstance().addToRequestQueue(mSkuRequest, this.toString());
        }
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillOut(String code) {
        TransferPage page = getCurrentPage();
        page.setOutBarcode(code);
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(code, "1"));
            BaseRequest<ResponsePosition> request =
                    new BaseRequest<>(RequestUtils.POSITION_BARCODE,
                            ResponsePosition.class,
                            param,
                            mPositionListener, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillIn(String code) {
        TransferPage page = getCurrentPage();
        page.setInBarcode(code);
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(code, "1"));
            BaseRequest<ResponsePosition> request =
                    new BaseRequest<>(RequestUtils.POSITION_BARCODE,
                            ResponsePosition.class,
                            param,
                            mPositionListener, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillCount(String code) {
        TransferPage page = getCurrentPage();
        if (Utils.filterInput(code.equals(page.getSkuModel().getBarcode()), false)) {
            addItem(page, 1);
            mModelWatcher.onPageUpdate(page);
        } else if (Utils.isDigital(code)) {
            try {
                int count = Integer.parseInt(code);
                setItem(page, count);
                mModelWatcher.onPageUpdate(page);
            } catch (Exception e) {
                SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                        mSkuCountListener, this);
                FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
            }
        } else {
            SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                    mSkuCountListener, this);
            FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
        }
    }

    private void addItem(TransferPage page, int count) {
//        int current = page.getSkuModel().getCount();
//        page.getSkuModel().setCount(count + current);
        page.getSkuModel().setCount(StringUtils.getString(count));
        mModelWatcher.onPageUpdate(page);
    }

    private void setItem(TransferPage page, int count) {
        page.getSkuModel().setCount(StringUtils.getString(count));
        mModelWatcher.onPageUpdate(page);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        clearRequestState();
    }

    public void onResponse(final ResponseSkuTransfer response) {
        clearRequestState();
        if (response != null && response.isSuccess()) {

            long skuId = response.getModule().getSkuId();
            String token = Login.getInstance().getToken();


            String s = RequestUtils.INTO_STORAGE + "request=" +
                    "{token:\"" + token + "\",data:\"" + skuId + "\"}";

            //获取库位的请求
            GetProtocol protocol = new GetProtocol(s);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    linkedData(response, json);
                }

                @Override
                public void onFail() {
                }
            });
        } else {
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }

    private void linkedData(ResponseSkuTransfer response, String json) {

        Gson gson = new Gson();
        CuWeiBean bean = gson.fromJson(json, CuWeiBean.class);

        TransferPage page = getCurrentPage();
        page.setSkuModel(response.getModule());

        if (bean.success && bean.target.size() > 0) {

            if (bean.target.get(0).pisitionVO != null) {
                String code = bean.target.get(0).pisitionVO.code;
                mBarcode = bean.target.get(0).pisitionVO.barcode;
                page.getSkuModel().setCuwei(code);
            }
        } else {
            ToastUtils.showToast("" + bean.message);
        }

        getAllKuCun(response.getModule().getSkuId()+"");

//        page.advance();
//        mModelWatcher.onPageUpdate(page);
    }

    public void onCountResponse(ResponseSku response) {
        clearRequestState();
        if (response != null && response.isSuccess()) {
            TransferPage page = getCurrentPage();
            if (page.getSkuModel().getSkuId() == response.getModule().getSkuId()) {
                addItem(page, 1);
            } else {
                mModelWatcher.onError("请输入正确的商品条码或者数量");
            }
        } else {
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }

    private void clearRequestState() {
        mIsRequesting = false;
    }

    public List<TransferPage> getTransferPages() {
        return mTransferPages;
    }

    private String count1;

    public void setCount(String s) {
        this.count1 = s;
    }
}
