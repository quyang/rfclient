package mobile.freshtime.com.freshtime.common.view;

import android.content.Context;
import android.graphics.Color;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

/**
 * Created by orchid on 16/9/14.
 */
public class NoInputEditText extends EditText {

    public NoInputEditText(Context context) {
        super(context);
        init();
    }

    public NoInputEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        this.setSelection(this.getText().length());
        this.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                int inType = NoInputEditText.this.getInputType(); // backup the input type
                NoInputEditText.this.setInputType(InputType.TYPE_NULL); // disable soft input
                NoInputEditText.this.onTouchEvent(event); // call native handler
                NoInputEditText.this.setInputType(inType); // restore input type
                return true; // consume touch even
            }
        });

        this.setCustomSelectionActionModeCallback(new ActionMode.Callback() {

            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public void onDestroyActionMode(ActionMode mode) {
            }

            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                return false;
            }

            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                return false;
            }
        });
    }
}
