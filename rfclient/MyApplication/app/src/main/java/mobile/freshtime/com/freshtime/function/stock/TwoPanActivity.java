package mobile.freshtime.com.freshtime.function.stock;

import android.app.Dialog;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.function.search.SearchBean;
import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.function.stock.module.IndexInfo;
import mobile.freshtime.com.freshtime.function.stock.module.SmallInfo;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPanClass;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPanForResult;
import mobile.freshtime.com.freshtime.function.stock.presenter.TwoPanActivityPresenter;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

import static mobile.freshtime.com.freshtime.R.id.item_input;

/**
 * 二盘明细页
 */
public class TwoPanActivity extends BaseActivity implements ViewInterface, View.OnClickListener {

    private TextView mItemName;
    private TextView mItemInputOne;
    private TextView mItemNameTwo;
    private TextView mItemInputTwo;
    private TextView mName;
    private TextView mGuige;
    private TextView mCode69;
    private TextView mShuliangOne;
    private TextView mGuigeOnePan;
    private TextView mShuliangTwo;
    private TextView mGuigeTwoPan;
    private TextView mCangwei;
    private TextView mJiegou;
    private TwoPanActivityPresenter mPresenter;
    private String mCode;
    private String mSkuId;
    private int mSaleUnite;
    private long mPid;
    private int mId;
    private String mKuwei;
    private String mIndex;
    private String mJigou1;
    private String mCode691;
    private String firstNo;
    private SmallInfo mInfo;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan_task_two);

        mInfo = (SmallInfo) getIntent().getSerializableExtra("info");
        findComponent();
        mPresenter = new TwoPanActivityPresenter(this);
    }

    @Override
    public void onScanResult(String result) {
        if (!StringUtils.isEmpty(result)) {
            mPresenter.onScanResult(result.trim());
        }
    }

    public TextView getItemInputOne() {
        return mItemInputOne;
    }

    public TextView getItemInputTwo() {
        return mItemInputTwo;
    }

    private void findComponent() {
        ViewGroup inputs = (ViewGroup) findViewById(R.id.inputs);
        inputs.setVisibility(View.GONE);

        mItemName = (TextView) inputs.findViewById(R.id.item_tag);
        mItemInputOne = (TextView) inputs.findViewById(item_input);
        mItemInputOne.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        mItemName.setText("商品条码: ");

        mItemNameTwo = (TextView) inputs.findViewById(R.id.item_tag_two);
        mItemInputTwo = (TextView) inputs.findViewById(R.id.item_input_two);
        mItemInputTwo.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        mItemNameTwo.setText("库位条码: ");


        ViewGroup panItemInfo = (ViewGroup) findViewById(R.id.pan_item_info);
        mName = (TextView) panItemInfo.findViewById(R.id.item_name);
        mGuige = (TextView) panItemInfo.findViewById(R.id.guige);
        mCode69 = (TextView) panItemInfo.findViewById(R.id.item_69code);
        mShuliangOne = (TextView) panItemInfo.findViewById(item_input);
        mGuigeOnePan = (TextView) panItemInfo.findViewById(R.id.onePan_guige);
        mShuliangTwo = (TextView) panItemInfo.findViewById(R.id.item_input_two);
        mGuigeTwoPan = (TextView) panItemInfo.findViewById(R.id.twoPan_guige);

        mShuliangOne.setFocusable(false);
        mShuliangTwo.setFocusable(true);

        mShuliangOne.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
        mShuliangTwo.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        ViewGroup skuInfoRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        mCangwei = (TextView) skuInfoRoot.findViewById(R.id.cangwei);
        mJiegou = (TextView) skuInfoRoot.findViewById(R.id.jiegou);

        TextView ment = (TextView) findViewById(R.id.last);
        TextView ok = (TextView) findViewById(R.id.next);
        ment.setText("返回");
        ok.setText("确定");

        ment.setOnClickListener(this);
        ok.setOnClickListener(this);

        mName.setText("商品名称: " + mInfo.itemName);
        mGuige.setText("规格: " + mInfo.guige);
//        mCode69.setText("商品条码: " + mInfo.code69);
        mShuliangOne.setText(mInfo.firstNo);
        mCangwei.setText("" + mInfo.kuwei);
        mJiegou.setText("" + mInfo.jigou);

        if (StringUtils.isEmpty(mInfo.secondNo + "")) {
            mShuliangTwo.setText("");
        } else {
            mShuliangTwo.setText(mInfo.secondNo + "");
        }

    }

    @Override
    public void success(String json) {
        parseJson(json);
    }

    private void parseJson(final String json1) {

        if (StringUtils.isEmpty(json1)) {
            return;
        }

        Gson gson = new Gson();
        final SearchBean bean = gson.fromJson(json1, SearchBean.class);

        if (bean == null) {
            return;
        }

        SearchBean.ModuleBean module = bean.module;
        String skuId = module.skuId;
        String s = RequestUtils.INTO_STORAGE + "request=" +
                "{token:\"" + Login.getInstance().getToken() + "\",data:\"" + skuId + "\"}";
        GetProtocol protocol = new GetProtocol(s);
        protocol.get();
        protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
            @Override
            public void onSuccess(String json) {
                linkData(bean, json);
            }

            @Override
            public void onFail() {
            }
        });
    }

    /**
     * @param json 包含商品positionID的json
     */
    private void linkData(SearchBean bean, String json) {

        if (json == null || "".equals(json) || "null".equals(json)) {
            ToastUtils.showToast(getResources().getString(R.string.no_more_msg));
            return;
        }

        try {
            Gson gson = new Gson();
            CuWeiBean bean2 = gson.fromJson(json, CuWeiBean.class);
            if (!bean2.success) {
                ToastUtils.showToast(bean2.message);
                return;
            }

            if (bean2.target != null && bean2.target.size() > 0) {
                for (int i = 0; i < bean2.target.size(); i++) {

                    if (bean2.target.get(0).pisitionVO != null) {
                        //获取商品的barcode
                        String barcode = bean2.target.get(0).pisitionVO.barcode;
                        mCode = bean2.target.get(0).pisitionVO.code;

                        mItemInputOne.setText(barcode);

                        SearchBean.ModuleBean module = bean.module;

                        mSkuId = module.skuId;
                        mSaleUnite = module.skuUnit;
                        mPid = bean2.target.get(0).pisitionVO.id;

                        mName.setText(module.title);
                        mGuige.setText(module.unStandardSpecification);
                        mCode69.setText(module.sku69Id);
                        mGuigeOnePan.setText(module.saleUnitStr);
                        mGuigeTwoPan.setText(module.saleUnitStr);

                    }
                }

            } else {
                ToastUtils.showToast(getResources().getString(R.string.no_kuwei_info));
            }
        } catch (Exception e) {
            ToastUtils.showToast(e.getMessage());
        }


    }

    @Override
    public void fail() {

    }


    Dialog mMMDialog;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.last://返回
                finish();
                break;
            case R.id.next://确定 提交二盘明细

                if (StringUtils.isEmpty2(mShuliangTwo.getText().toString().trim())) {
                    ToastUtils.showToast(getResources().getString(R.string.please_input_two_pan_count));
                } else {
                    TwoPanForResult result = new TwoPanForResult();
                    result.index = mInfo.index;
                    result.mStringCount = mShuliangTwo.getText().toString().trim();
                    EventBus.getDefault().post(result);

                    finish();
                }
                break;
        }
    }

    //提交二盘任务(明细)
    private void submitTwoPanDetails() {

        if (!StringUtils.isEmpty(mShuliangTwo.getText().toString().trim())) {

            JSONObject params = new JSONObject();
            JSONObject object = new JSONObject();
            try {
                JSONArray array = new JSONArray();
                //skuId\/positionId\/stocktakingId\/id
                params.put("id", mInfo.id);//信息id
                params.put("skuId", mInfo.skuId);
                params.put("positionId", mInfo.pid);
                params.put("stocktakingId", mInfo.stocktakingId);
                params.put("secondNo", mShuliangTwo.getText().toString().trim());

                array.put(params);

                object.put("token", Login.getInstance().getToken());
                object.put("data", array);

                GetProtocol protocol = new GetProtocol(RequestUtils.SUBMIT_STOCK + "?request=" + object);
                protocol.get();
                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                    @Override
                    public void onSuccess(String json) {
                        System.out.println("TwoPanActivity.submitTwoPanDetails=" + json);
                        if (!StringUtils.isEmpty(json)) {
                            try {
                                JSONObject object = new JSONObject(json);
                                if (object.getBoolean("success")) {
                                    ToastUtils.showToast(getResources().getString(R.string.commit_success));

                                    TwoPanClass info = new TwoPanClass();
                                    info.index = mInfo.index;
                                    EventBus.getDefault().post(info);

                                    finish();

                                } else {
                                    ToastUtils.showToast(getResources().getString(R.string.commit_fail));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                ToastUtils.showToast(e.getMessage());
                            }
                        }
                    }

                    @Override
                    public void onFail() {
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
                ToastUtils.showToast(e.getMessage());
            }
        }
    }

    //审核
    private void shenghe() {

        JSONObject object = new JSONObject();

        if (mInfo.stocktakingId != 0) {
            try {
                object.put("token", Login.getInstance().getToken());
                object.put("data", mInfo.stocktakingId + "");//任务id

                GetProtocol protocol = new GetProtocol(RequestUtils.AUDIT + "?request=" + object);
                protocol.get();
                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                    @Override
                    public void onSuccess(String json) {
                        System.out.println("TwoPanActivity.shenghe=" + json);
                        try {
                            JSONObject object1 = new JSONObject(json);

                            if (object1.getBoolean("success")) {
                                ToastUtils.showToast(getResources().getString(R.string.shenghe_success));
                                IndexInfo info = new IndexInfo();
                                info.index = mInfo.index;
                                EventBus.getDefault().post(info);
                                finish();

                            } else {
                                ToastUtils.showToast(getResources().getString(R.string.shenghe_fail));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            ToastUtils.showToast(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail() {
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
                ToastUtils.showToast(e.getMessage());
            }
        }
    }
}
