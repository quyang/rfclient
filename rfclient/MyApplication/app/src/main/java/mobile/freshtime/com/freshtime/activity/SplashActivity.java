package mobile.freshtime.com.freshtime.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.util.SparseArray;
import android.view.View;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.adapter.NewVersion;
import mobile.freshtime.com.freshtime.network.Conts;
import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import okhttp3.Call;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        ImageView img = (ImageView) findViewById(R.id.img);

        SparseArray<String> array = new SparseArray<>();

        initAnimation(img);
    }

    private void initAnimation(View view) {

        AlphaAnimation alphaAnimation = new AlphaAnimation(0.4f, 1.0f);
        alphaAnimation.setDuration(4000);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (NetUtils.hasNet(SplashActivity.this)) {
                    requestNet();
                } else {
                    Toast.makeText(SplashActivity.this, "当前网络不可用", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        view.startAnimation(alphaAnimation);

    }


    //request net for the version
    private void requestNet() {

        OkHttpUtils.get().url(Conts.URL_NEW_APP)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        enterLoginActivity();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        parseJson(response);
                    }
                });
    }

    //parse json
    private void parseJson(String json) {

        if (StringUtils.isEmpty(json)) {
            ToastUtils.showToast(getResources().getString(R.string.no_more_msg));
            enterLoginActivity();
            return;
        }

        Gson gon = new Gson();
        NewVersion version = gon.fromJson(json, NewVersion.class);
        NewVersion.XianzaishiRfBean xianzaishi_rf = version.getXianzaishi_rf();

        if (xianzaishi_rf == null) {
            enterLoginActivity();
            return;
        }

        if (StringUtils.isEmpty(xianzaishi_rf.getVersionCode())) {
            enterLoginActivity();
            return;
        }

        int versionCode = Integer.parseInt(xianzaishi_rf.getVersionCode());

        if (versionCode > 0 && getVersionCode() < versionCode) {
            downLoadNewApp(xianzaishi_rf);
        } else {
            enterLoginActivity();
        }
    }

    public int getVersionCode() {

        PackageManager manager = getPackageManager();

        try {
            PackageInfo packageInfo = manager.getPackageInfo(getPackageName(), 0);
            int versionCode = packageInfo.versionCode;
            return versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return -1;
    }

    //download the new app
    private void downLoadNewApp(NewVersion.XianzaishiRfBean version) {

        if (StringUtils.isEmpty(version.getUrl())) {
            ToastUtils.showToast("新版本的APP url为空");
            enterLoginActivity();
            return;
        }

        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setTitle("正在下载");
        dialog.setMessage(version.getDes() + "");
        dialog.setCancelable(false);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.show();

        if (!NetUtils.hasNet(SplashActivity.this)) {
            ToastUtils.showToast("请检查网络环境");
            return;
        }

        System.out.println("SplashActivity.downLoadNewApp=" + version.getUrl());

        OkHttpUtils
                .get()
                .url(version.getUrl())
                .build()
                .execute(new FileCallBack(Environment.getExternalStorageDirectory().getAbsolutePath(), Conts.NEW_APP_NAME) {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        enterLoginActivity();
                        System.out.println("SplashActivity.onError=哈哈哈");
                        ToastUtils.showToast("下载更新包失败");
                    }

                    @Override
                    public void inProgress(float progress, long total, int id) {
                        super.inProgress(progress, total, id);
                        dialog.setProgress((int) (100 * progress));
                    }

                    @Override
                    public void onResponse(File response, int id) {

                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_VIEW);
                        intent.addCategory(Intent.CATEGORY_DEFAULT);
                        intent.setDataAndType(Uri.fromFile(response),
                                "application/vnd.android.package-archive");
                        startActivityForResult(intent, 0);
                    }
                });
    }


    public void enterLoginActivity() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        enterLoginActivity();
    }
}
