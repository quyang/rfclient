package mobile.freshtime.com.freshtime.function.stock;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.view.NoInputEditText;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.stock.module.InSkuModel;
import mobile.freshtime.com.freshtime.function.stock.module.IndexInfo;
import mobile.freshtime.com.freshtime.function.stock.module.StockPage;
import mobile.freshtime.com.freshtime.function.stock.module.TaskOver;
import mobile.freshtime.com.freshtime.function.stock.presenter.PanTaskViewModule;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;

import static mobile.freshtime.com.freshtime.R.id.cangwei;

/**
 * 点击进入后的Activity  一盘
 */
public class PanTaskActivity extends BaseActivity implements ViewModelWatcher<StockPage>, PageListener<StockPage> {

    @Bind(R.id.item_tag)
    TextView mItemTag;
    @Bind(R.id.item_tag_two)
    TextView mItemTagTwo;
    @Bind(R.id.activity_pan_task)
    LinearLayout mActivityPanTask;
    @Bind(R.id.onePan)
    TextView mOnePan;
    @Bind(R.id.last)
    TextView mLast;
    @Bind(R.id.done)
    TextView mDone;
    @Bind(R.id.next)
    TextView mNext;
    private PanTaskViewModule mModule;
    private TextView mItemName;
    private TextView mGuige;
    private TextView mItem69code;
    public TextView mItemInput;
    private TextView mOnePanGuige;
    private ViewGroup mItemInfroRoot;
    private ViewGroup mSkuInfoRoot;
    public TextView mCangwei;
    public TextView mJiegou;
    private ViewGroup mInputsRoor;
    private NoInputEditText mInput;
    public NoInputEditText mItemInputTwo;
    public TextView mItemCode;
    public TextView mItemCodeTwo;
    private int mId;
    private String mTime;
    private int mFaqiren;
    private int mQuyu;
    private int mTaskId;
    private TextView mKucunCount;
    private TextView mGet;
    private TextView mSafeKucun;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pan_task);
        ButterKnife.bind(this);

        EventBus.getDefault().register(this);

        mId = getIntent().getIntExtra("id", -1);//index
        mTime = getIntent().getStringExtra("time");
        mFaqiren = getIntent().getIntExtra("faqiren", -1);
        mQuyu = getIntent().getIntExtra("quyu", -1);
        mTaskId = getIntent().getIntExtra("taskId", -1);

        TextView commit = (TextView) findViewById(R.id.commit);
        commit.setOnClickListener(new MyOnClickListener());

        mGet = (TextView) findViewById(R.id.get);

        mInputsRoor = (ViewGroup) findViewById(R.id.inputs);
        mItemCode = (TextView) mInputsRoor.findViewById(R.id.item_input);
        mItemCodeTwo = (TextView) mInputsRoor.findViewById(R.id.item_input_two);

        mInput = (NoInputEditText) mInputsRoor.findViewById(R.id.item_input);
        mItemInputTwo = (NoInputEditText) mInputsRoor.findViewById(R.id.item_input_two);

        mItemInfroRoot = (ViewGroup) findViewById(R.id.item_info_root);
        mItemName = (TextView) mItemInfroRoot.findViewById(R.id.item_name);
        mGuige = (TextView) mItemInfroRoot.findViewById(R.id.guige);
        mItem69code = (TextView) mItemInfroRoot.findViewById(R.id.item_69code);
        mItemInput = (TextView) mItemInfroRoot.findViewById(R.id.item_input);
        mOnePanGuige = (TextView) mItemInfroRoot.findViewById(R.id.onePan_guige);
        mKucunCount = (TextView) mItemInfroRoot.findViewById(R.id.kucun_count);//总库存
        mSafeKucun = (TextView) mItemInfroRoot.findViewById(R.id.safe_kucun);
        mSafeKucun.setVisibility(View.VISIBLE);

        mSkuInfoRoot = (ViewGroup) findViewById(R.id.sku_info_root);
        mCangwei = (TextView) mSkuInfoRoot.findViewById(cangwei);
        mJiegou = (TextView) mSkuInfoRoot.findViewById(R.id.jiegou);

        mItemInfroRoot.setVisibility(View.INVISIBLE);
        mSkuInfoRoot.setVisibility(View.INVISIBLE);

        mInput.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);
        mItemInputTwo.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        mItemTag.setText("商品条码: ");
        mItemTagTwo.setText("仓位编码: ");

        mModule = new PanTaskViewModule(this);
        mModule.addPageListener(this);
        mModule.registerContext(this);

    }


    public TextView getKucunCount() {
        return mKucunCount;
    }

    @Override
    public void onScanResult(String result) {
        if (StringUtils.isEmpty(result)) {
            return;
        }

        mModule.onScanResult(result.trim());
    }

    @Override
    public void onRequestStart() {

    }

    @Override
    public void onRequestStop() {

    }

    @Override
    public void onPageUpdate(StockPage page) {
        displayPage(page);
    }


    @Override
    public void onError(String errorCode) {
    }

    @Override
    public void onFinish() {
    }

    @Override
    public void onPageChanged(StockPage page) {
        displayPage(page);
    }


    private void displayPage(StockPage page) {

        if (mModule.hasBefore()) {
            mLast.setEnabled(true);
        } else {
            mLast.setEnabled(false);
        }

        if (!mModule.hasNext()
//                &&
//                page.getPhase() != StockPage.Phase.SKU) {
//                page.getPhase() != StockPage.Phase.START
                ) {
            mNext.setEnabled(true);
        } else {
            mNext.setEnabled(true);
        }

        StockPage.Phase phase = page.getPhase();
        if (phase == StockPage.Phase.START) {
            displayStart(page);
        } else if (phase == StockPage.Phase.SKU) {
            displaySku(page);//显示sku信息
        } else if (phase == StockPage.Phase.COUNT) {
//            displayPosition(page);//显示墙位position
        } else {
//            displayCount(page);
        }
    }

    /**
     * 初始化的UI
     *
     * @param page
     */
    private void displayStart(StockPage page) {

        mItemInfroRoot.setVisibility(View.INVISIBLE);
        mSkuInfoRoot.setVisibility(View.INVISIBLE);

        mItemCode.setText("");
        mItemCodeTwo.setText("");

        mItemCode.requestFocus();

    }

    //显示sku信息
    private void displaySku(StockPage page) {

//        mInfoRoot.setVisibility(View.VISIBLE);

        InSkuModel unit = page.getSkuModel();
        displayUnit(unit);

    }

    private void displayUnit(InSkuModel unit) {

        mItemInfroRoot.setVisibility(View.VISIBLE);
        mSkuInfoRoot.setVisibility(View.VISIBLE);

        mItemName.setText("商品: " + unit.getTitle());
        mGuige.setText("规格: " + unit.saleUnitStr);
        mItem69code.setText("69码: " + unit.getSku69Id() + "");
        mOnePanGuige.setText(unit.saleUnitStr);
//        mOnePanGuige.setText(unit.getSpecification().substring(1));
        mItemCode.setText(unit.item_input);//设置扫描到的码

        mCangwei.setText(unit.kuwei);
        mJiegou.setText(unit.getPositionId() + "");

        if (StringUtils.isKongStr(unit.barcodeTwo)) {
            mItemCodeTwo.setText("");
        } else {
            mItemCodeTwo.setText(unit.barcodeTwo);
        }

        if ("null".equals(unit.getCount() + "")) {
            mItemInput.setText("");//设置数量
        } else {
            mItemInput.setText(String.valueOf(unit.getCount()));//设置数量
        }

        if (StringUtils.isKongStr(unit.kucun)) {
            mKucunCount.setText("总库存: 无");
        } else {
            mKucunCount.setText("总库存: " + unit.kucun);
        }

        if (StringUtils.isKongStr(unit.safeKucun)) {
            mSafeKucun.setText("安全库存：");
        } else {
            mSafeKucun.setText("安全库存：" + unit.safeKucun);
        }


    }

    @OnClick({R.id.last, R.id.done, R.id.next})
    public void onClick(View view) {

        int id = view.getId();

        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {
            if (mItemInfroRoot.getVisibility() == View.INVISIBLE || mSkuInfoRoot.getVisibility() == View.INVISIBLE) {
                ToastUtils.showToast(getResources().getString(R.string.please_scan_tight_tiaoma_kuwei));
                return;
            }

            if (StringUtils.isKongStr(mItemInput.getText().toString().trim())) {
                Toast.makeText(PanTaskActivity.this, getResources().getString(R.string.please_input_count), Toast.LENGTH_LONG).show();
                return;
            }

            mModule.setCount(mItemInput.getText().toString());
            mModule.navNext();
            requestDone();
        } else if (id == R.id.last) {
            mModule.navBefore();
        } else if (id == R.id.next) {

            if (mItemInfroRoot.getVisibility() == View.INVISIBLE || mSkuInfoRoot.getVisibility() == View.INVISIBLE) {
                Toast.makeText(PanTaskActivity.this, getResources().getString(R.string.please_item_or_kuwei), Toast.LENGTH_LONG).show();
                return;
            }

            if (StringUtils.isKongStr(mItemInput.getText().toString().trim())) {
                Toast.makeText(PanTaskActivity.this, getResources().getString(R.string.please_input_count), Toast.LENGTH_LONG).show();
                return;
            }

            mModule.setCount(mItemInput.getText().toString());
            mItemInput.setText("");
            mModule.navNext();
        }

        if (id == R.id.get) {
            mModule.requestSku(mInput.getText().toString().trim());
        }
    }

    //提交一盘数据
    private void requestDone() {

        if (mId != -1 && mFaqiren != -1 && mTaskId != -1 && mQuyu != -1) {

            ArrayList<StockPage> stockPages = mModule.getStockPages();
            ArrayList<InSkuModel> modules = new ArrayList<>();

            Intent intent = new Intent(PanTaskActivity.this, PanListActivity.class);
            for (int i = 0; i < stockPages.size(); i++) {

                StockPage page = stockPages.get(i);
                InSkuModel model = page.getSkuModel();

                if (model != null && model.getPositionId() <= 0) {
                    ToastUtils.showToast(getResources().getString(R.string.has_no_kuwei));
                    return;
                }

                if (model != null && (page.getPhase() == StockPage.Phase.SKU)) {
                    if (!"0".equals(stockPages.get(i).getSkuModel().getCount())) {
                        modules.add(stockPages.get(i).getSkuModel());
                    }
                }
            }

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("units", modules);
            bundle.putInt("id", mId);//索引
            bundle.putInt("taskId", mTaskId);
            bundle.putInt("faqiren", mFaqiren);
            bundle.putInt("quyu", mQuyu);
            bundle.putString("time", mTime);

            intent.putExtras(bundle);
            startActivity(intent);

        }
    }

    Dialog mMMDialog;

    private class MyOnClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            mMMDialog = new Dialog(PanTaskActivity.this, R.style.MyDialog);
            View view = getLayoutInflater().inflate(R.layout.cancel, null);
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText(getResources().getString(R.string.submit));
            final Button mNo = (Button) view.findViewById(R.id.no);
            Button mYes = (Button) view.findViewById(R.id.yes);

            mMMDialog.setCancelable(false);
            mMMDialog.setContentView(view);

            //设置弹窗宽高
            mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(PanTaskActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

            mMMDialog.show();

            mYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeStatus(mTaskId);
                }
            });

            mNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMMDialog.dismiss();
                }
            });
        }
    }

    //改变状态 (一盘)
    private void changeStatus(final int taskId) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", taskId + "");

            GetProtocol protocol = new GetProtocol(RequestUtils.TO_CHECK + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    System.out.println("PanListActivityPresenter.onSuccess=" + json);
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        if (jsonObject.getBoolean("success")) {

                            IndexInfo info = new IndexInfo();
                            EventBus.getDefault().post(info);

                            Intent intent = new Intent(PanTaskActivity.this, FirstStockActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);

                        } else {
                            ToastUtils.showToast(getResources().getString(R.string.commit_fail));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(TaskOver info) {
        finish();
    }

}
