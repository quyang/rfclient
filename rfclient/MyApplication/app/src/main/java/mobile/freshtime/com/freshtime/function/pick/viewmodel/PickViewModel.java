package mobile.freshtime.com.freshtime.function.pick.viewmodel;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.model.position.ResponsePosition;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.viewmodel.ModelList;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.BaseViewModel;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.pick.PickActivity;
import mobile.freshtime.com.freshtime.function.pick.PickTaskListActivity;
import mobile.freshtime.com.freshtime.function.pick.model.PickPage;
import mobile.freshtime.com.freshtime.function.pick.model.PickTask;
import mobile.freshtime.com.freshtime.function.pick.model.Task;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuRequest;

/**
 * Created by orchid on 16/10/7.
 */

public class PickViewModel extends BaseViewModel<PickPage> implements ModelList<PickPage>, Response.ErrorListener, Response.Listener<BaseResponse> {

    private List<PickPage> mPickPages;
    private int mCurrentPageIndex;

    private PageListener mPageListener;
    private ViewModelWatcher mModelWatcher;

    private long mBasketPosition;


    ArrayList<String> mBarCodeList;

    private PickActivity mPickActivity;

    //扫描商品条码回调到此
    private Response.Listener<ResponseSku> mSkuModelListener =
            new Response.Listener<ResponseSku>() {

                @Override
                public void onResponse(ResponseSku response) {
                    PickPage page = getCurrentPage();
                    long itemId = page.getTask().skuId;
                    long scanId = response.getModule().getSkuId();
                    if (Utils.filterInput(itemId == scanId, true)) {
                        if (page.getPhase() == PickPage.Phase.START) {
                            //已拣货数自增
                            page.getTask().pickedNo++;
                            page.advance();
                            mModelWatcher.onPageUpdate(page);
                        } else {
                            addItem(page, -1);
//                            addItem(page, 1);
                        }
                    } else {
                        mModelWatcher.onError("" + response.getErrorMsg());
                    }
                }
            };

    //  这里本来就有positionId,是本地对比,还是发起网络请求?
    private Response.Listener<ResponsePosition> mPositionListener =
            new Response.Listener<ResponsePosition>() {
                @Override
                public void onResponse(ResponsePosition response) {
                    if (response != null && response.isSuccess()) {
                        PickPage page = getCurrentPage();
                        page.getTask().positionId = response.target.id;
                        page.advance();
                        mModelWatcher.onPageUpdate(page);
                    } else {
                        mModelWatcher.onError(response.getErrorMsg() + "");
                    }
                }
            };

    public PickViewModel(ViewModelWatcher watcher, PickTask tasks, long basketPosition, ArrayList<String> mBarCodeList) {
        this.mBasketPosition = basketPosition;
        this.mPickPages = new ArrayList<>();
        this.mCurrentPageIndex = 0;
        this.mModelWatcher = watcher;
        this.mBarCodeList = mBarCodeList;
        mPickActivity = (PickActivity) watcher;

        for (int i = 0; i < tasks.details.size(); i++) {
            Task task = tasks.details.get(i);
            PickPage page = new PickPage(task);
            mPickPages.add(page);
        }
    }

    public void addPageListener(PageListener pageListener) {
        this.mPageListener = pageListener;
        PickPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    /**
     * --------------------- BaseViewModel start 仓位.商品和数量扫码走这里---------------------
     **/
    @Override
    public void onScanResult(String input) {

        PickPage page = getCurrentPage();
        PickPage.Phase phase = page.getPhase();

        if (phase == PickPage.Phase.START) {
            //走商品条码
            dispatchSkuBarcode(page, input);
        } else if (phase == PickPage.Phase.COUNT) {
            //数量
            dispatchCount(page, input);
        } else if (phase == PickPage.Phase.DONE) {
            dispatchCount(page, input);
        }
    }

    //校验商品条码 Skubarcode
    private void dispatchSkuBarcode(PickPage page, String input) {
        page.getTask().skuBarcode = input;
        SkuRequest skuRequest = new SkuRequest(Utils.filterInput(input, "10002"),
                mSkuModelListener, this);
        FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
    }

    int count = 0;

    /**
     * 校验仓位吗
     * ${@link #mPositionListener}
     *
     * @param page
     * @param input 即barcode,仓位码
     */
    private void dispatchPosition(PickPage page, String input) {

        System.out.println("PickViewModel.dispatchPosition=count=" + count);

        int size = mBarCodeList.size();

        if (count == size) {
            return;
        }

        String c = mBarCodeList.get(count);
        if (c.equals(input)) {
            PickActivity pickActivity = (PickActivity) mModelWatcher;
            pickActivity.setBarCode(input);
            ((PickActivity) mModelWatcher).getFoucus();
            count++;
        } else {
            ToastUtils.showToast(mPickActivity.getResources().getString(R.string.please_scan_right_kuwei));
        }

    }

    private void dispatchCount(PickPage page, String input) {
        if (Utils.filterInput(input.equals(page.getTask().skuBarcode), false)) {
            addItem(page, 1);
        } else if (Utils.isDigital(input)) {
            long count = Long.parseLong(input);
            if (count == page.getTask().orderNo) {
                // add to full
                addItem(page, count - page.getTask().pickedNo);
            } else {
                SkuRequest skuRequest = new SkuRequest(Utils.filterInput(input, "10002"),
                        mSkuModelListener, this);
                FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
            }
        } else {
            SkuRequest skuRequest = new SkuRequest(Utils.filterInput(input, "10002"),
                    mSkuModelListener, this);
            FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
        }
    }

    private void addItem(PickPage page, long count) {

        long daiCount = mPickActivity.getDaiCount();

        if (page.getTask().pickedNo == page.getTask().orderNo) {
            ToastUtils.showToast(mPickActivity.getResources().getString(R.string.to_max_count));
        }

        if (page.getTask().pickedNo < daiCount) {
            page.getTask().pickedNo += count;

            if (page.getTask().pickedNo == page.getTask().orderNo) {
                ToastUtils.showToast(mPickActivity.getResources().getString(R.string.to_max_count));
                page.advance();
            }
        }

        mModelWatcher.onPageUpdate(page);
    }

    @Override
    public void onInterrupt() {
    }

    @Override
    public boolean isRequesting() {
        return false;
    }

    @Override
    public void registerContext(BaseActivity activity) {

    }

    @Override
    public void unregisterContext() {
        this.mPageListener = null;
        this.mModelWatcher = null;
    }
    /**
     * --------------------- BaseViewModel end ---------------------
     **/

    /**
     * --------------------- ModelList start ---------------------
     **/

    @Override
    public boolean hasBefore() {
        return mCurrentPageIndex != 0;
    }

    @Override
    public boolean hasNext() {
        return mCurrentPageIndex != mPickPages.size() - 1;
    }

    @Override
    public boolean isReady() {
        for (int i = 0; i < mPickPages.size(); i++) {
            PickPage page = mPickPages.get(i);
            if (page.getPhase() != PickPage.Phase.DONE)
                return false;
        }
        return true;
    }

    //下一页
    @Override
    public void navNext() {
        mCurrentPageIndex++;
        PickPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    @Override
    public void navBefore() {
        if (mCurrentPageIndex != 0) {
            mCurrentPageIndex--;
            PickPage page = getCurrentPage();
            mPageListener.onPageChanged(page);
        }
    }

    @Override
    public PickPage getCurrentPage() {
        return mPickPages.get(mCurrentPageIndex);
    }

    /**
     * --------------------- ModelList end 提交---------------------
     **/

    public void requestDone() {
        JSONArray skus = new JSONArray();
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            for (int i = 0; i < mPickPages.size(); i++) {
                Task task = mPickPages.get(i).getTask();
                JSONObject sku = new JSONObject();
                sku.put("id", task.id);
                sku.put("pickId", task.pickId);
                sku.put("skuId", task.skuId);
                sku.put("positionId", task.positionId);
                sku.put("pickedNo", task.pickedNo);
                sku.put("basketId", mBasketPosition);
                skus.put(sku);
                param.put("data", skus);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        BaseRequest<BaseResponse> mRequest = new BaseRequest<>(
                RequestUtils.SUBMIT_PICK,
                BaseResponse.class,
                param,
                this, this);
        FreshRequest.getInstance().addToRequestQueue(mRequest, this.toString());
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        mModelWatcher.onError(mPickActivity.getResources().getString(R.string.please_scan_right_code));
    }

    //点击完成的回调
    @Override
    public void onResponse(BaseResponse response) {
        if (response != null && response.isSuccess()) {

            ToastUtils.showToast(mPickActivity.getResources().getString(R.string.submit_pick_task_success));
            mModelWatcher.onFinish();
            mPickActivity.setZero();
            EventBus.getDefault().post(PickTaskListActivity.class);
            count = 0;
        } else {
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }
}
