package mobile.freshtime.com.freshtime.function.transfer.model;

/**
 * Created by orchid on 16/10/11.
 */

public class TransferPage {

    private TransferSkuModel mSkuModel;
    private Phase mPhase;

    private String mOutBarcode;
    private String mInBarcode;

    public String getInBarcode() {
        return mInBarcode;
    }

    public void setInBarcode(String inBarcode) {
        mInBarcode = inBarcode;
    }

    public String getOutBarcode() {
        return mOutBarcode;
    }

    public void setOutBarcode(String outBarcode) {
        mOutBarcode = outBarcode;
    }

    public TransferPage(TransferSkuModel skuModel) {
        this.mPhase = Phase.START;
        this.mSkuModel = skuModel;
    }

    public TransferSkuModel getSkuModel() {
        return mSkuModel;
    }

    public void setSkuModel(TransferSkuModel outgoUnit) {
        mSkuModel = outgoUnit;
    }

    public Phase getPhase() {
        return mPhase;
    }

    public void advance() {
        if (mPhase == Phase.START) {
            mPhase = Phase.OUT;
        } else if (mPhase == Phase.OUT) {
            mPhase = Phase.IN;
        } else if (mPhase == Phase.IN) {
            mPhase = Phase.COUNT;
        } else if (mPhase == Phase.COUNT) {
            mPhase = Phase.DONE;
        }
    }

    public enum Phase {
        START,
        OUT,
        IN,
        COUNT,
        DONE
    }
}
