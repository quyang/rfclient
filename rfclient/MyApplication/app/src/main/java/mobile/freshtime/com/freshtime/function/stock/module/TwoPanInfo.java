package mobile.freshtime.com.freshtime.function.stock.module;

import java.util.List;

/**
 * Created by Administrator on 2016/11/25.
 */
public class TwoPanInfo {

    /**
     * message : null
     * return_code : 0
     * success : true
     * target : {"agencyId":1,"auditTime":null,"auditor":null,"details":[{"auditor":null,"dr":0,"firstNo":"33","firstNoReal":33000,"id":36,"operator":null,"positionBarcode":null,"positionCode":"QC02705","positionId":568,"saleUnit":"块","saleUnitId":37,"saleUnitType":3,"secondNo":null,"secondNoReal":null,"skuBarcode":null,"skuId":20425,"skuName":"RitterSport瑞特斯波德 浓醇黑巧克力100g 德国进口","spec":null,"specUnit":null,"specUnitId":null,"stocktakingId":96,"theoreticalNo":null,"theoreticalNoReal":null,"ts":"2016-11-25T16:02:16"}],"dr":0,"id":96,"opTime":"2016-11-25T16:01:27","operate":55,"statu":1,"statuOld":null,"ts":"2016-11-25T16:02:16","type":null}
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * auditTime : null
     * auditor : null
     * details : [{"auditor":null,"dr":0,"firstNo":"33","firstNoReal":33000,"id":36,"operator":null,"positionBarcode":null,"positionCode":"QC02705","positionId":568,"saleUnit":"块","saleUnitId":37,"saleUnitType":3,"secondNo":null,"secondNoReal":null,"skuBarcode":null,"skuId":20425,"skuName":"RitterSport瑞特斯波德 浓醇黑巧克力100g 德国进口","spec":null,"specUnit":null,"specUnitId":null,"stocktakingId":96,"theoreticalNo":null,"theoreticalNoReal":null,"ts":"2016-11-25T16:02:16"}]
     * dr : 0
     * id : 96
     * opTime : 2016-11-25T16:01:27
     * operate : 55
     * statu : 1
     * statuOld : null
     * ts : 2016-11-25T16:02:16
     * type : null
     */

    private TargetBean target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public TargetBean getTarget() {
        return target;
    }

    public void setTarget(TargetBean target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private Object auditTime;
        private Object auditor;
        private int dr;
        private int id;
        private String opTime;
        private int operate;
        private int statu;
        private Object statuOld;
        private String ts;
        private Object type;
        /**
         * auditor : null
         * dr : 0
         * firstNo : 33
         * firstNoReal : 33000
         * id : 36
         * operator : null
         * positionBarcode : null
         * positionCode : QC02705
         * positionId : 568
         * saleUnit : 块
         * saleUnitId : 37
         * saleUnitType : 3
         * secondNo : null
         * secondNoReal : null
         * skuBarcode : null
         * skuId : 20425
         * skuName : RitterSport瑞特斯波德 浓醇黑巧克力100g 德国进口
         * spec : null
         * specUnit : null
         * specUnitId : null
         * stocktakingId : 96
         * theoreticalNo : null
         * theoreticalNoReal : null
         * ts : 2016-11-25T16:02:16
         */

        private List<DetailsBean> details;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public Object getAuditTime() {
            return auditTime;
        }

        public void setAuditTime(Object auditTime) {
            this.auditTime = auditTime;
        }

        public Object getAuditor() {
            return auditor;
        }

        public void setAuditor(Object auditor) {
            this.auditor = auditor;
        }

        public int getDr() {
            return dr;
        }

        public void setDr(int dr) {
            this.dr = dr;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOpTime() {
            return opTime;
        }

        public void setOpTime(String opTime) {
            this.opTime = opTime;
        }

        public int getOperate() {
            return operate;
        }

        public void setOperate(int operate) {
            this.operate = operate;
        }

        public int getStatu() {
            return statu;
        }

        public void setStatu(int statu) {
            this.statu = statu;
        }

        public Object getStatuOld() {
            return statuOld;
        }

        public void setStatuOld(Object statuOld) {
            this.statuOld = statuOld;
        }

        public String getTs() {
            return ts;
        }

        public void setTs(String ts) {
            this.ts = ts;
        }

        public Object getType() {
            return type;
        }

        public void setType(Object type) {
            this.type = type;
        }

        public List<DetailsBean> getDetails() {
            return details;
        }

        public void setDetails(List<DetailsBean> details) {
            this.details = details;
        }

        public static class DetailsBean {
            private Object auditor;
            private int dr;
            private String firstNo;
            private int firstNoReal;
            private int id;//任务id
            private Object operator;
            private Object positionBarcode;
            private String positionCode;
            private int positionId;
            private String saleUnit;
            private int saleUnitId;
            private int saleUnitType;
            private String secondNo;
            private Object secondNoReal;
            private Object skuBarcode;
            private int skuId;
            private String skuName;
            private Object spec;
            private Object specUnit;
            private Object specUnitId;
            private int stocktakingId;
            private Object theoreticalNo;
            private Object theoreticalNoReal;
            private String ts;


            public int count;

            public Object getAuditor() {
                return auditor;
            }

            public void setAuditor(Object auditor) {
                this.auditor = auditor;
            }

            public int getDr() {
                return dr;
            }

            public void setDr(int dr) {
                this.dr = dr;
            }

            public String getFirstNo() {
                return firstNo;
            }

            public void setFirstNo(String firstNo) {
                this.firstNo = firstNo;
            }

            public int getFirstNoReal() {
                return firstNoReal;
            }

            public void setFirstNoReal(int firstNoReal) {
                this.firstNoReal = firstNoReal;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public Object getOperator() {
                return operator;
            }

            public void setOperator(Object operator) {
                this.operator = operator;
            }

            public Object getPositionBarcode() {
                return positionBarcode;
            }

            public void setPositionBarcode(Object positionBarcode) {
                this.positionBarcode = positionBarcode;
            }

            public String getPositionCode() {
                return positionCode;
            }

            public void setPositionCode(String positionCode) {
                this.positionCode = positionCode;
            }

            public int getPositionId() {
                return positionId;
            }

            public void setPositionId(int positionId) {
                this.positionId = positionId;
            }

            public String getSaleUnit() {
                return saleUnit;
            }

            public void setSaleUnit(String saleUnit) {
                this.saleUnit = saleUnit;
            }

            public int getSaleUnitId() {
                return saleUnitId;
            }

            public void setSaleUnitId(int saleUnitId) {
                this.saleUnitId = saleUnitId;
            }

            public int getSaleUnitType() {
                return saleUnitType;
            }

            public void setSaleUnitType(int saleUnitType) {
                this.saleUnitType = saleUnitType;
            }

            public String getSecondNo() {
                return secondNo;
            }

            public void setSecondNo(String secondNo) {
                this.secondNo = secondNo;
            }

            public Object getSecondNoReal() {
                return secondNoReal;
            }

            public void setSecondNoReal(Object secondNoReal) {
                this.secondNoReal = secondNoReal;
            }

            public Object getSkuBarcode() {
                return skuBarcode;
            }

            public void setSkuBarcode(Object skuBarcode) {
                this.skuBarcode = skuBarcode;
            }

            public int getSkuId() {
                return skuId;
            }

            public void setSkuId(int skuId) {
                this.skuId = skuId;
            }

            public String getSkuName() {
                return skuName;
            }

            public void setSkuName(String skuName) {
                this.skuName = skuName;
            }

            public Object getSpec() {
                return spec;
            }

            public void setSpec(Object spec) {
                this.spec = spec;
            }

            public Object getSpecUnit() {
                return specUnit;
            }

            public void setSpecUnit(Object specUnit) {
                this.specUnit = specUnit;
            }

            public Object getSpecUnitId() {
                return specUnitId;
            }

            public void setSpecUnitId(Object specUnitId) {
                this.specUnitId = specUnitId;
            }

            public int getStocktakingId() {
                return stocktakingId;
            }

            public void setStocktakingId(int stocktakingId) {
                this.stocktakingId = stocktakingId;
            }

            public Object getTheoreticalNo() {
                return theoreticalNo;
            }

            public void setTheoreticalNo(Object theoreticalNo) {
                this.theoreticalNo = theoreticalNo;
            }

            public Object getTheoreticalNoReal() {
                return theoreticalNoReal;
            }

            public void setTheoreticalNoReal(Object theoreticalNoReal) {
                this.theoreticalNoReal = theoreticalNoReal;
            }

            public String getTs() {
                return ts;
            }

            public void setTs(String ts) {
                this.ts = ts;
            }
        }
    }
}
