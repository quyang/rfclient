package mobile.freshtime.com.freshtime.common.model.position;

/**
 * Created by orchid on 16/10/17.
 */

public class Position {

    public int agencyId;
    public int areaId;
    public int areaType;
    public String barcode;
    public String code;
    public int columnNo;
    public int dr;
    public int id;
    public int rowNo;
    public int slottingId;
    public int slottingType;
    public String ts;
}
