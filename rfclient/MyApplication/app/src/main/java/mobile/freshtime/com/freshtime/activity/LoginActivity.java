package mobile.freshtime.com.freshtime.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import mobile.freshtime.com.freshtime.Contants;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.SPUtils;
import mobile.freshtime.com.freshtime.login.Login;

/**
 * Created by orchid on 16/9/26.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener, Login.LoginListener {

    private EditText mNameInput;
    private EditText mPassInput;

    private View mProgressView;
    private TextView mClear;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mClear = (TextView) findViewById(R.id.clear);

        mNameInput = (EditText) findViewById(R.id.name_input);
        mPassInput = (EditText) findViewById(R.id.pass_input);
        mNameInput.setEnabled(true);
        mPassInput.setEnabled(true);

        findViewById(R.id.login).setOnClickListener(this);
        mClear.setOnClickListener(this);

        mProgressView = findViewById(R.id.progressbar);
        mProgressView.setVisibility(View.INVISIBLE);

    }

    @Override
    public void onScanResult(String result) {
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.login) {
            mProgressView.setVisibility(View.VISIBLE);
            Login.getInstance().login(mNameInput.getText().toString().trim(),
                    mPassInput.getText().toString().trim(), this);
        }

        if (id == R.id.clear) {
            mNameInput.setText("");
            mPassInput.setText("");
            mNameInput.requestFocus();
        }
    }

    @Override
    public void onLoginSuccess() {
        mProgressView.setVisibility(View.INVISIBLE);
        startActivity(new Intent(this, MenuActivity.class));
        SPUtils.putStringValue(LoginActivity.this, Contants.SP_FILE_NAME, Contants.OPERATER, mNameInput.getText().toString().trim());
        finish();
    }

    @Override
    public void onLoginFail(String reason) {
        mProgressView.setVisibility(View.INVISIBLE);
        Toast.makeText(this, "登录失败 ", Toast.LENGTH_LONG).show();
    }
}
