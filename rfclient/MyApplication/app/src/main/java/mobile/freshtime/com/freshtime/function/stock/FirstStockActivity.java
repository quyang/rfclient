package mobile.freshtime.com.freshtime.function.stock;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.utils.NetUtils;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.view.expandable.ExpandableLayoutListView;
import mobile.freshtime.com.freshtime.function.search.ViewInterface;
import mobile.freshtime.com.freshtime.function.stock.module.IndexInfo;
import mobile.freshtime.com.freshtime.function.stock.module.KuweiTaskInfo;
import mobile.freshtime.com.freshtime.function.stock.module.OnePanInfo;
import mobile.freshtime.com.freshtime.function.stock.module.OnePanzhuang;
import mobile.freshtime.com.freshtime.function.stock.module.PandianListAdapter;
import mobile.freshtime.com.freshtime.function.stock.module.SearchClass;
import mobile.freshtime.com.freshtime.function.stock.module.StocktakingInProgressInfo;
import mobile.freshtime.com.freshtime.function.stock.module.TaskOver;
import mobile.freshtime.com.freshtime.function.stock.module.TwoPandianListAdapter;
import mobile.freshtime.com.freshtime.function.stock.module.TwoStatusInfo;
import mobile.freshtime.com.freshtime.function.stock.presenter.FirstStockActivityPresenter;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * 盘点
 * Created by orchid on 16/10/12.
 * <p>
 * 1，创建任务/或提交拣货明细，区分方式是有没有任务ID，明细给不给都可以创建成功。
 * submitStocktakingDetail
 * 2，获取进行中任务，getStocktakingInProgress
 * <p>
 * 3，提交任务到二盘，submitStocktakingToCheck
 * 4，获取二盘任务，getStocktakingChecking
 * 5，提交二盘明细，submitStocktakingCheckDetail
 * 6，提交任务到待审核，submitStocktakingToAudit
 */

public class FirstStockActivity extends BaseActivity implements View.OnClickListener, ViewInterface, AdapterView.OnItemClickListener {

    private TextView mItemInput;

    public FirstStockActivityPresenter mPresenter;

    public ExpandableLayoutListView mList;

    private TextView mNewPanDian;

    TwoPandianListAdapter mTwoAdapter;

    private int mTaskId;

    private TextView mTitle;

    private ExpandableLayoutListView mErpan;

    private Handler mHandler;

    private TextView mCommit;

    private ArrayList<Integer> oneIndexArray = new ArrayList<>();

    private ArrayList<Integer> twoIndexArray = new ArrayList<>();

    private TextView mJiaru;

    private RecyclerView mRecyclerview;

    private int currentItemPosition = 0;

    private AdapterView<?> mParent;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_firststock);
        EventBus.getDefault().register(this);
        mHandler = new Handler(Looper.getMainLooper());

        mRecyclerview = (RecyclerView) findViewById(R.id.recyclerview);

        mRecyclerview.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mRecyclerview.setAdapter(new HomeAdapter());

        //提交按钮
        mCommit = (TextView) findViewById(R.id.commit);
        mCommit.setOnClickListener(this);
        mCommit.setVisibility(View.GONE);

        mJiaru = (TextView) findViewById(R.id.jiaru);
        mJiaru.setOnClickListener(this);

        mErpan = (ExpandableLayoutListView) findViewById(R.id.list_two);

        TextView tag = (TextView) findViewById(R.id.delivery_input).findViewById(R.id.item_tag);
        tag.setText("盘点任务: ");

        View top = findViewById(R.id.delivery_input);
        top.setVisibility(View.GONE);

        mTitle = (TextView) findViewById(R.id.title);
        mTitle.setText("新  盘  点");

        mList = (ExpandableLayoutListView) findViewById(R.id.list);

        mItemInput = (TextView) findViewById(R.id.delivery_input).findViewById(R.id.item_input);
        mItemInput.setInputType(InputType.TYPE_DATETIME_VARIATION_NORMAL);

        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.last).setOnClickListener(this);
        mNewPanDian = (TextView) findViewById(R.id.new_pandian);
        mNewPanDian.setOnClickListener(this);
        findViewById(R.id.choupan).setOnClickListener(this);

        mPresenter = new FirstStockActivityPresenter(this);

        mList.setVisibility(View.VISIBLE);
        mErpan.setVisibility(View.INVISIBLE);

        mList.setOnItemClickListener(this);

        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            mPresenter.submitStocktakingDetail(RequestUtils.SUBMIT_STOCK + "?request=" + object.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public int getCurrentItemPosition() {
        return currentItemPosition;
    }

    @Override
    public void onScanResult(String result) {
        mPresenter.onScanResult(result.trim());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
            case R.id.last:
                finish();
                break;
            case R.id.new_pandian://新盘点
                final Dialog mMDialog = new Dialog(FirstStockActivity.this, R.style.MyDialog);
                View view = getLayoutInflater().inflate(R.layout.cancel, null);
                TextView name = (TextView) view.findViewById(R.id.name);
                name.setText(getResources().getString(R.string.establish_task));
                Button mNo = (Button) view.findViewById(R.id.no);
                Button mYes = (Button) view.findViewById(R.id.yes);

                mMDialog.setCancelable(false);
                mMDialog.setContentView(view);

                //设置弹窗宽高
                mMDialog.getWindow().setLayout(UiUtils.getScreenHeight(FirstStockActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);
                mMDialog.show();
                mYes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getTask();//创建新盘点任务
                        mMDialog.dismiss();
                    }
                });

                mNo.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkTask();
                        mTitle.setText("新  盘  点");
                        mMDialog.dismiss();
                    }
                });

                break;
            case R.id.choupan://二盘
                getTwoPanTask();
                break;
            case R.id.commit://提交改变状态
                break;
            case R.id.jiaru://加入
                if (mList != null && mList.getVisibility() == View.VISIBLE) {
                    startOnePan();
                } else {
                    ToastUtils.showToast(getResources().getString(R.string.please_select_new_pandian));
                }
                break;
        }
    }

    //开始一盘
    private void startOnePan() {

        if (mItems != null) {
            StocktakingInProgressInfo.TargetBean.ItemsBean itemsBean = mItems.get(currentItemPosition);

            Intent intent = new Intent(FirstStockActivity.this, PanTaskActivity.class);
            intent.putExtra("id", currentItemPosition);//索引
            intent.putExtra("time", itemsBean.getOpTime().replace("T", " "));
            intent.putExtra("faqiren", itemsBean.getOperate());
            intent.putExtra("quyu", itemsBean.getAgencyId());
            intent.putExtra("taskId", itemsBean.getId());
            startActivity(intent);
        }
    }


    //创建任务
    public void getTask() {

        mList.setVisibility(View.VISIBLE);
        mErpan.setVisibility(View.INVISIBLE);

        mTitle.setText("新  盘  点");

        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            GetProtocol protocol = new GetProtocol(RequestUtils.SUBMIT_STOCK + "?request=" + object.toString());
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    try {
                        JSONObject object = new JSONObject(json);
                        if (object.getBoolean("success")) {
                            //再次查找任务
                            checkTask();
                        } else {
                            ToastUtils.showToast(getResources().getString(R.string.establish_fail));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //再次查询任务
    private void checkTask() {

        mList.setVisibility(View.VISIBLE);
        mErpan.setVisibility(View.INVISIBLE);

        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            GetProtocol protocol = new GetProtocol(RequestUtils.STOCK + "?request=" + object.toString());
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast(getResources().getString(R.string.no_more_msg));
                        return;
                    }

                    try {
                        JSONObject object = new JSONObject(json);
                        if (object.getBoolean("success")) {
                            Gson gson = new Gson();
                            StocktakingInProgressInfo info = gson.fromJson(json, StocktakingInProgressInfo.class);
                            if (info.isSuccess()) {
                                if (info.getTarget() != null && info.getTarget().getItems() != null && info.getTarget().getItems().size() > 0) {
                                    List<StocktakingInProgressInfo.TargetBean.ItemsBean> itemsBeanList = info.getTarget().getItems();

                                    mItems.clear();
                                    mItems = itemsBeanList;
                                    mAdapter = new PandianListAdapter(mItems, FirstStockActivity.this);
                                    mList.setAdapter(mAdapter);
                                }
                            } else {
                                ToastUtils.showToast(info.getMessage()+"");
                            }
                        } else {
                            ToastUtils.showToast("" + object.getString("message"));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        ToastUtils.showToast(e.getMessage());
                    }
                }

                @Override
                public void onFail() {
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            ToastUtils.showToast(e.getMessage());
        }
    }

    //获取二盘任务
    public void getTwoPanTask() {

        mList.setVisibility(View.INVISIBLE);
        mErpan.setVisibility(View.VISIBLE);

        mTitle.setText("二   盘");

        if (NetUtils.hasNet(this)) {

            JSONObject object = new JSONObject();
            try {
                object.put("token", Login.getInstance().getToken());

                GetProtocol protocol = new GetProtocol(RequestUtils.GET_TWO_PAN + "?request=" + object);
                protocol.get();
                protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                    @Override
                    public void onSuccess(String json) {
                        if (StringUtils.isEmpty(json)) {
                            ToastUtils.showToast(getResources().getString(R.string.no_more_msg));
                            return;
                        }

                        try {
                            Gson gson = new Gson();

                            StocktakingInProgressInfo stocktakingInfo = gson.fromJson(json, StocktakingInProgressInfo.class);

                            if (stocktakingInfo.isSuccess()) {

                                StocktakingInProgressInfo.TargetBean target = stocktakingInfo.getTarget();
                                mItems.clear();
                                mItems = target.getItems();
                                if (mItems.size() > 0) {
                                    mTwoAdapter = new TwoPandianListAdapter(mItems, FirstStockActivity.this);

                                    mErpan.setAdapter(mTwoAdapter);

                                } else {
                                    mNewPanDian.setClickable(true);
                                    mErpan.setVisibility(View.INVISIBLE);
                                }
                            } else {
                                ToastUtils.showToast("" + stocktakingInfo.getMessage());
                            }
                        } catch (Exception e) {
                            ToastUtils.showToast(e.getMessage());
                        }
                    }

                    @Override
                    public void onFail() {
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
                ToastUtils.showToast(e.getMessage());
            }
        }
    }

    public TextView getNewPanDian() {
        return mNewPanDian;
    }

    @Override
    public void success(String json) {
    }

    @Override
    public void fail() {
        ToastUtils.showToast(getResources().getString(R.string.get_pan_task_fail));
    }

    List<StocktakingInProgressInfo.TargetBean.ItemsBean> mItems = new ArrayList<>();

    private PandianListAdapter mAdapter;

    //回传任务id列表
    public void onTwoPan(List<StocktakingInProgressInfo.TargetBean.ItemsBean> items) {
        mItems = items;

        if (mItems != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {//一盘
                    mAdapter = new PandianListAdapter(mItems, FirstStockActivity.this);
                    mList.setAdapter(mAdapter);
                }
            });
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        mParent = parent;

        currentItemPosition = position;
        mAdapter.notifyDataSetChanged();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(OnePanInfo onePanInfo) { //一盘明细
        int mIndex = onePanInfo.id;//盘点任务列表索引

        mItems.get(mIndex).setChanged(true);
        mAdapter.notifyDataSetChanged();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(KuweiTaskInfo info) { //二盘明细
        int index = info.index;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(TwoStatusInfo info) { //二盘状态
        getTwoPanTask();
    }

    @Subscribe(threadMode = ThreadMode.MAIN) //一盘状态
    public void shuxing(IndexInfo info) {
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(TaskOver info) {//查询一盘任务
        mPresenter.checkTask();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(SearchClass info) {
        getTwoPanTask();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void shuxing(OnePanzhuang info) {
        mPresenter.checkTask();
    }

    class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.MyViewHolder> {

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            MyViewHolder holder = new MyViewHolder(LayoutInflater.from(
                    FirstStockActivity.this).inflate(R.layout.listitem_pandian, parent, false));
            return holder;
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, final int position) {
//            holder.tv.setText(mItems.get(position));
            if (mItems != null) {
                StocktakingInProgressInfo.TargetBean.ItemsBean bean = mItems.get(position);

                holder.mFaqiren.setText(bean.getOperate() + "");
                holder.mTime.setText(bean.getOpTime() + "");
                holder.mPandianAdress.setText(bean.getAgencyId() + "");
                holder.mPandianTask.setText("" + bean.getId());
                holder.mCommit.setText("查看详情");

                holder.mHeader.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        View childAt = mRecyclerview.getChildAt(position);
                        int count = mRecyclerview.getChildCount();
                        for (int i = 0; i < count; i++) {

                            if (i == position) {
                                childAt.setSelected(true);
                            } else {
                                childAt.setSelected(false);
                            }
                        }
                    }
                });
            }
        }

        @Override
        public int getItemCount() {
            return mItems == null ? 0 : mItems.size();
        }

        class MyViewHolder extends RecyclerView.ViewHolder {

            public TextView mFaqiren;
            public TextView mTime;
            public TextView mPandianAdress;
            public TextView mPandianTask;
            public TextView mPandianQuyu;
            public TextView mEnter;
            public TextView mCommit;
            public View mHeader;

            public MyViewHolder(View view) {
                super(view);

                mHeader = view.findViewById(R.id.one_pan_header);

                mFaqiren = (TextView) view.findViewById(R.id.item_name);
                mTime = (TextView) view.findViewById(R.id.item_count);
                mPandianAdress = (TextView) view.findViewById(R.id.item_code);
                mPandianTask = (TextView) view.findViewById(R.id.sku_info);
                mPandianQuyu = (TextView) view.findViewById(R.id.sku_code);
                mEnter = (TextView) view.findViewById(R.id.item_detail);
                mCommit = (TextView) view.findViewById(R.id.commit);

            }
        }
    }

}
