package mobile.freshtime.com.freshtime.function.stock.module;

import android.app.Dialog;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.function.stock.DetailsActivity;
import mobile.freshtime.com.freshtime.function.stock.FirstStockActivity;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

import static mobile.freshtime.com.freshtime.R.id.commit;

/**
 * 盘点列表适配器
 * Created by Administrator on 2016/11/15.
 */
public class PandianListAdapter extends BaseAdapter {

    private List<StocktakingInProgressInfo.TargetBean.ItemsBean> mList;

    private FirstStockActivity mActivity;

    private static final int KEY = R.id.expandableLayout;

    private Dialog mMMDialog;


    public PandianListAdapter(List<StocktakingInProgressInfo.TargetBean.ItemsBean> list, FirstStockActivity activity) {
        mList = list;
        mActivity = activity;
    }

    @Override
    public int getCount() {
        return mList == null ? 0 : mList.size();
    }

    @Override
    public StocktakingInProgressInfo.TargetBean.ItemsBean getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            convertView = View.inflate(mActivity, R.layout.listitem_pandian, null);

            holder = new ViewHolder();

            //body
            holder.faqiren = (TextView) convertView.findViewById(R.id.item_name);
            holder.time = (TextView) convertView.findViewById(R.id.item_count);
            holder.pandianAdress = (TextView) convertView.findViewById(R.id.item_code);

            //header
            holder.pandianTask = (TextView) convertView.findViewById(R.id.sku_info);
            holder.pandianQuyu = (TextView) convertView.findViewById(R.id.sku_code);
            holder.enter = (TextView) convertView.findViewById(R.id.item_detail);
            holder.commit = (TextView) convertView.findViewById(commit);
            holder.header = convertView.findViewById(R.id.one_pan_header);

            convertView.setTag(KEY, holder);

        } else {
            holder = (ViewHolder) convertView.getTag(KEY);
        }


        if (mActivity.getCurrentItemPosition() == position) {
//            holder.header.setBackgroundColor(Color.RED);
            holder.header.setSelected(true);
        } else {
//            holder.header.setBackgroundColor(Color.WHITE);
            holder.header.setSelected(false);
        }


        final StocktakingInProgressInfo.TargetBean.ItemsBean item = getItem(position);

        holder.faqiren.setText("发起人: " + item.getOperate());
        holder.time.setText("发起时间: " + item.getOpTime().replace("T", " "));
        holder.pandianAdress.setText("盘点区域: " + item.getAgencyId());
        holder.pandianTask.setText("盘点任务: " + item.getId());

        if (item.isChanged()) {
            holder.commit.setVisibility(View.VISIBLE);
        } else {
            holder.commit.setVisibility(View.VISIBLE);
        }

        holder.commit.setText("查看详情");

        holder.commit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mActivity, DetailsActivity.class);
                intent.putExtra("taskId", item.getId() + "");
                mActivity.startActivity(intent);
            }
        });

        return convertView;
    }


    public static class ViewHolder {

        public TextView faqiren;
        public TextView time;
        public TextView pandianAdress;
        public TextView pandianTask;
        public TextView pandianQuyu;
        public TextView enter;
        public TextView commit;
        public View header;

    }


    //改变状态 (一盘)
    private void changeStatus(final int taskId) {

        JSONObject object = new JSONObject();

        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", taskId + "");

            GetProtocol protocol = new GetProtocol(RequestUtils.TO_CHECK + "?request=" + object);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {

                    System.out.println("PanListActivityPresenter.onSuccess=" + json);
                    try {
                        JSONObject jsonObject = new JSONObject(json);
                        if (jsonObject.getBoolean("success")) {
                            ToastUtils.showToast("提交成功");
                            mMMDialog.dismiss();

//                            IndexInfo info = new IndexInfo();
//                            EventBus.getDefault().post(info);
//
//                            Intent intent = new Intent(PanTaskActivity.this, FirstStockActivity.class);
//                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                            startActivity(intent);

                            mActivity.mPresenter.checkTask();

                        } else {
                            ToastUtils.showToast("提交失败");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFail() {
                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
