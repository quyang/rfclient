package mobile.freshtime.com.freshtime.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.WindowManager;

import mobile.freshtime.com.freshtime.R;

/**
 * Created by Administrator on 2017/1/13.
 */

public class ProgressDialogUtils {

    public static Dialog getProgressDialog(Activity context){

        Dialog  mMMDialog = new Dialog(context, R.style.MyDialog);
        View view = context.getLayoutInflater().inflate(R.layout.loading, null);

        mMMDialog.setCancelable(false);
        mMMDialog.setContentView(view);

        //设置弹窗宽高
        mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(context).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

        return mMMDialog;
    }
}
