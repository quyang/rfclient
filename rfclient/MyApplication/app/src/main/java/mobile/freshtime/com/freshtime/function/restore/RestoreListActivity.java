package mobile.freshtime.com.freshtime.function.restore;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.delivery.ResponseUserTask;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.function.restore.adapter.RestoreListActivityBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * Created by orchid on 16/9/19.
 * 归还页
 */
public class RestoreListActivity extends BaseActivity implements View.OnClickListener,
        Response.ErrorListener, Response.Listener<ResponseUserTask> {

    private static final int ITEM = 1;

    private EditText mDeliverInput;

    private View mInfoRoot;
    private TextView mDeliverman;
    private TextView mProceedingTask;
    private TextView mAgencyId;
    private TextView mAreaId;

    private TextView mDone;
    private TextView mItem;

    private ResponseUserTask mResponseUserTask;
    private ArrayList<SkuModel> mSkuModels;
    private TextView mGet;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restore_list);
        EventBus.getDefault().register(this);

        mDeliverInput = (EditText) findViewById(R.id.item_input);

        mInfoRoot = findViewById(R.id.task_root);
        mInfoRoot.setVisibility(View.INVISIBLE);

        mGet = (TextView) findViewById(R.id.get);
        mGet.setOnClickListener(this);
        mGet.setEnabled(true);
        mGet.setClickable(true);

        mDeliverman = (TextView) mInfoRoot.findViewById(R.id.delivery_name);
        mProceedingTask = (TextView) mInfoRoot.findViewById(R.id.task_count);
        mAgencyId = (TextView) mInfoRoot.findViewById(R.id.agency);
        mAreaId = (TextView) mInfoRoot.findViewById(R.id.area);

        findViewById(R.id.back).setOnClickListener(this);


        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);
        mDone.setEnabled(false);

        mItem = (TextView) findViewById(R.id.item);
        mItem.setOnClickListener(this);
        mItem.setEnabled(false);

        mSkuModels = new ArrayList<>();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void finished(RestoreListActivityBean bean) {
        finish();
    }

    @Override
    public void onScanResult(String result) {

//        getInfo(result);
    }

    //根据配送员id获取信息
    private void getInfo(String result) {
        result = result.trim();
        mDeliverInput.setText(result);
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(result, "46"));
            BaseRequest<ResponseUserTask> request =
                    new BaseRequest<>(RequestUtils.GET_DELIVER_TASK,
                            ResponseUserTask.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.done) {//确定
            checkRestore();
        } else if (id == R.id.last) {
            finish();
        } else if (id == R.id.item) {
            scanSku();
        }

        switch (id) {
            case R.id.back:
                finish();
                break;
            case R.id.get:
                String input = mDeliverInput.getText().toString().trim();
                if (StringUtils.isEmpty(input)) {
                    Toast.makeText(RestoreListActivity.this, "请先输入配送员id", Toast.LENGTH_LONG).show();
                } else {
                    getInfo(input);
                }

        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(ResponseUserTask response) {
        if (response != null && response.success
                && response.target != null
                && response.target.details != null) {
            this.mResponseUserTask = response;
            displayTask(response);
        } else {
            Toast.makeText(RestoreListActivity.this, "没有数据", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 展示用户的任务数据
     */
    private void displayTask(ResponseUserTask response) {
        mDone.setEnabled(true);
        mItem.setEnabled(true);
        mInfoRoot.setVisibility(View.VISIBLE);
        mDeliverman.setText("配送员：" + response.target.operator);
        mProceedingTask.setText("进行中任务:1个");
        mAgencyId.setText("仓库编码:" + response.target.agencyId);
        mAreaId.setText("区域编码:" + response.target.areaId);
    }

    private void checkRestore() {
        if (mResponseUserTask != null) {
            Intent intent = new Intent(this, RestoreCheckActivity.class);
            intent.putExtra("task", mResponseUserTask.target);
            intent.putParcelableArrayListExtra("box", mResponseUserTask.target.boxes);
            intent.putParcelableArrayListExtra("detail", mResponseUserTask.target.details);
            intent.putParcelableArrayListExtra("distribution", mResponseUserTask.target.distributions);
            intent.putParcelableArrayListExtra("sku", mSkuModels);
            startActivity(intent);
        }
    }

    //扫描sku
    private void scanSku() {
        Intent intent = new Intent(this, RestoreItemActivity.class);
        intent.putParcelableArrayListExtra("sku", mSkuModels);
        intent.putParcelableArrayListExtra("task", mResponseUserTask.target.details);
        startActivityForResult(intent, ITEM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == ITEM) {
                mSkuModels = data.getParcelableArrayListExtra("sku");
            }
        }
    }
}
