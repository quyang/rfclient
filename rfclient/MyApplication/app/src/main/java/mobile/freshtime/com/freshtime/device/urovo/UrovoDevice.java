package mobile.freshtime.com.freshtime.device.urovo;

import android.content.IntentFilter;

import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.device.ScanDevice;

/**
 * Created by orkid on 2016/9/13.
 */
public class UrovoDevice extends ScanDevice {

    private BaseActivity mActivity;

    public final static String SCAN_ACTION = "urovo.rcv.message";//扫描结束action
    private UrovoBroadcastReceiver mScanReceiver;

    public UrovoDevice(BaseActivity activity) {
        if (activity == null)
            throw new NullPointerException("can't init device");
        this.mActivity = activity;
        mScanReceiver = new UrovoBroadcastReceiver(mActivity);
    }

    @Override
    public void registerScanDevice() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(SCAN_ACTION);
        mActivity.registerReceiver(mScanReceiver, filter);
    }

    @Override
    public void unregisterScanDevice() {
        try {
            mActivity.unregisterReceiver(mScanReceiver);
        } catch (Exception e) {
        }
    }

    @Override
    public void startScan() {

    }

    @Override
    public void stopScan() {

    }
}
