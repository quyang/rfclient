package mobile.freshtime.com.freshtime.activity.adapter;

/**
 * Created by Administrator on 2016/12/1.
 */
public class RfNewVersion {

    /**
     * des : 据说很好用
     * versionCode : 1
     * versionName : 2.0
     * url : http://192.168.18.132:8080/quyang.apk
     */

    private String des;
    private String versionCode;
    private String versionName;
    private String url;

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
