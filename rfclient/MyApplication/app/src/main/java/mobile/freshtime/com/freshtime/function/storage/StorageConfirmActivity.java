package mobile.freshtime.com.freshtime.function.storage;

import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.Contants;
import mobile.freshtime.com.freshtime.FreshTimeApplication;
import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.utils.ProgressDialogUtils;
import mobile.freshtime.com.freshtime.utils.SPUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.utils.UiUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.BaseResponse;
import mobile.freshtime.com.freshtime.common.view.expandable.ExpandableLayoutListView;
import mobile.freshtime.com.freshtime.function.storage.model.InExpandableAdapter;
import mobile.freshtime.com.freshtime.function.storage.model.InSkuModel;
import mobile.freshtime.com.freshtime.function.storage.printer.BtConfigActivity;
import mobile.freshtime.com.freshtime.function.storage.printer.JQPrinter;
import mobile.freshtime.com.freshtime.function.storage.printer.Port;
import mobile.freshtime.com.freshtime.function.storage.printer.enforcement_bill.ebMainActivity;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.utils.DateUtils;

/**
 * Created by orchid on 16/9/18.
 */
public class StorageConfirmActivity extends BaseActivity implements View.OnClickListener,
        Response.ErrorListener, Response.Listener<BaseResponse> {

    private ArrayList<InSkuModel> mSkuModels;

    private int mReason;

    private String mCount;

    private ViewGroup mTopView;

    private TextView mTime;

    private TextView mPrint;

    private Dialog mMMDialog;
    private Dialog mProgressDialog;

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        unregisterReceiver(mReceiver);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        mPrint = (TextView) findViewById(R.id.print);
        mPrint.setText("打印");
        mPrint.setVisibility(View.VISIBLE);
        mPrint.setOnClickListener(this);

        //初始化打印机
        init();//  第一步

        mTopView = (ViewGroup) findViewById(R.id.top_root);
        TextView operaer = (TextView) mTopView.findViewById(R.id.operator);
        operaer.setText("操作人:" + SPUtils.getStringValue(this, Contants.SP_FILE_NAME, Contants.OPERATER, null));
        mTime = (TextView) mTopView.findViewById(R.id.time);
        mTime.setText("操作时间:" + DateUtils.getFormedTimeWitheData(System.currentTimeMillis() + ""));


        findViewById(R.id.back).setOnClickListener(this);
        findViewById(R.id.done).setOnClickListener(this);
        TextView title = (TextView) findViewById(R.id.title);
        title.setText("入库确认");

        TextView print = (TextView) findViewById(R.id.print);
        print.setOnClickListener(this);

        //-----------------------------------------------------------------------------

        Bundle bundle = getIntent().getExtras();
        mReason = bundle.getInt("reason", 0);
        mSkuModels = bundle.getParcelableArrayList("units");

        InExpandableAdapter adapter = new InExpandableAdapter(this, mSkuModels, mCount);
        ExpandableLayoutListView listView = (ExpandableLayoutListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new MyOnItemClickListener());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.back) {
            finish();
        } else if (id == R.id.done) {

            mMMDialog = new Dialog(StorageConfirmActivity.this, R.style.MyDialog);
            View view = getLayoutInflater().inflate(R.layout.cancel, null);
            TextView name = (TextView) view.findViewById(R.id.name);
            name.setText("确定要提交吗?");
            Button mNo = (Button) view.findViewById(R.id.no);
            Button mYes = (Button) view.findViewById(R.id.yes);

            mMMDialog.setCancelable(false);
            mMMDialog.setContentView(view);

            //设置弹窗宽高
            mMMDialog.getWindow().setLayout(UiUtils.getScreenHeight(StorageConfirmActivity.this).get(0) - 100, WindowManager.LayoutParams.WRAP_CONTENT);

            mMMDialog.show();

            mYes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (mSkuModels != null) {

                        mMMDialog.dismiss();

                        mProgressDialog = ProgressDialogUtils.getProgressDialog(StorageConfirmActivity.this);
                        mProgressDialog.show();

                        submit();
                    }
                }
            });

            mNo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mMMDialog.dismiss();
                }
            });


        } else if (id == R.id.print) {
            //打印
            selectPrinter();
        }
    }

    //选择打印机
    private void selectPrinter() {
        //选择打印机
        bt_button_click();
    }


    //打印
    private void print() {
    }

    private final static int REQUEST_BT_ENABLE = 0;
    private final static int REQUEST_BT_ADDR = 1;

    private boolean mBtOpenSilent = true;
    private BluetoothAdapter btAdapter = null;
    private JQPrinter printer = null;

    private FreshTimeApplication mApp = null;

    // 行政执法文书
//    private Button mButtonLawEnforcementBill = null;


    private long mLastTime = 0;

    private void init() {
        mApp = (FreshTimeApplication) getApplication(); // tip:DemoApplication需要在AndroidManifest.xml的application中注册
        // android:name="com.jq.ui.DemoApplication"

        //打印
        TextView print = (TextView) findViewById(R.id.print);
        print.setOnClickListener(this);


        // 获取蓝牙适配器
        btAdapter = BluetoothAdapter.getDefaultAdapter();
        if (btAdapter == null) {
            Toast.makeText(this, "本机没有找到蓝牙硬件或驱动！", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        mApp.btAdapter = btAdapter;

        // 如果本地蓝牙没有开启，则开启
        // 以下操作需要在AndroidManifest.xml中注册权限android.permission.BLUETOOTH
        // ；android.permission.BLUETOOTH_ADMIN
        if (!btAdapter.isEnabled()) {
            Toast.makeText(this, "正在开启蓝牙", Toast.LENGTH_SHORT).show();
            if (!mBtOpenSilent) {
                // 我们通过startActivityForResult()方法发起的Intent将会在onActivityResult()回调方法中获取用户的选择，比如用户单击了Yes开启，
                // 那么将会收到RESULT_OK的结果，
                // 如果RESULT_CANCELED则代表用户不愿意开启蓝牙
                Intent mIntent = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(mIntent, REQUEST_BT_ENABLE);
            } else { // 用enable()方法来开启，无需询问用户(无声息的开启蓝牙设备),这时就需要用到android.permission.BLUETOOTH_ADMIN权限。
                btAdapter.enable();
//                Toast.makeText(this, "本地蓝牙已打开", Toast.LENGTH_SHORT).show();
                mPrint.setVisibility(View.VISIBLE);
            }
        } else {
//            Toast.makeText(this, "本地蓝牙已打开", Toast.LENGTH_SHORT).show();
            mPrint.setVisibility(View.VISIBLE);
        }
    }

    //第二部
    public void bt_button_click() {
        if (btAdapter == null)
            return;
//        mButtonLawEnforcementBill.setVisibility(Button.INVISIBLE);

        //打开新的Activity
        Intent myIntent = new Intent(StorageConfirmActivity.this, BtConfigActivity.class);
        startActivityForResult(myIntent, REQUEST_BT_ADDR);
    }


    //第三部
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_BT_ENABLE) {
            if (resultCode == RESULT_OK) {
                Toast.makeText(this, "蓝牙已打开", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "不允许蓝牙开启", Toast.LENGTH_SHORT).show();
                exit();
                return;
            }
        } else if (requestCode == REQUEST_BT_ADDR) {
            if (resultCode == Activity.RESULT_OK) {
                String btDeviceString = data
                        .getStringExtra(BtConfigActivity.EXTRA_BLUETOOTH_DEVICE_ADDRESS);
                if (btDeviceString != null) {
                    if (btAdapter.isDiscovering())
                        btAdapter.cancelDiscovery();

                    if (printer != null) {
                        printer.close();
                    }

                    printer = new JQPrinter(btAdapter, btDeviceString);

                    if (!printer.open(mobile.freshtime.com.freshtime.function.storage.printer.JQPrinter.PRINTER_TYPE.ULT113x)) {
                        Toast.makeText(this, "打印机连接失败,请重试", Toast.LENGTH_SHORT)
                                .show();
                        return;
                    }

                    if (!printer.wakeUp())
                        return;

                    mApp.printer1 = printer;

                    Toast.makeText(this, " 找到外部蓝牙设备",
                            Toast.LENGTH_LONG).show();

                    IntentFilter filter = new IntentFilter();
                    filter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);// 蓝牙断开
                    registerReceiver(mReceiver, filter);


                    preStart();
                }
            } else {
                Toast.makeText(StorageConfirmActivity.this, "请重试", Toast.LENGTH_LONG).show();
            }
        }
    }

    //第四部 注册服务
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(android.content.Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)) {
                if (printer != null) {
                    if (printer.isOpen)
                        printer.close();
                }
                Toast.makeText(context, "蓝牙连接已断开", Toast.LENGTH_LONG).show();
            }
        }

        ;
    };


    //第五步
    public void preStart() {

        if (btAdapter.isDiscovering())
            btAdapter.cancelDiscovery();

        if (printer.getPortState() != Port.PORT_STATE.PORT_OPEND) {
//            mButtonBtScan.setText("选择打印机");
//            mButtonLawEnforcementBill.setVisibility(Button.INVISIBLE);
            Toast.makeText(this, "失败:" + printer.getPortState(),
                    Toast.LENGTH_SHORT).show();
            return;
        }


        Intent myIntent = new Intent(StorageConfirmActivity.this, ebMainActivity.class);
        myIntent.putParcelableArrayListExtra("units", mSkuModels);
        startActivity(myIntent);
    }


    private void exit() {
        if (printer != null) {
            if (!printer.close())
                Toast.makeText(this, "打印机关闭失败", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "打印机关闭成功", Toast.LENGTH_SHORT).show();
            printer = null;
        }
        if (btAdapter != null) {
            if (btAdapter.isEnabled()) {
                btAdapter.disable();
                Toast.makeText(this, "关闭蓝牙成功", Toast.LENGTH_LONG).show();
            }
        }

        finish();
        System.exit(0); // 凡是非零都表示异常退出!0表示正常退出!
    }


    @Override
    public void onScanResult(String result) {
    }

    /**
     * 提交数据
     */
    private void submit() {

        if (mSkuModels.size() == 0) {
            ToastUtils.showToast(getResources().getString(R.string.no_data));
            return;
        }

        JSONArray skus = new JSONArray();
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            JSONObject data = new JSONObject();
            data.put("opReason", mReason);

            for (int i = 0; i < mSkuModels.size(); i++) {
                InSkuModel model = mSkuModels.get(i);
                JSONObject sku = new JSONObject();
                sku.put("skuId", model.getSkuId());
                sku.put("number", model.getCount());
                sku.put("positionId", model.getPositionId());

                sku.put("saleUnit", model.skuUnit);//线下id

                skus.put(sku);
            }
            data.put("details", skus);
            param.put("data", data);

            System.out.println("StorageConfirmActivity.submit=" + param);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        BaseRequest<BaseResponse> mRequest = new BaseRequest<>(
                RequestUtils.STORAGE,
                BaseResponse.class,
                param,
                this, this);
        FreshRequest.getInstance().addToRequestQueue(mRequest, this.toString());
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(this, error.getMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResponse(BaseResponse response) {
        if (response != null && response.isSuccess()) {
            Toast.makeText(this, getResources().getString(R.string.storeage_success), Toast.LENGTH_SHORT).show();

            mProgressDialog.dismiss();

            Intent intent = new Intent(this, StorageActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        } else {
            mProgressDialog.dismiss();
            ToastUtils.showToast(response.getErrorMsg()+"");
        }
    }


    private class MyOnItemClickListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }
    }
}
