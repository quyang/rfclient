package mobile.freshtime.com.freshtime.function.handover;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.delivery.DeliverBox;
import mobile.freshtime.com.freshtime.common.model.delivery.ResponseUserTask;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * Created by orchid on 16/9/20.
 * 交接
 */
public class HandoverActivity extends BaseActivity implements Response.ErrorListener,
        Response.Listener<ResponseUserTask>, View.OnClickListener {

    private EditText mDeliverInput;

    private View mInfoRoot;
    private TextView mDeliverman;
    private TextView mProceedingTask;
    private TextView mAgencyId;
    private TextView mAreaId;

    private TextView mDone;

    private ResponseUserTask mResponseUserTask;
    private TextView mGetINfo;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handover);
        EventBus.getDefault().register(this);

//        mDeliverInput = (EditText) findViewById(R.id.item_input);

        mInfoRoot = findViewById(R.id.task_root);
        mInfoRoot.setVisibility(View.INVISIBLE);

        mGetINfo = (TextView) findViewById(R.id.get);
        mGetINfo.setOnClickListener(this);

         findViewById(R.id.last).setOnClickListener(this);

        mDeliverman = (TextView) mInfoRoot.findViewById(R.id.delivery_name);
        mProceedingTask = (TextView) mInfoRoot.findViewById(R.id.task_count);
        mDeliverInput = (EditText) findViewById(R.id.delivery_input).findViewById(R.id.item_input);

        //配送单
        mAgencyId = (TextView) mInfoRoot.findViewById(R.id.agency);

        //配送箱
        mAreaId = (TextView) mInfoRoot.findViewById(R.id.area);

        mDone = (TextView) findViewById(R.id.done);
        mDone.setOnClickListener(this);
        mDone.setClickable(false);

        findViewById(R.id.back).setOnClickListener(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void finished(Class<HandoverActivity> clazz) {
        finish();
    }

    @Override
    public void onScanResult(final String result) {

//
//        runOnUiThread(new Runnable() {
//            @Override
//            public void run() {
//            }
//        });

//        result = result.trim();
//        mDeliverInput.setText(result);


//
//        JSONObject param = new JSONObject();
//        try {
//            param.put("token", Login.getInstance().getToken());
//            param.put("data", Utils.filterInput(result, "46"));
//            BaseRequest<ResponseUserTask> request =
//                    new BaseRequest<>(RequestUtils.GET_DEL_TASK,
//                            ResponseUserTask.class,
//                            param,
//                            this, this);
//            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

    }

    @Override
    public void onErrorResponse(VolleyError error) {

    }

    @Override
    public void onResponse(ResponseUserTask response) {

        if (response != null && response.success
                && response.target != null
                && response.target.details != null) {
            this.mResponseUserTask = response;
            displayTask(response);
        } else {
            mDone.setClickable(true);
//            Toast.makeText(HandoverActivity.this, "没有数据", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * 展示用户的任务数据
     */
    private void displayTask(ResponseUserTask response) {
        mDone.setEnabled(true);
        mDone.setClickable(true);
        mInfoRoot.setVisibility(View.VISIBLE);
        mDeliverman.setText("配送员：" + response.target.operator);
        mProceedingTask.setText("进行中任务:1个" /* 剑夕说暂时写死,只会分配一个任务 */);
        mAgencyId.setText("送货单:" + response.target.id);
        ArrayList<DeliverBox> boxes = response.target.boxes;
        mAreaId.setText("配送箱:" + boxes.size());
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.done) {
            if (mResponseUserTask != null) {
                Intent intent = new Intent(this, HandoverCheckActivity.class);
                intent.putExtra("task", mResponseUserTask.target);
                intent.putParcelableArrayListExtra("box", mResponseUserTask.target.boxes);
                intent.putParcelableArrayListExtra("detail", mResponseUserTask.target.details);
                intent.putParcelableArrayListExtra("distribution", mResponseUserTask.target.distributions);
                startActivity(intent);
            }
        } else if (id == R.id.back) {
            finish();
        } else if (id == R.id.get) {
            getTaskInfo();
        } else if (id == R.id.last) {
            finish();
        }
    }


    private void getTaskInfo() {

        String id = mDeliverInput.getText().toString().trim();

        if ("".equals(id)) {
            Toast.makeText(HandoverActivity.this, "请先输入id", Toast.LENGTH_LONG).show();
            return;
        }

        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(id, "46"));
            BaseRequest<ResponseUserTask> request =
                    new BaseRequest<>(RequestUtils.GET_DEL_TASK,
                            ResponseUserTask.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
