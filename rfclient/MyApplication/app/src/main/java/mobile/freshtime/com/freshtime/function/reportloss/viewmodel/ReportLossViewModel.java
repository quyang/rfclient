package mobile.freshtime.com.freshtime.function.reportloss.viewmodel;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.utils.ToastUtils;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.viewmodel.ModelList;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.BaseViewModel;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.reportloss.model.LossPage;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuRequest;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;

/**
 * Created by orchid on 16/9/21.
 */
public class ReportLossViewModel extends BaseViewModel<LossPage> implements ModelList<LossPage>, Response.ErrorListener {

    private List<LossPage> mLossPages;
    private int mCurrentPageIndex;

    private PageListener mPageListener;
    private ViewModelWatcher mModelWatcher;

    private boolean mIsRequesting;
    private Request<ResponseSku> mSkuRequest;
    /**
     * sku请求回调
     */
    private Response.Listener<ResponseSku> mSkuModelListener = new Response.Listener<ResponseSku>() {
        @Override
        public void onResponse(ResponseSku response) {
            ReportLossViewModel.this.onResponse(response);
        }
    };

    public ReportLossViewModel(ViewModelWatcher watcher) {
        this.mLossPages = new ArrayList<>();
        this.mCurrentPageIndex = 0;
        this.mModelWatcher = watcher;

        LossPage page = new LossPage(null);
        mLossPages.add(mCurrentPageIndex, page);

    }

    public void addPageListener(PageListener pageListener) {
        this.mPageListener = pageListener;
        LossPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    /**
     * --------------------- BaseViewModel start ---------------------
     **/
    @Override
    public void onScanResult(String input) {
        LossPage page = getCurrentPage();
        LossPage.Phase phase = page.getPhase();
        if (phase == LossPage.Phase.START) {
            requestSku(input);
        } else if (phase == LossPage.Phase.SKU) {
            fillPosition(input);
        }

//        else if (phase == LossPage.Phase.POSITION) {
//            fillCount(input);
//        }
    }

    @Override
    public void onInterrupt() {
        mIsRequesting = false;
        if (mSkuRequest != null)
            FreshRequest.getInstance().cancel(mSkuRequest.toString());
    }

    @Override
    public boolean isRequesting() {
        return mIsRequesting;
    }

    @Override
    public void registerContext(BaseActivity activity) {

    }

    @Override
    public void unregisterContext() {
        this.mPageListener = null;
        this.mModelWatcher = null;
        this.mSkuRequest = null;
    }

    /**
     * --------------------- BaseViewModel end ---------------------
     **/

    /**
     * --------------------- ModelList start ---------------------
     **/
    @Override
    public boolean hasNext() {
        return mCurrentPageIndex != mLossPages.size() - 1;
    }

    @Override
    public boolean hasBefore() {
        return mCurrentPageIndex != 0;
    }

    @Override
    public boolean isReady() {
        for (int i = 0; i < mLossPages.size(); i++) {
            LossPage page = mLossPages.get(i);
            if (page.getPhase() != LossPage.Phase.POSITION)
                return false;
        }
        return true;
    }

    @Override
    public void navNext() {
        int len = mLossPages.size();


        LossPage currentPage = getCurrentPage();
        currentPage.getSkuModel().setCount(Integer.parseInt(count));


        if (mCurrentPageIndex++ == len - 1) {
            // 新增一页
            LossPage page = new LossPage(null);
            mLossPages.add(mCurrentPageIndex, page);
        }
        LossPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    @Override
    public void navBefore() {
        if (mCurrentPageIndex != 0) {
            mCurrentPageIndex--;
            LossPage page = getCurrentPage();
            mPageListener.onPageChanged(page);
        }
    }

    @Override
    public LossPage getCurrentPage() {
        return mLossPages.get(mCurrentPageIndex);
    }

    /**
     * --------------------- ModelList end ---------------------
     **/

    /**
     * 获取sku信息
     *
     * @param code
     */
    private void requestSku(String code) {
        if (!mIsRequesting) {
            mIsRequesting = true;
            mModelWatcher.onRequestStart();
            mSkuRequest = new SkuRequest(code, mSkuModelListener, this);
            FreshRequest.getInstance().addToRequestQueue(mSkuRequest, this.toString());
        }
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillPosition(String code) {
        LossPage page = getCurrentPage();
        page.getSkuModel().setPosition(code);
        page.advance();
        mModelWatcher.onPageUpdate(page);
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillCount(String code) {
//        if (TextUtils.isDigitsOnly(code))
        try {
            int count = Integer.parseInt(code);
            LossPage page = getCurrentPage();
            page.getSkuModel().setCount(count);
            page.advance();
            mModelWatcher.onPageUpdate(page);
        } catch (Exception e) {
            e.printStackTrace();
            mModelWatcher.onError("请输入数字");
        }
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        clearRequestState();
    }

    public void onResponse(final ResponseSku response) {
        clearRequestState();
        if (response.isSuccess()) {

            long skuId = response.getModule().getSkuId();
            String token = Login.getInstance().getToken();


            String s = RequestUtils.INTO_STORAGE + "request=" +
                    "{token:\"" + token + "\",data:\"" + skuId + "\"}";

            //获取库位的请求
            GetProtocol protocol = new GetProtocol(s);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    linkedData(response, json);
                }

                @Override
                public void onFail() {
                }
            });
        } else {
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }

    private void linkedData(ResponseSku response, String json) {

        Gson gson = new Gson();
        CuWeiBean bean = gson.fromJson(json, CuWeiBean.class);

        if (bean.success) {
            if (bean != null && bean.target.size() > 0) {
                String code = bean.target.get(0).pisitionVO.code;

                LossPage page = getCurrentPage();
                page.setSkuModel(response.getModule());
                page.getSkuModel().kuwei = "" + code;
                page.advance();
                mModelWatcher.onPageUpdate(page);
            } else {
                ToastUtils.showToast("没有更多数据");
            }
        } else {
            ToastUtils.showToast("" + bean.message);
        }


    }

    private void clearRequestState() {
        mIsRequesting = false;
        mModelWatcher.onRequestStop();
    }

    public List<LossPage> getLossPages() {
        return mLossPages;
    }

    private String count;

    public void setCount(String s) {
        this.count = s;
    }
}
