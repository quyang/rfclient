package mobile.freshtime.com.freshtime.activity.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mobile.freshtime.com.freshtime.R;

/**
 * Created by orchid on 16/10/13.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.FunctionViewHolder> {

    private String[] mFunctions;
    private ItemClick.IClick mIClick;

    public MenuAdapter(String[] functions, ItemClick.IClick itemClick) {
        this.mFunctions = functions;
        this.mIClick = itemClick;
    }

    @Override
    public FunctionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.listitem_function, parent, false);
        return new FunctionViewHolder(v);
    }

    @Override
    public void onBindViewHolder(FunctionViewHolder holder, int position) {
        TextView view = (TextView) holder.itemView.findViewById(R.id.function);
        view.setText(mFunctions[position]);
        view.setOnClickListener(new ItemClick(position, mIClick));
    }

    @Override
    public int getItemCount() {
        return mFunctions.length;
    }


    public static class FunctionViewHolder extends RecyclerView.ViewHolder {
        public FunctionViewHolder(View itemView) {
            super(itemView);
        }
    }
}
