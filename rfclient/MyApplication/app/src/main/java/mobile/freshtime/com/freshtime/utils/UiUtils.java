package mobile.freshtime.com.freshtime.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.FreshTimeApplication;
import mobile.freshtime.com.freshtime.R;

/**
 * ui操作处理的工具类
 */
public class UiUtils {

    /**
     * 获取宽高 集合的第一个是屏幕宽,第二个是屏幕高
     *
     * @param activity
     * @return list
     */
    public static List<Integer> getScreenHeight(Activity activity) {
        DisplayMetrics metrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int widthPixels = metrics.widthPixels;
        int heightPixels = metrics.heightPixels;
        List<Integer> list = new ArrayList<>();
        list.add(widthPixels);
        list.add(heightPixels);

        return list;
    }

    public static Object getBean(String json, Class clazz) {
        Gson gson = new Gson();
        return gson.fromJson(json, clazz);
    }

    public static View inflate(int resId) {
        return View.inflate(UiUtils.getContext(), resId, null);
    }

    public static Context getContext() {
        return FreshTimeApplication.context;
    }

    // dp--px
    public static int getDp2px(int dp) {
        float density = getContext().getResources().getDisplayMetrics().density;// 获取屏幕密度
        return (int) (dp * density + 0.5);
    }

    // px--dp
    public static int getPxToDp(int px) {
        float density = getContext().getResources().getDisplayMetrics().density;// 获取屏幕密度
        return (int) (px / density + 0.5);
    }

    public static boolean isEmpty(String json) {
        if (StringUtils.isEmpty(json)) {
            Toast.makeText(getContext(), getContext().getResources().getString(R.string.no_more_msg), Toast.LENGTH_SHORT).show();
            return true;
        } else {
            return false;
        }
    }

}
