package mobile.freshtime.com.freshtime.network;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by orchid on 16/9/15.
 */
public class TesterRunnable implements Runnable {

    private static String serverUrl = "139.196.92.240";
    private static String url = serverUrl;

    @Override
    public void run() {
        try {
            String param = "data=" + URLEncoder.encode("{\"sign\":\"603f6220c45ccd2c3f190d3d118da2cf\",\"sysTime\":1475151573903,\"equipmentId\":\"hardcode\",\"userId\":\"5\"}",
                    "UTF-8");
            String urlPath = "http://" + url + "/purchaseadmin/user/login";


            URL url = new URL(urlPath);
            HttpURLConnection mConn = (HttpURLConnection) url.openConnection();
            mConn.setDoOutput(true);
            mConn.setDoInput(true);
            mConn.setRequestMethod("POST");
            mConn.setRequestProperty("Connection", "Keep-Alive");
            mConn.setConnectTimeout(3000);
            mConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            OutputStream os = null;
            try {
                os = mConn.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                writer.write(param);
                writer.flush();
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (os != null) {
                    os.close();
                }
            }
            mConn.connect();

            int resultCode = mConn.getResponseCode();
            if (HttpURLConnection.HTTP_OK == resultCode) {
                StringBuffer sb = new StringBuffer();
                String readLine = new String();
                BufferedReader responseReader = new BufferedReader(new InputStreamReader(mConn.getInputStream(), "UTF-8"));
                while ((readLine = responseReader.readLine()) != null) {
                    sb.append(readLine).append("\n");
                }
                responseReader.close();
                System.out.println(sb.toString());
            } else {
                System.out.println(resultCode);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
