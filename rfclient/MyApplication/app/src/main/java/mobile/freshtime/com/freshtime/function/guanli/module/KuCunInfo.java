package mobile.freshtime.com.freshtime.function.guanli.module;

/**
 * Created by Administrator on 2016/12/4.
 */
public class KuCunInfo {

    /**
     * message : null
     * return_code : 0
     * success : true
     * target : {"agencyId":1,"areaId":null,"areaType":null,"dr":null,"guardBit":"1","id":null,"maySale":true,"number":9,"numberReal":9000,"numberRealTotal":10000,"numberShow":"9","numberShowTotal":"10","numberTotal":10,"positionCode":null,"positionId":null,"saleUnit":null,"saleUnitId":null,"saleUnitType":null,"skuId":11021,"skuName":null,"skuPlu":null,"slottingId":null,"slottingType":null,"spec":null,"specUnit":null,"specUnitId":null,"ts":null}
     */

    private Object message;
    private int return_code;
    private boolean success;
    /**
     * agencyId : 1
     * areaId : null
     * areaType : null
     * dr : null
     * guardBit : 1
     * id : null
     * maySale : true
     * number : 9
     * numberReal : 9000
     * numberRealTotal : 10000
     * numberShow : 9
     * numberShowTotal : 10
     * numberTotal : 10
     * positionCode : null
     * positionId : null
     * saleUnit : null
     * saleUnitId : null
     * saleUnitType : null
     * skuId : 11021
     * skuName : null
     * skuPlu : null
     * slottingId : null
     * slottingType : null
     * spec : null
     * specUnit : null
     * specUnitId : null
     * ts : null
     */

    private TargetBean target;

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public int getReturn_code() {
        return return_code;
    }

    public void setReturn_code(int return_code) {
        this.return_code = return_code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public TargetBean getTarget() {
        return target;
    }

    public void setTarget(TargetBean target) {
        this.target = target;
    }

    public static class TargetBean {
        private int agencyId;
        private Object areaId;
        private Object areaType;
        private Object dr;
        private String guardBit;//安全库存
        private Object id;
        private boolean maySale;
        private int number;
        private int numberReal;
        private int numberRealTotal;
        private String numberShow;
        private String numberShowTotal;
        private int numberTotal;
        private Object positionCode;
        private Object positionId;
        private Object saleUnit;
        private Object saleUnitId;
        private Object saleUnitType;
        private int skuId;
        private Object skuName;
        private Object skuPlu;
        private Object slottingId;
        private Object slottingType;
        private Object spec;
        private Object specUnit;
        private Object specUnitId;
        private Object ts;

        public int getAgencyId() {
            return agencyId;
        }

        public void setAgencyId(int agencyId) {
            this.agencyId = agencyId;
        }

        public Object getAreaId() {
            return areaId;
        }

        public void setAreaId(Object areaId) {
            this.areaId = areaId;
        }

        public Object getAreaType() {
            return areaType;
        }

        public void setAreaType(Object areaType) {
            this.areaType = areaType;
        }

        public Object getDr() {
            return dr;
        }

        public void setDr(Object dr) {
            this.dr = dr;
        }

        public String getGuardBit() {
            return guardBit;
        }

        public void setGuardBit(String guardBit) {
            this.guardBit = guardBit;
        }

        public Object getId() {
            return id;
        }

        public void setId(Object id) {
            this.id = id;
        }

        public boolean isMaySale() {
            return maySale;
        }

        public void setMaySale(boolean maySale) {
            this.maySale = maySale;
        }

        public int getNumber() {
            return number;
        }

        public void setNumber(int number) {
            this.number = number;
        }

        public int getNumberReal() {
            return numberReal;
        }

        public void setNumberReal(int numberReal) {
            this.numberReal = numberReal;
        }

        public int getNumberRealTotal() {
            return numberRealTotal;
        }

        public void setNumberRealTotal(int numberRealTotal) {
            this.numberRealTotal = numberRealTotal;
        }

        public String getNumberShow() {
            return numberShow;
        }

        public void setNumberShow(String numberShow) {
            this.numberShow = numberShow;
        }

        public String getNumberShowTotal() {
            return numberShowTotal;
        }

        public void setNumberShowTotal(String numberShowTotal) {
            this.numberShowTotal = numberShowTotal;
        }

        public int getNumberTotal() {
            return numberTotal;
        }

        public void setNumberTotal(int numberTotal) {
            this.numberTotal = numberTotal;
        }

        public Object getPositionCode() {
            return positionCode;
        }

        public void setPositionCode(Object positionCode) {
            this.positionCode = positionCode;
        }

        public Object getPositionId() {
            return positionId;
        }

        public void setPositionId(Object positionId) {
            this.positionId = positionId;
        }

        public Object getSaleUnit() {
            return saleUnit;
        }

        public void setSaleUnit(Object saleUnit) {
            this.saleUnit = saleUnit;
        }

        public Object getSaleUnitId() {
            return saleUnitId;
        }

        public void setSaleUnitId(Object saleUnitId) {
            this.saleUnitId = saleUnitId;
        }

        public Object getSaleUnitType() {
            return saleUnitType;
        }

        public void setSaleUnitType(Object saleUnitType) {
            this.saleUnitType = saleUnitType;
        }

        public int getSkuId() {
            return skuId;
        }

        public void setSkuId(int skuId) {
            this.skuId = skuId;
        }

        public Object getSkuName() {
            return skuName;
        }

        public void setSkuName(Object skuName) {
            this.skuName = skuName;
        }

        public Object getSkuPlu() {
            return skuPlu;
        }

        public void setSkuPlu(Object skuPlu) {
            this.skuPlu = skuPlu;
        }

        public Object getSlottingId() {
            return slottingId;
        }

        public void setSlottingId(Object slottingId) {
            this.slottingId = slottingId;
        }

        public Object getSlottingType() {
            return slottingType;
        }

        public void setSlottingType(Object slottingType) {
            this.slottingType = slottingType;
        }

        public Object getSpec() {
            return spec;
        }

        public void setSpec(Object spec) {
            this.spec = spec;
        }

        public Object getSpecUnit() {
            return specUnit;
        }

        public void setSpecUnit(Object specUnit) {
            this.specUnit = specUnit;
        }

        public Object getSpecUnitId() {
            return specUnitId;
        }

        public void setSpecUnitId(Object specUnitId) {
            this.specUnitId = specUnitId;
        }

        public Object getTs() {
            return ts;
        }

        public void setTs(Object ts) {
            this.ts = ts;
        }
    }
}
