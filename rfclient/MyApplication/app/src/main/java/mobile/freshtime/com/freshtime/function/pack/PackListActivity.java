package mobile.freshtime.com.freshtime.function.pack;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;

/**
 * 配货台
 * Created by orchid on 16/10/12.
 */

public class PackListActivity extends BaseActivity implements Response.ErrorListener, Response.Listener<ResponseSku>, View.OnClickListener {

    private TextView mPeiSosngId;
    private TextView mBociDanHao;
    private TextView mTaskCount;
    private TextView mCompletedTask;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pack_list);


        //配送员id
        mPeiSosngId = (TextView) findViewById(R.id.delivery_input).findViewById(R.id.item_input);
        View pick_info_root = findViewById(R.id.pick_info_root);
        //波次单号
        mBociDanHao = (TextView) pick_info_root.findViewById(R.id.item_name);
        //任务数
        mTaskCount = (TextView) pick_info_root.findViewById(R.id.task_count);
        //已完成
        mCompletedTask = (TextView) pick_info_root.findViewById(R.id.pack_count);


        findViewById(R.id.back).setOnClickListener(this);
    }

    @Override
    public void onScanResult(String result) {

        mPeiSosngId.setText(result);

        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", result);
            BaseRequest<ResponseSku> request =
                    new BaseRequest<>(RequestUtils.PACK_TASK,
                            ResponseSku.class,
                            param,
                            this, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        Toast.makeText(PackListActivity.this, "" + error.getMessage(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onResponse(ResponseSku response) {

        if (response == null) {
            return;
        }

        if (response != null && response.isSuccess()) {


            SkuModel module = response.getModule();
            long skuId = module.getSkuId();
            long id = module.getId();
            String barcode = module.getBarcode();
            long catId = module.getCatId();
            String reason = module.getCheckReason();
            String checkUserName = module.getCheckUserName();
            int count = module.getCount();
            String title = module.getTitle();
            String subTitle = module.getSubTitle();

            System.out.println("PackListActivity.onResponse=skuId=" + skuId);
            System.out.println("PackListActivity.onResponse=Id=" + id);
            System.out.println("PackListActivity.onResponse=barcode=" + barcode);
            System.out.println("PackListActivity.onResponse=catId=" + catId);
            System.out.println("PackListActivity.onResponse=reason=" + reason);
            System.out.println("PackListActivity.onResponse=checkUserName=" + checkUserName);
            System.out.println("PackListActivity.onResponse=count=" + count);
            System.out.println("PackListActivity.onResponse=title=" + title);
            System.out.println("PackListActivity.onResponse=subTitle=" + subTitle);

//            mTaskCount.setText(count+"");
//            mCompletedTask.setText("111");



        } else {
            System.out.println("PackListActivity.onResponse" + response.getErrorMsg());
            Toast.makeText(PackListActivity.this, response.getErrorMsg(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back:
                finish();
                break;
        }
    }
}
