package mobile.freshtime.com.freshtime.function.pick.model;

import android.os.Parcel;
import android.os.Parcelable;

import mobile.freshtime.com.freshtime.common.model.sku.SkuModel;

/**
 * Created by orchid on 16/10/6.
 */

public class Task implements Parcelable, Cloneable {

    public long agencyId;
    public long areaId;
    public long basketBarcode;
    public long basketId;
    public long dr;
    public long id;
    public String oos;
    public long orderNo;
    public long pickId;
    public long pickedNo;
    public String positionBarcode;
    public long positionId;
    public String skuBarcode;
    public long skuId;
    public String ts;

    @Override
    public String toString() {
        return "Task{" +
                "agencyId=" + agencyId +
                ", areaId=" + areaId +
                ", basketBarcode=" + basketBarcode +
                ", basketId=" + basketId +
                ", dr=" + dr +
                ", id=" + id +
                ", oos='" + oos + '\'' +
                ", orderNo=" + orderNo +
                ", pickId=" + pickId +
                ", pickedNo=" + pickedNo +
                ", positionBarcode='" + positionBarcode + '\'' +
                ", positionId=" + positionId +
                ", skuBarcode='" + skuBarcode + '\'' +
                ", skuId=" + skuId +
                ", ts='" + ts + '\'' +
                ", title='" + title + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", sku69Id=" + sku69Id +
                ", specification='" + specification + '\'' +
                '}';
    }

    /**
     * 一下三个参数从接口融合而来{@link SkuModel#title#subTitle#sku69Id}
     */
    public String title;
    public String subTitle;
    public long sku69Id;
    public String specification;

    public void merge(SkuModel model) {
        this.title = model.getTitle();
        this.subTitle = model.getSubTitle();
        this.sku69Id = model.getSku69Id();
        this.specification = model.getUnStandardSpecification();
    }

    /**
     * for test
     * @return
     * @throws CloneNotSupportedException
     */
    public Task clone() throws CloneNotSupportedException {
        return (Task) super.clone();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(agencyId);
        dest.writeLong(areaId);
        dest.writeLong(basketBarcode);
        dest.writeLong(basketId);
        dest.writeLong(dr);
        dest.writeLong(id);
        dest.writeString(oos);
        dest.writeLong(orderNo);
        dest.writeLong(pickId);
        dest.writeLong(pickedNo);
        dest.writeString(positionBarcode);
        dest.writeLong(positionId);
        dest.writeString(skuBarcode);
        dest.writeLong(skuId);
        dest.writeString(ts);

        dest.writeString(title);
        dest.writeString(subTitle);
        dest.writeLong(sku69Id);
        dest.writeString(specification);
    }

    public static final Parcelable.Creator<Task> CREATOR =
            new Parcelable.Creator<Task>() {

                @Override
                public Task createFromParcel(Parcel source) {
                    Task model = new Task();
                    model.agencyId = source.readLong();
                    model.areaId = source.readLong();
                    model.basketBarcode = source.readLong();
                    model.basketId = source.readLong();
                    model.dr = source.readLong();
                    model.id = source.readLong();
                    model.oos = source.readString();
                    model.orderNo = source.readLong();
                    model.pickId = source.readLong();
                    model.pickedNo = source.readLong();
                    model.positionBarcode = source.readString();
                    model.positionId = source.readLong();
                    model.skuBarcode = source.readString();
                    model.skuId = source.readLong();
                    model.ts = source.readString();

                    model.title = source.readString();
                    model.subTitle = source.readString();
                    model.sku69Id = source.readLong();
                    model.specification = source.readString();
                    return model;
                }

                @Override
                public Task[] newArray(int size) {
                    return new Task[size];
                }
            };
}
