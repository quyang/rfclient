package mobile.freshtime.com.freshtime.common.model.viewmodel;

/**
 * Created by orchid on 16/9/14.
 */
public interface ModelList<Page> {

    boolean hasNext();

    boolean hasBefore();

    boolean isReady();

    void navNext();

    void navBefore();

    Page getCurrentPage();
}
