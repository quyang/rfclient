package mobile.freshtime.com.freshtime.function.outgoing.viewmodel;

import android.view.View;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import mobile.freshtime.com.freshtime.R;
import mobile.freshtime.com.freshtime.activity.BaseActivity;
import mobile.freshtime.com.freshtime.common.Utils;
import mobile.freshtime.com.freshtime.common.model.position.ResponsePosition;
import mobile.freshtime.com.freshtime.common.model.sku.ResponseSku;
import mobile.freshtime.com.freshtime.common.model.viewmodel.ModelList;
import mobile.freshtime.com.freshtime.common.model.viewmodel.PageListener;
import mobile.freshtime.com.freshtime.common.viewmodel.BaseViewModel;
import mobile.freshtime.com.freshtime.common.viewmodel.ViewModelWatcher;
import mobile.freshtime.com.freshtime.function.guanli.module.KuCunInfo;
import mobile.freshtime.com.freshtime.function.outgoing.OutgoActivity;
import mobile.freshtime.com.freshtime.function.outgoing.model.OutSkuRequest;
import mobile.freshtime.com.freshtime.function.outgoing.model.OutgoPage;
import mobile.freshtime.com.freshtime.function.outgoing.model.ResponseSkuOut;
import mobile.freshtime.com.freshtime.function.storage.StorageBean;
import mobile.freshtime.com.freshtime.function.storage.model.CuWeiBean;
import mobile.freshtime.com.freshtime.login.Login;
import mobile.freshtime.com.freshtime.network.RequestUtils;
import mobile.freshtime.com.freshtime.network.request.BaseRequest;
import mobile.freshtime.com.freshtime.network.request.FreshRequest;
import mobile.freshtime.com.freshtime.network.request.SkuRequest;
import mobile.freshtime.com.freshtime.protocol.BaseProtocal;
import mobile.freshtime.com.freshtime.protocol.GetProtocol;
import mobile.freshtime.com.freshtime.utils.StringUtils;
import mobile.freshtime.com.freshtime.utils.ToastUtils;

/**
 * Created by orchid on 16/9/22.
 */
public class OutgoViewModel extends BaseViewModel<OutgoPage> implements ModelList<OutgoPage>,
        Response.ErrorListener {

    private List<OutgoPage> mOutgoPages;
    private int mCurrentPageIndex;

    private PageListener mPageListener;
    private ViewModelWatcher mModelWatcher;

    private OutgoActivity mOutgoActivity;

    private boolean mIsRequesting;

    private Request<ResponseSkuOut> mSkuRequest;

    /**
     * sku请求回调
     */
    private Response.Listener<ResponseSkuOut> mSkuModelListener = new Response.Listener<ResponseSkuOut>() {

        @Override
        public void onResponse(ResponseSkuOut response) {
            OutgoViewModel.this.onResponse(response);
        }
    };

    /**
     * sku请求回调2，负责添加数量
     */
    private Response.Listener<ResponseSku> mSkuCountListener = new Response.Listener<ResponseSku>() {
        @Override
        public void onResponse(ResponseSku response) {
            OutgoViewModel.this.onCountResponse(response);
        }
    };

    private Response.Listener<ResponsePosition> mPositionListener =
            new Response.Listener<ResponsePosition>() {
                @Override
                public void onResponse(ResponsePosition response) {
                    if (response != null && response.isSuccess()) {
                        OutgoPage page = getCurrentPage();
                        page.getSkuModel().setPositionId(response.target.id);
                        page.advance();
                        mModelWatcher.onPageUpdate(page);
                    } else {
                        String msg = response == null ? "未获取到仓位信息" : response.getErrorMsg();
                        mModelWatcher.onError(msg);
                    }
                }
            };


    public OutgoViewModel(ViewModelWatcher watcher) {
        this.mOutgoPages = new ArrayList<>();
        this.mCurrentPageIndex = 0;
        this.mModelWatcher = watcher;
        mOutgoActivity = (OutgoActivity) mModelWatcher;

        OutgoPage page = new OutgoPage(null);
        mOutgoPages.add(mCurrentPageIndex, page);

    }

    public void addPageListener(PageListener pageListener) {
        this.mPageListener = pageListener;
        OutgoPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    /**
     * --------------------- BaseViewModel start ---------------------
     **/
    @Override
    public void onScanResult(String input) {
        OutgoPage page = getCurrentPage();
        OutgoPage.Phase phase = page.getPhase();
        if (phase == OutgoPage.Phase.START) {
            requestSku(input);
        } else if (phase == OutgoPage.Phase.SKU) {
//            fillPosition(input);
            if (mOutgoActivity.mNewInput.hasFocus()) {
                fillCuKei(input);
            }
        } else if (phase == OutgoPage.Phase.POSITION) {
            fillCount(input);
        }
    }

    //获取库位和pid
    private void fillCuKei(String data) {

        JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", data);

            String url = RequestUtils.POSITION_BARCODE + "?request=" + object.toString();
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast(mOutgoActivity.getResources().getString(R.string.no_more_msg));
                        return;
                    }

                    Gson gson = new Gson();
                    StorageBean bean = gson.fromJson(json, StorageBean.class);
                    if (bean != null && bean.isSuccess() && bean.getTarget() != null && bean.getTarget() != null) {

                        mOutgoActivity.mBangDing.setEnabled(true);

                        mOutgoActivity.mSkuCode.setText(bean.getTarget().getCode());
                        mOutgoActivity.mNewInput.setText(bean.getTarget().getBarcode());

                        //请求成功后才能绑定
                        mOutgoActivity.mBangDing.setOnClickListener(new MyOnClickListener(bean));

                    } else {
                        ToastUtils.showToast("" + bean.getMessage());
                    }
                }

                @Override
                public void onFail() {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    //邦定
    private class MyOnClickListener implements View.OnClickListener {

        private StorageBean bean;

        public MyOnClickListener(StorageBean bean) {
            this.bean = bean;
        }

        @Override
        public void onClick(View v) {

            String code = mOutgoActivity.mNewInput.getText().toString().trim();//code
            if (StringUtils.isEmpty(code)) {
                ToastUtils.showToast("请先输入目标库位");
                return;
            }

            String url = RequestUtils.ADD_CODE + "?request={token:'" +
                    Login.getInstance().getToken() + "',data:{skuId:" +
                    getCurrentPage().getSkuModel().getSkuId() + ",positionId:" + bean.getTarget().getId() + "}}";

            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    if (!StringUtils.isEmpty(json)) {
                        try {
                            JSONObject object = new JSONObject(json);

                            if (object.getBoolean("success")) {
                                ToastUtils.showToast("绑定成功");
                                mOutgoActivity.mPostionCount.requestFocus();

                                OutgoPage currentPage = getCurrentPage();
                                currentPage.getSkuModel().setCuwei(bean.getTarget().getCode());
                                currentPage.getSkuModel().setPositionId(bean.getTarget().getId());
                            } else {
                                ToastUtils.showToast("" + object.getString("message"));
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        ToastUtils.showToast(mOutgoActivity.getResources().getString(R.string.no_more_msg));
                    }
                }

                @Override
                public void onFail() {
                }
            });
        }
    }

    @Override
    public void onInterrupt() {
        mIsRequesting = false;
        if (mSkuRequest != null)
            FreshRequest.getInstance().cancel(mSkuRequest.toString());
    }

    @Override
    public boolean isRequesting() {
        return mIsRequesting;
    }

    @Override
    public void registerContext(BaseActivity activity) {

    }

    @Override
    public void unregisterContext() {
        this.mPageListener = null;
        this.mModelWatcher = null;
        this.mSkuRequest = null;
    }
    /**
     * --------------------- BaseViewModel end ---------------------
     **/

    /**
     * --------------------- ModelList start ---------------------
     **/

    @Override
    public boolean hasBefore() {
        return mCurrentPageIndex != 0;
    }

    @Override
    public boolean hasNext() {
        return mCurrentPageIndex != mOutgoPages.size() - 1;
    }

    @Override
    public boolean isReady() {
        for (int i = 0; i < mOutgoPages.size(); i++) {
            OutgoPage page = mOutgoPages.get(i);
            if (page.getPhase() != OutgoPage.Phase.DONE || page.getPhase() != OutgoPage.Phase.POSITION)
                return false;
        }
        return true;
    }

    @Override
    public void navNext() {
        int len = mOutgoPages.size();
        OutgoPage currentPage = getCurrentPage();
        if (currentPage.getSkuModel().getPositionId() <= 0) {
            ToastUtils.showToast(mOutgoActivity.getResources().getString(R.string.has_no_kuwei));
            return;
        }

        currentPage.getSkuModel().setCount(count);


        if (mCurrentPageIndex++ == len - 1) {
            // 新增一页
            OutgoPage page = new OutgoPage(null);
            mOutgoPages.add(mCurrentPageIndex, page);
        }
        OutgoPage page = getCurrentPage();
        mPageListener.onPageChanged(page);
    }

    @Override
    public void navBefore() {
        if (mCurrentPageIndex != 0) {
            mCurrentPageIndex--;
            OutgoPage page = getCurrentPage();
            mPageListener.onPageChanged(page);
        }
    }

    @Override
    public OutgoPage getCurrentPage() {
        return mOutgoPages.get(mCurrentPageIndex);
    }
    /**
     * --------------------- ModelList end ---------------------
     **/

    /**
     * 获取sku信息
     *
     * @param code
     */
    private void requestSku(String code) {
        if (!mIsRequesting) {
            mIsRequesting = true;
            mSkuRequest = new OutSkuRequest(code, mSkuModelListener, this);
            FreshRequest.getInstance().addToRequestQueue(mSkuRequest, this.toString());
        }
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillPosition(String code) {
        OutgoPage page = getCurrentPage();
        page.getSkuModel().setPosition(code);
        JSONObject param = new JSONObject();
        try {
            param.put("token", Login.getInstance().getToken());
            param.put("data", Utils.filterInput(code, "1"));
            BaseRequest<ResponsePosition> request =
                    new BaseRequest<>(RequestUtils.POSITION_BARCODE,
                            ResponsePosition.class,
                            param,
                            mPositionListener, this);
            FreshRequest.getInstance().addToRequestQueue(request, this.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * 填充库位信息
     *
     * @param code
     */
    private void fillCount(String code) {
        OutgoPage page = getCurrentPage();
        if (Utils.filterInput(code.equals(page.getSkuModel().getBarcode()), false)) {
            addItem(page, 1, code);
        } else if (Utils.isDigital(code)) {
            try {
                int count = Integer.parseInt(code);
                setItem(page, count);
            } catch (Exception e) {
                SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                        mSkuCountListener, this);
                FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
            }
        } else {
            SkuRequest skuRequest = new SkuRequest(Utils.filterInput(code, "10002"),
                    mSkuCountListener, this);
            FreshRequest.getInstance().addToRequestQueue(skuRequest, skuRequest.toString());
        }
    }

    private void addItem(OutgoPage page, int count, String code) {
//        int current = page.getSkuModel().getCount();
//        page.getSkuModel().setCount(count + current);

        page.getSkuModel().setCount(code);
//        page.getSkuModel().setCount(Integer.parseInt(code));
        mModelWatcher.onPageUpdate(page);
    }

    private void setItem(OutgoPage page, int count) {
        page.getSkuModel().setCount(StringUtils.getString(count));
        mModelWatcher.onPageUpdate(page);
    }

    @Override
    public void onErrorResponse(VolleyError error) {
        clearRequestState();
    }

    public void onResponse(final ResponseSkuOut response) {

        clearRequestState();

        if (response != null && response.isSuccess()) {

            //获取扫描到的条码
            OutgoActivity storageActivity = (OutgoActivity) mModelWatcher;

            long skuId = response.getModule().getSkuId();

            String tiaoma = storageActivity.getTiaoma();
            String token = Login.getInstance().getToken();


            String s = RequestUtils.INTO_STORAGE + "request=" +
                    "{token:\"" + token + "\",data:\"" + skuId + "\"}";

            //获取库位的请求
            GetProtocol protocol = new GetProtocol(s);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    linkedData(response, json);
                }

                @Override
                public void onFail() {
                }
            });
        } else {
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }


    private void getAllKuCun(String skuid) {
        final OutgoPage page = getCurrentPage();

        //GET_KUCUN
        final JSONObject object = new JSONObject();
        try {
            object.put("token", Login.getInstance().getToken());
            object.put("data", skuid);

            String url = RequestUtils.GET_KUCUN + "?request=" + object;
            GetProtocol protocol = new GetProtocol(url);
            protocol.get();
            protocol.setOnDataListener(new BaseProtocal.OnDataListener() {
                @Override
                public void onSuccess(String json) {
                    System.out.println("OutgoViewModel.onSuccess======" + json);

                    if (StringUtils.isEmpty(json)) {
                        ToastUtils.showToast("没有库存信息");
                    } else {
                        Gson gson = new Gson();
                        KuCunInfo info = gson.fromJson(json, KuCunInfo.class);
                        if (info.isSuccess()) {
                            page.getSkuModel().kucun = info.getTarget().getNumberShowTotal();
                            page.getSkuModel().setSafeKucun(info.getTarget().getGuardBit());
                        }
                    }

                    page.advance();
                    mModelWatcher.onPageUpdate(page);

                }

                @Override
                public void onFail() {
                    page.advance();
                    mModelWatcher.onPageUpdate(page);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void linkedData(ResponseSkuOut response, String jsonCode) {
        System.out.println("OutgoViewModel.onSuccess=3=" + jsonCode);

        OutgoPage page = getCurrentPage();
        page.setSkuModel(response.getModule());

        if (StringUtils.isEmpty(jsonCode)) {
            getAllKuCun(response.getModule().getSkuId() + "");
            return;
        }

        Gson gson = new Gson();
        CuWeiBean bean = gson.fromJson(jsonCode, CuWeiBean.class);

        if (bean.success) {

            if (bean != null && bean.target != null && bean.target.size() > 0) {

                //库位
                if (bean.target.get(0).pisitionVO != null) {
                    String code = bean.target.get(0).pisitionVO.code;
                    page.getSkuModel().kuwei = code;
                    page.getSkuModel().setPositionId(bean.target.get(0).positionId);
                } else {
                    page.getSkuModel().kuwei = "";

                }
            } else {
                page.getSkuModel().kuwei = "";
                ToastUtils.showToast(mOutgoActivity.getResources().getString(R.string.no_kuwei_info));
            }

        } else {
            ToastUtils.showToast("" + bean.message);
        }

        getAllKuCun(response.getModule().getSkuId() + "");

//        page.advance();
//        mModelWatcher.onPageUpdate(page);


    }

    public void onCountResponse(ResponseSku response) {
        clearRequestState();
        if (response != null && response.isSuccess()) {
            OutgoPage page = getCurrentPage();
            if (page.getSkuModel().getSkuId() == response.getModule().getSkuId()) {
                addItem(page, 1, "");
            } else {
                mModelWatcher.onError("请输入正确的商品条码或者数量");
            }
        } else {
            ToastUtils.showToast("" + response.getErrorMsg());
        }
    }

    private void clearRequestState() {
        mIsRequesting = false;
    }

    public List<OutgoPage> getOutgoPages() {
        return mOutgoPages;
    }


    private String count;

    public void setCount(String count) {
        this.count = count;
    }
}
