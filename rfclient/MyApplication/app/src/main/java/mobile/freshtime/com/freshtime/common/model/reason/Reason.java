package mobile.freshtime.com.freshtime.common.model.reason;

/**
 * Created by orkid on 2016/10/19.
 */
public class Reason {

    public long dr;
    public long id;
    public int opType;
    public String reasonName;
    public String ts;
}
