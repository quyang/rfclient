package mobile.freshtime.com.freshtime.function.requisition.model;

import java.util.List;

/**
 * Created by orchid on 16/10/12.
 */

public class RequisitionModel {

    public boolean firstPage;
    public boolean lastPage;
    public int nextPage;
    public int page;
    public int pageCount;
    public int previousPage;
    public int size;
    public int totalCount;

    public List<RequisitionDetailModel> items;
}
