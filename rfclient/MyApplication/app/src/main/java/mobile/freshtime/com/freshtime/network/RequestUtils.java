package mobile.freshtime.com.freshtime.network;

/**
 * Created by orkid on 2016/9/12.
 */
public final class RequestUtils {

    private static final String TEST_ROOT = "http://139.224.13.67:";

//    private static final String URL = "http://purchaseop.xianzaishi.com/";
    private static final String URL = "http://purchaseop.xianzaishi.net/purchaseadmin/";

    private static final String TMS_PORT = "7003/";

    private static final String WMS_PORT = "7004/";

    private static final String TEST_TMS = "http://tmsrf.xianzaishi.net/";

    private static final String TEST_WMS = "http://wmsrf.xianzaishi.net/";

    private static final String ONLINE_TMS = "http://tmsrf.xianzaishi.com/";

    private static final String ONLINE_WMS = "http://wmsrf.xianzaishi.com/";

//    private static final String TMS_URL = ONLINE_TMS;
    private static final String TMS_URL = TEST_TMS;

//    private static final String WMS_URL = ONLINE_WMS;
    private static final String WMS_URL = TEST_WMS;

    public static final String SKU = URL + "item/queryskuInfo";

    public static final String SKU_LIST = URL + "item/queryskubyidlist/";

    public static final String POSITION = WMS_URL + "getPositionBySku";

    public static final String GET_COUNT_IN_ONE_KUWEI = WMS_URL + "getInventoryByPositionAndSku";

    //更具墙位barcode获取库位信息
    public static final String POSITION_BARCODE = WMS_URL + "getPositionByBarcode";

    public static final String POSITION_UPDATE = WMS_URL + "submitAdjustDetail";

    public static final String LOSS = TMS_URL + "submitSpoilageDetail";

    public static final String OUTGO = WMS_URL + "submitOutgoingDetail";

    public static final String STORAGE = WMS_URL + "submitStorageDetail";

    //查询sku商品对应的所有酷我信息
    public static final String INTO_STORAGE = WMS_URL + "recommendPositionBySku?";

    public static final String TRANSFER = WMS_URL + "submitTransferDetail";

    //上架
    public static final String TRADE_SHELF = WMS_URL + "submitShelvingDetail";

    public static final String PRODUCE_SHELF = TMS_URL + "productionShelving";

    public static final String REQUISITION = WMS_URL + "queryRequisitions";

    public static final String REQUISITION_DETAIL = TMS_URL + "getRequisitionsDetail";

    public static final String SUBMIT_REQUISITION = TMS_URL + "submitRequisitionsDetail";

    //创建任务 盘点
    public static final String SUBMIT_STOCK = WMS_URL + "submitStocktakingDetail";

    //获取进行中的任务
    public static final String STOCK = WMS_URL + "getStocktakingInProgress";

    //提交任务到二盘(改变状态)
    public static final String TO_CHECK = WMS_URL + "submitStocktakingToCheck";

    //获取二盘任务
    public static final String GET_TWO_PAN = WMS_URL + "getStocktakingChecking";

    //提交二盘明细
    public static final String CHECK_STOCK = WMS_URL + "submitStocktakingCheckDetail";

    //提交任务到待审核
    public static final String AUDIT = WMS_URL + "submitStocktakingToAudit";

    //获取盘点任务的明细
    public static final String GET_STOCK = WMS_URL + "getStocktaking";

    //总库存
    public static final String GET_KUCUN = WMS_URL + "getInventoryBySku";

    public static final String BOOK = TMS_URL + "queryBookingDelivery";

    public static final String BOOK_DETAIL = TMS_URL + "getBookingDeliveryDetails";

    public static final String ORDER_DETAIL = TMS_URL + "submitOrderStorageDetail";

    public static final String INSPECTION_DETAIL = TMS_URL + "submitInspectionDetail";

    public static final String PICK_TASK = TMS_URL + "getPickedTask";

    public static final String PICK_WALL_POSITION = TMS_URL + "getPickingWallPositionByBarcode";

    public static final String SUBMIT_PICK = TMS_URL + "submitPickedDetail";

    public static final String CANCEL_PICK = TMS_URL + "cancelPickTask";

    public static final String GET_WAVE = TMS_URL + "getWaveVOByPickingBasketBarcode";

    public static final String PICK_WALL = TMS_URL + "assignPickingWallPosition";

    public static final String RECEIVE_PICK = TMS_URL + "recievePickingBasket";

    public static final String POSITION_PICK = TMS_URL + "getWaveVOByPickingWallPositionBarcode";

    public static final String WAVE_FINISH = TMS_URL + "waveFinished";

    public static final String PACK_TASK = TMS_URL + "getPackTask";

    public static final String SUBMIT_DELIVER = TMS_URL + "submitDeliver";

    public static final String DELIVER_TASK = TMS_URL + "getDeliverTask";

    public static final String GET_DELIVER_TASK = TMS_URL + "getUserDeliverTaskOngoing";

    public static final String GET_DEL_TASK = TMS_URL + "getUserDeliverTask";

    public static final String HANDOVER_DELIVER = TMS_URL + "handOverDeliver";

    public static final String RETURN_BOX = TMS_URL + "returnDeliverBox";

    public static final String LOGIN = URL + "employee/login";

    public static final String BATCH_DISTRI_BOX = TMS_URL + "batchGetDistributionBoxVOByBarcode";

    public static final String DISTRI_BOX = TMS_URL + "getDistributionBoxByBarcode";

    public static final String STORAGE_REASON = WMS_URL + "getInventoryOpReasonByOpType";

    //拣货框  获取正在拣货的拣货框
    public static final String PICK_BASKET = TMS_URL + "getPickingBasketByBarcode";

    public static final String PICK_POISITION = WMS_URL + "batchGetPositionVOByID";

    public static final String ADD_CODE = WMS_URL + "addInventoryCoinfig";

    public static final String DEL_CODE = WMS_URL + "delInventoryCoinfig";

    public static final String VALUE_SERVICE = URL + "property/queryvalue";

    public static final String SAFEKUCUN = WMS_URL + "";
}
