package mobile.freshtime.com.freshtime.activity.adapter;

/**
 * Created by Administrator on 2016/12/1.
 */
public class NewVersion {


    /**
     * android_version : 8
     * versionname : 1.1.1
     * ios_version : 1.1.3
     * remind : 100
     * xianzaishi_rf : {"des":"据说很好用","versionCode":"1","versionName":"2.0","url":"http://192.168.18.132:8080/quyang.apk"}
     */

    private String android_version;
    private String versionname;
    private String ios_version;
    private String remind;
    /**
     * des : 据说很好用
     * versionCode : 1
     * versionName : 2.0
     * url : http://192.168.18.132:8080/quyang.apk
     */

    private XianzaishiRfBean xianzaishi_rf;

    public String getAndroid_version() {
        return android_version;
    }

    public void setAndroid_version(String android_version) {
        this.android_version = android_version;
    }

    public String getVersionname() {
        return versionname;
    }

    public void setVersionname(String versionname) {
        this.versionname = versionname;
    }

    public String getIos_version() {
        return ios_version;
    }

    public void setIos_version(String ios_version) {
        this.ios_version = ios_version;
    }

    public String getRemind() {
        return remind;
    }

    public void setRemind(String remind) {
        this.remind = remind;
    }

    public XianzaishiRfBean getXianzaishi_rf() {
        return xianzaishi_rf;
    }

    public void setXianzaishi_rf(XianzaishiRfBean xianzaishi_rf) {
        this.xianzaishi_rf = xianzaishi_rf;
    }

    public static class XianzaishiRfBean {
        private String des;
        private String versionCode;
        private String versionName;
        private String url;

        public String getDes() {
            return des;
        }

        public void setDes(String des) {
            this.des = des;
        }

        public String getVersionCode() {
            return versionCode;
        }

        public void setVersionCode(String versionCode) {
            this.versionCode = versionCode;
        }

        public String getVersionName() {
            return versionName;
        }

        public void setVersionName(String versionName) {
            this.versionName = versionName;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
