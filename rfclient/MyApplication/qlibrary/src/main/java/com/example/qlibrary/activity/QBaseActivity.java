package com.example.qlibrary.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;

public abstract class QBaseActivity extends Activity implements View.OnClickListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVarirables();
        initView();
        initListener();
        loadData();
    }

    /**
     * 初始化数据，如接受intent中的数据
     */
    public abstract void initVarirables();

    /**
     * find view
     */
    public abstract void initView();

    /**
     * set listener
     */
    public abstract void initListener();

    /**
     * 加载一些数据，如，一进入界面就要请求数据
     */
    public abstract void loadData();

}
