package com.example.qlibrary.utils;

/**
 * Created by Administrator on 2017/5/24.
 */
public class StringUtil {

    public static boolean isEmty(String target) {
        if (null == target || "".equals(target) || "null".equals(target)) {
            return true;
        }
        return false;
    }

    public static boolean isNotEmty(String target) {
        return !isEmty(target);
    }

    public static boolean containSub(String src, String sub) {
        if (isEmty(src)) {
            return false;
        }
        if (src.contains(sub)) {
            return true;
        }
        return false;
    }


}
